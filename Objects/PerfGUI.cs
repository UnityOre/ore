/*! @file       Objects/PerfGUI.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2023-05-31
**/

using JetBrains.Annotations;

using UnityEngine;


namespace Ore
{
  [AddComponentMenu("Ore/Perf GUI")]
  [ExecuteAlways]
  public sealed class PerfGUI : OComponent
  {
    public enum SampleType
    {
      FPS,
      RAM,
      Custom
    }


    [PublicAPI]
    public Histogram.Draw DrawOptions
    {
      get => m_DrawOptions;
      set => m_DrawOptions = value;
    }

    [PublicAPI]
    public Rect Placement
    {
      get => m_Placement;
      set => m_Placement = value;
    }

    [PublicAPI]
    public Color32 Background
    {
      get => m_Background;
      set => m_Background = value;
    }

    [PublicAPI]
    public Color32 Line
    {
      get => m_Line;
      set => m_Line = value;
    }

    [PublicAPI]
    public TimeInterval AutosizeInterval
    {
      get => m_AutosizeInterval;
      set => m_AutosizeInterval = value;
    }

    [PublicAPI]
    public SampleType Type
    {
      get => m_SampleType;
      set => m_SampleType = value;
    }

    [PublicAPI, NotNull]
    public Histogram Graph => m_Graph;


  [Header("GUI Options")]

    [SerializeField]
    Histogram.Draw m_DrawOptions = Histogram.Draw.Default;

    [SerializeField]
    Rect m_Placement = new Rect
    (
           x: 8f,
           y: 8f,
       width: 180f,
      height: 60f
    );

    [SerializeField]
    Color32 m_Background = Colors.Background;
    [SerializeField]
    Color32 m_Line = Colors.Success;

    [SerializeField, Tooltip("Set 0 to disable auto-recalculation.")]
    TimeInterval m_AutosizeInterval = TimeInterval.OfMillis(1600);

    [SerializeField]
    Vector2 m_ReferenceScreenSize = new Vector2(362f, 643f); /* default is 9x16 in editor */

  [Header("Data Collection")]

    [SerializeField]
    SampleType m_SampleType;

    [SerializeField, Min(0)]
    int m_MaxSampleCount = 40;

    [SerializeField, Min(15)]
    int m_InitialCeiling = 75;
    [SerializeField]
    int m_InitialFloor = 15;

    [SerializeField]
    TimeInterval m_SamplePeriod = TimeInterval.OfMillis(750);


    [System.NonSerialized]
    TimeInterval m_CountdownAutosize;

    [System.NonSerialized]
    readonly Histogram m_Graph = new Histogram();

    [System.NonSerialized]
    System.Func<float> m_CustomSampleGetter = () => 0f;


    [PublicAPI]
    public void SetCustomSampler([CanBeNull] System.Func<float> sampler)
    {
      if (sampler is null)
      {
        m_CustomSampleGetter = () => 0f;
      }
      else
      {
        m_SampleType = SampleType.Custom;
        m_CustomSampleGetter = sampler;
      }
    }


    float GetSample()
    {
      switch (m_SampleType)
      {
        default:
        case SampleType.FPS:
          return DeviceSpy.CurrentFPS;

        case SampleType.RAM:
          return DeviceSpy.CurrentRAM;

        case SampleType.Custom:
          return m_CustomSampleGetter();
      }
    }


    void OnValidate()
    {
      m_Graph.MaxSampleCount = m_MaxSampleCount;
      m_Graph.Ceiling        = m_InitialCeiling;
      m_Graph.Floor          = m_InitialFloor;
      m_Graph.SamplePeriod   = m_SamplePeriod;
    }

    void OnEnable()
    {
      if (!CheckCanDraw())
      {
        Orator.Log("skipping FPS histogram due to configuration!", this);
        enabled = false;
        return;
      }

      OnValidate();

      m_CountdownAutosize = m_AutosizeInterval;
    }

    void Update()
    {
      m_Graph.SampleValue(GetSample());

      if (m_AutosizeInterval.Ticks <= 0)
        return;

      m_CountdownAutosize.DecrementFrame();

      if (m_CountdownAutosize.Ticks <= 0)
      {
        m_CountdownAutosize = m_AutosizeInterval;
        m_Graph.RecalulateHeight();
      }
    }

    void OnGUI()
    {
      MeGUI.NormalizeGUIScale(m_ReferenceScreenSize, out _ );

      if (MeGUI.IsTapped(m_Placement))
      {
        m_DrawOptions ^= Histogram.Draw.StdDev;
      }

      m_Graph.DrawOnGUI(m_Placement, m_Line, m_Background, m_DrawOptions);
    }


    bool CheckCanDraw()
    {
      return m_DrawOptions != Histogram.Draw.None &&
             m_MaxSampleCount > 0 &&
             m_SamplePeriod.Ticks > 0 &&
             !m_Line.IsClear() &&
             !m_Placement.ApproximatelyZero();
    }

  } // end class PerfGUI
}
