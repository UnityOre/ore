/*! @file       Objects/ActiveScene.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2022-06-06
 *
 *  A Scene Singleton that sticks to the current "Active Scene", even as this
 *  status moves around among different loaded Scenes.
 *
 *  TODO add profiler markers
**/

#if KOOBOX
#warning KOOBOX is deprecated. Define ORE_DEBUG instead.
#define ORE_DEBUG
#endif

// ReSharper disable HeapView.DelegateAllocation

using JetBrains.Annotations;

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

using System.Collections;


namespace Ore
{

  [DefaultExecutionOrder(-1337)] // rationale: Many things might depend on this class early-on.
  [DisallowMultipleComponent]
  [PublicAPI]
  public sealed class ActiveScene : OSingleton<ActiveScene>
  {
    public static Scene Scene => s_ActiveScene;

    // ReSharper disable once ConvertToNullCoalescingCompoundAssignment
    [NotNull]
    public static ICoroutineRunner Coroutines => s_Coroutiner ?? (s_Coroutiner = new CoroutineRunnerBuffer());

    public static bool IsPlaying => !IsQuitting && Instance && Application.IsPlaying(Instance);

    public static bool IsQuitting { get; private set; }


    public static event UnityAction<Scene> OnActiveSceneChanged
    {
      add    => s_OnActiveSceneChanged += value;
      remove => s_OnActiveSceneChanged -= value;
    }

    public static event UnityAction<Scene> OnSceneUnloaded
    {
      add    => s_OnSceneUnloaded += value;
      remove => s_OnSceneUnloaded -= value;
    }

    public static event UnityAction OnFixedUpdate
    {
      add    => s_OnFixedUpdate += value;
      remove => s_OnFixedUpdate -= value;
    }

    public static event UnityAction OnUpdate
    {
      add    => s_OnUpdate += value;
      remove => s_OnUpdate -= value;
    }

    public static event UnityAction OnLateUpdate
    {
      add    => s_OnLateUpdate += value;
      remove => s_OnLateUpdate -= value;
    }

    public static event UnityAction OnDebugGUI
    {
      #if DEBUG
      add    => s_OnDebugGUI += value;
      remove => s_OnDebugGUI -= value;
      #else
      add    {  }
      remove {  }
      #endif
    }


    /// <returns>
    ///   The current active Scene's realtime age.
    /// </returns>
    [Pure]
    public static TimeInterval GetSceneAge()
    {
      if (s_SceneBirthdays.Find(s_ActiveScene, out long birth))
      {
        return new TimeInterval(System.DateTime.UtcNow.Ticks - birth);
      }

      return TimeInterval.Zero;
    }

    /// <returns>
    ///   The given Scene's realtime age. <br/>
    ///   Zero if the scene is not loaded.
    /// </returns>
    [Pure]
    public static TimeInterval GetSceneAge(Scene scene)
    {
      if (s_SceneBirthdays.Find(scene, out long birth))
      {
        return new TimeInterval(System.DateTime.UtcNow.Ticks - birth);
      }

      return TimeInterval.Zero;
    }

    /// <returns>
    ///   The given Scene's realtime age. <br/>
    ///   Zero if the scene with name is not found.
    /// </returns>
    [Pure]
    public static TimeInterval GetSceneAge(string scene_name)
    {
      return GetSceneAge(SceneManager.GetSceneByName(scene_name));
    }


    //
    // serialized fields
    //

  [Header("[ActiveScene]"), Space]
    [SerializeField]
    TimeInterval m_DelayStartCoroutineRunner = TimeInterval.Frame;

  [Header("Scene Events")]
    [SerializeField]
    SceneEvent m_OnActiveSceneChanged = new SceneEvent();
    [SerializeField]
    SceneEvent m_OnSceneUnloaded      = new SceneEvent();

  [Header("Update Events (tip: simple callbacks only!)")]
    [SerializeField]
    VoidEvent  m_OnFixedUpdate = new VoidEvent();
    [SerializeField]
    VoidEvent  m_OnUpdate      = new VoidEvent();
    [SerializeField]
    VoidEvent  m_OnLateUpdate  = new VoidEvent();

  #if DEBUG
  [Header("#if DEBUG")]
    [SerializeField]
    VoidEvent  m_OnDebugGUI = new VoidEvent();
  #endif


    //
    // private instance methods, unity messages
    //

    IEnumerator Start()
    {
      if (m_DelayStartCoroutineRunner <= TimeInterval.Frame)
      {
        return new DelayedRoutine(SetupCoroutineRunner, frameDelay: 1);
      }
      else
      {
        return new DelayedRoutine(SetupCoroutineRunner, m_DelayStartCoroutineRunner);
      }
    }

    void SetupCoroutineRunner()
    {
      if (this != Current)
        return;

      var coroutiner = GetComponent<CoroutineRunner>();
      if (!coroutiner)
      {
        coroutiner = gameObject.AddComponent<CoroutineRunner>();
      }

      if (s_Coroutiner is CoroutineRunnerBuffer buffer)
      {
        bool sane = coroutiner.AdoptAndRun(buffer);
        OAssert.True(sane, this);
      }

      s_Coroutiner = coroutiner;
    }


    [System.NonSerialized]
    bool m_WasDisabled;

    protected override void OnEnable()
    {
      if (TryInitialize(this) && m_WasDisabled)
      {
        SetupCoroutineRunner();
      }
    }

    protected override void OnDisable()
    {
      m_WasDisabled = this == Current;

      base.OnDisable();

      if (!m_WasDisabled)
        return;

      if (s_Coroutiner is CoroutineRunner)
      {
        s_Coroutiner = null;
      }
    }

    protected override void OnValidate()
    {
      // don't do base logic

      // force:
      m_IsValidWhileDisabled = false;
    }


    void FixedUpdate()
    {
      s_OnFixedUpdate?.Invoke();
      m_OnFixedUpdate.Invoke();
    }

    void Update()
    {
      s_OnUpdate?.Invoke();
      m_OnUpdate.Invoke();
    }

    void LateUpdate()
    {
      s_OnLateUpdate?.Invoke();
      m_OnLateUpdate.Invoke();
    }


    #if DEBUG

    void OnGUI()
    {
      s_OnDebugGUI?.Invoke();
      m_OnDebugGUI.Invoke();
    }

    #endif // DEBUG

    //
    // static
    //

    static Scene s_ActiveScene;

    static UnityAction<Scene> s_OnActiveSceneChanged;
    static UnityAction<Scene> s_OnSceneUnloaded;

    static UnityAction s_OnFixedUpdate;
    static UnityAction s_OnUpdate;
    static UnityAction s_OnLateUpdate;
    #if DEBUG
    static UnityAction s_OnDebugGUI;
    #endif

    static readonly HashMap<Scene,long> s_SceneBirthdays = new HashMap<Scene,long>();

    static ICoroutineRunner s_Coroutiner;


    // the rest ensures this singleton is ALWAYS on the "active" scene.


    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterAssembliesLoaded)]
    static void RegisterActiveSceneListener()
    {
      SceneManager.activeSceneChanged += UpdateActiveSceneObject;
      SceneManager.sceneLoaded        += OnSceneBirth;
      SceneManager.sceneUnloaded      += OnSceneDeath;

      Application.quitting += OnQuitting;

      #if UNITY_EDITOR
      UnityEditor.EditorApplication.playModeStateChanged += (change) =>
      {
        if (change == UnityEditor.PlayModeStateChange.ExitingPlayMode)
          OnQuitting();
      };
      #endif
    }

    static void UpdateActiveSceneObject(Scene prev, Scene next)
    {
      s_ActiveScene = next;

      var curr = Current;
      if (!curr)
        curr = Instantiate();

      #if DEBUG
      OAssert.Exists(curr, $"{nameof(ActiveScene)}.{nameof(Current)}");
      #endif

      curr.SetDontDestroyOnLoad(!next.isLoaded);

      s_OnActiveSceneChanged?.Invoke(next);

      curr.m_OnActiveSceneChanged.Invoke();
    }

    static void OnSceneBirth(Scene scene, LoadSceneMode mode)
    {
      s_SceneBirthdays[scene] = System.DateTime.UtcNow.Ticks;
    }

    static void OnSceneDeath(Scene scene)
    {
      s_SceneBirthdays.Unmap(scene);

      s_OnSceneUnloaded?.Invoke(scene);

      if (Instance)
      {
        Instance.m_OnSceneUnloaded.Invoke(scene);
      }
    }

    static void OnQuitting()
    {
      if (IsQuitting)
        return;

      IsQuitting = true;

      #if ORE_DEBUG
      Orator.Log<ActiveScene>(nameof(OnQuitting));
      #endif
    }


    static ActiveScene Instantiate()
    {
      IsQuitting = false;

      var obj = new GameObject($"[{nameof(ActiveScene)}]")
      {
        hideFlags = HideFlags.DontSave,
        isStatic  = true
      };

      var bud = obj.AddComponent<ActiveScene>();
      bud.m_IsReplaceable = true;

      #if DEBUG
      OAssert.True(Current == bud);
      #endif // DEBUG

      return bud;
    }


  #region DEPRECATIONS

    const string OBSOLETE_MSG_EnqueueCoroutine =
      "ActiveScene.EnqueueCoroutine is obsolete. Use ActiveScene.Coroutines.Run " +
      "instead (UnityUpgradeable) -> [Ore] Ore.ActiveScene.Coroutines.Run(*)";
    const bool OBSOLETE_ERR_EnqueueCoroutine   = false;

    const string OBSOLETE_MSG_CancelCoroutinesForContract =
      "ActiveScene.CancelCoroutinesForContract is obsolete. Use ActiveScene.Coroutines.Halt " +
      "instead (UnityUpgradeable) -> [Ore] Ore.ActiveScene.Coroutines.Halt(*)";
    const bool OBSOLETE_ERR_CancelCoroutinesForContract   = false;


    [System.Obsolete(OBSOLETE_MSG_EnqueueCoroutine, OBSOLETE_ERR_EnqueueCoroutine)]
    public static void EnqueueCoroutine([NotNull] IEnumerator routine)
    {
      Coroutines.Run(routine);
    }

    [System.Obsolete(OBSOLETE_MSG_EnqueueCoroutine, OBSOLETE_ERR_EnqueueCoroutine)]
    public static void EnqueueCoroutine([NotNull] IEnumerator routine, [NotNull] Object contract)
    {
      Coroutines.Run(routine, contract);
    }

    [System.Obsolete(OBSOLETE_MSG_EnqueueCoroutine, OBSOLETE_ERR_EnqueueCoroutine)]
    public static void EnqueueCoroutine([NotNull] IEnumerator routine, [NotNull] string key)
    {
      Coroutines.Run(routine, key);
    }

    [System.Obsolete(OBSOLETE_MSG_EnqueueCoroutine, OBSOLETE_ERR_EnqueueCoroutine)]
    public static void EnqueueCoroutine([NotNull] IEnumerator routine, [NotNull] out string guidKey)
    {
      Coroutines.Run(routine, out guidKey);
    }

    [System.Obsolete(OBSOLETE_MSG_CancelCoroutinesForContract, OBSOLETE_ERR_CancelCoroutinesForContract)]
    public static void CancelCoroutinesForContract([NotNull] object contract)
    {
      Coroutines.Halt(contract);
    }


    #region MonoBehaviour API mistake correction

    const string OBSOLETE_MSG_StartCoroutine =
      "Do not use the base Unity APIs to start coroutines on the ActiveScene.\n" +
      "Use the `ActiveScene.Coroutines` API instead.";


    [System.Obsolete(OBSOLETE_MSG_StartCoroutine)]
    public /**/ new /**/ Coroutine StartCoroutine(IEnumerator mistake)
    {
      Orator.Error(OBSOLETE_MSG_StartCoroutine);

      if (s_Coroutiner is CoroutineRunner runner)
        return runner.StartCoroutine(mistake);

      return null;
    }

    [System.Obsolete(OBSOLETE_MSG_StartCoroutine, true)]
    public /**/ new /**/ Coroutine StartCoroutine(string mistake)
    {
      // usage is compile-time error
      return null;
    }

    [System.Obsolete(OBSOLETE_MSG_StartCoroutine, true)]
    public /**/ new /**/ Coroutine StartCoroutine(string mis, object take)
    {
      // usage is compile-time error
      return null;
    }

    #endregion MonoBehaviour API mistake correction

  #endregion DEPRECATIONS

  } // end class ActiveScene

}
