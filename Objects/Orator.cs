/*! @file       Objects/Orator.cs
    @author     levianperez\@gmail.com
    @author     levi\@leviperez.dev
    @date       2022-01-01

    @brief  Debug logging API that simultaneously provides static interfaces as well as
            UnityEngine.Object-based INSTANCE equivalents.

    @remark If an asset for this Object does not exist, one will be auto-created in the
            Editor

    @remark See `UnityEngine.BuildOptions.ForceEnableAssertions` to turn on assertions
            even in non-development builds!

    @remark Style Guide divergence: lowercase methods in this class denote
            INSTANCE methods, and PascalCase methods denote STATIC methods
            (which typically call the instance methods on the singleton).
**/

#if AGGRESSIVE_ORATOR // deprecated symbol
#warning AGGRESSIVE_ORATOR is deprecated. Define ORE_ORATOR_ALWAYS_LOG instead.
#define ORE_ORATOR_ALWAYS_LOG
#endif

#if DEBUG_ORATOR // deprecated symbol
#warning DEBUG_ORATOR is deprecated. Define ORE_ORATOR_DEBUG instead.
#define ORE_ORATOR_DEBUG
#endif

#if KOOBOX // deprecated symbol
#warning KOOBOX is deprecated. Define ORE_DEBUG instead.
#define ORE_DEBUG
#endif

#if ORE_ORATOR_DEBUG && !DEBUG
#undef ORE_ORATOR_DEBUG
#endif

// ReSharper disable ConvertIfStatementToNullCoalescingAssignment

using JetBrains.Annotations;

using System.ComponentModel;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Serialization;

// ReSharper disable once RedundantUsingDirective
using UnityEngine.Diagnostics; // do not remove if unused

#if UNITY_EDITOR
using MenuItem = UnityEditor.MenuItem;
#endif

using ObsoleteAttribute    = System.ObsoleteAttribute;
using ConditionalAttribute = System.Diagnostics.ConditionalAttribute;
using AssException         = UnityEngine.Assertions.AssertionException;
using Type                 = System.Type;


namespace Ore
{
  #if !ORE_ORATOR_DEBUG
  [System.Diagnostics.DebuggerStepThrough]
  #endif
  [DefaultExecutionOrder(-1337)]
  [AutoCreateAsset("Resources/Orator.asset")]
  [PublicAPI]
  public sealed class Orator : OAssetSingleton<Orator>
  {

  #region     Static public API

    public static string Prefix
    {
      get => Instance ? Instance.m_OratorPrefix : DEFAULT_ORATOR_PREFIX;
      set
      {
        if (Instance)
          Instance.m_OratorPrefix = value;
      }
    }

    public static bool RaiseExceptions
    {
      // ReSharper disable once SimplifyConditionalTernaryExpression
      get => Instance ? Instance.m_AssertionsRaiseExceptions : DEFAULT_ASSERT_EXCEPTIONS;
      set
      {
        if (Instance)
          Instance.m_AssertionsRaiseExceptions = value;
      }
    }


    public static void Reached([CanBeNull] string msg, [CanBeNull] Object ctx)
    {
      var self = Instance;
      if (!self)
      {
        #if ORE_ORATOR_ALWAYS_LOG
        if (ctx)
          Debug.Log($"{DEFAULT_ORATOR_PREFIX} {DEFAULT_REACHED_MSG} {msg}\n(name=\"{ctx}\")", ctx);
        else
          Debug.Log($"{DEFAULT_ORATOR_PREFIX} {DEFAULT_REACHED_MSG} {msg}");
        #endif
        return;
      }

      msg = $"{self.ReachedMessage} {msg}";

      if (self.ShouldIgnore(self.m_ReachedFormat.LogType, msg))
        return;

      self.FixupMessageContext(ref msg, ref ctx);

      // TODO improved stacktrace info from PyroDK

      Debug.LogFormat(self.m_ReachedFormat.LogType, self.m_ReachedFormat.LogOption, ctx, msg);
    }

    public static void Reached()
    {
      Reached(msg: null, ctx: null);
    }

    public static void Reached([CanBeNull] Object ctx)
    {
      Reached(msg: null, ctx);
    }

    public static void Reached([CanBeNull] string msg)
    {
      Reached(msg, ctx: null);
    }

    public static void Reached([NotNull] Type tctx, string msg = DEFAULT_REACHED_MSG)
    {
      Reached($"[{tctx.NiceName()}] {msg}");
    }

    public static void Reached<TContext>(string msg = DEFAULT_REACHED_MSG)
    {
      Reached(typeof(TContext), msg);
    }


    public static void Log([CanBeNull] string msg, [CanBeNull] Object ctx)
    {
      var self = Instance;
      if (!self)
      {
        #if ORE_ORATOR_ALWAYS_LOG
        if (ctx)
          Debug.Log($"{DEFAULT_ORATOR_PREFIX} {msg}\n(name=\"{ctx}\")", ctx);
        else
          Debug.Log($"{DEFAULT_ORATOR_PREFIX} {msg}");
        #endif
        return;
      }

      if (self.ShouldIgnore(LogType.Log, ref msg))
        return;

      self.FixupMessageContext(ref msg, ref ctx);

      Debug.LogFormat(LogType.Log, self.m_LogStackTracePolicy, ctx, "{0} {1}", self.m_OratorPrefix, msg);
    }

    public static void Log([CanBeNull] string msg)
    {
      Log(msg, ctx: null);
    }

    public static void Log([CanBeNull] Object ctx)
    {
      Log(msg: null, ctx);
    }

    public static void Log([NotNull] Type tctx, [CanBeNull] string msg)
    {
      Log(msg: $"[{tctx.NiceName()}] {msg}", ctx: null);
    }

    public static void Log<TContext>([CanBeNull] string msg)
    {
      Log(tctx: typeof(TContext), msg);
    }


    // TODO provide Verbose(...) publicly?

    [Conditional("ORE_DEBUG")]
    internal static void Verbose([CanBeNull] string msg, [CanBeNull] Object ctx, bool trace = false)
    {
      var opt = trace ? LogOption.None : LogOption.NoStacktrace;
      var self = Instance;
      if (!self)
      {
        if (ctx)
          Debug.LogFormat(LogType.Log, opt, ctx, "{0] {1}\n(name=\"{2}\")", DEFAULT_ORATOR_PREFIX, msg, ctx.name);
        else
          Debug.LogFormat(LogType.Log, opt, null, "{0} {1}", DEFAULT_ORATOR_PREFIX, msg);
        return;
      }

      if (self.ShouldIgnore(LogType.Log, ref msg))
        return;

      self.FixupMessageContext(ref msg, ref ctx);

      Debug.LogFormat(LogType.Log, opt, ctx, "{0} {1}", self.m_OratorPrefix, msg);
    }

    [Conditional("ORE_DEBUG")]
    internal static void Verbose([CanBeNull] string msg, bool trace = false)
    {
      Verbose(msg, ctx: null, trace);
    }

    [Conditional("ORE_DEBUG")]
    internal static void Verbose([CanBeNull] Object ctx, bool trace = false)
    {
      Verbose(msg: null, ctx, trace);
    }

    [Conditional("ORE_DEBUG")]
    internal static void Verbose([NotNull] Type tctx, [CanBeNull] string msg, bool trace = false)
    {
      Verbose(msg: $"[{tctx.NiceName()}] {msg}", ctx: null, trace);
    }

    [Conditional("ORE_DEBUG")]
    internal static void Verbose<TContext>([CanBeNull] string msg, bool trace = false)
    {
      Verbose(tctx: typeof(TContext), msg, trace);
    }


    /// <summary>
    ///   Logs a "Non-Fatal Error" via the vanilla means.
    /// </summary>
    /// <param name="ex">
    ///   The Exception representing the error to log.
    ///   If null is supplied, this function is an immediate no-op.
    /// </param>
    /// <param name="ctx">
    ///   A context <see cref="Object"/> for the error.
    ///   If supplied and you are running in the editor, the editor will attempt
    ///   to ping this object. 
    /// </param>
    public static void NFE([CanBeNull] System.Exception ex, [CanBeNull] Object ctx)
    {
      if (ex is null)
        return;

      #if ORE_ORATOR_ALWAYS_LOG
        if (Instance && Instance.ShouldIgnore(LogType.Exception, ex.Message ?? string.Empty))
          return;
      #else
        if (!Instance || Instance.ShouldIgnore(LogType.Exception, ex.Message ?? string.Empty))
          return;
      #endif

      if (ex is FauxException faux)
      {
        #if DEBUG
          Log(faux.Message, ctx);
        #endif
      }
      else
      {
        Debug.LogException(ex, ctx);
      }
    }

    /// <inheritdoc cref="NFE(System.Exception,UnityEngine.Object)"/>
    public static void NFE([CanBeNull] System.Exception ex)
    {
      NFE(ex, ctx: null);
    }


    public static void Warn([CanBeNull] string msg, [CanBeNull] Object ctx)
    {
      var self = Instance;
      if (!self)
      {
        #if ORE_ORATOR_ALWAYS_LOG
        if (ctx)
          Debug.LogWarning($"{DEFAULT_ORATOR_PREFIX} {msg}", ctx);
        else
          Debug.LogWarning($"{DEFAULT_ORATOR_PREFIX} {msg}");
        #endif
        return;
      }

      if (self.ShouldIgnore(LogType.Warning, ref msg))
        return;

      self.FixupMessageContext(ref msg, ref ctx);

      Debug.LogFormat(LogType.Warning, LogOption.None, ctx, "{0} {1}", self.m_OratorPrefix, msg);
    }

    public static void Warn([CanBeNull] string msg)
    {
      Warn(msg, ctx: null);
    }

    public static void Warn([CanBeNull] Object ctx)
    {
      Warn(msg: null, ctx);
    }

    public static void Warn([NotNull] Type tctx, [CanBeNull] string msg)
    {
      Warn(msg: $"[{tctx.NiceName()}] {msg}", ctx: null);
    }

    public static void Warn<TContext>([CanBeNull] string msg)
    {
      Warn(tctx: typeof(TContext), msg);
    }


    public static void Error([CanBeNull] string msg, [CanBeNull] Object ctx)
    {
      var self = Instance;
      if (!self)
      {
        #if ORE_ORATOR_ALWAYS_LOG
        if (ctx)
          Debug.LogError($"{DEFAULT_ORATOR_PREFIX} {msg}", ctx);
        else
          Debug.LogError($"{DEFAULT_ORATOR_PREFIX} {msg}");
        #endif
        return;
      }

      if (self.ShouldIgnore(LogType.Error, ref msg))
        return;

      self.FixupMessageContext(ref msg, ref ctx);

      Debug.LogFormat(LogType.Error, LogOption.None, ctx, "{0} {1}", self.m_OratorPrefix, msg);
    }

    public static void Error([CanBeNull] string msg)
    {
      Error(msg, ctx: null);
    }

    public static void Error([CanBeNull] Object ctx)
    {
      Error(msg: null, ctx);
    }

    public static void Error([NotNull] Type tctx, [CanBeNull] string msg)
    {
      Error($"[{tctx.NiceName()}] {msg}", ctx: null);
    }

    public static void Error<TContext>([CanBeNull] string msg)
    {
      Error(tctx: typeof(TContext), msg);
    }


    public static void FailAssertion([CanBeNull] string msg, [CanBeNull] Object ctx)
    {
      var self = Instance;
      if (!self)
      {
        #if ORE_ORATOR_ALWAYS_LOG
        if (DEFAULT_INCLUDE_CONTEXT && ctx)
        {
          msg = AppendContext(msg, ctx);
        }

        if (DEFAULT_ASSERT_EXCEPTIONS)
        {
          throw new AssException(DEFAULT_ASSERT_MSG, msg);
        }

        #pragma warning disable CS0162
        Debug.LogFormat(DEFAULT_ASSERT_LOGTYPE, DEFAULT_ASSERT_LOGOPT,
                        ctx, "{0} {1}", DEFAULT_ASSERT_MSG, msg);
        #pragma warning restore CS0162
        #else
        return;
        #endif
      }

      if (self.ShouldIgnore(self.m_AssertionFailedFormat.LogType, ref msg))
        return;

      self.FixupMessageContext(ref msg, ref ctx);

      // TODO improved stacktrace info from PyroDK

      if (self.m_AssertionsRaiseExceptions)
        throw new AssException(self.AssertMessage, msg);

      Debug.LogFormat(self.m_AssertionFailedFormat.LogType, self.m_AssertionFailedFormat.LogOption,
                      ctx, "{0} {1}", self.AssertMessage, msg);
    }

    public static void FailAssertionNoThrow([CanBeNull] string msg, [CanBeNull] Object ctx)
    {
      var self = Instance;
      if (!Instance)
      {
        #if ORE_ORATOR_ALWAYS_LOG
        if (DEFAULT_INCLUDE_CONTEXT && ctx)
        {
          msg = AppendContext(msg, ctx);
        }

        Debug.LogFormat(DEFAULT_ASSERT_LOGTYPE, DEFAULT_ASSERT_LOGOPT,
                        ctx, "{0} {1}", DEFAULT_ASSERT_MSG, msg);
        #endif
        return;
      }

      if (self.ShouldIgnore(self.m_AssertionFailedFormat.LogType, ref msg))
        return;

      self.FixupMessageContext(ref msg, ref ctx);

      // TODO improved stacktrace info from PyroDK

      Debug.LogFormat(self.m_AssertionFailedFormat.LogType, self.m_AssertionFailedFormat.LogOption,
                      ctx, "{0} {1}", self.AssertMessage, msg);
    }


    public static void Panic([CanBeNull] string msg, [CanBeNull] Object ctx)
    {
      if (!Instance)
      {
        throw new UnanticipatedException($"PANIC! {msg}\n(Orator was destroyed = throwing this instead of crashing)");
      }

      NFE(new UnanticipatedException($"PANIC! {msg}"), ctx);

      #if UNITY_EDITOR

      UnityEditor.EditorApplication.Beep();
      UnityEditor.EditorApplication.Beep();
      UnityEditor.EditorApplication.Beep();

      if (UnityEditor.EditorApplication.isPlayingOrWillChangePlaymode)
      {
        UnityEditor.EditorApplication.ExitPlaymode();
      }

      if (ctx)
      {
        UnityEditor.EditorGUIUtility.PingObject(ctx);
      }

      #else

      // UnityEngine.Diagnostics
      #if UNITY_2020_1_OR_NEWER
      Utils.ForceCrash(ForcedCrashCategory.MonoAbort);
      #endif
      Utils.ForceCrash(ForcedCrashCategory.Abort);
      Utils.ForceCrash(ForcedCrashCategory.FatalError);
      Utils.ForceCrash(ForcedCrashCategory.AccessViolation);
      Utils.ForceCrash(ForcedCrashCategory.PureVirtualFunction);
      // one of em's gotta work, right??

      #endif // UNITY_EDITOR
    }

    public static void Panic([CanBeNull] string msg)
    {
      Panic(msg, Instance);
    }

    public static void Panic([CanBeNull] Object ctx)
    {
      Panic(string.Empty, ctx);
    }

    public static void Panic([NotNull] Type tctx)
    {
      Panic($"[{tctx.NiceName()}]", null);
    }

    public static void Panic<TContext>()
    {
      Panic(tctx: typeof(TContext));
    }

  #endregion  Static public API


  #region     Static "Log Once" API

    public static void ReachedOnce(Object ctx)
    {
      if (AlreadyLogged(nameof(ReachedOnce), ctx))
        return;

      Reached(ctx);
    }

    public static void LogOnce(string msg, Object ctx = null)
    {
      if (AlreadyLogged(msg, ctx))
        return;

      Log(msg, ctx);
    }

    public static void WarnOnce(string msg, Object ctx = null)
    {
      if (AlreadyLogged(msg, ctx))
        return;

      Warn(msg, ctx);
    }

    public static void ErrorOnce(string msg, Object ctx = null)
    {
      if (AlreadyLogged(msg, ctx))
        return;

      Error(msg, ctx);
    }


    static readonly HashSet<int> s_LoggedOnceHashes = new HashSet<int>();
    static bool AlreadyLogged(string msg, Object ctx)
    {
      int cap = Instance ? Instance.m_LogOnceMemorySize : DEFAULT_LOGONCE_MEMORY_SIZE;

      if (cap <= 0)
      {
        return false;
      }

      int hash;

      #if UNITY_EDITOR

        #if UNITY_2021_1_OR_NEWER
        var stage = UnityEditor.SceneManagement.PrefabStageUtility.GetCurrentPrefabStage();
        #else
        var stage = UnityEditor.Experimental.SceneManagement.PrefabStageUtility.GetCurrentPrefabStage();
        #endif

        #if UNITY_2020_3_OR_NEWER
        // ReSharper disable once ConvertIfStatementToConditionalTernaryExpression
        if (stage)
        {
          hash = Hashing.MakeHash(msg, stage.assetPath);
        }
        #else
        if (stage != null && stage.prefabContentsRoot)
        {
          hash = Hashing.MakeHash(msg, stage.prefabAssetPath);
        }
        #endif
        else
        {
          hash = Hashing.MakeHash(msg, ctx);
        }

      #else // if !UNITY_EDITOR

        hash = Hashing.MakeHash(msg, ctx);

      #endif // UNITY_EDITOR

      if (s_LoggedOnceHashes.Count >= cap)
      {
        // Log($"LogOnce memory has overflowed. Resetting. (cap={cap})");
        s_LoggedOnceHashes.Clear();

        #if UNITY_EDITOR
        _ = Filesystem.TryDeletePath(CACHE_PATH);
        #endif // UNITY_EDITOR
      }

      return !s_LoggedOnceHashes.Add(hash);
    }

    #if UNITY_EDITOR

    const string CACHE_PATH = "Temp/Orator.LogOnce.cache";

    [UnityEditor.InitializeOnLoadMethod]
    static void OnScriptLoad()
    {
      if (!ReadCacheLogOnce() && s_LoggedOnceHashes.Count > 0)
      {
        if (!WriteCacheLogOnce())
        {
          Warn($"could not write to \"{CACHE_PATH}\"");
        }
      }

      UnityEditor.EditorApplication.wantsToQuit += () =>
      {
        _ = Filesystem.TryDeletePath(CACHE_PATH);
        return true;
      };
    }


    static bool WriteCacheLogOnce()
    {
      if (OAssert.Fails(Filesystem.TryDeletePath(CACHE_PATH), $"could not delete \"{CACHE_PATH}\""))
        return false;
      if (s_LoggedOnceHashes.Count == 0)
        return true;

      var strb = new System.Text.StringBuilder(7 * s_LoggedOnceHashes.Count);

      foreach (int hash in s_LoggedOnceHashes)
      {
        _ = strb.Append(hash.ToInvariant()).Append('\n');
      }

      return Filesystem.TryWriteText(CACHE_PATH, strb.ToString());
    }

    static bool ReadCacheLogOnce()
    {
      if (!Filesystem.TryReadLines(CACHE_PATH, out string[] lines))
        return false;

      s_LoggedOnceHashes.Clear();
      foreach (var line in lines)
      {
        if (Parsing.TryParseInt32(line, out int hash))
          s_LoggedOnceHashes.Add(hash);
      }

      return true;
    }

    #endif

  #endregion  Static "Log Once" API


  #region     Instance fields

    [System.Serializable]
    struct LogFormatDef
    {
      [SerializeField]
      public LogType LogType;
      [SerializeField]
      public LogOption LogOption;
      [SerializeField, Delayed]
      public string BaseMessage;

      [SerializeField, HideInInspector]
      public Color32 RichTextColor;
        // TODO: would be SO easy to finish hooking this up
        // TODO: if we moved Ore.Editor.Styles.ColorText(...) into runtime code!
    } // end struct LogFormatDef

    // compile-time default values:

    const EditorBrowsableState INSTANCE_BROWSABLE_POLICY = EditorBrowsableState.Never;

    const string    DEFAULT_ORATOR_PREFIX        = "<color=\"orange\">[" + nameof(Orator) + "]</color>";
    const bool      DEFAULT_INCLUDE_CONTEXT       = true;
    const LogOption DEFAULT_LOG_LOGOPT            = LogOption.NoStacktrace;

    const string    DEFAULT_REACHED_MSG           = "<b>Reached!</b>";
    const LogType   DEFAULT_REACHED_LOGTYPE       = LogType.Warning;
    const LogOption DEFAULT_REACHED_LOGOPT        = LogOption.None;

    const LogType   DEFAULT_ASSERT_LOGTYPE        = LogType.Assert;
    const string    DEFAULT_ASSERT_MSG            = "<b>Assertion failed!</b>";
    const LogOption DEFAULT_ASSERT_LOGOPT         = LogOption.None;
    const bool      DEFAULT_ASSERT_EXCEPTIONS     = true;
    const bool      DEFAULT_ASSERTIONS_IN_RELEASE = false;

    const int       DEFAULT_LOGONCE_MEMORY_SIZE   = 256;


    [Header("Orator Properties")]

    [SerializeField]
    string m_OratorPrefix = DEFAULT_ORATOR_PREFIX;

    [SerializeField]
    bool m_IncludeContextInMessages = DEFAULT_INCLUDE_CONTEXT;

    [SerializeField]
    LogOption m_LogStackTracePolicy = DEFAULT_LOG_LOGOPT;

    [SerializeField]
    bool m_AssertionsRaiseExceptions = DEFAULT_ASSERT_EXCEPTIONS;

  #pragma warning disable CS0414
    [SerializeField]
    [HideInInspector, Obsolete("Assertions in release have never been fully implemented.", error: true)]
    bool m_ForceAssertionsInRelease = DEFAULT_ASSERTIONS_IN_RELEASE;  // TODO
  #pragma warning restore CS0414

    [Space]

    [SerializeField]
    LogFormatDef m_ReachedFormat = new LogFormatDef
    {
      LogType = DEFAULT_REACHED_LOGTYPE,
      LogOption = DEFAULT_REACHED_LOGOPT,
      BaseMessage = DEFAULT_REACHED_MSG
    };

    [Space]

    [SerializeField]
    LogFormatDef m_AssertionFailedFormat = new LogFormatDef
    {
      LogType = DEFAULT_ASSERT_LOGTYPE,
      LogOption = DEFAULT_ASSERT_LOGOPT,
      BaseMessage = DEFAULT_ASSERT_MSG
    };

    [Space]

    [SerializeField, Tooltip("or, the maximum number of log signatures to keep in RAM to prevent duplicate logging.")]
    int m_LogOnceMemorySize = DEFAULT_LOGONCE_MEMORY_SIZE;

    [Space]

    [SerializeField, FormerlySerializedAs("m_Filters")]
    List<OratorFilter> m_IgnoreFilters = new List<OratorFilter>();

  #endregion  Instance fields


  #region     Internal section

    string ReachedMessage
    {
      get
      {
        if (m_FormattedReachedMessage == null)
          m_FormattedReachedMessage = $"{m_OratorPrefix} {m_ReachedFormat.BaseMessage}";
        // TODO RichText
        return m_FormattedReachedMessage;
      }
    }

    string AssertMessage
    {
      get
      {
        if (m_FormattedAssertMessage == null)
          m_FormattedAssertMessage = $"{m_OratorPrefix} {m_AssertionFailedFormat.BaseMessage}";
        // TODO RichText
        return m_FormattedAssertMessage;
      }
    }


    [System.NonSerialized]
    string m_FormattedReachedMessage;
    [System.NonSerialized]
    string m_FormattedAssertMessage;

    [System.NonSerialized]
    OratorFilter[] m_RtIgnoreFilters = System.Array.Empty<OratorFilter>();


    // TODO fancy formatting w/ PyroDK.RichText API


    #if !UNITY_EDITOR // Remove rich text tags from strings in builds!
    void Awake()
    {
      m_OratorPrefix                     = Strings.RemoveHypertextTags(m_OratorPrefix);
      m_ReachedFormat.BaseMessage         = Strings.RemoveHypertextTags(m_ReachedFormat.BaseMessage);
      m_AssertionFailedFormat.BaseMessage = Strings.RemoveHypertextTags(m_AssertionFailedFormat.BaseMessage);

      RecompileFilters();
    }
    #endif

    protected override void OnValidate()
    {
      base.OnValidate();

      // reset cached message strings
      m_FormattedReachedMessage = null;
      m_FormattedAssertMessage  = null;

      RecompileFilters();
    }

    void RecompileFilters()
    {
      int i = m_IgnoreFilters.Count;
      if (i == 0)
      {
        m_RtIgnoreFilters = System.Array.Empty<OratorFilter>();
        return;
      }

      var rtIgnoreFilters = new List<OratorFilter>(i);

      var ignoreFlags = new OratorFilter();

      while (i --> 0)
      {
        var filter = m_IgnoreFilters[i];

        if (filter.HasRegex())
        {
          rtIgnoreFilters.Add(filter);
        }
        else if (filter.Invert)
        {
          ignoreFlags.Types |= ~ filter.Types;
        }
        else
        {
          ignoreFlags.Types |= filter.Types;
        }
      }

      if (ignoreFlags.Types != LogTypeFlags.None)
      {
        rtIgnoreFilters.Add(ignoreFlags);
          // it's important that this be the top of the stack
      }

      m_RtIgnoreFilters = rtIgnoreFilters.ToArray();
    }


    bool ShouldIgnore(LogType type, [NotNull] string msg)
    {
      int i = m_RtIgnoreFilters.Length;

      while (i --> 0)
      {
        if (m_RtIgnoreFilters[i].IsMatch(type, msg))
          return true;
      }

      return false;
    }

    bool ShouldIgnore(LogType type, [CanBeNull] ref string msg)
    {
      if (msg is null)
        msg = string.Empty;

      return ShouldIgnore(type, msg);
    }

    void FixupMessageContext(ref string msg, ref Object ctx)
    {
      if (!ctx)
      {
        ctx = this;
      }
      else if (m_IncludeContextInMessages)
      {
        msg = AppendContext(msg, ctx);
      }
    }


    static string AppendContext(string msg, [NotNull] Object ctx)
    {
      string name = ctx.name;
      if (name.IsEmpty())
        name = "<no name>";

      string typeName = ctx == Instance ? string.Empty : $"[{ctx.GetType().NiceName()}] ";

      if (ctx is UnityEngine.Component c)
      {
        return $"{typeName}{msg}\n(scene path: \"{c.gameObject.scene.name}/{name}\")";
      }

      if (ctx is GameObject go)
      {
        return $"{typeName}{msg}\n(scene path: \"{go.scene.name}/{name}\")";
      }

      return $"{typeName}{msg}\n(asset name: \"{name}\")";
    }


    #if UNITY_EDITOR

    const string MENU_PREFIX = Consts.MenuItemPrefixOre + nameof(Orator) + "/";
    const int    MENU_PRIO   = Consts.MenuPriorityOre;

    [MenuItem(MENU_PREFIX + "Test All Log Types", priority = MENU_PRIO)]
    static void Menu_TestLogs()
    {
      ReachedOnce(Instance);

      Reached();
      Log("message");
      Warn("warning");
      Error("error");
      OAssert.True(false, Instance);
      Log("(post-assert failure)");
    }

    #if ORE_DEBUG
    [MenuItem(MENU_PREFIX + "Test Custom StackTrace", priority = MENU_PRIO)]
    static void Menu_TestStackTrace()
    {
      Exceptions.DisableTraceBlacklist();
      string stacktrace = Exceptions.FormatCurrentStackTrace(12);
      Exceptions.EnableTraceBlacklist();

      Debug.LogFormat(LogType.Log, LogOption.NoStacktrace, Agent, "Custom StackTrace:\n\n\n{0}", stacktrace);

      Debug.Log("not custom stacktrace", Agent);
    }
    #endif // ORE_DEBUG

    [MenuItem(MENU_PREFIX + "Write Cache", priority = MENU_PRIO)]
    static void Menu_WriteCache()
    {
      ReachedOnce(Instance);

      if (!WriteCacheLogOnce())
        Error("failed to write Orator cache!");
    }

    [MenuItem(MENU_PREFIX + "Clear Cache", priority = MENU_PRIO)]
    static void Menu_ClearCache()
    {
      _ = Filesystem.TryDeletePath(CACHE_PATH);
      s_LoggedOnceHashes.Clear();
    }

    #endif // UNITY_EDITOR

  #endregion  Internal section

  } // end class Orator

}
