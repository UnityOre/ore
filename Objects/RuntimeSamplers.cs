/*! @file       Objects/RuntimeSamplers.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2023-04-19
 *
 *  This API MAY be moved to the PerfSpy package, or bits of the PerfSpy package
 *  may be moved to Ore. We shall see.
**/

using JetBrains.Annotations;

using UnityEngine;
using UnityEngine.Serialization;

using System.Collections;
using System.Collections.Generic;

using DateTime = System.DateTime;


namespace Ore
{
  /// <summary>
  ///   A singleton asset for defining and executing Ore's runtime sampling
  ///   behaviour, e.g., calculating average RAM values over a span.
  /// </summary>
  /// <remarks>
  ///   Intent = calculate perf metrics for logging purposes, with minimal
  ///   overhead cost.
  /// </remarks>
  [AutoCreateAsset(path: "Resources/" + nameof(RuntimeSamplers),
                   PreloadByDefault = true,
                   DefaultHideFlags = HideFlags.DontUnloadUnusedAsset)]
  public class RuntimeSamplers : OAssetSingleton<RuntimeSamplers>
  {
    // TODO consider moving MTTimer logic to separate comp unit (much like ActiveScene's Coroutines API)

    [PublicAPI]
    public static DurationPolicy DefaultDurationPolicy
    {
      get => Instance ? Instance.m_DefaultDurationPolicy : DurationPolicy.Default;
      set
      {
        if (Instance)
          Instance.m_DefaultDurationPolicy = value;
      }
    }


    /// <summary>
    ///   Registers a new timer iff one with the given identifier doesn't already
    ///   exist.
    /// </summary>
    [PublicAPI]
    public static void CreateTimer([NotNull] string identifier)
    {
      if (s_Timers.ContainsKey(identifier))
        return;

      var timer = new MTTimer();
      timer.Start();
      s_Timers[identifier] = timer;
    }

    /// <summary>
    ///   Creates/Gets the timer associated with the given identifier, and
    ///   starts/restarts it.
    /// </summary>
    [PublicAPI]
    public static void StartTimer([NotNull] string identifier)
    {
      if (s_Timers.Find(identifier, out var timer))
      {
        // ReSharper disable once PossibleNullReferenceException
        timer.Start();
      }
      else
      {
        timer = new MTTimer();
        timer.Start();
        s_Timers[identifier] = timer;
      }
    }

    /// <inheritdoc cref="GetDuration(string,DurationPolicy)"/>
    [PublicAPI]
    public static TimeInterval GetDuration([NotNull] string identifier)
    {
      return GetDuration(identifier, DefaultDurationPolicy);
    }

    /// <summary>
    ///   Get the current duration of the timer associated with the given
    ///   identifier.
    ///   <ul><li>
    ///   If a <see cref="DurationPolicy"/> is not specified, the current
    ///   <see cref="DefaultDurationPolicy"/> will be used for the result.
    ///   </li></ul>
    /// </summary>
    /// <returns>
    ///   <see cref="TimeInterval.Zero"/> if no running timer for the identifier
    ///   was found.
    /// </returns>
    [PublicAPI]
    public static TimeInterval GetDuration([NotNull] string identifier, DurationPolicy policy)
    {
      if (s_Timers.Find(identifier, out var timer))
      {
        // ReSharper disable once PossibleNullReferenceException
        return timer.GetElapsed(policy);
      }

      return TimeInterval.Zero;
    }

    /// <inheritdoc cref="StopTimer(string,DurationPolicy)"/>
    [PublicAPI]
    public static TimeInterval StopTimer([NotNull] string identifier)
    {
      return StopTimer(identifier, DefaultDurationPolicy);
    }

    /// <summary>
    ///   Stops the associated timer and returns its final duration.
    ///   <ul><li>
    ///   If a <see cref="DurationPolicy"/> is not specified, the current
    ///   <see cref="DefaultDurationPolicy"/> will be used for the result.
    ///   </li></ul>
    /// </summary>
    /// <returns>
    ///   <see cref="TimeInterval.Zero"/> if no running timer for the identifier
    ///   was found.
    /// </returns>
    /// <remarks>
    ///   You probably won't need to call this function; most use-cases will be
    ///   satisfied with utilizing only <see cref="StartTimer"/> +
    ///   <see cref="GetDuration(string)"/>, since the former can also REstart
    ///   the timer, <i>and</i> most usages will only need to sample the duration
    ///   result just one time.
    /// </remarks>
    [PublicAPI]
    public static TimeInterval StopTimer([NotNull] string identifier, DurationPolicy policy)
    {
      if (s_Timers.Find(identifier, out var timer))
      {
        // ReSharper disable once PossibleNullReferenceException
        timer.Stop();
        return timer.GetElapsed(policy);
      }

      return TimeInterval.Zero;
    }

    /// <summary>
    ///   Resume a stopped timer.
    ///   <ul><li>
    ///   If the timer is currently running (or doesn't exist), this is a no-op.
    ///   </li></ul>
    /// </summary>
    /// <remarks>
    ///   You probably won't need to call this function for most usages—it is
    ///   provided merely for completeness.
    /// </remarks>
    [PublicAPI]
    public static void ResumeTimer([NotNull] string identifier)
    {
      if (s_Timers.Find(identifier, out var timer))
      {
        // ReSharper disable once PossibleNullReferenceException
        timer.Resume();
      }
    }

    /// <returns>
    ///   <c>true</c> iff there is a timer associated with the given identifier,
    ///   and it is currently running.
    /// </returns>
    [PublicAPI]
    public static bool IsTimerRunning([NotNull] string identifier)
    {
      // ReSharper disable once PossibleNullReferenceException
      return s_Timers.Find(identifier, out var timer) && timer.IsRunning;
    }

    /// <summary>
    ///   Remove the timer associated with identifier and stop tracking it, if
    ///   it exists.
    /// </summary>
    /// <remarks>
    ///   Unless cleaning up after test code, you probably won't need to call
    ///   this function.
    /// </remarks>
    [PublicAPI]
    public static void DeleteTimer([NotNull] string identifiier)
    {
      _ = s_Timers.Unmap(identifiier);
    }

    /// <summary>
    ///   Unregisters and forgets all timers previously created via the
    ///   <see cref="RuntimeSamplers"/> API.
    /// </summary>
    [PublicAPI]
    public static void ClearAllTimers()
    {
      s_Timers.Clear();
    }


    [PublicAPI]
    public static int CountRecentSlowFrames()
    {
      return s_RecentSlowFrames;
    }

    [PublicAPI]
    public static float PercentRecentSlowFrames()
    {
      return (float)s_RecentSlowFrames / s_RecentFrameCount;
    }


    /// <summary>
    ///   Attempts to sample a 32-bit float representation of total <b>system</b>
    ///   RAM in IEC mebibytes, provided by the current <see cref="CoarseSampler"/>
    ///   configuration.
    ///   <ul><li>
    ///   <b>Reminder:</b> if this is not working in builds, double-check the
    ///   relevant "Enabled In" settings on <c>Resources/RuntimeSamplers.asset</c>
    ///   before you report a bug!
    ///   </li></ul>
    /// </summary>
    /// <returns>
    ///   <c>true</c> if there is a loaded instance of
    ///   <see cref="RuntimeSamplers"/>, <i>and</i> its CoarseSampler for system
    ///   RAM has valid samples to report.
    /// </returns>
    /// <remarks>
    ///   <b>
    ///   This is the one you want for logging!!!
    ///   </b>
    /// </remarks>
    [PublicAPI]
    public static bool SampleSystemRAM(out float mebibytes)
    {
      mebibytes = 0;

      if (!Instance)
        return false;

      var sampler = Instance.m_RAMSampler;

      if (sampler is null)
        return false;

      mebibytes = (float)(sampler.LastSample / Consts.Mebi);
      return sampler.HasSamples;
    }

    /// <inheritdoc cref="SampleSystemRAM(out float)"/>
    [PublicAPI]
    public static bool SampleSystemRAM(out long bytes)
    {
      bytes = 0;

      if (!Instance)
        return false;

      var sampler = Instance.m_RAMSampler;

      if (sampler is null)
        return false;

      bytes = sampler.LastSample.Rounded();
      return sampler.HasSamples;
    }

    // TODO add API for grabbing mean over time

    /// <summary>
    ///   Attempts to sample a 32-bit float representation of total <b>managed</b>
    ///   RAM in IEC mebibytes, provided by the current <see cref="CoarseSampler"/>
    ///   configuration.
    ///   <ul><li>
    ///   <b>Reminder:</b> if this is not working in builds, double-check the
    ///   relevant "Enabled In" settings on <c>Resources/RuntimeSamplers.asset</c>
    ///   before you report a bug!
    ///   </li></ul>
    /// </summary>
    /// <returns>
    ///   <c>true</c> if there is a loaded instance of
    ///   <see cref="RuntimeSamplers"/>, <i>and</i> its CoarseSampler for
    ///   managed RAM has valid samples to report.
    /// </returns>
    /// <remarks>
    ///   This is <b>NOT</b> the one you want for logging!!! <br/>
    ///   <i>(... although it can be useful for debugging/optimizing the heap)</i>
    /// </remarks>
    [PublicAPI]
    public static bool SampleManagedRAM(out float mebibytes)
    {
      mebibytes = 0;

      if (!Instance)
        return false;

      var sampler = Instance.m_ManagedRAMSampler;

      if (sampler is null)
        return false;

      mebibytes = (float)(sampler.LastSample / Consts.Mebi);
      return sampler.HasSamples;
    }

    /// <inheritdoc cref="SampleManagedRAM(out float)"/>
    [PublicAPI]
    public static bool SampleManagedRAM(out long bytes)
    {
      bytes = 0;

      if (!Instance)
        return false;

      var sampler = Instance.m_ManagedRAMSampler;

      if (sampler is null)
        return false;

      bytes = sampler.LastSample.Rounded();
      return sampler.HasSamples;
    }

    /// <summary>
    ///   Try to sample a stat that was customized (at editor-time) in
    ///   <c>Resources/RuntimeSampler.asset</c>.
    /// </summary>
    /// <param name="stat">
    ///   A <see cref="ProfilerStat"/> string to attempt to find an existing
    ///   sampler for.
    /// </param>
    /// <param name="sample">
    ///   The last sample value as a "raw" double. Its meaning is dependent on
    ///   the stat, and your configuration of the <see cref="CoarseSampler"/>.
    /// </param>
    /// <returns>
    ///   <c>true</c> if a sampler for the given stat was found, and if it had
    ///   valid samples to report.
    /// </returns>
    [PublicAPI]
    public static bool SampleOther(string stat, out double sample)
    {
      // TODO this lazy system is bad, doesn't handle multiple samplers with the same key, needs change

      if (s_Samplers.Find(stat, out var sampler))
      {
        // ReSharper disable once PossibleNullReferenceException
        sample = sampler.LastSample;
        return sampler.HasSamples;
      }

      sample = 0;
      return false;
    }


  [Header("Timers"), Space]

    [SerializeField, FormerlySerializedAs("m_TimerDurationPolicy")]
    DurationPolicy m_DefaultDurationPolicy = DurationPolicy.Default;

  [Header("Recent Stats (Sliding Window)"), Space]
    [SerializeField]
    TimeInterval m_RecentStatsInterval = TimeInterval.OfSeconds(30);

  [Header("Samplers"), Space]

    [SerializeField]
    CoarseSampler m_RAMSampler = CoarseSampler.SystemRAM;

    [SerializeField]
    CoarseSampler m_ManagedRAMSampler = CoarseSampler.ManagedRAM;

    [SerializeField]
    List<CoarseSampler> m_AdditionalSamplers = new List<CoarseSampler>();


    //
    // Private section
    //

    class MTTimer
    {
      public bool IsRunning => m_Start > m_Stopt;

      public TimeInterval GetElapsed(DurationPolicy policy)
      {
        long ticks = (m_Stopt < m_Start ? DateTime.UtcNow.Ticks : m_Stopt) - m_Start;

        if (policy == DurationPolicy.IgnoreMultitask)
          ticks -= m_Taskt;

        return new TimeInterval(ticks);
      }

      public void Start()
      {
        m_Start = DateTime.UtcNow.Ticks;
        m_Stopt = 0;
        m_Taskt = 0;
      }

      public void Stop()
      {
        if (m_Start > 0)
        {
          m_Stopt = DateTime.UtcNow.Ticks;
        }
      }

      public void Resume()
      {
        if (m_Start > m_Stopt)
          return;

        m_Start += DateTime.UtcNow.Ticks - m_Stopt;
        m_Stopt  = 0;
        // don't touch m_Taskt in this case
      }


      public void OnTaskIn(long now)
      {
        if (m_TasktOut <= 0)
          return;

        m_Taskt += now - m_TasktOut;
        m_TasktOut = 0;
      }

      public void OnTaskOut(long now)
      {
        if (m_Start > m_Stopt)
          m_TasktOut = now;
      }

      long m_Start;
      long m_Stopt;
      long m_TasktOut;
      long m_Taskt;
    } // end class MTTimer

    static readonly HashMap<string,MTTimer> s_Timers = new HashMap<string,MTTimer>();

    static int s_RecentStatsSeconds = 30;

    static readonly List<(int second, short frames, short slow)> s_RecentSeconds = new List<(int,short,short)>();

    static int s_RecentFrameCount, s_RecentSlowFrames;

    static readonly HashMap<string,CoarseSampler> s_Samplers = new HashMap<string,CoarseSampler>();

    static readonly List<(IEnumerator wait,IEnumerator sampler)> s_RunningSamplers = new List<(IEnumerator,IEnumerator)>();


    static void UpdateRecentStats()
    {
      var now = (int)GetDuration(nameof(UpdateRecentStats), DurationPolicy.IgnoreMultitask).FSeconds;

      int i = s_RecentSeconds.Count;
      while (i --> 0)
      {
        if (now - s_RecentSeconds[i].second < s_RecentStatsSeconds)
          continue;

        int j = i + 1;
        while (j --> 0)
        {
          s_RecentFrameCount -= s_RecentSeconds[j].frames;
          s_RecentSlowFrames -= s_RecentSeconds[j].slow;
        }

        s_RecentSeconds.RemoveRange(0, i + 1);
        break;
      }

      ++ s_RecentFrameCount;

      int slow = DeviceSpy.IsSlowFrame(Time.unscaledDeltaTime) ? 1 : 0;

      s_RecentSlowFrames += slow;

      i = s_RecentSeconds.Count - 1;
      if (i < 0 || s_RecentSeconds[i].second != now)
      {
        s_RecentSeconds.Add((now, 1, (short)slow));
      }
      else
      {
        var (n, f, s) = s_RecentSeconds[i];
        s_RecentSeconds[i] = (n, ++f, (short)(s + slow));
      }
    }


    static bool IsSamplerValid(CoarseSampler sampler)
    {
      return sampler != null &&
            !sampler.Category.IsEmpty() &&
            !sampler.Stat.IsEmpty() &&
            // ReSharper disable once ConditionIsAlwaysTrueOrFalse // ([Range] attribute isn't a guarantee)
             sampler.BatchSize > 0;
    }

    IEnumerable<CoarseSampler> AllSamplers()
    {
      if (IsSamplerValid(m_RAMSampler))
        yield return m_RAMSampler;

      if (IsSamplerValid(m_ManagedRAMSampler))
        yield return m_ManagedRAMSampler;

      foreach (var sampler in m_AdditionalSamplers)
      {
        if (IsSamplerValid(sampler))
          yield return sampler;
      }
    }

    bool SetupSamplers()
    {
      s_Samplers.Clear();
      s_RunningSamplers.Clear();

      foreach (var sampler in AllSamplers())
      {
        if (s_Samplers.Map(sampler.Stat, sampler) && sampler.Setup())
        {
          s_RunningSamplers.Add((((IEnumerator)sampler).Current as IEnumerator, sampler));
        }
      }

      return s_RunningSamplers.Count > 0;
    }

    static void UpdateSamplers()
    {
      // (basically a lightweight coroutine impl.)

      int i = s_RunningSamplers.Count;
      while (i --> 0)
      {
        var (wait,sampler) = s_RunningSamplers[i];

        if (wait != null && wait.MoveNext())
          continue;

        if (!sampler.MoveNext())
        {
          s_RunningSamplers.RemoveAt(i);
          continue;
        }

        s_RunningSamplers[i] = (sampler.Current as IEnumerator, sampler);
      }
    }


    protected override void OnEnable()
    {
      if (OAssert.Fails(TryInitialize(this), "bad singleton init"))
        return;

      Application.focusChanged -= OnTaskInOut;
      Application.focusChanged += OnTaskInOut;

      #if UNITY_EDITOR
      if (Application.isPlaying)
      {
        UnityEditor.EditorApplication.pauseStateChanged -= OnPauseStateChanged;
        UnityEditor.EditorApplication.pauseStateChanged += OnPauseStateChanged;
      }
      #endif

      CreateTimer(nameof(UpdateRecentStats));
      s_RecentStatsSeconds = (int)m_RecentStatsInterval.FSeconds.AtLeast(1f);
      ActiveScene.OnUpdate -= UpdateRecentStats;
      ActiveScene.OnUpdate += UpdateRecentStats;

      if (!SetupSamplers())
        return;

      ActiveScene.OnLateUpdate -= UpdateSamplers;
      ActiveScene.OnLateUpdate += UpdateSamplers;
    }

    protected override void OnDisable()
    {
      base.OnDisable();

      if (ActiveScene.IsQuitting)
        return;

      Application.focusChanged -= OnTaskInOut;

      #if UNITY_EDITOR
      if (Application.isPlaying)
      {
        UnityEditor.EditorApplication.pauseStateChanged -= OnPauseStateChanged;
      }
      #endif

      DeleteTimer(nameof(UpdateRecentStats));
      s_RecentSeconds.Clear();
      s_RecentSeconds.TrimExcess();
      s_RecentFrameCount = 0;
      s_RecentSlowFrames = 0;
      ActiveScene.OnUpdate -= UpdateRecentStats;

      ActiveScene.OnLateUpdate -= UpdateSamplers;

      foreach (var sampler in AllSamplers())
      {
        sampler.Dispose();
      }
    }


    #if UNITY_EDITOR
    static void OnPauseStateChanged(UnityEditor.PauseState state)
    {
      OnTaskInOut(state == UnityEditor.PauseState.Unpaused);
    }
    #endif // UNITY_EDITOR


    static void OnTaskInOut(bool taskedIn)
    {
      if (s_Timers.Count == 0)
        return;

      long now = DateTime.UtcNow.Ticks;

      if (taskedIn)
      {
        foreach (var timer in s_Timers.Values)
        {
          timer.OnTaskIn(now);
        }
      }
      else
      {
        foreach (var timer in s_Timers.Values)
        {
          timer.OnTaskOut(now);
        }
      }
    }


    public void DebugDrawOnGUI()
    {
      int line = 1;
      foreach (var sampler in AllSamplers())
      {
        line = DoGUILine(sampler, line);
      }
    }

    int DoGUILine(CoarseSampler sampler, int line)
    {
      if (!sampler.IsReady)
        return line;

      sampler.Report(out double max, out double min, out double mean, out double stdev,
                     div: 1024 * 1024);

      var label = MeGUI.Scratch;
      using (new RecycledStringBuilder(sampler.Stat, out var bob))
      {
        bob.Append(": max="  ).Append(  max.ToString("F1"));
        bob.Append(", min="  ).Append(  min.ToString("F1"));
        bob.Append(", mean=" ).Append( mean.ToString("F1"));
        bob.Append(", stdev=").Append(stdev.ToString("F1"));
        label.text = bob.ToString();
      }

      var style = MeGUI.TextStyle;

      const float X = 5f, Y = 5f, H = 22f;

      var pos = new Rect(x: X,
                         y: Screen.height - Y - line * (H + 2f),
                     width: Screen.width  - 2 * X,
                    height: H);

      if (line == 1)
      {
        var scale = MeGUI.CalcContentScale(label, style);
        var pivot = new Vector2(X, Screen.height - Y);

        GUI.matrix = Matrix4x4.identity;
        GUIUtility.ScaleAroundPivot(scale, pivot);
      }

      GUI.Label(pos, label, style);
      return line + 1;
    }

  } // end class RuntimeSamplers
}