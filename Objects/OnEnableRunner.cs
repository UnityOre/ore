/*! @file       Objects/OnEnableRunner.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2023-02-01
**/

using UnityEngine;


namespace Ore
{
  [AddComponentMenu("Ore/On Enable Runner")]
  [DisallowMultipleComponent]
  public class OnEnableRunner : OComponent
  {

    [Space]

    [SerializeField]
    DelayedEvent[] m_OnEnabled = System.Array.Empty<DelayedEvent>();

    [Space]

    [SerializeField]
    DelayedEvent[] m_OnDisabled = System.Array.Empty<DelayedEvent>();


    void OnEnable()
    {
      foreach (var evt in m_OnEnabled)
      {
        if (evt.RunsGlobally)
          evt.TryInvokeOnActiveScene();
        else
          evt.TryInvokeOn(this);
      }
    }

    void OnDisable()
    {
      foreach (var evt in m_OnDisabled)
      {
        if (evt.RunsGlobally)
          evt.TryInvokeOnActiveScene();
        else
          evt.TryInvokeOn(this);
      }
    }
  } // end class OnEnableRunner
}
