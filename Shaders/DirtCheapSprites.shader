/*! @file       DirtCheapParticles.shader
 *  @author     Levi Perez (levianperez\@gmail.com)
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2023-01-31
**/


Shader "Dirt Cheap/Sprite"
{
    Properties
    {
        [PerRendererData] _MainTex   ("Sprite Texture", 2D) = "white" { }
        [MaterialToggle]   PixelSnap ("Pixel snap", Float)  =  0
//      [HideInInspector] _Flip      ("Flip", Vector)       = (1,1,1,1)
    }

    SubShader
    {
        Tags
        {
            "Queue" = "Transparent"
            "RenderType" = "Transparent"
            "CanUseSpriteAtlas" = "true"
            "PreviewType" = "Plane"
            "IgnoreProjector" = "true"
            "ForceNoShadowCasting" = "true"
        }

        ZWrite Off

//      Cull Off // (needed to support flip)

        Blend One OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment SpriteFrag

            #include "UnitySprites.cginc"

            v2f vert (const appdata_t v)
            {
                v2f o;

                UNITY_SETUP_INSTANCE_ID(v);
                UNITY_INITIALIZE_OUTPUT(v2f, o);

                // no flip
                o.vertex = UnityObjectToClipPos(v.vertex.xyz);

                #ifdef PIXELSNAP_ON
                o.vertex = UnityPixelSnap(o.vertex);
                #endif

                o.texcoord = v.texcoord;
                o.color = v.color * _RendererColor;

                return o;
            }

            ENDCG
        }
    }

    FallBack "Sprites/Default"
}
