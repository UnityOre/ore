/*! @file       DirtCheapParticles.shader
 *  @author     Levi Perez (levianperez\@gmail.com)
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2023-01-31
**/


Shader "Dirt Cheap/Particles"
{
    SubShader
    {
        Tags
        {
            "Queue" = "Geometry+10"
            "RenderType" = "Opaque"
            "PreviewType" = "Plane"
            "IgnoreProjector" = "true"
            "ForceNoShadowCasting" = "true"
        }

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityStandardParticles.cginc"

            struct appdata_cheap
            {
                float3 vertex : POSITION;
                fixed4 color  : SV_Target;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };
            
            struct v2f
            {
                float4 vertex : SV_POSITION;
                fixed4 color : SV_Target;
                UNITY_VERTEX_OUTPUT_STEREO
            };

            v2f vert (const appdata_cheap v)
            {
                UNITY_SETUP_INSTANCE_ID(v);

                v2f o;
                UNITY_INITIALIZE_OUTPUT(v2f, o);

                o.vertex = UnityObjectToClipPos(v.vertex);
                o.color = v.color;
                vertColor(o.color);
                // no texcoord
                // no fading
                // no distortion
                // no fog

                return o;
            }

            fixed4 frag (const v2f i) : SV_Target
            {
                return i.color;
            }

            ENDCG
        }
    }

    FallBack "Mobile/Particles/Alpha Blended"
}
