/*! @file       Editor/Shell.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2023-09-29
**/

using JetBrains.Annotations;

using UnityEngine;
using UnityEditor;

using System.Diagnostics;
using System.IO;

using Debug = UnityEngine.Debug;


namespace Ore.Editor
{
  [InitializeOnLoad]
  public static class Shell
  {
    [PublicAPI]
    public struct Result
    {
      public string Command;
      public string StdOut;
      public string StdErr;
      public int    ExitCode;

      public bool Succeeded => ExitCode == 0 && StdErr.IsEmpty();
      public bool IsEmpty   => StdOut.IsEmpty();

      public bool PrintFailure(bool verbose = Consts.IsOreDebug)
      {
        if (ExitCode == 0 && StdErr.IsEmpty())
        {
          if (verbose)
            Debug.Log(this);
          return false;
        }

        Debug.LogError(this);
        return true;
      }

      public override string ToString()
      {
        using (new RecycledStringBuilder(out var bob))
        {
          bob.Append(ExitCode == 0 && StdErr.IsEmpty() ? "SUCCEEDED: " : "FAILED: ")
             .Append(Command);

          bob.Append("\n# stdout ####\n").Append(StdOut.IsEmpty() ? "(empty)" : StdOut);
          bob.Append("\n# stderr ####\n").Append(StdErr.IsEmpty() ? "(empty)" : StdErr);
          bob.Append("\n# exit code: ").Append(ExitCode);

          return bob.ToString();
        }
      }

    } // end nested struct Output


    public static bool KnowsBash => !k_PathToBash.IsEmpty();
    public static bool KnowsGit  => !k_PathToGit.IsEmpty();


    [PublicAPI]
    public static Result RunBashOneshot(string cmd, string pwd = null)
    {
      using (var process = new Process())
      {
        process.StartInfo = new ProcessStartInfo
        {
          UseShellExecute        = false,
          CreateNoWindow         = true,
          RedirectStandardInput  = false,
          RedirectStandardOutput = true,
          RedirectStandardError  = true,
          LoadUserProfile        = false,
          WorkingDirectory       = pwd ?? Paths.ProjectRoot,
          FileName               = k_PathToBash,
          Arguments              = $"-l -c '{cmd}'",
        };

        try
        {
          process.Start();
          process.WaitForExit();

          return new Result
          {
            Command  = cmd,
            StdOut   = process.StandardOutput.ReadToEnd(),
            StdErr   = process.StandardError.ReadToEnd(),
            ExitCode = process.ExitCode,
          };
        }
        catch (System.Exception ex)
        {
          Debug.LogException(ex);

          return new Result
          {
            Command  = cmd,
            StdErr   = ex.Message,
            ExitCode = process.ExitCode
          };
        }
      }
    }

    [PublicAPI]
    public static Result RunGitOneshot(string subCommand, string pwd = null)
    {
      using (var process = new Process())
      {
        process.StartInfo = new ProcessStartInfo
        {
          UseShellExecute        = false,
          CreateNoWindow         = true,
          RedirectStandardInput  = false,
          RedirectStandardOutput = true,
          RedirectStandardError  = true,
          LoadUserProfile        = false,
          WorkingDirectory       = pwd ?? Paths.ProjectRoot,
          FileName               = "git",
          Arguments              = subCommand,
        };

        try
        {
          process.Start();
          process.WaitForExit();

          return new Result
          {
            Command  = $"git {subCommand}",
            StdOut   = process.StandardOutput.ReadToEnd(),
            StdErr   = process.StandardError.ReadToEnd(),
            ExitCode = process.ExitCode,
          };
        }
        catch (System.Exception ex)
        {
          Debug.LogException(ex);

          return new Result
          {
            Command  = $"git {subCommand}",
            StdErr   = ex.Message,
            ExitCode = process.ExitCode
          };
        }
      }
    }


    // TODO keep a single shell instance alive, to optimize batching commands?


    //
    // privates

    static readonly string k_PathToBash;
    static readonly string k_PathToGit;

    static Shell()
    {
      string[] PATH = System.Environment.GetEnvironmentVariable("PATH")?
                                        .Split(new []{ Path.PathSeparator },
                                               System.StringSplitOptions.RemoveEmptyEntries)
                      ?? System.Array.Empty<string>();

      foreach (var path in PATH)
      {
        if (k_PathToBash.IsEmpty())
        {
          string bash = Path.Combine(path, "bash");

          CheckBash:
          if (File.Exists(bash))
          {
            #if ORE_DEBUG
            Debug.Log($"[{nameof(Shell)}] Found path to bash: \"{bash}\"");
            #endif

            k_PathToBash = bash;
          }
          #if UNITY_EDITOR_WIN
          else if (!bash.EndsWith(".exe"))
          {
            bash += ".exe";
            goto CheckBash;
          }
          #endif
        }

        if (k_PathToGit.IsEmpty())
        {
          string git = Path.Combine(path, "git");

          CheckGit:
          if (File.Exists(git))
          {
            #if ORE_DEBUG
            Debug.Log($"[{nameof(Shell)}] Found path to git: \"{git}\"");
            #endif

            k_PathToGit = git;
          }
          #if UNITY_EDITOR_WIN
          else if (!git.EndsWith(".exe"))
          {
            git += ".exe";
            goto CheckGit;
          }
          #endif
        }

        if (!k_PathToBash.IsEmpty() && !k_PathToGit.IsEmpty())
          return;
      }

      #if ORE_DEBUG
      if (k_PathToBash.IsEmpty())
        Debug.LogWarning($"[{nameof(Shell)}] Could not find any bash executable on your PATH.");
      if (k_PathToGit.IsEmpty())
        Debug.LogWarning($"[{nameof(Shell)}] Could not find any git executable on your PATH.");
      #endif
    }

  } // end class Shell
}