/*! @file       Editor/EventDrawer.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2022-06-14
**/

using UnityEngine;
using UnityEditor;


namespace Ore.Editor
{

  [CustomPropertyDrawer(typeof(VoidEvent))]
  [CustomPropertyDrawer(typeof(DelayedEvent))]
  [CustomPropertyDrawer(typeof(ThingEvent<>), useForChildren: true)]
  internal class EventDrawer : UnityEditorInternal.UnityEventDrawer
  {
    const string UNITYEVENT_LAST_PROPERTY = "m_PersistentCalls";
    const string LABEL_SUFFIX_DISABLED = " (event disabled)";

    const float STD_PAD           = OGUI.STD_PAD;
    const float STD_LINE_HEIGHT   = OGUI.STD_LINE_HEIGHT;
    const float UNEXPANDED_HEIGHT = STD_LINE_HEIGHT + OGUI.STD_PAD;

    class DrawerState : PropertyDrawerState
    {
      public int    ChildCount;
      public float  ExtraHeight;
      public string EventLabel;

      public SerializedProperty IsEnabled;
      public SerializedProperty RunInGlobalContext, Context;

      protected override void OnUpdateProperty()
      {
        if (CheckFails(out var root))
          return;

        IsEnabled = root.FindPropertyRelative("m_IsEnabled");

        ChildCount = 0;
        ExtraHeight = UNEXPANDED_HEIGHT;

        var iterator = root.FindPropertyRelative(UNITYEVENT_LAST_PROPERTY);

        while (iterator.NextVisible(false) &&
               iterator.depth == root.depth + 1 &&
               iterator.propertyPath.StartsWith(root.propertyPath))
        {
          ExtraHeight += EditorGUI.GetPropertyHeight(iterator, iterator.isExpanded) + OGUI.STD_PAD;
          ++ChildCount;
        }

        if (root.TryGetUnderlyingValue(out object boxed, warn: true))
        {
          EventLabel = $"{boxed.GetType().NiceName()}: {root.displayName}";
        }
        else
        {
          EventLabel = $"ErrorErrorError: {root.displayName}";
        }

        // done filling state.

        AutoUpdateHiddenFields(root);
      }

      public void UpdateExtraHeight()
      {
        if (CheckFails(out var root))
          return;

        IsStale = false;

        if (ChildCount == 0)
          return;

        ExtraHeight = UNEXPANDED_HEIGHT;

        if (Context != null && RunInGlobalContext != null)
          ExtraHeight += STD_LINE_HEIGHT + STD_PAD;

        var iterator = root.FindPropertyRelative(UNITYEVENT_LAST_PROPERTY);

        int i = 0;
        while (i++ < ChildCount && iterator.NextVisible(false))
        {
          ExtraHeight += EditorGUI.GetPropertyHeight(iterator, iterator.isExpanded) + STD_PAD;
        }
      }

      public SerializedProperty GetChildIterator()
      {
        if (ChildCount == 0 || CheckFails(out var root))
          return null;

        return root.FindPropertyRelative(UNITYEVENT_LAST_PROPERTY);
      }


      internal void AutoUpdateHiddenFields(SerializedProperty root)
      {
        // MonoBehaviour m_Context
        Context = root.FindPropertyRelative("m_Context");
        if (Context is null)
        {
        }
        else if (OAssert.Fails(Context.propertyType == SerializedPropertyType.ObjectReference))
        {
          Context = null;
        }
        else
        {
          if (root.serializedObject.targetObject is MonoBehaviour owner)
            Context.objectReferenceValue = owner;
          else if (root.serializedObject.targetObject is ScriptableObject contract)
            Context.objectReferenceValue = contract;
          else
            Context = null;
        }

        // bool m_RunInGlobalContext
        RunInGlobalContext = root.FindPropertyRelative("m_RunInGlobalContext");
        if (RunInGlobalContext is null)
        {
        }
        else if (OAssert.Fails(RunInGlobalContext.propertyType == SerializedPropertyType.Boolean))
        {
          RunInGlobalContext = null;
        }
        else if (Context == null || !Context.objectReferenceValue || Context.objectReferenceValue is ScriptableObject)
        {
          RunInGlobalContext.boolValue = true;
        }

        root.serializedObject.ApplyModifiedProperties();
      }

    } // end internal class DrawerState


    public override void OnGUI(Rect total, SerializedProperty root, GUIContent label)
    {
      PropertyDrawerState.Restore(root, out DrawerState state);

      // enable/disable button
      float btnX = OGUI.FieldStartX + OGUI.FieldWidth * 0.45f;
      var pos = new Rect(btnX, total.y + 1f, total.xMax - btnX, STD_LINE_HEIGHT);

      string btnLabel = state.IsEnabled.boolValue ? "Disable Event" : "Enable Event";

      if (GUI.Button(pos, btnLabel))
      {
        state.IsEnabled.boolValue = !state.IsEnabled.boolValue;
        return;
      }

      if (state.IsEnabled.boolValue)
        label.text = root.displayName;
      else
        label.text = root.displayName + LABEL_SUFFIX_DISABLED;

      // now do foldout header:
      pos.x = total.x;
      pos.xMax = btnX - STD_PAD * 2;

      using (var header = FoldoutHeader.Open(pos, label, root, !state.IsEnabled.boolValue, indent: root.depth+1))
      {
        if (!header.IsOpen)
          return;

        pos.x = header.Rect.x;
        pos.xMax = total.xMax;
        pos.y += pos.height + STD_PAD;

        // draw the optional field for "Run In Global Context" bool (if applicable)
        if (state.RunInGlobalContext != null)
        {
          label.text = state.RunInGlobalContext.displayName;
          pos.height = STD_LINE_HEIGHT;

          pos.xMax = OGUI.LabelEndX;

          EditorGUI.BeginDisabledGroup(root.serializedObject.targetObject is ScriptableObject);
          _ = EditorGUI.PropertyField(pos, state.RunInGlobalContext, label);
          EditorGUI.EndDisabledGroup();

          // draw context reference (read-only info)
          pos.x = pos.xMax + STD_LINE_HEIGHT;
          pos.xMax = total.xMax;

          if (state.RunInGlobalContext.boolValue)
          {
            label.text = $"→ will run on Runtime {Styles.ColorText(nameof(ActiveScene), Colors.Reference)}";
          }
          else if (state.Context != null && state.Context.objectReferenceValue)
          {
            label.text = $"→ will run on this {Styles.ColorText(state.Context.objectReferenceValue.GetType().NiceName(), Colors.Reference)}";
          }
          else
          {
            label.text = Styles.ColorText("ERR: bad serial context", Colors.Attention);
            state.AutoUpdateHiddenFields(root);
          }

          EditorGUI.LabelField(pos, label);

          pos.x = header.Rect.x;

          pos.xMax = total.xMax;
          pos.y += pos.height + STD_PAD;
        }

        // get the property iterator for our extra members:
        var childProp = state.GetChildIterator();
        if (childProp != null)
        {
          int i = 0;

          // iterate:
          while (i++ < state.ChildCount && childProp.NextVisible(false))
          {
            label.text = childProp.displayName;
            pos.height = EditorGUI.GetPropertyHeight(childProp, label, includeChildren: true);

            _ = EditorGUI.PropertyField(pos, childProp, label, includeChildren: true);

            pos.y += pos.height + STD_PAD;
          }

          childProp.Dispose();
        }

        // finally, draw the vanilla event interface:
        pos.yMax = total.yMax;
        label.text = state.EventLabel;
        base.OnGUI(pos, root, label);
      } // end using scope
    }


    public override float GetPropertyHeight(SerializedProperty prop, GUIContent label)
    {
      if (!prop.isExpanded)
        return UNEXPANDED_HEIGHT;

      PropertyDrawerState.Restore(prop, out DrawerState state);

      if (state.IsStale)
        state.UpdateExtraHeight();

      return base.GetPropertyHeight(prop, label) + 2f + state.ExtraHeight;
    }

  } // end class EventDrawer

}
