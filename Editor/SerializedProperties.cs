/*! @file       Editor/SerializedProperties.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2022-06-15
**/

using JetBrains.Annotations;

using UnityEngine;
using UnityEditor;

using System.Collections;
using System.Collections.Generic;

using FieldInfo = System.Reflection.FieldInfo;


namespace Ore.Editor
{

  public static class SerializedProperties
  {

    public static bool IsDisposed([CanBeNull] this SerializedProperty prop)
    {
      // JESUS CHRIST WAS IT SO HARD TO PROVIDE A METHOD LIKE THIS, UNITY ?!

      // I HAD TO SCOUR THE SOURCE CODE IN ORDER TO FIGURE OUT THAT
      // I COULD CHECK FOR DISPOSAL THIS WAY:

      if (SerializedProperty.EqualContents(null, prop))
      {
        // (this 1st call checks the internal C++ pointer if it's nullptr.)
        return true;
      }

      try
      {
        return !!!!!!!!!!!SerializedProperty.EqualContents(prop, prop);
        // (this fucky call checks for more recent (Unity 2020) fuckery, and is what throws the NRE.)
        // ((no, the exclamation bangs are not necessary for this to work.))
        // (((...other than the fact that there are an odd number of them...)))
      }
      catch (System.NullReferenceException)
      {
        // I hate everyone. (Except for you, reader <3)
        return true;
      }
    }


    public static bool IsArrayElement([NotNull] this SerializedProperty prop)
    {
      return prop.propertyPath.EndsWith("]");
    }

    public static bool IsPartOfArrayElement([NotNull] this SerializedProperty prop)
    {
      return prop.propertyPath.LastIndexOf(".Array.data[", System.StringComparison.Ordinal) > 0;
    }

    public static bool IsNonReorderable(this SerializedProperty prop)
    {
      #if UNITY_2020_1_OR_NEWER
      return !TryGetFieldInfo(prop, out FieldInfo field) || field.IsDefined<NonReorderableAttribute>();
      #else
      return true;
      #endif
    }

    public static bool IsReadOnly(this SerializedProperty prop)
    {
      bool ok = TryGetFieldInfo(prop, out FieldInfo field);
      OAssert.True(ok, "prop.TryGetFieldInfo");
      return field.IsDefined<ReadOnlyAttribute>();
    }


    public static uint GetPropertyHash(this SerializedProperty prop)
    {
      if (prop.IsDisposed())
      {
        Orator.Log("Called GetPropertyHash() on a disposed SerializedProperty!");
        return 0;
      }

      return Hashing.MixHashes(prop.propertyPath.GetHashCode(),
                               prop.serializedObject.targetObject.GetInstanceID());
    }


    public static bool TryGetParentArray(this SerializedProperty elem, out SerializedProperty arr)
    {
      var path = elem.propertyPath;
      int cut = path.LastIndexOf(".Array.data[", System.StringComparison.Ordinal);
      if (cut < 0)
      {
        arr = null;
        return false;
      }

      arr = elem.serializedObject.FindProperty(path.Remove(cut));
      return arr?.isArray ?? false;
    }


    public static bool TryGetFieldInfo(this SerializedProperty prop, out FieldInfo field)
    {
      field = null;

      string[] splits = prop.propertyPath.Split('.');
      var currType = prop.serializedObject.targetObject.GetType();

      for (int i = 0, ilen = splits.Length; i < ilen; ++i)
      {
        if (currType.TryGetSerializableField(splits[i], out field))
        {
          currType = field.FieldType;

          if (i == ilen - 1)
            break;

          if (currType.IsArray)
            currType = currType.GetElementType();
          else if (currType.IsGenericType && currType.GetGenericTypeDefinition() == typeof(List<>))
            currType = currType.GetGenericArguments()[0];
          else // (skip the remaining block if it's just a normal field)
            continue;

          // Ignore "Array.data[idx]" slugs:
          i += 2;
        }
        else
        {
          Orator.Reached(typeof(SerializedProperties));
          return false;
        }
      }

      return field != null;
    }


    public static bool TryGetUnderlyingValue<T>(this SerializedProperty prop,
                                                out T fieldValue,
                                                bool warn = false)
    {
      if (TryGetUnderlyingBoxedValue(prop, out object boxed))
      {
        if (boxed is T valid)
        {
          fieldValue = valid;
          return true;
        }

        Orator.Error($"Cannot cast found field to type {typeof(T).NiceName()}!");
      }
      else if (warn)
      {
        Orator.Warn($"No field value found! type: {typeof(T).NiceName()}; path: \"{prop.propertyPath}\"");
      }

      fieldValue = default;
      return false;
    }

    public static bool TryGetUnderlyingBoxedValue(SerializedProperty prop,
                                                  out object boxed)
    {
      boxed = prop.serializedObject.targetObject;
      return TryGetUnderlyingBoxedValue(prop.propertyPath, ref boxed);
    }


    // private

    static bool TryGetUnderlyingBoxedValue(string propPath, ref object boxed)
    {
      if (boxed is null)
        return false;

      string[] pathSplits = propPath.Split('.');

      for (int i = 0; i < pathSplits.Length; ++i)
      {
        var fieldName = pathSplits[i];

        // Special handling to support Array/List nested types:
        if (fieldName == "Array" && boxed is IList boxedList)
        {
          if (Parsing.TryParseNextIndex(pathSplits[++i], out int idx) && idx < boxedList.Count)
          {
            boxed = boxedList[idx];
            continue;
          }

          return false;
        }

        if (!boxed.GetType().TryGetSerializableField(fieldName, out var field))
          return false;

        boxed = field.GetValue(boxed);

      } // end for-loop

      return boxed != null;
    }

  } // end static class SerializedProperties

}
