﻿/*! @file       Editor/TimeIntervalDrawer.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2022-11-08
**/

using UnityEngine;
using UnityEditor;


namespace Ore.Editor
{
  [CustomPropertyDrawer(typeof(TimeInterval))]
  internal sealed class TimeIntervalDrawer : PropertyDrawer
  {

    // It was not fine for m_Units to be shared among arrays.

    sealed class DrawerState : PropertyDrawerState
    {
      public SerializedProperty  Ticks;
      public SerializedProperty  AsFrames;
      public TimeInterval.Units? Units;

      protected override void OnUpdateProperty()
      {
        if (CheckFails(out var root))
          return;

        Ticks    = root.FindPropertyRelative(nameof(TimeInterval.Ticks));
        AsFrames = root.FindPropertyRelative("m_AsFrames");

        if (AsFrames.boolValue)
        {
          Units = TimeInterval.Units.Frames;
        }
        else if (!Units.HasValue || Units == TimeInterval.Units.Frames)
        {
          Units = TimeInterval.DetectUnits(Ticks.longValue);
        }
      }
    } // end nested class DrawerState


    public override void OnGUI(Rect total, SerializedProperty root, GUIContent label)
    {
      PropertyDrawerState.Restore(root, out DrawerState state);

      var pos = EditorGUI.PrefixLabel(total, label);

      OGUI.IndentLevel.Push(0);

      pos.width = (pos.width - 2f) / 2f;

      long edit = 0L;
      double d;

      EditorGUI.BeginChangeCheck();
      switch (state.Units)
      {
        case TimeInterval.Units.Frames:
        case TimeInterval.Units.Ticks:
          edit = EditorGUI.LongField(pos, state.Ticks.longValue);
          break;

        case TimeInterval.Units.Microseconds:
          d    = EditorGUI.DoubleField(pos, state.Ticks.longValue * TimeInterval.TICKS2US);
          edit = (long)(d * 10 + (d >= 0 ? 0.5 : -0.5));
          break;

        case TimeInterval.Units.Milliseconds:
          d    = EditorGUI.DoubleField(pos, state.Ticks.longValue * TimeInterval.TICKS2MS);
          edit = (long)(d / TimeInterval.TICKS2MS + (d >= 0 ? 0.5 : -0.5));
          break;

        case TimeInterval.Units.Seconds:
          d    = EditorGUI.DoubleField(pos, state.Ticks.longValue * TimeInterval.TICKS2SEC);
          edit = (long)(d / TimeInterval.TICKS2SEC + (d >= 0 ? 0.5 : -0.5));
          break;

        case TimeInterval.Units.Minutes:
          d    = EditorGUI.DoubleField(pos, state.Ticks.longValue * TimeInterval.TICKS2MIN);
          edit = (long)(d / TimeInterval.TICKS2MIN + (d >= 0 ? 0.5 : -0.5));
          break;

        case TimeInterval.Units.Hours:
          d    = EditorGUI.DoubleField(pos, state.Ticks.longValue * TimeInterval.TICKS2HR);
          edit = (long)(d / TimeInterval.TICKS2HR + (d >= 0 ? 0.5 : -0.5));
          break;

        case TimeInterval.Units.Days:
          d    = EditorGUI.DoubleField(pos, state.Ticks.longValue * TimeInterval.TICKS2DAY);
          edit = (long)(d / TimeInterval.TICKS2DAY + (d >= 0 ? 0.5 : -0.5));
          break;

        case TimeInterval.Units.Weeks:
          d    = EditorGUI.DoubleField(pos, state.Ticks.longValue * TimeInterval.TICKS2WK);
          edit = (long)(d / TimeInterval.TICKS2WK + (d >= 0 ? 0.5 : -0.5));
          break;
      }
      if (EditorGUI.EndChangeCheck())
      {
        state.Ticks.longValue = edit;
      }

      pos.x += pos.width + 2f;

      var prevUnit = state.Units;
      state.Units = (TimeInterval.Units)EditorGUI.EnumPopup(pos, state.Units);
      if (prevUnit != state.Units)
      {
        if (prevUnit == TimeInterval.Units.Frames)
        {
          state.Ticks.longValue = (state.Ticks.longValue * TimeInterval.SmoothTicksLastFrame).Rounded();
          state.AsFrames.boolValue = false;
        }
        else if (state.Units == TimeInterval.Units.Frames)
        {
          state.Ticks.longValue = (state.Ticks.longValue / TimeInterval.SmoothTicksLastFrame).Rounded();
          state.AsFrames.boolValue = true;
        }
      }

      OGUI.IndentLevel.Pop();
    }


    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
      return OGUI.STD_LINE_HEIGHT;
    }

  } // end class TimeIntervalDrawer
}
