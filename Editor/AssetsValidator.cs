/*! @file       Editor/AssetsValidator.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2022-06-06
**/

using System.Collections.Generic;
using System.Reflection;
using JetBrains.Annotations;
using UnityEngine;
using UnityEditor;


namespace Ore.Editor
{

  public sealed class AssetsValidator : UnityEditor.AssetModificationProcessor
  {

  #region     AssetModificationProcessor Messages

    static readonly Stack<bool> s_AssetValidationStack = new Stack<bool>();
      // fuck yeah, editor code = i don't care, use ALL the overkill data structures

    [PublicAPI] // the only one in this class
    public class NoValidateZone : System.IDisposable
    {
      bool m_Disposed;
      public NoValidateZone(bool negate = false)
      {
        s_AssetValidationStack.Push(true ^ negate);
      }
      public void Dispose()
      {
        if (m_Disposed)
          return;
        _ = s_AssetValidationStack.Pop();
        m_Disposed = true;
      }
    } // end nested class NoValidateZone


    static string[] OnWillSaveAssets(string[] paths)
    {
      foreach (string path in paths)
      {
        try
        {
          if (FixupTMProFontAsset(path))
            continue;

          // TODO any others?
        }
        catch (System.Exception ex)
        {
          Orator.NFE(ex);
        }
      }

      return paths;
    }

    static AssetMoveResult OnWillMoveAsset(string from, string to)
    {
      #if ORE_SHUT_UP
      return AssetMoveResult.DidNotMove;
      #else

      if (s_AssetValidationStack.Count > 0 && s_AssetValidationStack.Peek())
        return AssetMoveResult.DidNotMove;

      const string MsgSuffix = "\n  <color=#808080>(add 'ORE_SHUT_UP' to your scripting defines to, well, you know...)</color>";

      int flags = 0;

      if (Paths.ContainsComponent(from, "Resources"))
        flags |= 0b0001;
      if (Paths.ContainsComponent(to, "Resources"))
        flags |= 0b0010;
      if (Paths.ContainsComponent(from, "Editor"))
        flags |= 0b0100;
      if (Paths.ContainsComponent(to, "Editor"))
        flags |= 0b1000;

      switch (flags)
      {
        case 0b0000: // no op
        case 0b0011: // no op: resources -> resources
        case 0b0100: // no op: editor -> non-editor
        case 0b0101: // no op: editor resources -> non-editor non-resources
        case 0b1000: // no op: non-editor -> editor
        case 0b1100: // no op: editor -> editor
        case 0b1101: // no op: editor resources -> editor non-resources
        case 0b1111: // no op: editor resources -> editor resources
          break;

        case 0b0010: // non-resources -> resources
        case 0b0110: // editor non-resources -> resources
        case 0b0111: // editor resources -> resources
          Orator.Warn("ಠ▃ಠ YOU JusT MADE THJE /Resources/ FOLDER BIgGER >:|" + MsgSuffix);
          break;

        case 0b0001: // resources -> non-resources
        case 0b1001: // resources -> editor non-resources
          Orator.Log("Resources folder smoller now?? GOLD STAR 4 YOU ☜(ﾟヮﾟ☜)" + MsgSuffix);
          break;

        case 0b1010: // non-resources -> editor resources
        case 0b1011: // resources -> editor resources
        case 0b1110: // editor non-resources -> editor resources
          Orator.Log("/Editor/ + /Resources/ = noice choice :)" + MsgSuffix);
          break;
      }

      return AssetMoveResult.DidNotMove;
      #endif // ORE_SHUT_UP
    }

    static void OnWillCreateAsset(string path)
    {
      OnWillMoveAsset("", path);
    }

    static AssetDeleteResult OnWillDeleteAsset(string path, RemoveAssetOptions _ )
    {
      OnWillMoveAsset(path, "");
      return AssetDeleteResult.DidNotDelete;
    }

  #endregion  AssetModificationProcessor Messages


  #region     On Load + MenuItems

    [InitializeOnLoadMethod]
    static void ValidateOnLoad()
    {
      EditorApplication.delayCall += ValidateTypeAttributes;
      EditorApplication.delayCall += ValidatePreloadedAssets;
      EditorApplication.delayCall += ValidateGitConfig;
    }


    const string MENU_PREFIX = Consts.MenuItemPrefixOre + nameof(AssetsValidator) + "/";
    const int    MENU_PRIO   = Consts.MenuPriorityOre;


    [MenuItem(MENU_PREFIX + "Generate Missing OAssetSingletons + [AutoCreateAsset]", priority = MENU_PRIO)]
    static void ValidateTypeAttributes()
    {
      var silencers = new []
      {
        typeof(System.ObsoleteAttribute)
      };

      foreach (var tasset in TypeCache.GetTypesWithAttribute<AutoCreateAssetAttribute>())
      {
        if (tasset is null || tasset.IsAbstract || tasset.IsGenericType || tasset.AreAnyDefined(silencers))
          continue;

        if (!tasset.IsSubclassOf(typeof(ScriptableObject)))
        {
          Orator.Error($"[{nameof(AutoCreateAssetAttribute)}] is only intended for ScriptableObject types! (t:{tasset.Name})");
          continue;
        }

        var attr = tasset.GetCustomAttribute<AutoCreateAssetAttribute>();

        string filepath = attr.Path;

        if (filepath is null) // package user has squelched us
          continue;

        if (filepath.Length == 0)
          filepath = $"Assets/Resources/{tasset.Name}.asset";

        if (Filesystem.PathExists(filepath) || AssetTypeHasInstance(tasset))
          continue;

        ScriptableObject asset;
        using (new NoValidateZone())
        {
          if (!OAsset.TryCreate(tasset, out asset, filepath))
            continue;
        }

        if (attr.PreloadByDefault)
        {
          EditorBridge.TrySetPreloadedAsset(asset, set: true);
        }

        asset.hideFlags = attr.DefaultHideFlags;

        Orator.Log($"Created new Asset of type {tasset.NiceName()} at \"{filepath}\"", asset);
      } // end [AutoCreateAsset] loop

      silencers = new []
      {
        typeof(CreateAssetMenuAttribute),
        typeof(AutoCreateAssetAttribute), // we just checked these = skip
        typeof(System.ObsoleteAttribute),
      };

      foreach (var tsingleton in TypeCache.GetTypesDerivedFrom(typeof(OAssetSingleton<>)))
      {
        if (tsingleton is null || tsingleton.IsAbstract || tsingleton.IsGenericType || tsingleton.AreAnyDefined(silencers))
          continue;

        string filepath = $"Assets/Resources/{tsingleton.Name}.asset";

        if (Filesystem.PathExists(filepath) || AssetTypeHasInstance(tsingleton))
          continue;

        OAsset singleton;
        using (new NoValidateZone())
        {
          if (!OAsset.TryCreate(tsingleton, out singleton, filepath))
            continue;
        }

        Orator.Log($"Created new OAssetSingleton {tsingleton.NiceName()} at \"{filepath}\"", singleton);
      }
    }

    [MenuItem(MENU_PREFIX + "Validate Preloaded Asset Registration")]
    static void ValidatePreloadedAssets()
    {
      var preloaded = new List<Object>(PlayerSettings.GetPreloadedAssets());
      var set = new HashSet<Object>(preloaded);

      set.RemoveWhere(obj => !obj);

      int changed = preloaded.Count - set.Count;

      // add missing OAssetSingleton instances (that have "Is Required On Launch" [x])

      foreach (var tasset in TypeCache.GetTypesDerivedFrom(typeof(OAsset)))
      {
        if (tasset is null || tasset.IsAbstract || tasset.IsGenericType || tasset.IsDefined<System.ObsoleteAttribute>())
          continue;

        foreach (var instance in GetAssetInstances(tasset))
        {
          var asset = (OAsset)instance;
          if (asset.IsRequiredOnLaunch)
          {
            if (set.Add(asset))
            {
              // preloaded.Add(asset);
              // will get added by asset's OnValidate after being loaded
              ++ changed;
            }
          }
          else
          {
            set.Remove(asset);
            // (btw, changed is ++'d for this case later, 2 loops down)
          }
        }
      }

      // call ValidateOnReload for all Ore.Assets

      #if !UNITY_2020_1_OR_NEWER
      bool maybeChangedContents = false;
      #endif
      foreach (var asset in set)
      {
        if (!(asset is OAsset oreAsset))
          continue;

        try
        {
          oreAsset.ValidateOnReload();
          #if UNITY_2020_1_OR_NEWER
          AssetDatabase.SaveAssetIfDirty(oreAsset);
          #else
          maybeChangedContents = true;
          #endif
        }
        catch (System.Exception ex)
        {
          Orator.NFE(ex, oreAsset);
        }
      }

      #if !UNITY_2020_1_OR_NEWER
      if (maybeChangedContents)
      {
        AssetDatabase.SaveAssets();
      }
      #endif

      // Finally, update preloaded via set

      int i = preloaded.Count;
      while (i --> 0)
      {
        if (set.Contains(preloaded[i]))
        {
          set.Remove(preloaded[i]);
        }
        else
        {
          preloaded.RemoveAt(i);
          ++ changed;
        }
      }

      if (changed > 0)
      {
        PlayerSettings.SetPreloadedAssets(preloaded.ToArray());
        Orator.Log(typeof(AssetsValidator), $"Changed {changed} \"Preloaded Asset\" entries.");
      }
    }

    [MenuItem(MENU_PREFIX + "Validate configuration for Git")]
    static void ValidateGitConfig()
    {
      if (!GitStamp.IsGitControlledProject)
        return;

      // TODO move to settings window
      (string file, string content)[] mustExist =
      {
        ("Assets/GeneratedLocalRepo/.gitignore",   "*\n!.gitignore"),
        ("Assets/Resources/Generated/.gitignore",  "*\n!.gitignore"),
        ("Assets/Samples/.gitignore",              "*\n!.gitignore"),
      };

      AssetDatabase.StartAssetEditing();

      int created = 0;

      foreach (var (file,content) in mustExist)
      {
        if (Filesystem.PathExists(file))
          continue;

        if (Filesystem.TryWriteText(file, content))
        {
          ++ created;
          Orator.Log(typeof(AssetsValidator), $"Created default '{file}' - Note: You may customize its contents!");
        }
      }

      AssetDatabase.StopAssetEditing();

      if (created > 0)
      {
        using (new NoValidateZone())
        {
          foreach (var (file,_) in mustExist)
            AssetDatabase.ImportAsset(file);
        }
      }
    }

  #endregion  On Load + MenuItems


  #region     Utils

    static bool AssetTypeHasInstance(System.Type tasset)
    {
      using (var enumu = GetAssetInstances(tasset).GetEnumerator())
      {
        return enumu.MoveNext();
      }
    }

    static IEnumerable<Object> GetAssetInstances(System.Type tasset)
    {
      string[] guids = AssetDatabase.FindAssets("t:" + tasset.Name);

      if (guids.IsEmpty())
        yield break;

      foreach (string guid in guids)
      {
        var maybes = AssetDatabase.LoadAllAssetsAtPath(AssetDatabase.GUIDToAssetPath(guid));

        foreach (var maybe in maybes)
        {
          if (maybe.GetType() == tasset)
            yield return maybe;
        }
      }
    }

    static bool FixupTMProFontAsset(string path)
    {
      #if ORE_TMPRO
      // inspired by this lovely discussion:
      // https://forum.unity.com/threads/tmpro-dynamic-font-asset-constantly-changes-in-source-control.1227831/

      var tmpFont = AssetDatabase.LoadAssetAtPath<TMPro.TMP_FontAsset>(path);

      if (!tmpFont)
        return false;

      if (tmpFont.atlasPopulationMode == TMPro.AtlasPopulationMode.Dynamic)
      {
        tmpFont.ClearFontAssetData(setAtlasSizeToZero: true);
      }

      return true;
      #else
      return false;
      #endif // ORE_TMPRO
    }

  #endregion  Utils

  } // end static calss AssetsValidator

}
