/*! @file       Editor/PropertyDrawerState.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2022-06-15
 *
 *  @brief      A base class for tracking/caching states for
 *              non-trivial "Drawer" implementations.
**/

using UnityEngine;

using UnityEditor;


namespace Ore.Editor
{

  public abstract class PropertyDrawerState
  {

    #region INSTANCE

    public bool IsStale { get; protected set; } = true;


    protected abstract void OnUpdateProperty(); // <-- 1 required override

    protected bool CheckFails(out SerializedProperty rootProperty)
    {
      rootProperty = m_RootProp;
      return OAssert.Fails(!m_RootProp.IsDisposed());
    }


    void UpdateProperty(SerializedProperty rootProperty)
    {
      m_RootProp = rootProperty;
      OnUpdateProperty();
    }


    SerializedProperty m_RootProp;


    #endregion INSTANCE


    #region STATIC

    static readonly HashMap<uint,PropertyDrawerState> s_StateMap = new HashMap<uint,PropertyDrawerState>();

    static ActiveEditorTracker s_InspectorTracker;


    public static void Restore<TState>(SerializedProperty givenRoot, out TState state)
      where TState : PropertyDrawerState, new()
    {
      uint id = givenRoot.GetPropertyHash();
      if (id == 0)
      {
        state = new TState();
        state.UpdateProperty(givenRoot);
        return;
      }

      if (s_StateMap.TryGetValue(id, out var basestate))
      {
        state = (TState)basestate;

        if (state.m_RootProp.IsDisposed() || !SerializedProperty.EqualContents(state.m_RootProp, givenRoot))
        {
          state.m_RootProp.Dispose();
          state.UpdateProperty(givenRoot);
          state.IsStale = true;
        }

        return;
      }

      s_StateMap[id] = state = new TState();
      state.UpdateProperty(givenRoot);

      if (s_StateMap.Count == 1)
      {
        s_InspectorTracker = ActiveEditorTracker.sharedTracker;
        EditorApplication.update += TickStaleStates;
        EditorApplication.playModeStateChanged += ClearForPlaystate;
      }
    }

    static void TickStaleStates()
    {
      OAssert.NotNull(s_InspectorTracker);

      if (s_InspectorTracker.activeEditors.Length == 0 || s_StateMap.Count == 0)
      {
        foreach (var state in s_StateMap.Values)
          state.m_RootProp.Dispose();

        s_StateMap.Clear();

        EditorApplication.update -= TickStaleStates;
        EditorApplication.playModeStateChanged -= ClearForPlaystate;
      }
    }

    static void ClearForPlaystate(PlayModeStateChange change)
    {
      if (change == PlayModeStateChange.EnteredPlayMode)
      {
        foreach (var state in s_StateMap.Values)
        {
          state.m_RootProp.Dispose();
        }

        s_StateMap.Clear();
      }
    }

    #endregion STATIC

  } // end abstract class PropertyDrawerState

}
