/*! @file       Editor/GitSpy.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2023-09-29
**/

using UnityEngine;
using UnityEditor;
using UnityEditor.PackageManager;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;

using System.Collections.Generic;


namespace Ore.Editor
{
  internal struct GitSpy : IPreprocessBuildWithReport
  {
    int IOrderedCallback.callbackOrder => 0;

    [UnityEditor.Callbacks.DidReloadScripts]
    static void FirstRefresh()
    {
      RefreshAllIntel(force: false);
    }

    void IPreprocessBuildWithReport.OnPreprocessBuild(BuildReport _ )
    {
      RefreshAllIntel(force: true);
    }

    const string MENU_PREFIX = Consts.MenuItemPrefixOre + nameof(GitSpy) + "/";
    const int    MENU_PRIO   = Consts.MenuPriorityOre;

    [MenuItem(MENU_PREFIX + "Refresh All Intel", priority = MENU_PRIO)]
    static void RefreshAllIntel()
    {
      RefreshAllIntel(force: true);
    }

    static void RefreshAllIntel(bool force)
    {
      const string JsonFile = "Assets/Resources/" + GitStamp.JsonResource + ".json";

      if (!force && Filesystem.PathExists(JsonFile))
      {
        // don't spam devs lol
        return;
      }

      var intel = new Dictionary<string,GitStamp.RepoState>();
      RefreshTopLevel(intel);
      RefreshPackages(intel);

      // hacky but whatever
      var (author,email) = GitWhoAmI();
      intel["whoami"] = new GitStamp.RepoState
      {
        Path = author,
        URL  = email,
      };

      if (!force && GitStamp.DataEqual(intel))
      {
        return;
      }

      if (!Filesystem.TryWriteJson(JsonFile, intel))
      {
        Filesystem.LogLastException();
        return;
      }

      using (new AssetsValidator.NoValidateZone())
      {
        AssetDatabase.ImportAsset(JsonFile);
      }

      GitStamp.SetData(intel);
    }



    static void RefreshTopLevel(IDictionary<string,GitStamp.RepoState> intel)
    {
      var (hash,reff,url) = GitInfoBatch();

      intel[Application.identifier] = new GitStamp.RepoState
      {
        Path    = ".",
        URL     = url,
        Hash    = hash,
        RefName = reff,
        #if UNITY_ANDROID
        Version = $"{PlayerSettings.Android.bundleVersionCode}.aab",
        #elif UNITY_IOS
        Version = PlayerSettings.iOS.buildNumber,
        #else
        Version = $"{{\"versionName\":\"{Application.version}\"}}",
        #endif
      };
    }

    static void RefreshPackages(IDictionary<string,GitStamp.RepoState> intel)
    {
      var lsRequest = Client.List(offlineMode: true, includeIndirectDependencies: false);

      while (!lsRequest.IsCompleted)
      {
        if (EditorUtility.DisplayCancelableProgressBar(nameof(GitSpy), nameof(RefreshPackages), 0.5f))
          return;
      }

      EditorUtility.ClearProgressBar();

      if (lsRequest.Error != null)
      {
        Orator.Error(lsRequest.Error.message);
        return;
      }

      foreach (var pkg in lsRequest.Result)
      {
        if (pkg.name.IsEmpty())
          continue;

        switch (pkg.source)
        {
          case PackageSource.Git:
          {
            intel[pkg.name] = new GitStamp.RepoState
            {
              Path    = pkg.assetPath,
              #if UNITY_2020_1_OR_NEWER
              URL     = pkg.repository?.url?.Replace("git+http", "http"),
              #else
              URL     = pkg.packageId?.Substring(1 + pkg.packageId.IndexOf('@')),
              #endif
              Hash    = pkg.git?.hash,
              RefName = pkg.git?.revision,
              Version = pkg.version,
            };
            break;
          }

          case PackageSource.Embedded:
          {
            var (hash,reff,url) = GitInfoBatch(pkg.assetPath);
            intel[pkg.name] = new GitStamp.RepoState
            {
              Path    = pkg.assetPath,
              URL     = url,
              Hash    = hash,
              RefName = reff,
              Version = pkg.version,
            };
            break;
          }
        }
      }
    }


    static (string hash, string reff, string url) GitInfoBatch(string relPath = null)
    {
      const string cmd = "hash=$(git describe 2>/dev/null || git log -1 --format='%h');" +
                         "echo $hash;"                                                   +
                         "git name-rev --name-only $hash;"                               +
                         "git remote get-url origin";

      var result = Shell.RunBashOneshot(cmd, relPath);
      if (result.PrintFailure() || result.IsEmpty)
        return (null,null,null);

      var split = result.StdOut.Replace("\r", "").Split('\n');
      return (split[0],split[1],split[2]);
    }

    static (string name, string email) GitWhoAmI(string relPath = null)
    {
      const string cmd = "git config user.name;git config user.email";

      var result = Shell.RunBashOneshot(cmd, relPath);
      if (result.PrintFailure() || result.IsEmpty)
        return ("nameless","halp@bore.com");

      var split = result.StdOut.Replace("\r", "").Split('\n');
      return (split[0],split[1]);
    }

  } // end class GitSpy
}
