/*! @file       Editor/CoarseSamplerDrawer.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2023-04-20
**/

#if UNITY_2020_2_OR_NEWER

using UnityEngine;
using UnityEditor;

using System.Collections.Generic;

using Array = System.Array;


namespace Ore.Editor
{
  [CustomPropertyDrawer(typeof(CoarseSampler))]
  public class CoarseSamplerDrawer : PropertyDrawer
  {
    // cut -f1 Release.txt Development.txt | sort -u | grep -v ':' | awk 'BEGIN {i=0} {print "/* "i" */ \""$0"\"," ; i+=1}'
    static readonly string[] k_AllCats =
    {
      /*  0 */ "Ai",
      /*  1 */ "Animation",
      /*  2 */ "Audio",
      /*  3 */ "Audio Job",
      /*  4 */ "Audio Update Job",
      /*  5 */ "Director",
      /*  6 */ "FileIO",
      /*  7 */ "GC",
      /*  8 */ "Gui",
      /*  9 */ "Internal",
      /* 10 */ "Lighting",
      /* 11 */ "Loading",
      /* 12 */ "Memory",
      /* 13 */ "Network",
      /* 14 */ "Other",
      /* 15 */ "Overhead",
      /* 16 */ "Particles",
      /* 17 */ "Physics",
      /* 18 */ "PlayerLoop",
      /* 19 */ "Render",
      /* 20 */ "Scripts",
      /* 21 */ "UI Layout",
      /* 22 */ "UI Render",
      /* 23 */ "VFX",
      /* 24 */ "VSync",
      /* 25 */ "Video",
      /* 26 */ "Vr",
    };

    // git grep -h 'public const' -- 'Static/ProfilerStat.cs' | sed -E 's|^[ ]*public const string ([^ ]+) .+$|ProfilerStat.\1,|'
    static readonly (int cat, string stat)[] k_CatStats =
    {
      ( 7, ProfilerStat.GCCollect),
      ( 9, ProfilerStat.MainThread),
      (12, ProfilerStat.AudioReservedMemory),
      (12, ProfilerStat.AudioUsedMemory),
      (12, ProfilerStat.GCReservedMemory),
      (12, ProfilerStat.GCUsedMemory),
      (12, ProfilerStat.SystemUsedMemory),
      (12, ProfilerStat.TotalReservedMemory),
      (12, ProfilerStat.TotalUsedMemory),
      (12, ProfilerStat.AssetCount),
      (12, ProfilerStat.AudioClipCount),
      (12, ProfilerStat.AudioClipMemory),
      (12, ProfilerStat.GameObjectCount),
      (12, ProfilerStat.GCAllocated),
      (12, ProfilerStat.GCAllocations),
      (12, ProfilerStat.GCAllocTime),
      (12, ProfilerStat.SceneObjectCount),
      (12, ProfilerStat.TextureCount),
      (12, ProfilerStat.TextureMemory),
      (14, ProfilerStat.CoroutineCallTime),
      (14, ProfilerStat.Destroy),
      (14, ProfilerStat.Instantiate),
      (14, ProfilerStat.TransformChanged),
      (16, ProfilerStat.ParticleUpdate),
      (16, ProfilerStat.ParticleUpdate2),
      (16, ProfilerStat.ParticleSimulate),
      (16, ProfilerStat.ParticleSync),
      (17, ProfilerStat.Physics2DRaycast),
      (17, ProfilerStat.Physics2DRaycastAll),
      (17, ProfilerStat.Physics2DSimulate),
      (17, ProfilerStat.Physics2DStep),
      (17, ProfilerStat.Physics2DSyncTrans),
      (17, ProfilerStat.Physics2DUpdate),
      (18, ProfilerStat.ScriptsUpdate),
      (18, ProfilerStat.ScriptsFixedUpdate),
    };

    static readonly string[] s_Cats;

    const string EMPTY_CAT_LABEL  = "Select Profiler Category";
    const string EMPTY_STAT_LABEL = "(none)";

    static CoarseSamplerDrawer()
    {
      var catList = new List<string>(k_AllCats.Length + 1)
      {
        EMPTY_CAT_LABEL
      };

      int i = k_CatStats.Length, j = -1;
      while (i --> 0)
      {
        if (j == k_CatStats[i].cat)
          continue;

        j = k_CatStats[i].cat;

        catList.Add(k_AllCats[j]);
      }

      s_Cats = catList.ToArray();
    }


    string[] m_Stats;

    void PopulateStatsFor(int i)
    {
      var statList = new List<string>(k_CatStats.Length)
      {
        EMPTY_STAT_LABEL
      };

      if (i > 0)
        i = System.Array.IndexOf(k_AllCats, s_Cats[i]);

      int s = k_CatStats.Length;
      while (s --> 0)
      {
        int c = k_CatStats[s].cat;
        if (i > 0)
        {
          if (c > i)
            continue;
          if (c < i)
            break;
        }

        statList.Add(k_CatStats[s].stat);
      }

      m_Stats = statList.ToArray();
    }

    /* phew. ok that's over now. */


    public override void OnGUI(Rect total, SerializedProperty root, GUIContent label)
    {
      var propCat  = root.FindPropertyRelative(nameof(CoarseSampler.Category));
      var propStat = root.FindPropertyRelative(nameof(CoarseSampler.Stat));
      var propGrit = root.FindPropertyRelative(nameof(CoarseSampler.BatchSize));
      var propEnbl = root.FindPropertyRelative(nameof(CoarseSampler.EnabledIn));
      var propRest = root.FindPropertyRelative(nameof(CoarseSampler.RestInterval));

      if (root.IsArrayElement())
      {
        OAssert.True(Parsing.TryParseLastIndex(root.propertyPath, out int idx));
        if (propCat.stringValue.IsEmpty() && propStat.stringValue.IsEmpty())
        {
          label.text = $"[{idx}]: (unconfigured {nameof(CoarseSampler)})";
        }
        else
        {
          label.text = $"[{idx}]: {propCat.stringValue} / {propStat.stringValue}";
        }
      }

      var pos = new Rect(total)
      {
        height = OGUI.STD_LINE_HEIGHT
      };

      using (var header = FoldoutHeader.Open(pos, label, root))
      {
        if (!header.IsOpen)
          return;

        pos.y    += pos.height + 2f;
        pos.width = OGUI.LabelWidth.Peek();

        int i = Array.IndexOf(s_Cats, propCat.stringValue, startIndex: 1).AtLeast(0);

        int edit = EditorGUI.Popup(pos, i, s_Cats);

        if (edit != i || m_Stats is null)
        {
          PopulateStatsFor(edit);
          propCat.stringValue = (edit == 0) ? string.Empty : s_Cats[edit];
        }

        Debug.Assert(m_Stats != null, nameof(m_Stats));

        if (edit != i && !propStat.stringValue.IsEmpty())
        {
          propStat.stringValue = string.Empty;
          i = 0;
        }
        else
        {
          i = Array.IndexOf(m_Stats, propStat.stringValue, startIndex: 1).AtLeast(0);
        }

        pos.x += pos.width + 2f;
        pos.xMax = total.xMax;

        edit = EditorGUI.Popup(pos, i, m_Stats);

        if (edit != i)
        {
          string stat = propStat.stringValue = (edit == 0) ? string.Empty : m_Stats[edit];

          if (m_Stats.Length - 1 == k_CatStats.Length)
          {
            int s = k_CatStats.Length;
            while (s --> 0)
            {
              if (stat == k_CatStats[s].stat)
              {
                propCat.stringValue = k_AllCats[k_CatStats[s].cat];
                break;
              }
            }
          }
        }

        pos.x     = total.x;
        pos.width = total.width;
        pos.y    += pos.height + 2f;

        label.text = propGrit.displayName;
        EditorGUI.PropertyField(pos, propGrit, label);

        pos.y += pos.height + 2f;

        label.text = propEnbl.displayName;
        EditorGUI.PropertyField(pos, propEnbl, label);

        pos.y += pos.height + 2f;

        label.text = propRest.displayName;
        EditorGUI.PropertyField(pos, propRest, label);
      }
    }

    public override float GetPropertyHeight(SerializedProperty prop, GUIContent label)
    {
      return OGUI.STD_LINE_ADVANCE * (prop.isExpanded ? 5 : 1);
    }

  } // end class CoarseSamplerDrawer
}

#endif // UNITY_2020_2_OR_NEWER
