/*! @file       Attributes/AutoCreateAssetAttribute.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2022-08-06
**/

using JetBrains.Annotations;

using UnityEngine;


namespace Ore
{
  /// <summary>
  ///   Apply me to ScriptableObjects to supply them with a custom path to be
  ///   auto-created in.
  /// </summary>
  /// <remarks>
  ///   (1) 'Assets/' is already implied in the relative path.<br/>
  ///   (2) Please use forward slashes like a good lad.
  /// </remarks>
  [System.AttributeUsage(System.AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
  [System.Diagnostics.Conditional("UNITY_EDITOR")]
  public class AutoCreateAssetAttribute : System.Attribute
  {

    public readonly string Path;

    /// <summary>
    ///   Tell the Editor at compile-time that you want your auto-created asset
    ///   to be added to the "Preloaded Assets" list (in Player Settings) upon
    ///   generation.
    /// </summary>
    /// <remarks>
    ///   This is only applied at creation time; final value can be manually
    ///   changed later in the Inspector (but NOT at runtime).
    /// </remarks>
    [PublicAPI]
    public bool PreloadByDefault { get; set; } = false;

    /// <summary>
    ///   Tell the Editor at compile-time what kind of HideFlags you want your
    ///   auto-created asset to have upon generation.
    /// </summary>
    /// <remarks>
    ///   This is only at auto-creation time; value can be manually changed
    ///   later in the Inspector or at runtime (MyOAsset.Instance.hideFlags = x).
    /// </remarks>
    [PublicAPI]
    public HideFlags DefaultHideFlags { get; set; } = HideFlags.None;


    /// <param name="path">
    ///   Path relative to the Assets/ folder, e.g., "Resources/GoodBoy.asset"
    /// </param>
    [PublicAPI]
    public AutoCreateAssetAttribute([NotNull] string path)
    {
      #if UNITY_EDITOR
      OAssert.True(Paths.IsValidPath(path), $"invalid path: \"{path}\"");
      #endif

      Path = Paths.DetectAssetPathAssumptions(path);
    }

    /// <param name="doIt"> <br/>
    ///   <c>true</c> (default) = assume an asset path, constructed like so: <br/>
    ///   <c>"Assets/Resources/{typeName}.asset"</c> <br/><br/>
    ///   <c>false</c> = squelch (disable) any auto-instantiation logic for the
    ///   applied type.
    /// </param>
    [PublicAPI]
    public AutoCreateAssetAttribute(bool doIt = true)
    {
      Path = doIt ? string.Empty : null;
    }

  }
}