/*! @file       Runtime/HashMapParams.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2022-09-28
 *
 *  A parameter POD storing and modifying performance-critical HashMap
 *  configurations.
**/

using JetBrains.Annotations;

using UnityEngine;


namespace Ore
{
  [System.Serializable]
  public struct HashMapParams
  {

  #region Constants + Defaults

    const int USERCAPACITY_DEFAULT = 10;

    const int HASHPRIME_DEFAULT    = Hashing.DefaultHashPrime;

    const int INTERNALSIZE_MIN     = Primes.MinValue;
    const int INTERNALSIZE_MAX     = Primes.MaxSizePrime;

    const float LOADFACTOR_DEFAULT = 0.72f;
    const float LOADFACTOR_MIN     = 0.1f * LOADFACTOR_DEFAULT;
    const float LOADFACTOR_MAX     = 1f;    // danger

    const float GROWFACTOR_MIN     = 1.05f;
    const float GROWFACTOR_DEFAULT = 2f;

    const bool UNMAP_NULLS_DEFAULT = false;


    [PublicAPI]
    public static readonly HashMapParams Default = new HashMapParams(USERCAPACITY_DEFAULT);

  #endregion Constants + Defaults


  #region Properties + Fields

    public bool IsFixedSize => GrowFactor < GROWFACTOR_MIN;


    [SerializeField, Range(INTERNALSIZE_MIN, INTERNALSIZE_MAX)]
    public int MinSize;

    [SerializeField, HideInInspector]
    public int StoredSize;

    [SerializeField, Range(LOADFACTOR_MIN, LOADFACTOR_MAX)]
    public float LoadFactor;

    [SerializeField, Min(Primes.MinValue)]
    public int HashPrime;

    [SerializeField, Min(1f)]
    public float GrowFactor;

  #endregion Properties + Fields


  #region Constructors + Factory Funcs

    [PublicAPI]
    public static HashMapParams FixedCapacity(int userCapacity, float loadFactor = LOADFACTOR_DEFAULT)
    {
      return new HashMapParams(
        initialCapacity: userCapacity,
        isFixed:         true,
        loadFactor:      loadFactor,
        hashPrime:       HASHPRIME_DEFAULT);
    }

    /// <summary>
    /// You should only need to instantiate this struct if you're trying to play
    /// around with or optimize a particular HashMap instance.
    /// </summary>
    /// <param name="initialCapacity">
    /// Interpreted as initial "user" capacity, not physical capacity.
    /// </param>
    /// <param name="hashPrime">
    /// You should (probably) select a pre-designated hashprime, such as from
    /// the <see cref="Hashing"/> class. If a non-prime number is given, the
    /// nearest prime number will be used instead.
    /// </param>
    [PublicAPI]
    public HashMapParams(
      int      initialCapacity,
      float    loadFactor = LOADFACTOR_DEFAULT,
      bool     isFixed    = false,
      int      hashPrime  = HASHPRIME_DEFAULT)
    {
      LoadFactor = loadFactor.Clamp(LOADFACTOR_MIN, LOADFACTOR_MAX);

      HashPrime = Primes.NearestTo(hashPrime & int.MaxValue);

      StoredSize = MinSize = Primes.NextHashableSize((int)(initialCapacity / LoadFactor), HashPrime, 0);

      GrowFactor = isFixed ? 1f : GROWFACTOR_DEFAULT;
    }

    public HashMapParams(int hashPrime, bool isFixed, float loadFactor)
      : this(USERCAPACITY_DEFAULT, loadFactor, isFixed, hashPrime)
    {
    }

  #endregion Constructors + Factory Funcs


  #region Methods

    [Pure]
    public bool Check()
    {
      #if UNITY_EDITOR || DEBUG || UNITY_INCLUDE_TESTS
      // ReSharper is saying the next line of code is always true, but I disagree.
      // Just consider if this struct was default constructed, and MinSize = 0...
      return  (INTERNALSIZE_MIN <= MinSize && MinSize <= INTERNALSIZE_MAX)   &&
              (LOADFACTOR_MIN <= LoadFactor && LoadFactor <= LOADFACTOR_MAX) &&
              (HashPrime == HASHPRIME_DEFAULT || Primes.IsPrime(HashPrime))  &&
              (MinSize == 7 || Primes.IsPrime(MinSize)                       &&
              (GrowFactor >= 1f));
      #else
      return MinSize > 0 && HashPrime > Primes.MinValue && GrowFactor >= 1f;
      #endif
    }


    public int StoreLoadLimit(int loadLimit)
    {
      return StoredSize = CalcInternalSize(loadLimit);
    }

    public int StoreInternalSize(int internalSize)
    {
      return StoredSize = internalSize.AtLeast(MinSize);
    }

    public int ResetStoredSize()
    {
      return StoredSize = MinSize;
    }


    [Pure]
    public int CalcInternalSize(int loadLimit)
    {
      return Primes.NextHashableSize((int)(loadLimit / LoadFactor), HashPrime, 0).AtLeast(MinSize);
    }

    [Pure]
    public int CalcLoadLimit(int internalSize) // "load limit" == User Capacity,
    {
      // without the rounding, we get wonky EnsureCapacity behavior
      return (int)(internalSize * LoadFactor + 0.5f);
    }

    [Pure]
    public int CalcLoadLimit()
    {
      return (int)(StoredSize * LoadFactor + 0.5f);
    }

    [Pure]
    public int CalcJump(int hash31, int size)
    {
      return 1 + ((hash31 * HashPrime) & int.MaxValue) % (size - 1);
    }

    [Pure]
    public int CalcNextSize(int prevSize, int maxSize = Primes.MaxValue)
    {
      if (GrowFactor < GROWFACTOR_MIN)
        return prevSize;

      if ((int)(maxSize / GrowFactor) < prevSize)
        return maxSize;

      return Primes.NextHashableSize((int)(prevSize * GrowFactor), HashPrime);
    }

  #endregion Methods


    public static implicit operator HashMapParams (int initialCapacity)
    {
      return new HashMapParams(initialCapacity);
    }

  } // end class HashMapParams
}