/*! @file       Runtime/GreyList.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2022-08-18
**/

// TODO another candidate for the "Collections" namespace/package

// ReSharper disable HeapView.BoxingAllocation

using JetBrains.Annotations;

using UnityEngine;

using System.Collections;
using System.Collections.Generic;


namespace Ore
{
  [System.Serializable]
  public class StringGreyList : GreyList<string>
  {
    public StringGreyList(GreyListType type, IEnumerable<string> items = null)
      : base(type, items)
    {
    }
  }

  [System.Serializable]
  public class ObjectGreyList : GreyList<Object>
  {
    public ObjectGreyList(GreyListType type, IEnumerable<Object> items = null)
      : base(type, items)
    {
    }
  }

  // non-serializable
  public class BoxedGreyList : GreyList<object>
  {
    public BoxedGreyList(GreyListType type, IEnumerable<object> items = null)
      : base(type, items)
    {
    }
  }


  public enum GreyListType
  {
    NoFilter  = 0,
    Blacklist = 1,
    Whitelist = 2,
  }


  [System.Serializable]
  public class GreyList<T> : SerialSet<T>
  {
    [PublicAPI]
    public GreyListType Type
    {
      get => m_Type;
      set => m_Type = value;
    }

    [SerializeField]
    GreyListType m_Type;


    public GreyList(GreyListType type, IEnumerable<T> items = null) : base(items)
    {
      m_Type = type;
    }


    [Pure]
    public bool Accepts([CanBeNull] T item)
    {
      return m_Type == GreyListType.NoFilter || ((m_Type == GreyListType.Whitelist) == Contains(item));
    }

    [Pure]
    public bool Rejects([CanBeNull] T item)
    {
      return m_Type != GreyListType.NoFilter && ((m_Type == GreyListType.Blacklist) == Contains(item));
    }


    public void FlipType()
    {
      switch (m_Type)
      {
        case GreyListType.Blacklist:
          m_Type = GreyListType.Whitelist;
          break;
        case GreyListType.Whitelist:
          m_Type = GreyListType.Blacklist;
          break;
      }
    }

  } // end class GreyList<T>
}