using JetBrains.Annotations;

namespace Ore
{
  public class StringComparator : Comparator<string>
  {
    public StringComparator()
    {
      // TODO faster... FASTER
      m_Hasher = new EntropicHasher(UnityEngine.Random.state);
    }

    [Pure]
    public override int GetHashCode(in string str)
    {
      if (str.IsEmpty())
        return 0;

      // ReSharper disable once PossibleNullReferenceException

      #if ORE_UNSAFE
      unsafe
      {
        fixed (char* ptr = str)
          return m_Hasher.GetHashCode((byte*)ptr, str.Length * sizeof(char));
      }
      #else
      return str.GetHashCode(); // TODO m_Hasher useless without unsafe ctx
      #endif
    }


    readonly EntropicHasher m_Hasher;

  } // end class StringComparator
}