/*! @file       Runtime/EntropicHasher.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2023-05-08
**/

using JetBrains.Annotations;

using System.Collections.Generic;

using Random = UnityEngine.Random;


namespace Ore
{

  internal struct EntropicHasher : IEqualityComparer<string>
  {
    public EntropicHasher([NotNull] object state)
    {
      Lo = (uint)state.GetHashCode();
      Hi = Lo ^ 0xDEADFACE;
    }

    #if ORE_UNSAFE

    public EntropicHasher(Random.State state)
    {
      unsafe
      {
        uint* ptr = (uint*)(&state);
        Lo = ptr[0];
        Hi = ptr[2];
      }
    }

    [Pure]
    public unsafe int GetHashCode(byte* ptr, int len)
    {
      uint slo = Lo, shi = Hi;

      while (len >= 4)
      {
        // how about no-endian?
        Hi ^= Lo += *(uint*)ptr;
        RotLoHi();
        ptr += 4;
        len -= 4;
      }

      uint dword = 0x80;
      switch (len)
      {
        case 3:
          dword = (dword << 8) | ptr[2];
          goto case 2;
        case 2:
          dword = (dword << 8) | ptr[1];
          goto case 1;
        case 1:
          dword = (dword << 8) | ptr[0];
          break;
      }

      Hi ^= Lo += dword;
      RotLoHi();
      RotLoHi();

      dword = Lo ^ Hi;
      Lo = slo;
      Hi = shi;
      return (int)dword;
    }

    #endif // ORE_UNSAFE


    uint Lo;
    uint Hi;


    void RotLoHi()
    {
      // TODO this class is a no-op if no unsafe
      Lo = ((Lo << 20) | (Lo >> 12)) + Hi;
      Hi = ((Hi <<  9) | (Hi >> 23)) ^ Lo;
      Lo = ((Lo << 27) | (Lo >>  5)) + Hi;
      Hi =  (Hi << 19) | (Hi >> 13);
    }


    bool IEqualityComparer<string>.Equals(string a, string b)
    {
      return a == b; // TODO lazy impl.
    }

    int IEqualityComparer<string>.GetHashCode(string str)
    {
      if (str.IsEmpty())
        return 0;

      #if ORE_UNSAFE
      unsafe
      {
        fixed (char* ptr = str)
          return GetHashCode((byte*)ptr, str.Length * sizeof(char));
      }
      #else
      return str.GetHashCode();
      #endif
    }

  } // end struct EntropicHasher

}