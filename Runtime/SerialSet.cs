/*! @file       Runtime/SerialSet.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2022-08-18
 *
 *  It's like a HashSet<T>, but faster and serializable.
 *
 *  TODO I really want to delete this class...
 *  ... but it has a custom property drawer ...
**/

using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;


namespace Ore
{
  [System.Serializable]
  public class Int32Set : SerialSet<int>
  {
    public Int32Set() : base()
    {
    }
    public Int32Set([CanBeNull] IEnumerable<int> items) : base(items)
    {
    }
  }

  [System.Serializable]
  public class StringSet : SerialSet<string>
  {
    public StringSet() : base()
    {
    }
    public StringSet([CanBeNull] IEnumerable<string> items) : base(items)
    {
    }
  }

  [System.Serializable]
  public class ObjectSet : SerialSet<Object>
  {
    public ObjectSet() : base()
    {
    }
    public ObjectSet([CanBeNull] IEnumerable<Object> items) : base(items)
    {
    }
  }

  [System.Serializable]
  public class GameObjectSet : SerialSet<GameObject>
  {
    public GameObjectSet() : base()
    {
    }
    public GameObjectSet([CanBeNull] IEnumerable<GameObject> items) : base(items)
    {
    }
  }


  /// <summary>
  /// Serializable HashSet. The original plan was to implement a faster version,
  /// but now this is TODO.
  /// </summary>
  [System.Serializable]
  public class SerialSet<T> : ISet<T>, ISerializationCallbackReceiver
  {
    public int Count => m_Set.Count;

    bool ICollection<T>.IsReadOnly => false;


    [SerializeField]
    T[] m_Items = System.Array.Empty<T>();

    [System.NonSerialized]
    readonly HashSet<T> m_Set;
    [System.NonSerialized]
    bool m_Dirty;


    public SerialSet()
    {
      m_Set = new HashSet<T>();
    }

    public SerialSet([CanBeNull] IEnumerable<T> items)
    {
      m_Set = items is null ? new HashSet<T>() : new HashSet<T>(items);
    }


    public bool Add([CanBeNull] T item)
    {
      return m_Dirty |= m_Set.Add(item);
    }

    void ICollection<T>.Add(T item)
    {
      m_Dirty |= m_Set.Add(item);
    }

    public void Clear()
    {
      m_Set.Clear();
    }

    public bool Contains(T item)
    {
      return m_Set.Contains(item);
    }

    public void CopyTo(T[] array, int start)
    {
      m_Set.CopyTo(array, start);
    }

    public bool Remove(T item)
    {
      return m_Set.Remove(item);
    }

    public IEnumerator<T> GetEnumerator()
    {
      return m_Set.GetEnumerator();
    }
    IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

    public void ExceptWith(IEnumerable<T> other)
    {
      int precount = m_Set.Count;
      m_Set.ExceptWith(other);
      m_Dirty |= (m_Set.Count != precount);
    }

    public void IntersectWith(IEnumerable<T> other)
    {
      int precount = m_Set.Count;
      m_Set.IntersectWith(other);
      m_Dirty |= (m_Set.Count != precount);
    }

    public bool IsProperSubsetOf(IEnumerable<T> other)
    {
      return m_Set.IsProperSubsetOf(other);
    }

    public bool IsProperSupersetOf(IEnumerable<T> other)
    {
      return m_Set.IsProperSupersetOf(other);
    }

    public bool IsSubsetOf(IEnumerable<T> other)
    {
      return m_Set.IsSubsetOf(other);
    }

    public bool IsSupersetOf(IEnumerable<T> other)
    {
      return m_Set.IsSupersetOf(other);
    }

    public bool Overlaps(IEnumerable<T> other)
    {
      return m_Set.Overlaps(other);
    }

    public bool SetEquals(IEnumerable<T> other)
    {
      return m_Set.SetEquals(other);
    }

    public void SymmetricExceptWith(IEnumerable<T> other)
    {
      int precount = m_Set.Count;
      m_Set.SymmetricExceptWith(other);
      m_Dirty |= (m_Set.Count != precount);
    }

    public void UnionWith(IEnumerable<T> other)
    {
      int precount = m_Set.Count;
      m_Set.UnionWith(other);
      m_Dirty |= (m_Set.Count != precount);
    }


    public void TrimSerialList()
    {
      m_Set.Clear();
      m_Set.UnionWith(m_Items);
      m_Items = new T[m_Set.Count];
      m_Set.CopyTo(m_Items);
      m_Dirty = false;
    }

    void ISerializationCallbackReceiver.OnAfterDeserialize()
    {
      m_Set.Clear();
      m_Set.UnionWith(m_Items);
      m_Dirty = false;
    }

    void ISerializationCallbackReceiver.OnBeforeSerialize()
    {
      if (!m_Dirty)
        return;

      m_Items = new T[m_Set.Count];
      m_Set.CopyTo(m_Items);
      m_Dirty = false;
    }

  } // end class SerialSet
}