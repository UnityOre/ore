/*! @file       Runtime/CodeJudge.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2023-04-11
**/

using UnityEngine;

using JetBrains.Annotations;

using System.Collections.Generic;

using Stopwatch = System.Diagnostics.Stopwatch;


namespace Ore
{
  [PublicAPI]
  public struct CodeJudge : System.IDisposable
  {
    /// <param name="identifier">
    ///   Name your code segment or test to be judged.
    /// </param>
    /// <param name="increment">
    ///   Increase to account for internal call counts, potentially to balance
    ///   tests. <br/>
    ///   You can also pass <c>0u</c> to append timer metrics to an identifier
    ///   without increasing its existing sample count, which might have
    ///   otherwise skewed results.
    /// </param>
    public CodeJudge([NotNull] string identifier, uint increment = 1u)
    {
      m_Identifier = identifier;
      m_Increment  = increment;
      m_StartBytes = GetBytes(collect: true);
      m_StartTicks = GetTicks();
    }

    /// <summary>
    ///   Kept public in case you need to call it yourself.
    /// </summary>
    public void Dispose()
    {
      if (m_Identifier is null)
        return;

      long elapsed = GetTicks() - m_StartTicks;
      if (elapsed <= 1)
      {
        #if DEBUG
        Orator.Warn<CodeJudge>($"identifier \"{m_Identifier}\" elapsed {elapsed} ticks - Is it an empty scope?");
        #endif
      }

      long bytes = GetBytes(collect: false) - m_StartBytes;

      ref var kase = ref s_AllCases.FindRef(m_Identifier, out bool found);

      if (found)
      {
        double deviatn = kase.Count == 0 ? 0 : kase.Time / kase.Count - elapsed;
        kase.Time       += elapsed;
        kase.Count      += m_Increment;
        kase.Deviations += deviatn * deviatn * (deviatn < 0 ? -1 : 1);
        kase.Bytes      += bytes;
      }
      else
      {
        s_AllCases.Map(m_Identifier, new Case
        {
          Time       = elapsed,
          Count      = m_Increment,
          Bytes      = bytes,
          Deviations = 0,
          OnGUILine  = -1
        });
      }

      m_Identifier = null;
    }


    string        m_Identifier;
    readonly uint m_Increment;
    readonly long m_StartBytes;
    readonly long m_StartTicks;


    //
    // statics
    //

    public static long GetCount([NotNull] string identifier)
    {
      _ = s_AllCases.Find(identifier, out Case kase);
      return kase.Count;
    }

    public static void Report([NotNull] string identifier, out TimeInterval totalTime,
                                                           out long callCount)
    {
      _ = s_AllCases.Find(identifier, out var kase);

      totalTime = new TimeInterval((long)(kase.Time + 0.5));
      callCount = kase.Count;
    }

    public static void Report([NotNull] string identifier, out long totalTime,
                                                           out long callCount,
                                                           out long bytes,
                                                           out long avgTime,
                                                           out long variance,
                                                           out int  guiLine)
    {
      totalTime =  0;
      callCount =  0;
      bytes     =  0;
      avgTime   =  0;
      variance  =  0;
      guiLine   = -1;

      if (!s_AllCases.Find(identifier, out var kase))
        return;

      totalTime = kase.Time;
      callCount = kase.Count;
      bytes     = kase.Bytes;

      if (callCount > 0)
      {
        avgTime  = (long)((double)kase.Time  / callCount + 0.5);
        variance = (kase.Deviations / kase.Time).Rounded();
      }
      else
      {
        avgTime  = kase.Time;
        variance = kase.Deviations.Rounded();
      }

      guiLine = kase.OnGUILine;
    }

    public static IDictionary<string,object> Report(string identifier)
    {
      Report(identifier, out long totalTime,
                         out long callCount,
                         out long bytes,
                         out long avgTime,
                         out long variance,
                         out int  guiLine);

      var stdev = new TimeInterval((long)(System.Math.Sqrt(variance.Abs()) + 0.5));

      return new HashMap<string,object>(13)
      {
        [nameof(identifier)] = identifier,
        [nameof(totalTime)]  = new TimeInterval(totalTime),
        [nameof(callCount)]  = callCount,
        [nameof(bytes)]      = bytes,
        [nameof(avgTime)]    = new TimeInterval(avgTime),
        [nameof(variance)]   = new TimeInterval(variance),
        [nameof(stdev)]      = stdev,
        [nameof(guiLine)]    = guiLine, // idk why you'd need guiLine, but w/e
      };
    }

    public static void ReportJson([NotNull] string identifier, out string json, TimeInterval.Units units = TimeInterval.Units.Microseconds, string fmt = "0.#")
    {
      // keeping this custom, non-JsonAuthority-using code to continue supporting
      // the optional parameters

      Report(identifier, out long ticks, out long callCount, out long bytes, out long avg, out long varp, out _ );

      string nl = JsonAuthority.PrettyPrint ? "\n  " : "";

      using (new RecycledStringBuilder(out var bob))
      {
        bob.Append('{').Append(nl);

        bob.Append("\"identifier\": \"").Append(identifier).Append("\",").Append(nl);

        bob.Append("\"callCount\": ").Append(callCount.ToInvariant()).Append(',').Append(nl);

        var ti = new TimeInterval(ticks);
        bob.Append("\"totalTime\": \"").Append(ti.ToString(units, fmt)).Append("\",").Append(nl);

        ti.Ticks = avg;
        bob.Append("\"average\": \"").Append(ti.ToString(units, fmt)).Append("\",").Append(nl);

        ti.Ticks = varp;
        bob.Append("\"variance\": \"").Append(ti.ToString(units, fmt)).Append("\",").Append(nl);

        ti.Ticks = (long)(System.Math.Sqrt(varp.Abs()) + 0.5);
        bob.Append("\"stdev\": \"").Append(ti.ToString(units, fmt)).Append("\"").Append(nl);

        bob.Append("\"bytes\": ").Append(bytes.ToInvariant()).Append(nl);

        bob.Append('}');

        json = bob.ToString();
      }
    }

    public static void ReportOnGUI([NotNull] string identifier, TimeInterval.Units units = TimeInterval.Units.Microseconds, string fmt = "0.#")
    {
      Report(identifier, out long ticks, out long callCount, out long bytes, out long avg, out long varp, out int line);

      if (line < 0)
      {
        ref var kase = ref s_AllCases.FindRef(identifier, out bool found);
        if (!found)
          return;

        kase.OnGUILine = line = s_OnGUILine++;
      }

      var total = new TimeInterval(ticks);
      var mean  = new TimeInterval(avg);
      var stdev = new TimeInterval((long)(System.Math.Sqrt(varp.Abs()) + 0.5));

      var label = MeGUI.Scratch;
      using (new RecycledStringBuilder(identifier, out var bob))
      {
        bob.Append(": count=").Append(callCount.ToInvariant());
        bob.Append(", total=").Append(total.ToString(units, fmt));
        bob.Append(", mean=" ).Append(mean.ToString(units, fmt));
        bob.Append(", stdev=").Append(stdev.ToString(units, fmt));
        bob.Append(", bytes=").Append(Strings.FormatBytesKibi(bytes));
        label.text = bob.ToString();
      }

      var style = MeGUI.TextStyle;

      if (line == 0)
      {
        s_GUIScale = MeGUI.CalcContentScale(label, style);
      }

      const float X = 5f, Y = 5f, H = 22f;

      var pos = new Rect(x: X,
                         y: Y + line * (H + 2f),
                     width: Screen.width - 2 * X,
                    height: H);

      var pivot = new Vector2(X, Y);

      GUI.matrix = Matrix4x4.identity;
      GUIUtility.ScaleAroundPivot(s_GUIScale, pivot);

      GUI.Label(pos, label, style);
    }

    public static void Reset([NotNull] string identifier)
    {
      ref var kase = ref s_AllCases.FindRef(identifier, out bool found);
      if (found)
      {
        kase.Time       = 0;
        kase.Count      = 0;
        kase.Deviations = 0;
        kase.Bytes      = 0;
      }
    }

    public static void Remove([NotNull] string identifier)
    {
      if (s_AllCases.Pop(identifier, out Case pop) && pop.OnGUILine > -1)
      {
        using (var iter = s_AllCases.GetEnumerator())
        {
          while (iter.MoveNext())
          {
            iter.CurrentValueRef.OnGUILine = -1;
          }
        }

        s_OnGUILine = 0;
      }
    }

    public static void RemoveAll()
    {
      s_AllCases.Clear();
      s_OnGUILine = 0;
    }


    //
    // private statics
    //


    static long GetBytes(bool collect)
    {
      #if !DEBUG

      return 0;

      #elif CODEJUDGE_COLLECT_GC

      if (collect)
        System.GC.Collect();

      // this is OK since the C# code we judgin will likely be allocating mono heap
      return UnityEngine.Profiling.Profiler.GetMonoHeapSizeLong();

      #else

      return DeviceSpy.CalcRAMUsageBytes(); // TODO not yet accurate

      #endif
    }

    static long GetTicks()
    {
      return s_Stopwatch.Elapsed.Ticks.AtLeast(1);
    }


    struct Case
    {
      public long   Time;
      public long   Count;
      public long   Bytes;
      public double Deviations;
      public int    OnGUILine;
    }

    static readonly Stopwatch s_Stopwatch = Stopwatch.StartNew(); // doesn't matter to us when this happens

    static readonly HashMap<string,Case> s_AllCases = new HashMap<string,Case>();

    static int     s_OnGUILine;
    static Vector2 s_GUIScale = Vector2.one;

  } // end class CodeJudge
}