/*! @file       Runtime/ThingEvent.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2023-05-04
**/

using JetBrains.Annotations;

using UnityEngine;
using UnityEngine.Events;

using Scene = UnityEngine.SceneManagement.Scene;


namespace Ore
{

  [System.Serializable]
  public class SceneEvent : ThingEvent<Scene>
  {
    /// <inheritdoc cref="ThingEvent{T}(bool)"/>
    public SceneEvent()
      : base(isEnabled: false)
    {
    }

    /// <inheritdoc cref="ThingEvent{T}(bool)"/>
    public SceneEvent(bool isEnabled)
      : base(isEnabled)
    {
    }
  } // end class SceneEvent

  [System.Serializable]
  public class ThingEvent : ThingEvent<object>
  {
    /// <inheritdoc cref="ThingEvent{T}(bool)"/>
    public ThingEvent()
      : base(isEnabled: false)
    {
    }

    /// <inheritdoc cref="ThingEvent{T}(bool)"/>
    public ThingEvent(bool isEnabled)
      : base(isEnabled)
    {
    }
  } // end class ThingEvent


  [System.Serializable]
  public class ThingEvent<T> : UnityEvent<T>, IEnableable
  {
    public ThingEvent()
    {
    }

    /// <param name="isEnabled">
    ///   Whether or not the event should start enabled (making it invokable). <br/>
    ///   False by default, for base use case optimization.
    /// </param>
    public ThingEvent(bool isEnabled)
    {
      m_IsEnabled = isEnabled;
    }

    /// <param name="listener">
    ///   A runtime delegate to register with the event.
    /// </param>
    /// <param name="isEnabled">
    ///   Whether or not the event should start enabled (making it invokable). <br/>
    ///   This overload has it <c>true</c> by default, since we can assume our
    ///   listener is probably ready to roll at construction time.
    /// </param>
    public ThingEvent([CanBeNull] UnityAction<T> listener, bool isEnabled = true)
    {
      m_IsEnabled = isEnabled;

      if (listener != null)
      {
        AddListener(listener);
      }
    }

    /// <inheritdoc cref="ThingEvent{T}(UnityAction{T},bool)"/>
    public ThingEvent([CanBeNull] UnityAction listener, bool isEnabled = true)
    {
      m_IsEnabled = isEnabled;

      m_VoidListeners += listener;
    }


    public bool IsEnabled
    {
      get => m_IsEnabled;
      set => m_IsEnabled = value;
    }


    [SerializeField, HideInInspector] // handled by custom drawer
    protected bool m_IsEnabled;


    event UnityAction m_VoidListeners;


    public void AddListener([NotNull] UnityAction listener)
    {
      m_VoidListeners += listener;
    }

    public void RemoveListener([NotNull] UnityAction listener)
    {
      m_VoidListeners -= listener;
    }


    public new void Invoke(T thing = default)
    {
      if (!m_IsEnabled)
        return;

      base.Invoke(thing);

      if (m_VoidListeners is null)
        return;

      var targ = m_VoidListeners.Target;

      if (targ is null || !(targ is Object utarg) || utarg)
        m_VoidListeners();
    }

    public bool TryInvoke()
    {
      if (!m_IsEnabled)
        return false;

      try
      {
        Invoke(default(T));
      }
      catch (System.Exception ex)
      {
        Orator.NFE(ex);
        return false;
      }

      return true;
    }


    [NotNull]
    public static ThingEvent<T> operator + ([CanBeNull] ThingEvent<T> lhs,
                                            [CanBeNull] UnityAction   rhs)
    {
      if (lhs is null)
        return new ThingEvent<T>(rhs);

      if (rhs != null)
        lhs.AddListener(rhs);

      return lhs;
    }

    [NotNull]
    public static ThingEvent<T> operator + ([CanBeNull] ThingEvent<T>  lhs,
                                            [CanBeNull] UnityAction<T> rhs)
    {
      if (lhs is null)
        return new ThingEvent<T>(rhs);

      if (rhs != null)
        lhs.AddListener(rhs);

      return lhs;
    }

    [CanBeNull]
    public static ThingEvent<T> operator - ([CanBeNull] ThingEvent<T> lhs,
                                            [CanBeNull] UnityAction   rhs)
    {
      if (lhs != null && rhs != null)
      {
        lhs.RemoveListener(rhs);
      }

      return lhs;
    }

    [CanBeNull]
    public static ThingEvent<T> operator - ([CanBeNull] ThingEvent<T>  lhs,
                                            [CanBeNull] UnityAction<T> rhs)
    {
      if (lhs != null && rhs != null)
      {
        lhs.RemoveListener(rhs);
      }

      return lhs;
    }

  } // end class ThingEvent<T>
}