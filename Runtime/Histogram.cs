/*! @file       Runtime/Histogram.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2023-05-10
**/

using JetBrains.Annotations;

using UnityEngine;

using System.Collections.Generic;

using Stopwatch = System.Diagnostics.Stopwatch;


namespace Ore
{
  public class Histogram
  {
    [System.Flags]
    public enum Draw
    {
      None        = 0b00000000,
      Background  = 0b00000001,
      MeanGraph   = 0b00000010,
      MeanLabel   = 0b00000100,
      StdDevGraph = 0b00001000,
      StdDevLabel = 0b00010000,
      CeilLabel   = 0b00100000,
      FloorLabel  = 0b01000000,
      Everything  = 0b11111111,

      Mean   = MeanGraph   | MeanLabel,
      StdDev = StdDevGraph | StdDevLabel,
      Graph  = MeanGraph   | StdDevGraph,
      Label  = MeanLabel   | StdDevLabel,

      RangeLabels = CeilLabel | FloorLabel,

      Default = Background | Mean | RangeLabels,
    }

    public Histogram()
      : this(samplePeriod: TimeInterval.OfMillis(500))
    {
    }

    public Histogram(TimeInterval samplePeriod, int maxSamples = 60)
    {
      m_Period       = samplePeriod.AtLeast(TimeInterval.Frame);
      m_SampleBucket = new List<float>();
      m_Samples      = new Sample[maxSamples];
    }


    public int MaxSampleCount
    {
      get => m_Samples.Length;
      set
      {
        if (m_Samples.Length == value)
          return;

        OAssert.True(value > 0, "positive values only <- Histogram.MaxSampleCount");

        var newArr = new Sample[value];
        System.Array.Copy(m_Samples, newArr, value.AtMost(m_Samples.Length));
        m_Samples = newArr;
        m_Length = m_Length.AtMost(value);
        m_Next %= value;
      }
    }

    public int SampleCount => m_Length;

    public TimeInterval SamplePeriod
    {
      get => m_Period;
      set => m_Period = value;
    }

    public float Ceiling
    {
      get => m_Ceiling;
      set
      {
        if (m_Ceiling < m_Floor)
          m_Floor = value - (m_Ceiling - m_Floor).Abs();
        m_Ceiling = value;
      }
    }

    public float Floor
    {
      get => m_Floor;
      set
      {
        if (value > m_Ceiling)
          m_Ceiling = value + (m_Ceiling - m_Floor).Abs();
        m_Floor = value;
      }
    }

    public float LastSample { get; private set; }


    [PublicAPI]
    public int AggregateSamples(out float mean, out float stdev, out float min, out float max)
    {
      mean  = 0f;
      stdev = 0f;
      min   = float.MaxValue;
      max   = float.MinValue;

      if (m_Length == 0)
        return 0;

      int n = m_Length;
      int i = (m_Next - 1 - n).WrapIndex(m_Samples.Length);
      while (n --> 0)
      {
        var sample = m_Samples[i];

        if (sample.Mean < min)
          min = sample.Mean;
        if (sample.Mean > max)
          max = sample.Mean;

        mean  += sample.Mean;
        stdev += sample.StdDev * sample.StdDev;

        i = (i + 1) % m_Samples.Length;
      }

      mean /= m_Length;
      stdev = Mathf.Sqrt(stdev / m_Length);

      return m_Length;
    }


    [PublicAPI]
    public void SampleValue(float value)
    {
      if (!ShouldAggregate(value))
        return;

      if (m_SampleBucket.Count > 0)
      {
        m_Samples[m_Next] = new Sample(m_SampleBucket);
        m_SampleBucket.Clear();
        m_SampleBucket.Add(value);
      }
      else if (m_Length > 0)
      {
        var prev = m_Samples[(m_Next - 1).WrapIndex(m_Samples.Length)];
        m_Samples[m_Next] = new Sample(value, prev);

        if (prev.StdDev > m_MaxStdDev)
          m_MaxStdDev = prev.StdDev;
        else if (prev.StdDev < m_MinStdDev)
          m_MinStdDev = prev.StdDev;
      }
      else
      {
        m_Samples[m_Next] = new Sample(value);
      }

      LastSample = m_Samples[m_Next].Mean;

      ++ m_Next;

      m_Next %= m_Samples.Length;

      if (m_Length < m_Samples.Length)
        ++ m_Length;
    }

    [PublicAPI]
    public void Clear()
    {
      m_SampleBucket.Clear();
      m_LastSampledAt = 0L;
      m_Next = 0;
      m_Length = 0;
    }

    [PublicAPI]
    public void RecalulateHeight(int skipFirst = 1)
    {
      m_MinStdDev = m_Ceiling;
      m_MaxStdDev = 1f;

      if (m_Length <= skipFirst)
        return;

      float mean  = 0f;
      float devis = 0f;
      float min   = float.MaxValue;
      float max   = float.MinValue;

      int i = (m_Next - m_Length + skipFirst).WrapIndex(m_Samples.Length);
      int n = m_Length - skipFirst;
      while (n --> 0)
      {
        var sample = m_Samples[i];

        if (sample.Mean < min)
          min = sample.Mean;
        else if (sample.Mean > max)
          max = sample.Mean;

        mean  += sample.Mean;
        devis += sample.StdDev;

        if (sample.StdDev > m_MaxStdDev)
          m_MaxStdDev = sample.StdDev;
        else if (sample.StdDev < m_MinStdDev)
          m_MinStdDev = sample.StdDev;

        i = (i + 1) % m_Samples.Length;
      }

      n = m_Length - skipFirst;

      mean  /= n;
      devis /= n;

      if (devis > 2f)
      {
        min = min.AtLeast(mean - 3f * devis);
        max = max.AtMost( mean + 3f * devis);
      }

      float pad = max - min;

      if (pad < 1.5f)
      {
        m_Floor   = mean - 1f;
        m_Ceiling = mean + 1f;
      }
      else if (min < m_Floor || m_Ceiling < max || m_Floor < min - devis || max + devis < m_Ceiling)
      {
        m_Floor   = mean - 0.6f * pad;
        m_Ceiling = mean + 0.6f * pad;
      }

      if (LastSample < m_Floor)
        m_Floor = LastSample - (1f + devis);
      else if (LastSample > m_Ceiling)
        m_Ceiling = LastSample + (1f + devis);
    }


    [PublicAPI]
    public void DrawOnGUI(Rect screenRect, Draw flags = Draw.Default)
    {
      DrawOnGUI(screenRect, line: Colors.Green, background: Colors.Background, flags);
    }

    [PublicAPI]
    public void DrawOnGUI(Rect screenRect, Color32 line, Draw flags = Draw.Default)
    {
      DrawOnGUI(screenRect, line, background: Colors.Background, flags);
    }

    [PublicAPI]
    public void DrawOnGUI(Rect screenRect, Color32 line, Color32 background, Draw flags = Draw.Default)
    {
      // TODO split flag combinations out into delineated switch cases,
      // since flags seem to change each others behaviour.

      const float LABELWIDTH = 30f;

      if (flags == Draw.None || Event.current.type != EventType.Repaint)
        return;

      if ((flags & Draw.Background) == Draw.Background)
      {
        MeGUI.DrawBox(screenRect,
                        fill: background.AlphaWash(),
                     outline: background.WithValue(line.Value()));
        screenRect.x += 1f;
        screenRect.y += 1f;
        screenRect.width -= 2f;
        screenRect.height -= 2f;
      }

      if (m_Length == 0) // used to check for line.IsClear() too, but if you wanna arg-disable just pass Draw.None~
        return;

      if ((flags & Draw.Label) != 0)
      {
        screenRect.width -= LABELWIDTH;

        // var (a, b) = AABBs.GetEdge(screenRect, AABBs.Edge.Right);

        var b = screenRect.max;
        var a = b;
        a.y = screenRect.yMin;

        MeGUI.DrawLine(a, b, line.Alpha(0.3f));
      }

      var restoreColor = GUI.color;
      var style = MeGUI.TextStyle;
      var label = MeGUI.Scratch;

      GUI.color = line.Alpha(0.5f); // for floor/ceil

      // TODO scale down floor/ceil labels?

      if ((flags & Draw.CeilLabel) != 0)
      {
        var pos = new Rect(
          x:      screenRect.x + 2f,
          y:      screenRect.yMin + 2f,
          width:  LABELWIDTH,
          height: style.lineHeight
        );

        label.text = m_Ceiling.ToString("F0", Strings.InvariantFormatter);

        GUI.Label(pos, label, style);
      }

      if ((flags & Draw.FloorLabel) != 0)
      {
        var pos = new Rect(
          x:      screenRect.x + 2f,
          y:      screenRect.yMax,
          width:  LABELWIDTH,
          height: style.lineHeight
        );

        pos.y -= pos.height + 2f;

        label.text = m_Floor.ToString("F0", Strings.InvariantFormatter);

        GUI.Label(pos, label, style);
      }

      GUI.color = line;

      var popAlign = style.alignment;
      style.alignment = TextAnchor.MiddleCenter;

      var last = m_Samples[(m_Next - 1).WrapIndex(m_Samples.Length)];

      if ((flags & Draw.MeanLabel) != 0)
      {
        var pos = new Rect(
          x:      screenRect.x + screenRect.width + 2f,
          y:      screenRect.y + screenRect.height * 0.325f - 0.5f * style.lineHeight,
          width:  LABELWIDTH,
          height: 1f
        );

        if ((flags & Draw.Label) == Draw.MeanLabel)
        {
          pos.y += screenRect.height * 0.17f;
        }

        label.text = last.Mean < 10 ? last.Mean.ToString("0.#", Strings.InvariantFormatter) :
                                      last.Mean.ToString("F0",  Strings.InvariantFormatter);

        GUI.Label(pos, label, style);
      }

      if ((flags & Draw.MeanGraph) != 0)
      {
        MeGUI.BeginLineGL(line);
        DrawMeanLine(screenRect);
        MeGUI.EndGL();
      }

      if ((flags & Draw.Mean) != 0)
      {
        line = line.MultValue(0.55f);
      }

      if ((flags & Draw.StdDevLabel) != 0)
      {
        var pos = new Rect(
          x:      screenRect.x + screenRect.width + 2f,
          y:      screenRect.yMax - screenRect.height * 0.325f,
          width:  LABELWIDTH,
          height: 1f
        );

        if ((flags & Draw.Label) == Draw.StdDevLabel)
        {
          pos.y -= screenRect.height * 0.17f;
        }

        label.text = last.StdDev < 90 ? last.StdDev.ToString("F1", Strings.InvariantFormatter) :
                                        last.StdDev.ToString("F0", Strings.InvariantFormatter);

        GUI.color = line;
        GUI.Label(pos, label, style);
      }

      if ((flags & Draw.StdDevGraph) != 0)
      {
        MeGUI.BeginLineGL(line);
        DrawStdDevLine(screenRect);
        MeGUI.EndGL();
      }

      style.alignment = popAlign;
      GUI.color = restoreColor;
    }


    //
    // private methods

    void DrawMeanLine(Rect bounds)
    {
      float x     = bounds.xMax;
      float floor = bounds.yMax;
      float xIncr = -bounds.width / (m_Samples.Length - 1);
      float span  = (m_Ceiling - m_Floor).AtLeast(1f);

      int i = (m_Next - 1).WrapIndex(m_Samples.Length);
      int n = m_Length;
      while (n --> 0)
      {
        float y = floor - bounds.height * ((m_Samples[i].Mean - m_Floor) / span).Clamp01();

        GL.Vertex3(x, y, 0f);

        x += xIncr;

        i = (i - 1).WrapIndex(m_Samples.Length);
      }
    }

    void DrawStdDevLine(Rect bounds)
    {
      const float SCALE = 1.15f;

      int skipFirst = m_Length < m_Samples.Length ? 1 : 0;

      float x     = bounds.xMax;
      float floor = bounds.yMax;
      float xIncr = -bounds.width / (m_Samples.Length - 1);
      float span  = (m_MaxStdDev * SCALE).AtLeast(1f);
      float min   = (m_MinStdDev / SCALE).AtLeast(0f);

      span -= min;

      int i = (m_Next - 1).WrapIndex(m_Samples.Length);
      int n = m_Length - skipFirst;
      while (n --> 0)
      {
        float y = floor - bounds.height * ((m_Samples[i].StdDev - min) / span).Clamp01();

        GL.Vertex3(x, y, 0f);

        x += xIncr;

        i = (i - 1).WrapIndex(m_Samples.Length);
      }
    }

    bool ShouldAggregate(float sample)
    {
      long now = s_Clock.ElapsedTicks;

      if (m_Period.Ticks < 10)
      {
        m_LastSampledAt = now;
        return true;
      }

      if (m_LastSampledAt == 0)
      {
        m_LastSampledAt = now;
        m_SampleBucket.Add(sample);
        LastSample = sample;
        return false;
      }

      if (now - m_LastSampledAt < m_Period.Ticks)
      {
        m_SampleBucket.Add(sample);
        return false;
      }

      m_LastSampledAt = now;

      return true;
    }

    internal IEnumerable<(float mean, float stdev)> GetInternalSamples()
    {
      int i = (m_Next - 1).WrapIndex(m_Samples.Length);
      int n = m_Length;
      while (n --> 0)
      {
        var sample = m_Samples[i];

        yield return (sample.Mean, sample.StdDev);

        i = (i - 1).WrapIndex(m_Samples.Length);
      }
    }


    //
    // instance fields

    readonly List<float> m_SampleBucket;

    TimeInterval m_Period;
    float        m_Ceiling = 1f;
    float        m_Floor;
    float        m_MinStdDev, m_MaxStdDev;
    Sample[]     m_Samples;

    long m_LastSampledAt;
    int  m_Next;
    int  m_Length;


    //
    // static impl.
    //

    static readonly Stopwatch s_Clock = Stopwatch.StartNew();


    struct Sample
    {
      public readonly float Mean;
      public readonly float StdDev;

      public Sample(List<float> bucket)
      {
        Mean   = 0f;
        StdDev = 0f;

        int i = bucket.Count;
        while (i --> 0)
        {
          Mean += bucket[i];
        }

        i = bucket.Count;
        Mean /= i;

        while (i --> 0)
        {
          float devi = bucket[i] - Mean;
          StdDev += devi * devi;
        }

        if (StdDev.ApproximatelyZero())
        {
          StdDev = 0f;
        }
        else
        {
          StdDev = Mathf.Sqrt(StdDev / bucket.Count);
        }
      }

      public Sample(float value)
      {
        Mean   = value;
        StdDev = 0f;
      }

      public Sample(float value, Sample prev)
      {
        Mean   = value;
        StdDev = (value - prev.Mean).Abs();

        if (prev.StdDev > 0f)
        {
          StdDev = Mathf.Sqrt((StdDev * StdDev + prev.StdDev * prev.StdDev) / 2f);
        }
      }

    } // end nested struct Sample

  } // end class Histogram
}