/*! @file       Runtime/CoarseSampler.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2023-04-20
**/

using JetBrains.Annotations;

using UnityEngine;
using Unity.Profiling;

using System.Collections;


namespace Ore
{
  #if UNITY_2020_2_OR_NEWER

  [System.Serializable]
  internal sealed class CoarseSampler : IEnumerator
  {
    public enum EnableLevel
    {
      Disabled     = 0,
      Editor       = 1,
      DebugBuild   = 2,
      ReleaseBuild = 3,
    }

    [NotNull]
    public static CoarseSampler SystemRAM => new CoarseSampler("Memory", ProfilerStat.SystemUsedMemory)
    {
      EnabledIn = EnableLevel.ReleaseBuild
    };

    [NotNull]
    public static CoarseSampler ManagedRAM => new CoarseSampler("Memory", ProfilerStat.GCReservedMemory)
    {
    };

    [NotNull]
    public static CoarseSampler ManagedGCTime => new CoarseSampler("GC", ProfilerStat.GCCollect)
    {
      BatchSize = 60,
    };


    public CoarseSampler(string category, string stat)
    {
      Category = category;
      Stat     = stat;
    }


    [SerializeField]
    public string Category;
    [SerializeField]
    public string Stat;

    [SerializeField, Range(1, 144)]
    public int          BatchSize    = DEFAULT_BATCH_SIZE;
    [SerializeField]
    public TimeInterval RestInterval = TimeInterval.OfSeconds(DEFAULT_REST_SECONDS);

    [SerializeField]
    public EnableLevel EnabledIn = DEFAULT_ENABLE_LVL;


    const int         DEFAULT_BATCH_SIZE   = 3;
    const float       DEFAULT_REST_SECONDS = 5f;
    const EnableLevel DEFAULT_ENABLE_LVL   = EnableLevel.DebugBuild;


    public bool IsReady    => m_Min != 0 && m_Max != 0;
    public bool HasSamples => m_Count > 0;
    public bool IsSampling => m_Recorder.IsRunning;
    public bool IsResting  => IsReady && m_WaitForRest != null;

    public double LastSample => m_Last;
    public double Mean => m_Count > 0 ? m_Sum / m_Count : 0;


    [Pure] // so you don't accidentally throw away the return value
    public bool Setup()
    {
      const ProfilerRecorderOptions flags = ProfilerRecorderOptions.WrapAroundWhenCapacityReached |
                                            ProfilerRecorderOptions.StartImmediately;

      // ReSharper disable once ConditionIsAlwaysTrueOrFalse
      if (Category.IsEmpty() || Stat.IsEmpty() || BatchSize < 1 || ShouldSkip(EnabledIn))
        return false;

      m_Recorder = new ProfilerRecorder(Category, Stat, BatchSize, flags);
      m_Max = double.MinValue;
      m_Min = double.MaxValue;
      m_Sum = m_Last = m_Deviations = m_Count = 0;

      m_WaitForRest = (IEnumerator)RestInterval.Yield(scaledTime: false);
                                   // parameter guarantees IEnumerator or null

      return ((IEnumerator)this).MoveNext();
    }


    public void Report(out double max, out double min, out double mean, out double stdev, double div = 1)
    {
      if (m_Count == 0)
      {
        mean  = 0;
        stdev = 0;
      }
      else
      {
        mean  = m_Sum / m_Count / div;
        stdev = System.Math.Sqrt(m_Deviations / m_Count) / div;
      }

      if (m_Max <= 0)
      {
        max = min = mean / div;
      }
      else
      {
        max = m_Max / div;
        min = m_Min / div;
      }
    }


    public void Dispose()
    {
      m_Recorder.Dispose();
      m_Sum = m_Last = m_Max = m_Min = m_Deviations = m_Count = 0;
    }


    //
    // privates
    //


    static bool ShouldSkip(EnableLevel lvl)
    {
      if (Application.isEditor)
        return lvl < EnableLevel.Editor;

      if (Debug.isDebugBuild)
        return lvl < EnableLevel.DebugBuild;

      return lvl < EnableLevel.ReleaseBuild;
    }


    [System.NonSerialized]
    ProfilerRecorder m_Recorder;

    [System.NonSerialized]
    double m_Sum, m_Last;
    [System.NonSerialized]
    double m_Deviations, m_Max, m_Min; // TODO remove these?
    [System.NonSerialized]
    long   m_Count;

    [System.NonSerialized]
    IEnumerator m_WaitForRest;


    void ProcessSamples()
    {
      if (BatchSize == 1)
      {
        m_Last = m_Recorder.LastValueAsDouble.AtLeast(m_Recorder.CurrentValueAsDouble);
      }
      else
      {
        long count = 0;
        m_Last = 0;

        int i = m_Recorder.Count;
        while (i --> 0)
        {
          var sample = m_Recorder.GetSample(i);
          count  += sample.Count;
          m_Last += sample.Value;
        }

        if (count == 0)
          return;

        m_Last /= count;
      }

      if (m_Last < m_Min)
        m_Min = m_Last;
      if (m_Last > m_Max)
        m_Max = m_Last;

      double deviatn = m_Count == 0 ? 0 : m_Sum / m_Count - m_Last;

      m_Sum += m_Last;
      ++ m_Count;
      m_Deviations += deviatn * deviatn * (deviatn < 0 ? -1 : 1);
    }


    object IEnumerator.Current => m_WaitForRest;

    bool IEnumerator.MoveNext()
    {
      if (!IsReady)
        return false;

      if (!m_Recorder.IsRunning)
      {
        m_Recorder.Start();
        return true;
      }

      if (m_Recorder.Count < BatchSize)
        return true;

      m_Recorder.Stop();
      ProcessSamples();
      m_Recorder.Reset();

      m_WaitForRest?.Reset();

      return true;
    }

    void IEnumerator.Reset()
    {
      throw new System.NotSupportedException();
    }

  } // end struct CoarseSampler

  #else

  internal class CoarseSampler : IEnumerator
  {
    public string Category  => string.Empty;
    public string Stat      => string.Empty;
    public int BatchSize    => 0;
    public int RestInterval => 0;
    public int EnabledIn    => 0;

    [NotNull]
    public static CoarseSampler SystemRAM     => new CoarseSampler();
    [NotNull]
    public static CoarseSampler ManagedRAM    => new CoarseSampler();
    [NotNull]
    public static CoarseSampler ManagedGCTime => new CoarseSampler();

    public bool   IsReady    => false;
    public bool   HasSamples => false;
    public bool   IsSampling => false;
    public bool   IsResting  => false;
    public double LastSample => 0;

    public bool Setup() => false;

    public void Dispose() {  }

    public void Report(out double max, out double min, out double mean, out double stdev, double div = 1)
    {
      max = min = mean = stdev = div;
    }


    object IEnumerator.Current => null;

    bool IEnumerator.MoveNext() => false;

    void IEnumerator.Reset() {  }
  }

  #endif // UNITY_2020_2_OR_NEWER
}