/*! @file       Runtime/MultiException.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2023-03-02
 *
 *  2024-03-17: OBSOLETE in v8.7.0.  Will be removed in major version v9.
**/

using JetBrains.Annotations;

using Exception = System.Exception;


namespace Ore
{
  [PublicAPI]
  [System.Obsolete("Use System.AggregateException instead.")]
  public sealed class MultiException : System.AggregateException
  {

    [NotNull]
    [System.Obsolete("Use new System.AggregateException instead.")]
    public static MultiException Create([NotNull] Exception top,
                                        [NotNull] Exception next,
                                        [ItemNotNull] params Exception[] theRest)
    {
      if (theRest.IsEmpty())
      {
        return new MultiException(outer: next, inner: top);
      }

      int i = theRest.Length;

      var curr = theRest[--i];

      while (i --> 0)
      {
        curr = new MultiException(outer: curr, inner: theRest[i]);
      }

      return new MultiException(outer: new MultiException(outer: curr, inner: next), inner: top);
    }


    private MultiException([NotNull] Exception outer, [NotNull] Exception inner)
      : base(MakeMessage(inner, outer), inner)
    {
    }

    private static string MakeMessage([NotNull] Exception inner, [NotNull] Exception outer)
    {
      int flags = 0;

      if (inner is MultiException)
        flags |= 0b01;
      if (outer is MultiException)
        flags |= 0b10;

      switch (flags)
      {
        default:
        case 0b00:
          return $"{inner.GetType().NiceName()}: {inner.Message}\n{outer.GetType().NiceName()}: {outer.Message}";

        case 0b01:
          return $"{inner.Message}\n{outer.GetType().NiceName()}: {outer.Message}";

        case 0b10:
          return $"{inner.GetType().NiceName()}: {inner.Message}\n{outer.Message}";

        case 0b11:
          return $"{inner.Message}\n{outer.Message}";
      }
    }

  } // end class MultiException
}
