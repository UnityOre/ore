/*! @file       Abstract/IEnableable.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2022-06-06
**/


namespace Ore
{

  public interface IEnableable
  {
    bool IsEnabled { get; set; }
  }

}
