/*! @file       Abstract/IWriteJson.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2023-05-02
**/

using JetBrains.Annotations;


namespace Ore
{
  public interface IWriteJson
  {

    [NotNull]
    string WriteJson(bool pretty);

  }
}