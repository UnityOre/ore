/*! @file       Abstract/IReadJson.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2023-05-02
**/

using JetBrains.Annotations;


namespace Ore
{
  public interface IReadJson
  {

    bool ReadJson([NotNull] object json);

  }
}