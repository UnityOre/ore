/*! @file       Abstract/ICoroutineRunner.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2022-12-06
**/

using UnityEngine;

using JetBrains.Annotations;

using IEnumerator = System.Collections.IEnumerator;


namespace Ore
{
  [PublicAPI]
  public interface ICoroutineRunner
  {
    /// <param name="key">
    ///   A key object previously associated with coroutine(s) in this runner
    ///   instance.
    /// </param>
    /// <returns>
    ///   <c>true</c> iff any routines at the given key are currently running.
    /// </returns>
    bool IsRunning([CanBeNull] object key);

    /// <summary>
    ///   Enqueues the routine to be run by this ICoroutineRunner. Primarily
    ///   useful for non-Scene-bound code to start a coroutine even if not part
    ///   of a MonoBehaviour, and even if there aren't any scenes loaded yet.
    /// </summary>
    ///
    /// <param name="routine">
    ///   A valid IEnumerator object representing a coroutine function body (or
    ///   clever equivalent).
    /// </param>
    ///
    /// <remarks>
    ///   This overload can only be halted by natural completion of the routine,
    ///   or by the parent runner halting altogether.
    /// </remarks>
    ///
    /// <returns>
    ///   <c>true</c> iff the routine was started successfully.
    /// </returns>
    bool Run([NotNull] IEnumerator routine);

     /// <summary>
     ///   Enqueues the routine to be run by this ICoroutineRunner. Primarily
     ///   useful for non-Scene-bound code to start a coroutine even if not part
     ///   of a MonoBehaviour, and even if there aren't any scenes loaded yet.
     /// </summary>
     ///
     /// <param name="routine">
     ///   A valid IEnumerator object representing a coroutine function body (or
     ///   clever equivalent).
     /// </param>
     ///
     /// <param name="key">
     ///   A reference to this contractual key will be stored. If/When the contract
     ///   object expires (either due to GC cleanup or Unity Object deletion), the
     ///   associated coroutine will halt itseslf, if it is still running.
     ///   Multiple routines may be associated with the same key.
     ///   A retained key can be used to Halt its associated coroutine(s) manually.
     /// </param>
     ///
     /// <returns>
     ///   <c>true</c> iff the routine was started successfully and paired to key.
     /// </returns>
    bool Run([NotNull] IEnumerator routine, [NotNull] Object key);

    /// <summary>
    ///   Enqueues the routine to be run by this ICoroutineRunner. Primarily
    ///   useful for non-Scene-bound code to start a coroutine even if not part
    ///   of a MonoBehaviour, and even if there aren't any scenes loaded yet.
    /// </summary>
    ///
    /// <param name="routine">
    ///   A valid IEnumerator object representing a coroutine function body (or
    ///   clever equivalent).
    /// </param>
    ///
    /// <param name="key">
    ///   A non-null string key to associate with <paramref name="routine"/>.
    ///   Multiple routines may be associated with the same key.
    ///   A retained key can be used to Halt its associated coroutine(s) manually.
    /// </param>
    ///
    /// <returns>
    ///   <c>true</c> iff the routine was started successfully and paired to key.
    /// </returns>
    bool Run([NotNull] IEnumerator routine, [NotNull] string key);

    /// <summary>
    ///   Enqueues the routine to be run by this ICoroutineRunner. Primarily
    ///   useful for non-Scene-bound code to start a coroutine even if not part
    ///   of a MonoBehaviour, and even if there aren't any scenes loaded yet.
    /// </summary>
    ///
    /// <param name="routine">
    ///   A valid IEnumerator object representing a coroutine function body (or
    ///   clever equivalent).
    /// </param>
    ///
    /// <param name="guidKey">
    ///   A randomly-generated GUID string that is associated with the running
    ///   <paramref name="routine"/>.
    ///   You may associate other routines with this same key.
    ///   A retained key can be used to Halt its associated coroutine(s) manually.
    /// </param>
    ///
    /// <returns>
    ///   <c>true</c> iff the routine was started successfully and paired to key.
    /// </returns>
    bool Run([NotNull] IEnumerator routine, [NotNull] out string guidKey);

    /// <summary>
    ///   Cancels all enqueued coroutines owned by this runner that are paired
    ///   with the given <paramref name="key"/>, if any.
    /// </summary>
    ///
    /// <param name="key">
    ///   Maybe the thing you associated with the intended routine(s) when
    ///   Run(...) was called on this runner instance. Maybe null.
    /// </param>
    ///
    /// <returns>
    ///   <c>true</c> iff there was at least one running routine paired to the
    ///   key (which would now be halted). <br/>
    ///   <c>false</c> iff there were no running routines found paired to the
    ///   key.
    /// </returns>
    bool Halt([CanBeNull] object key);

  }
}