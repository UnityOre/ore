# Changelog
All notable changes to this project will be documented in this file.

The format is *loosely* based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [v8.7.0](../../tags/v8.7.0) - 2024-03-26

(more was planned for this release, but certain time constraints are expediting us here.)

#### Deprecated
- `MultiException`: I admit I didn't know about System.AggregateException.  You should use that instead.  Until this class is removed in v9, it now inherits from System.AggregateException, to ease the transition.

#### Fixed
- LICENSE was missing a meta file, resulting in annoying warnings. (#5)


## [v8.6.2](../../tags/v8.6.2) - 2024-02-22

#### Fixed
- Too many slashes in some (but not all) usages of [AddComponentMenu("...")]
- Certain optional preprocessor defines that were missed in the last k2o conversion


## [v8.6.1](../../tags/v8.6.1) - 2023-12-04

> Da Big k2o update (v7.0.0 -> v8.6)

#### Fixed
- Valid Unity compilation (tested in Unity 2022)
- Deprecation warnings in Unity 2022+ (DeviceSpy)


## [v8.6.0](../../tags/v8.6.0) - 2023-11-16

#### Fixed
- NREs originating in RuntimeSamplers.

#### Added
- Some fast Vector2 utils (in [Static/Vectors.cs](Static/Vectors.cs))
- DeviceSpy.IsSlowFrame() definition
- RuntimeSamplers.CountRecentSlowFrames() -> int
- RuntimeSamplers.PercentRecentSlowFrames() -> float


## [v8.5.1](../../tags/v8.5.1) - 2023-11-07

HOTFIX: Test-driven correctness

#### Fixed
- `HashMap.Union(other)`: a short-circuit optimization that snuck in last release broke Union correctness.
- `MiniJson.ReflectFields(...)`: identified an edge case that would improperly reflect data of type `IDictionary<string,object>` onto other types of dictionaries (namely `Dictionary<string,string>` in this case).
- (all Editor and Runtime unit tests now pass :partying_face:)
- ((always run yer tests before releasing, kids :older_man:))


## [v8.5.0](../../tags/v8.5.0) - 2023-11-06

#### Added
- Editor.AssetsValidator.NoValidateZone: Scope object that allows your editor code to alter the AssetDatabase without fear of triggering Ore's various validations.
- Also: OAssetsValidator now automatically creates blanket .gitignore files in the following `Assets/` subdirectories:
  - `GeneratedLocalRepo/`
  - `Resources/Generated/`
  - `Samples/`
  - Feel free to edit the contents of these new gitignore files; they will only be regenerated if they are missing.
- Editor.Shell: Invoke Bash or Git oneshots from C# (only in the editor).
- Editor.GitSpy: Automatically saves the Git state(s) for the project (including UPM packages / submodules), which can be read at runtime.
- GitStamp: Public API for reading stored Git state(s) (saved by GitSpy) at runtime.
  - Lazy loaded - virtually zero overhead if you don't use this API.
- JsonAuthority.Scope(allowUnityTypes...): New constructor overload that marks specific UnityEngine.Object subtypes to allow their serialized fields to be serialized to JSON.
  - Supports supertypes, i.e. `UnityEngine.MonoBehaviour`, for specifying the serialization of all types inheriting therefrom.
- HashMap\<K,V\> now implements IReadOnlyDictionary\<K,V\>.
- HashMap optimized copy constructor (uses Union(other) to reuse hashes).
- DeviceSpy.GLVersion -> SerialVersion: Gets a cleaned-up representation for the available graphics library version.
- DeviceSpy.IsAndroidGO -> bool: Currently not confirmed to work on all actual GO devices. Confirmed to work on some old ones.
- AndroidBridge.VersionCode -> int: Uses the GitSpy instrumentation to give you the Android version code at runtime.
- SerialVersion: Added operators <=, >=, and Contains(string).
- Strings.AppendJson(obj\[, pretty\]) -> StringBuilder (`StringBuilder` extension method).
- DateTimes.Approximately(other\[, epsilon\]) -> bool (`DateTime` extension method).

#### Fixed
- JsonAuthority: Now properly has a way to serialize UnityEngine.Object types (when whitelisted with the new Scope constructor).
- MiniJson: Now supports reading into more generic (boxed) dictionary types, such as IDictionary\<object,object\>.
- SerialVersion: Now properly parses GL versions.

#### Changed
- JsonAuthority: When a UnityEngine.Object type is _not_ whitelisted for write, it will serialize as an empty JSON object `{}` instead of `null`.
- RecycledStringBuilder: Default builder capacity = 64 (was 16).


## [v8.4.3](../../tags/v8.4.3) - 2023-09-15

The Scripting Symbol Define hotfix.

#### Fixed
- Rare scenario where if the Orator object is destroyed or missing, both `Orator.Error(...)` and `Orator.AssertionFailed(...)` would interpret the compilation symbol `AGGRESSIVE_ORATOR` incorrectly and log when it should be squelched.

#### Changed
- `Orator.Panic(...)` now at least tries to log an NFE before flipping the fuck out.

#### Deprecated
As part of scripting define symbol standardization, the following symbols now have upstream replacements:
- `AGGRESSIVE_ORATOR` -> `ORE_ORATOR_ALWAYS_LOG` (user-provided)
- `DEBUG_ORATOR` -> `ORE_ORATOR_DEBUG` (user-provided)
- `KOOBOX` -> `ORE_DEBUG` (user-provided)
- `KOOBOX_EXPLORE` -> `ORE_DEBUG` (user-provided)
- `PREFER_NEWTONSOFT` -> `ORE_PREFER_NEWTONSOFT` (user-provided, but plz don't ever)
- `NEWTONSOFT_JSON` -> `ORE_NEWTONSOFT` (Ore-provided)
- `TMPRO` -> `ORE_TMPRO` (Ore.Editor-provided)

> Note: the old user-provided symbols should still work as before, though be advised that support may be fully removed with the next major update (v9.x).

#### Added
- C# `#warning` directives for each user-provided compilation symbol deprecated above (if they continue being used).
- C# `#warning` directive if you define `ORE_PREFER_NEWTONSOFT`, but Unity's Newtonsoft package is unavailable in the current project.


## [v8.4.2](../../tags/v8.4.2) - 2023-09-14

#### Fixed
- Debug-only issue (failed assertion on app quit) when also using the phoenix-unity client package.


## [v8.4.1](../../tags/v8.4.1) - 2023-09-13 (again)

#### Fixed
- Unity 2019 / iOS compatibility (compilation issues only arose when building)


## [v8.4.0](../../tags/v8.4.0) - 2023-09-13

#### Fixed:
- Unity 2019 (iOS) only: NRE in `RuntimeSamplers.SampleSystemRAM` (#78)

#### Added:
- in `DeviceSpy`:
  - property CurrentRAM -> float (megabytes)
  - property CurrentFPS -> float (instantaneous)
  - property IsRAMLow -> bool (non-Android platforms are still TODO, but may work provisionally)
  - method NormalizeFPS(fps) -> float (normalizes the given raw FPS to this device's established maximum)
- in `Consts`: Values to help standardize package `[MenuItem]` paths and priorities: MenuItemPrefix, MenuItemPriorityStandard
- in `Histogram`:
  - method AggregateSamples(out mean, out stdev, out min, out max) -> int
  - method Clear()
- in `VisualTestingWindow`: Visualizer for testing Histograms
- in `Strings`: extension method StringBuilder.AppendSignature(method) -> StringBuilder
- in `SerialVersion`:
  - Characters `'r'` (for **r**evision, **r**elease candidate) and `'p'` (for **p**review, **p**rerelease) are now valid tag deliminators.
  - Whitespace (space, tab, newline) are now included in TRIM\_CHARS.
- in `GreyList<T>`: Constructor taking a GreyListType; derived classes also implement the same.
- in `Floats`: overloaded Approximately(...) for doubles

#### Changed:
- All `[MenuItem]` commands added by Ore now adhere to the standard format; the "Ore/\*" menu bar layout has been rearranged.
- in `DeviceSpy`:
  - Querying Android memory info from the OS is now rate limited, and can occur at most every 37 frames _(prime constant is intentional)_
  - RAM-related properties (i.e. TotalRAM) now return **mega**bytes (was mebi), and as a float (was int)
- in `Orator`:
  - Refactored and cleaned out a lot of messy internal implementation
  - Implemented a failed (and not compiled by default) attempt at fixing the stacktrace links issue (#76)
  - Public API methods now have proper nullability annotations
  - Panic() now attempts to force a crash in all the different ways provided by Unity (instead of just one)
- Cleaned up the SerialSet API.
- SerialSet.Deserialize no longer trims parentheses.

#### Deprecated:
- in `DeviceSpy`: (see [Static/DeviceSpy+Deprecated.cs](Static/DeviceSpy+Deprecated.cs))
  - property LowRAMThreshold - package users shouldn't do their own checks with this; prefer to use property DeviceSpy.IsRAMLow instead
  - method CalcRAMUsageMB() - equivalent to property DeviceSpy.CurrentRAM, use that instead
  - method CalcRAMUsageMiB() - data team wants to use megabytes instead of mebi, so shouldn't use this anymore
  - method CalcRAMUsagePercent() - if you really need, you can calculate this yourself from DeviceSpy properties
- enum value `DeviceDimension.ThresholdRAM`


## [v8.3.2](../../tags/v8.3.2) - 2023-09-01

#### Hotfix: DeviceSpy (Android-only)
- Fixed: LowRAMThreshold is now calculated correctly.
- Changed: TotalRAM will now use the value pulled from the Android OS, _IFF_ it is less than the value returned by UnityEngine.SystemInfo.systemMemorySize.


## [v8.3.1](../../tags/v8.3.1) - 2023-08-31 (again)

#### Hotfix: Unity 2019 (iOS) compatibility
- Fixed: missing using statement in DeviceSpy+iOS.cs
- Fixed: usage of AssetDatabase.SaveAssetIfDirty(asset) in AssetsValidator.cs


## [v8.3.0](../../tags/v8.3.0) - 2023-08-31

#### in `DeviceSpy`:
- Added: new property FallbackID represents the value of UDID in any case where we had to fall back to generating our own GUID. It *may* reset upon reinstall, but hey it's a fallback.
  - Also: added LittleBirdie.FallbackID to match.
- Changed: Implemented an Android-specific provider for LowRAMThreshold (solves #21), replacing the previously-spoofed value.
- Changed: Supplying a null or empty value to LittleBirdie.{FallbackID,IDFV,UDID} now also calls PlayerPrefs.DeleteKey() as appropriate for those properties.
- Improved: cleaned up the code by making DeviceSpy a partial class and moving nested APIs and platform-specific implementations to separate files. This change has no effect on the APIs' usability or performance.

#### in `AssetsValidator`: (editor-only)
- Added: Now inherits from AssetModificationProcessor, and implements the following messages:
  - "OnWillSaveAssets": If the asset being saved is a TextMesh Pro TMP\_FontAsset set to dynamic atlas mode, the dynamic font data will be cleared before the asset is saved. This fixes some very annoying oddities when using TMPro with source control. ([more info](https://forum.unity.com/threads/tmpro-dynamic-font-asset-constantly-changes-in-source-control.1227831/))
  - "OnWillMoveAsset" + "OnWillCreateAsset" + "OnWillDeleteAsset":
    - You will now receive a cheeky warning whenever you move (or create) an asset _into_ a Resources folder, and likewise, a gold start whenever you move (or delete) an asset _out of_ a Resources folder.
    - You may define the scripting symbol `ORE_SHUT_UP` to disable this behavior.
- Added: OnValidate() is invoked on all preloaded instances of Ore.Asset upon editor refresh.
  - Normally, OnValidate would only be called on refresh for assets that have recently been viewed in an inspector.
  - Additionally invokes AssetDatabase.SaveAssetIfDirty(yourAsset), making any changes made during OnValidate get applied to disk.
  - Note: This implicates any Ore.OAssetSingleton instance as well.

#### in `AssetInspector`: (editor-only)
- Added: protected virtual function DoCustomInspectorEGL(propertyIterator), allowing child classes to more easily use EditorGUILayout to customize their inspector. (See the [AutoVersioner](../../../../autoversioner) package for working example.)
- Changed: If inspecting an editor-only asset, the "Preload" and "Hide Flags" header toggles are replaced with the label "(Editor-only asset)".
- Changed: static function IterateProperties(...) is now protected instead of private.

#### the rest
- Added: in `Ore.Editor.asmdef`: now references Unity.TextMeshPro assemblies (used by AssetsValidator).
  - Note: this is only a _soft_ dependency; it's still safe for your project to not have TMP in it.
- Added: in `VisualTestingWindow`: can now debug DiagonalDistance coverage using Ore.Raster.
- Added: in `Paths`: utility func ContainsComponent(path, name) -> bool
- Added: the `PerfGUI` component is now in the "Add Component" menu: "Ore//Perf GUI"
- Added: in `MeGUI`: equivalents to Screen.safeArea and Screen.cutouts, but in IMGUI space.
- Added: in `Editor.Styles.Defaults`: FrameBox style.
- Changed: in `SerialVersion`: DeepCompareTo(other) now orders `-pre*` versions sensibly vs non-preview versions.
- Changed: in `AndroidBridge`: Application.lowMemory is only honored once per fresh launch in terms of releasing JVM memory held by AndroidBridge.
- Improved: in `CoroutineRunner`: internal coroutine lists are now pooled, reducing the amount of GC overhead.


## [v8.2.2](../../tags/v8.2.2) - 2023-07-07

#### in `DeviceSpy.GeoIP`: 
- Fixed: property `IsFetching` should no longer throw ArgumentNullException -> \_unity\_self (#65).
- Fixed: function `FetchIfStale(...)` was forgetting to propagate cached geo values to LittleBirdie.
  - Possibly related to issue #58.
- Added: property `IsUsed` -> bool. Returns true if the current DeviceSpy.CountryISOString value matches the last fetched ip geo value.

#### the rest:
- Fixed: in `MiniJson`: "Deserialize" functions now properly respect IReadJson types being passed in as the primary target type.

- Changed: in `CoarseSampler`: SystemRAM sampler is now enabled in release builds by default.
  - Note: This will not retroactively change a pre-existing `Resources/RuntimeSamplers.asset`. To match the new default behaviour, you will need to regenerate this asset, or manually set its "Enabled In" property to "Release Build".

- Improved: in `Promise`: All overloads of AwaitCoroutine(...) now avoid spawning an actual coroutine if the promise is already fulfilled.
- Improved: in `Orator`: Context name format.


## [v8.2.1](../../tags/v8.2.1) - 2023-06-21

#### Unity 2019 (iOS) hotfix
- Fixed: MeGUI.IsTapped(...) no longer uses UnityEngine.EventType.TouchDown in Unity \< 2020.


## [v8.2.0](../../tags/v8.2.0) - 2023-06-19

#### New nicety: `PerfGUI`
- A MonoBehaviour component that integrates `Histogram` + `RuntimeSamplers` for you to easily view FPS/RAM graphs in builds.
- The defaults were chosen to be easy-to-use, just add and play!
  - However there also should be enough tweakables in the inspector to suit your specific needs.
  - Lemme know if not :wink:
- Comes with a simple API for you to provide custom sample values thru code, if you need to quickly graph something other than the provided defaults.

#### New static utility: `AABBs`
- Various utilities for dealing with axis-aligned bounding box math.
- Specifically covers the two UnityEngine types:
  - `Rect` (for 2D) and
  - `Bounds` (for 3D).

#### in `DeviceSpy`:
- Changed: `CalcRAMUsage*()` APIs now prefer to get their RAM samples from `RuntimeSamplers` first, if available.
- Added: property `TotalRAM -> int` - wraps SystemInfo.systemMemorySize, plus converts megabytes to mebibytes to match RAM unit standards.
  - Also: can be overidden in LittleBirdie.
- Added: property `GeoIP.HasFetched -> bool` - returns true if this device has ever successfully fetched its geoip.
- Added: a destructive `Reset()` method. After invoking, DeviceSpy will behave as if this is the user's first session ever, and re-calculate all values as they are requested.
  - **Please only use this for testing purposes.**
- Fixed: (Android) `Browser` no longer always returns "UnityEngine.AndroidJavaObject", and typically returns the correct name of the user's default browser.
  - Note: Known bug = browsers with wide character (Unicode) names will currently be reported as escape sequence strings. See Issue #61
- Fixed: `LowRAMThreshold` now returns much more accurate values for most devices, although it is still only an estimation.
  - Also: It is now guaranteed to return a value of at least 200 (unless LittleBirdie sets something lower). 
- Fixed: in GeoIP.Fetch(), PlayerPrefs.Save() is now properly called _after_ the FETCHED_AT timestamp is registered.

#### in `RuntimeSamplers`:
- Added: new overload: `SampleSystemRAM(out long bytes) -> bool`
- Added: new overload: `SampleManagedRAM(out long bytes) -> bool`

#### in `Histogram`:
- Added: Enum values `Draw.FloorLabel` and `Draw.CeilLabel`, and corresponding drawing logic.
  - Note: These values are now also part of the default drawing flags.
- Fixed: Property `MaxSampleCount` no longer throws array-related exceptions when tweaking at runtime.
- Fixed: Setters for properties `Ceiling` and `Floor` now enforce the axiom "floor is always less than ceiling".

#### in `TimeInterval`:
- Fixed: Static property `LastFrame -> TimeInterval` now returns the correct value.
- Added: Static property `NowFrames -> TimeInterval`.
- Added: The TimeInterval struct now also implements `IEnumerator`, so technically you could use it directly as a wait object in a coroutine.
  - Note: Don't reuse it though. IEnumerator.Reset() will throw NotSupportedException.

#### in `Colors`:
- Added: New extension methods:
  - `color32.ValueInverted([shift]) -> Color32`
  - `color32.ValueRotated(t) -> Color32`
  - `color32.Complement() -> Color32`
  - `color32.Hue() -> float`
  - `color32.WithHue(newValue) -> Color32`
  - `color32.PerceivedLuminance() -> float`
  - `color32.PerceivedLuminanceHQ() -> float` (more correct but also more expensive)
- Added: New static methods:
  - `NiceForegroundFor(background[, contrast]) -> Color32`
  - `FromHSV(hueDeg, sat01, val01) -> Color32`
- Changed: Renamed:
  - const Color32 `Value` -> `ValueType`
  - extension method `color32.HSVValue()` -> `color32.Value()`
  - extension method `color32.HSVValue(newValue)` -> `color32.WithValue(newValue)`
  - extension method `color32.MultHSVValue(factor)` -> `color32.MultValue(factor)`

#### the rest
- Fixed: `MeGUI.IsTapped(pos)` no longer returns true when a touch begins outside and ends inside the position rect (via dragging).
- Fixed: `WebRequests.PromiseText(...)` no longer throws an assertion exception in certain edge cases. Instead, prints a warning in debug.
- Fixed: (unit testing) `DelayedRoutineTests` are correct again and pass.
- Added: (unit testing) `DelayOnActiveScene` test (under DelayedRoutineTests).
- Added: (EditorBridge) New asset context menu item: `Ore/Delete Missing Scripts`
  - Select one or more prefabs that contain missing script references, and this utility will clean them up.
- Added: New utility class (for unit tests): `MegaAssert` - just a collection of reusable special assertions.
- Added: `Floats.Sqrt3` precomputed constant.
- Improved: `Floats.IsNegativeZero(val)` when unsafe code is _not_ enabled (`#if !ORE_UNSAFE`).
- Changed: You now must explicitly define `ORE_UNSAFE` in your scripting symbols for any of Ore's unsafe implementations to run.


## [v8.1.2](../../tags/v8.1.2) - 2023-06-07

#### Hotfixed: `CoarseSampler` (again)
- Fixed: Needed to have the flag `ProfilerRecorderOptions.WrapAroundWhenCapacityReached` when constructing ProfilerRecorders in order to avoid later indexing issues.
  - note: still blame unity `o~O` always


## [v8.1.1](../../tags/v8.1.1) - 2023-06-02

for u @albert.chen

#### Hotfixed: `CoarseSampler`
- Fixed: 'IndexOutOfRangeException' in ProcessSamples() (lineno 166).
- Fixed: Sampling works correctly when "Rest Interval" is non-positive.
  - Interpreted as no rest interval, simply restart as soon as the current batch of samples is full.
- Fixed: Sampling works correctly when "Batch Size" is 1.
  - (previously it would freeze after the first value.)
- Fixed: (Unity 2019 compat) CoarseSampler is now always a class. Fixes compilation error in RuntimeSamplers.cs.

#### the rest.
- Added: new overloads in `Floats`: double.AtMost(d), double.AtLeast(d). (needed by CoarseSampler)
- Fixed: `MeGUI`.IsTapped(rect) - EventType.TouchUp does not exist until Unity 2020, but is also not needed.
- Fixed: `PrimesInEditor` (editor unit test) - Unity 2019 C# compilation detail.
- Fixed: `CodeJudgeJudgement` (runtime unit test) - in u2019, `CustomYieldInstruction.Reset()` is not virtual and so cannot be overidden (by ephemeral test class WaitForFrames2).
  - _lucky for me, the language gives us a workaround :sunglasses:_


## [v8.1.0](../../tags/v8.1.0) - 2023-05-23

#### Now (officially) available: `RuntimeSamplers` (+ `CoarseSampler`)
- Changed: The OAssetSingleton instance is now auto-generated, preloaded, and marked with HideFlags.DontUnloadUnusedAsset by default.
- Changed: Renamed `OnGUI()` -> `DebugDrawOnGUI()`, to clarify that the method is _not_ implicitly called like the MonoBehaviour message.
- Changed: Tweaked the default values in `CoarseSampler` to be cheaper, without sacrificing accuracy in most cases:
  - DEFAULT\_BATCH\_SIZE: 13 -> 3
  - DEFAULT\_REST\_SECONDS: 2.5 -> 5
- Changed: The following predefinitions in `CoarseSampler` are now properties instead of readonly fields:
  - `SystemRAM`
  - `ManagedRAM`
  - `ManagedGCTime`
  - (to prevent the predefinitons from being accidentally mutated at runtime.)
- Changed: Moved nested enum `DurationPolicy` up to the namespace level, and into its own file (Static/DurationPolicy.cs).
- Added: Property drawer for CoarseSampler.RestInterval, so you can actually edit the value (oopsie).
- Fixed: Timers were not working due to a bug with ref-returns from a HashMap of structs. This has been alleviated by changing `struct Timer` to `class MTTimer`.
- Improved: CoarseSamplers are no longer ticked as coroutines on ActiveScene. The new impl is lighter-weight, more deterministic, and more (scene-)state-independent.
- Improved: Generally safer lifecycle for the asset object and its subscription to app-level events.

For the following items in particular, refer to the accompanying inline documentation for details.
- Added: Official public API for utilizing `CoarseSampler`s:
  - `SampleSystemRAM(out float mebibytes)`
  - `SampleManagedRAM(out float mebibytes)`
  - `SampleOther(string stat, out double sample)`
  - Note: while this API _is_ official now, a better one may replace it in a future release.
- Added: Additional supporting functions for the "\*Timer" API:
  - `CreateTimer(id)`
  - `ResumeTimer(id)`
  - `IsTimerRunning(id) -> bool`
  - `DeleteTimer(id)`
  - `ClearAllTimers()`

#### in `EditorBridge`:
- Added: `GetSubAssets(asset) -> Object[]`
- Added: `HasSubAssets(asset) -> bool`
- Added: `DeleteSubAssets(asset, commit) -> bool`
- Added: `DeleteAllSubAssets(asset, commit) -> int`
- Added: `CommitChangedPath(path) -> bool`
- Added: `CommitChangedAsset(asset) -> bool`
- Added: `CommitAllChanges(reimport)`
- Added: Context menu item for the Project (Assets) window: `Assets/Ore/Delete Sub Asset(s)`
  - _Aside: We can't believe this isn't a thing built into Unity already_ :grimace:
- Improved: BrowseTo(path) is properly compiled out if !UNITY\_EDITOR.

#### in `Histogram`:
- Added: Property `LastSample -> float`
- Changed: Renamed the following:
  - property `SampleCeiling` -> `Ceiling`
  - property `SampleFloor` -> `Floor`
  - (not counting these as breaking changes... because... er, cuz I said so :stuck_out_tongue_winking_eye:)
- Changed: Histogram now draws its own background, and the "border" Color parameter to `DrawOnGUI(...)` has been renamed to "background" (to be used for such).
  - Also: Changed: Renamed the enum `Draw.Border` -> `Draw.Background`.
  - Note: The border color is now calculated from the background color
- Fixed: the major bug where the histogram rotates/skews wonkily whenever the camera moves or rotates.
- Improved: `RecalculateHeight()` works much better for different kinds of datasets (E.G. large sample values with very small std dev).
- Improved: `Draw.StdDevGraph` now does objectively better y-bounding.
  - Also: Improved: The StdDev graph is no longer inversely proportional to the per-sample mean. Stats = leveld up!

#### in `MeGUI`:
- Added: Moved Histogram's private implementation for line drawing in the GUI to MeGUI, and made the API public:
  - `DrawLine(a, b, color)`
  - `DrawOutline(rect, color)`
  - `DrawBox(rect, fill, outline)`
  - `BeginGL(color, mode)` (lower-level util)
  - `BeginLineGL(color)` (lower-level util)
  - `EndGL()` (lower-level util)
  - Also: Fixed & Improved a lot of visual glitches present in Histogram's original GL drawing impl.
- Added: `IsTapped(rect) -> bool`

#### the rest
- Added: `[AutoCreateAsset(...)]` can now optionally specify 2 properties:
  - `bool PreloadByDefault`
  - `HideFlags DefaultHideFlags`
- Added: `DeviceSpy.ToJson()` now contains an entry for MathfInternal.IsFlushToZeroEnabled -> bool.
- Added: `color32.MultHSVValue(f) -> Color32` (Static/Colors.cs)


## [v8.0.0](../../tags/v8.0.0) - 2023-05-12

#### In addition to the notes for v8.0.0-pre (below)...
- Changed: **Unsafe C# code is now permitted in the main Ore assembly.**
  - Note: So far, no existing systems have been overhauled with unsafe code. The allowance is provisionary, for future testing.
- Changed: HashMapParams no longer utilize a curve for calculating growth.

- Added: `EntropicHasher` - unsafe utility for hashing some quantity of raw bytes, utilizing seed entropy from UnityEngine.Random.
  - Note: It's about as fast as System.String.GetHashCode(), but exploration here is not over yet.
- Added: in `MeGUI`: NormalizeGUIScale(referenceSize), BoxStyle, ButtonStyle
- Added: new fast shaders migrated from KooBox: "Dirt Cheap/Particles", "Dirt Cheap/Particles (Alpha Blended)".
- Added: in `JsonAuthority`: WriteCSV(...) - pivots some parsed Json-like data to format a CSV text representation of it.
- Added: new runtime class `Histogram`: add data samples over time, and call the DrawOnGUI(...) methods to represent the histogram on screen.

- Fixed: Serialized events were broken in most inspectors. Now they aren't.
- Fixed: `CodeJudge` variance / std deviation estimations are now much more accurate.

- Improved: general `HashMap` speed by testing and tweaking default parameters.
  - Also: Improved: HashMap insertion speed by utilizing an existing memo pattern for the last attempted lookup or insertion.
  - Also: Improved: `Primes` speed and edge case accuracy.

#### P.S. Forgot to mention...
- Added: 2 shaders + 2 materials to new subdirectory `Shaders/`:
  - `DirtCheapParticles.shader` :arrow_left: `DirtCheapParticle.mat`
  - `DirtCheapParticlesBlendable.shader` :arrow_left: `DirtCheapParticleBlended.mat`
  - Benchmarking against Unity's mobile particle shaders show these bare-metal shaders to be **~8x more efficient** at runtime (tested on device: Kyocera Duro 2).


## [v8.0.0-pre](../../tags/v8.0.0-pre) - 2023-05-05 :flag_mx:

- Note: This major release is in "preview" (`-pre`) only due to the full test cycle not yet being checked off.

#### in `Promise<T>` **[contains breaking changes]**:
- Changed: The following have been renamed for API parity:
  - property `Succeeded` -> `IsSucceeded`
  - property `Failed` -> `IsFailed`
  - event `OnSucceeded` -> `Succeeded`
  - event `OnFailed` -> `Failed`
  - event `OnCompleted` -> `Completed`

- Changed: public delegate void FailureAction({-T flotsam, -}Exception ex)
  - BREAKING: affects the signature of the `Failed` event API.
  - Note: If your callback still needs the flotsam, you can get it by capturing a reference to the promise in your delegate, and using `promise.Value` to access the same flotsam.

- (potentially soft-breaking) Changed: The Promise's internal value is not cleared if it has been "forgotten".
  - Note: Clearing the value is now only done manually by the caller with Reset(), or else letting the Promise object go out of scope / get collected.

#### in `ICoroutineRunner`:
All interface items have been tweaked in a non-destructive way:
- bool IsRunning({-[NotNull]-} object key)
- bool IsRunning({+[CanBeNull]+} object key)
- {-void-} Run([NotNull] IEnumerator routine)
- {+bool+} Run([NotNull] IEnumerator routine)
- {-void-} Run([NotNull] IEnumerator routine, [NotNull] Object key)
- {+bool+} Run([NotNull] IEnumerator routine, [NotNull] Object key)
- {-void-} Run([NotNull] IEnumerator routine, [NotNull] string key)
- {+bool+} Run([NotNull] IEnumerator routine, [NotNull] string key)
- {-void-} Run([NotNull] IEnumerator routine, [NotNull] out string guidKey)
- {+bool+} Run([NotNull] IEnumerator routine, [NotNull] out string guidKey)
- {-void-} Halt({-[NotNull]-} object key)
- {+bool+} Halt({+[CanBeNull]+} object key)

#### in `ActiveScene` **[contains breaking changes]**:
- Added: 2 new events available in both serialized and runtime forms: `OnSceneUnloaded` and `OnDebugGUI`.
  - Note: OnDebugGUI is invoked in the `OnGUI` Unity message, and is compiled out of release builds.
  - Note: m_OnSceneUnloaded is a `SceneEvent`, not a VoidEvent. (See next section.)
- Changed: static C# event interface.
  - public static event {-UnityAction-} OnActiveSceneChanged
  - public static event {+UnityAction\<Scene\>+} OnActiveSceneChanged
  - Also: All event delegates registered via code are now _always_ stored in static fields, which are no longer transfered from the static context to the ActiveScene's instance context.
  - Also: Changed: m_OnActiveSceneChanged is now a `SceneEvent`, from VoidEvent. (See next section.)
- Fixed: "MissingReferenceException: The object of type 'CoroutineRunner' has been destroyed..."
  - (somewhat rare bug, occurred on quit or when exiting play mode if someone tried to call ActiveScene.Coroutines.Run(...) in that final frame)
- Improved: ... not that you should ever manually disable the ActiveScene, but if it somehow happens, then any attached logic (Coroutines) operate much more safely.
- Improved: (editor-only) OnValidate() now disallows you from setting m_IsValidWhileDisabled to true.
- Improved: Annotated the retval of `ActiveScene.Coroutines` with [NotNull], so yall feel can keep calm and stop nullcheck branching :smile_cat:

#### New API: `ThingEvent<T>` (+ `SceneEvent`)
- (Cousins of `VoidEvent` + `DelayedEvent`)
- ThingEvent\<T\> is equivalent to `UnityEngine.Events.UnityEvent<T>`.
  - Much like UnityEvent\<T\>, you must create a non-generic child class if you want your event to be serializable.
  - SceneEvent is a working example of this.
- Note: These new APIs have not had an exhaustive round of usage testing, but such testing is planned for upcoming hotfix releases.

Notes on API justification: <br/>
The reasoning for preferring these custom serialized event APIs (as opposed to simply using UnityEngine.Events.\*)
is as follows:
1. They can be non-destructively enabled/disabled, via code or in the Inspector.
2. They have a custom Inspector drawer that—unlike UnityEngine.Events.\*—supports drawing any additional serialized fields from user-defined classes.
  - (`DelayedEvent` is a good showcase of this.)
3. Smaller point, but this custom Inspector drawer also allows you to collapse each serialized event's property section (_including_ the list of event listeners), which can greatly reduce vertical clutter while using the Inspector.
4. Enforcing our own event interface means that in the future, we could potentially implement our own serialized event backend as well – _and potentially_, implement a backend that is more performant and/or better suited for our development needs.

#### New static API: `MeGUI`
Contains helpers and scratch objects for use with Unity's IMGUI API (a.k.a. `UnityEngine.GUI`, `UnityEditor.{Handles,EditorGUI}`).
Currently:
- property `MeGUI.Scratch` -> GUIContent
- property `MeGUI.TextStyle` -> GUIStyle
- function `MeGUI.CalcTextScale(content, style)` -> Vector2

#### in `CodeJudge`:
- Added: `CodeJudge.Report(id) -> IDictionary<string,object>`
  - Note: Plans for v8.1 include to write a CSV report interface, or at least some converter for one.
- Changed: The implementation for the reported "bytes" value now does not call `GC.Collect()` _unless_ you explicitly supply the scripting define symbol `CODEJUDGE_COLLECT_GC` (the bool parameter is still honoured if so).
  - Note: The default implemenation in debug has been switched to use DeviceSpy.CalcRAMUsageBytes(), which currently is using UnityEngine.Profiling.Profiler.GetTotalReservedMemoryLong().
  - Note Note: All this was supposed to be reimplemented using `RuntimeSamplers` by this release, but this has been postponed to v8.1 at the earliest.
- Changed: Reported byte values now give the sum of allocated bytes instead of the maximum delta.
  - Note: this will _also_ likely change in the future.

#### (miscellaneous)
- Added: `Lists` utility method: list.RandomItem()
- Added: `Strings` utility methods: FormatBytesKibi(bytes), FormatBytesSI(bytes)
- Added: extension method: `System.Type.NiceName()` - gets a better name for the given type, primarily for logging purposes.
- Added: `new FauxException(string)`
- Removed: grossly unused field `HashMapParams.Policy` + its nested enum type `Policies`.
- Fixed: many internal logic corrections in `HashMapParams` and `HashMap<K,V>`. I won't enumerate on them all here.
- Improved: `Primes.NextHashableSize(...)` - Discovered that the binary search implementation being used was **~51x slower than it could've been**.
  - Note: This fix signifcantly reduces the growth cost of dynamic HashMaps at runtime.
  - Also: Marked the `Lists.BinarySearch(...)` utils (the slow binary search impl previously used by Primes.cs) as `[Obsolete]`, so that others are warned of the costs.

#### (editor-only)
- Added: Several previously Runtime-only unit tests can now _also_ be ran as Editor tests. (`PrimesInEditor`, `HashMapsInEditor`)
- Added: Menu items under "Ore/JsonAuthority/\*" for setting the global `JsonAuthority.PrettyPrint` setting in the editor.
  - Note: might be temporary until we get a proper config window for Ore happening.
- Fixed: Headaches caused by some of the custom inspector drawers for Ore types (chiefly VoidEvent, DelayedEvent etc).
- Fixed: `TypeMembers.TryGetSerializableField(...)` was broken for private serializable fields in base classes.


## [v7.3.0-pre](../../tags/v7.3.0-pre) - 2023-05-04

**Ignore this release, you should move on to major version v8.0 or else stick with v7.2.2.**


## [v7.2.2](../../tags/v7.2.2) - 2023-05-02 (again)
- Fixed: `MiniJson` reflective parsing has been improved and fixed to support over-stringified data types (such as `"1"` -> `true`).
- Fixed: (editor-only) Rare edge case causing `EventDrawer` to screw up serialized events after a Reset() is called on their owning object.


## [v7.2.1](../../tags/v7.2.1) - 2023-05-02
- Added: utility methods in `TimeInterval`: Clamped(min,max), AtLeast(min), AtMost(max).
- Added: static bool property in `ActiveScene`: IsQuitting
- Added: [new unit test](Tests/Runtime/AsyncTests.cs) that proves sharing "wait" objects (WaitForSecondsRealtime) results in undefined runtime behaviour.
- Added: Customize JSON (de)serialization per-class by implementing 1 or 2 simple interfaces: `IReadJson`, `IWriteJson`.
  - Also: `SerialVersion` and `TimeInterval` become the first implementors for these interfaces.
  - Note: supplying custom JSON behaviour via these interfaces will _most likely_ significantly improve amortized JSON performance.

- Changed: TimeInterval.SmolParse - now assumes ticks (instead of frames) if no unit is specified.
  - Also: Improved: SmolParse recognizes 'µ' (previously, only 'u') as a suffix for microseconds.

- Improved: `JsonAuthority` APIs which should not be used with UnityEngine.Object types now throw informative errors if it happens.

- Fixed: `MiniJson` can now serialize / deserialize direct references to UnityEngine.Object types.
- Fixed: `Promise<T>` events now guard against accidental infinite recursion.
- Fixed: (editor-only) `PropertyDrawerState` caused certain property drawers to flicker.


## [v7.2.0](../../tags/v7.2.0) - 2023-05-01

#### JSON-related API
- Changed: Most of the primary API has been refactored (renamed), though the old names / signatures still remain as [Obsolete]'d APIs.
  - These are the new names (list is intentionally shorter due to compressing overloads):
  - Write(...) - formerly Serialize(...), SerializeTo(...)
  - Read(...) - formerly Deserialize(...), DeserializeObject(...), DeserializeStream(...)
  - ReadDictionary(...) - formerly DeserializeObject(...)
  - ReadArray(...) - formerly DeserializeArray(...)
  - TryRead\<T\>(...) - formerly TryDeserialize(...), TryDeserializeStream(...)
  - TryPopulate\<T\>(...) - formerly TryDeserializeOverwrite(...), TryDeserializeStreamOverwrite(...)
  - The deprecated APIs may be removed in future major versions.
- Also: The above renames additionally remove the latter 2 optional parameters "mapMaker" and "listMaker" from method signatures. The proper way to supply these parameters is now via one of two `JsonAuthority.Scope` constructors in a `using (...)` scope.
- Changed: in `Filesystem`: Try\*Object(...) are now soft-deprecated, and will simply pass their parameters to Try\*Json equivalents.
  - The deprecated APIs may be removed in future major versions.
- Changed: enum value `JsonProvider.Default` is now deprecated. See instead: constant `JsonAuthority.DefaultProvider`.

- Added: enum value `JsonProvider.JsonUtility` - provides __some__ APIs with an implementation using Unity's built-in `JsonUtility` module. APIs whose internal implementation cannot be supported by JsonUtility have been clearly marked, and will fallback to JsonAuthority.DefaultProvider if accidentally invoked.
- Added: `JsonAuthority.ReflectFields<T>(data, out T) -> bool` - shouldn't need to use this directly, but provided in case you do.
- Added: `JsonAuthority.IsValidJson(string) -> bool` - recommended for debug or edit-time validation.

- Fixed: in `MiniJson`: methods which call ReflectFields(...) now support deep-reflected type deserialization, and more edge cases.

#### the rest
- Added: `Bitwise.ToHexString(this byte[])` - fast hexification of raw byte data. Used to speed up Hashing.MD5(string).
- Added: in `DateTimes`: "From" converters for each "To" utility. For example:
  - `DateTimes.FromISO8601(string) -> DateTime`
  - `DateTimes.FromUnixSeconds(double) -> DateTime`
  - etc.
- Added: extension method `IDictonary<K,V>.Find<T>(K key, out T item) where T : V` (maybe a mouthful to read the signature, but much more useful than it looks!)

- Improved: Hashing.MD5 is now more than twice as fast as before.

#### the rest of the rest (Editor-only edition)
- Added: Menu items under `"Ore/JsonAuthority/*"` to help set / test / debug the JsonAuthority.

- Fixed: PropertyDrawerState now ticks down properly in Unity 2020.
- Fixed: TimeIntervalDrawer works properly for array elements (nested or not).


## [v7.1.1](../../tags/v7.1.1) - 2023-04-30

#### MD5 hotfix
- Fixed: repeat use of `Hashing.MD5` no longer throws disposal issues.


## [v7.1.0](../../tags/v7.1.0) - 2023-04-25

#### In addtion to the below section for `v7.1.0-pre`:
- Added: `Hashing.MD5(string) -> string` - implements a standard MD5 hashing algorithm you can use to hash, for instance, web request parameters.
- Added: `Promise.FailWithBrief(string)` - allows you to supply a briefer explanation of what went wrong than an exception.
- Changed: Errors logged due to invalid use of `Promise` objects have been demoted to warnings.


## [v7.1.0-pre](../../tags/v7.1.0-pre) - 2023-04-21
- I am "releasing" this tag as a preview of v7.1, but will not merge to `stable` just yet.

#### Added: `RuntimeSamplers`
- An _optional_ OAssetSingleton (not auto-created).
- Create normally via the Create Asset menu.

Currently has 2 primary types of samplers:
1. Named, configurable duration timers that can respect (subtract) user multitask durations.
  - Intended to perhaps provide a better time_sec definition, among other uses.
  - API (currently) works entirely through static methods on the class
2. `CoarseSampler` - only available in Unity 2020+
  - Can sample a variety of perf metrics (like RAM) at runtime, reporting back aggregated values **ready for logging**.
  - Utilizes Unity's ProfilerRecorders in the safest, most performant, and most accurate way we know how to do.
  - API (currently) works entirely through serialized values on the RuntimeSamplers asset. A code-side API is in the works.

Also: Added: static class `ProfilerStat` consisting of string constants, which are valid for you to use to initialize your own ProfilerRecorders.

#### `CodeJudge` updates
- Added: Now also records a mono heap allocation (RAM) metric reported in bytes.
- Changed: The internal timing mechanism has reverted to using a System.Diagnostics.Stopwatch, albeit with a different approach.
  - Fixed: Due to this, timing metrics are more accurate due to a 10x higher sampling frequency.
  - Fixed: Also do to this—and I have no idea why this is the case—CodeJudge scopes now work as expected inside iterator ("yield return") methods, such as coroutines.
- Added: Some rough unit tests to judge/utilize CodeJudge's ability to judge.
- Added: In debug, you now get warned when a CodeJudge scope executed too quickly to record a time delta (i.e. in the case of no code in the scope).
- Fixed: `ReportJson(...)` - now respects JsonAuthority.PrettyPrint, despite not otherwise using JsonAuthority :yum:.
- Improved: `ReportOnGUI(...)` - now scales based on the current screen width in order to print more readable text on any screen resolution / DPI.

#### `ActiveScene` updates
- Changed: `GetSceneAge(...)` - all overloads now return TimeIntervals instead of float seconds.
  - Note: Shouldn't be breaking for anyone, since TimeIntervals are implicitly castable to float seconds.
  - Also: Fixed: Scenes that have been unloaded properly return an age of 0.
- Fixed: runtime listeners subscribed to OnFixedUpdate or OnLateUpdate _before_ the ActiveScene singleton has finished initializing are now properly passed on once the singleton _does_ init.

#### the rest
- Added: `SceneLord.AddSceneBuildOnly(int)` - only loads the given scene additively if the player is not in the editor.
- Added: `TimeInterval` - now supports a new Unit: Microseconds!
  - Also: Changed: —as it is useful in the context of code clocking—`CodeJudge` now reports time in microseconds by default.
- Added: `DeviceSpy.DPI` - identical to UnityEngine.Screen.dpi, however this one can be manipulated by _LittleBirdies_...
  - Also: DeviceDimension.PixelDensity, when queried (perhaps i.e. for LAUD), will use DeviceSpy's DPI value rather than Unity's.
- Fixed: `WaitForFrames` - now properly resets itself after being used to completion.
- Improved: `Parsing`.
- Improved: cleaned up a lot of smol snippets here and there & applied new styleguide when convenient.

#### the rest of the rest (Editor-only edition)
- Changed: `FoldoutHeader` class has been structified. Should work exactly the same as before.
- Fixed: `PropertyDrawerState` now gets ticked properly again after its first stale-clear.
- Fixed: `JsonAuthorityInEditor` - several unit tests were broken, and are not broken anymore.


## [v7.0.2](../../tags/v7.0.2) - 2023-04-18 (again)
#### DeviceSpy.GeoIP hotfix
- **Fixed:** the `Fetch` implementation was still using Newtonsoft to parse server response JSON. It now properly uses JsonAuthority.
- Also:
  - Added: runtime unit tests in [GeoIPService.cs](Tests/Runtime/GeoIPService.cs).
  - Added: property IsFetching, method AbortFetch() to DeviceSpy.GeoIP
  - Fixed: edge case where geo strings might've been returned as string.Empty when null was expected.

#### the rest
- Ignore Vectors.cs. It may be removed and does not count as part of the public API.


## [v7.0.1](../../tags/v7.0.1) - 2023-04-18
#### AssetInspector hotfix
- Fixed the issue where custom Asset types (PhoenixCloud.asset, Orator.asset, etcetera) were uneditable.


## [v7.0.0](../../tags/v7.0.0) - 2023-04-17
#### Breaking API changes
- Changed: `JsonAuthority` and `Filesystem.Try*Json` no longer work the same way w.r.t. using Newtonsoft.Json types.
  - All the same behaviour is available as before, but some now has to be accomplished via using the JsonProvider + NewtonsoftAuthority APIs.
  - If Newtonsoft is available, **it is still the default JsonProvider,** unless you add the scripting define symbol `PREFER_MINIJSON`.
  - (You can also change the default JsonProvider at runtime; see JsonAuthority.{Scope,TrySetProvider}.)
- Changed: renamed `JsonAuthority.ProviderScope` -> `JsonAuthority.Scope`.
- Changed: renamed `SerialDeviceDecider` -> `DeviceDeciderData`.
- Changed: renamed several methods in `DeviceDecider`.
  - Also: Changed: access modifiers for several members.

#### JsonAuthority updates
- Added: static APIs that support reflecting on custom types
  - DeserializeObject(string, Type)
  - TryDeserialize\<T\>(string, out T)
  - TryDeserializeOverwrite\<T\>(string, ref T)
- Added: static APIs that support dealing with streams (TextReader, TextWriter) instead of raw strings
  - SerializeTo(stream, data)
  - DeserializeStream(stream[, Type])
  - TryDeserializeStream\<T\>(stream, out T)
  - TryDeserializeStreamOverwrite\<T\>(stream, ref T)
- Added: static property `bool PrettyPrint`
- Added: new constructors & functionality in JsonAuthority.Scope: now supports prettyPrint scopes.
- Also: cleaned up the code in `Filesystem.Try*Json`; the default APIs utilize JsonAuthority instead of Newtonsoft.
- Note: again, if you have Newtonsoft.Json linked, compile with `PREFER_MINIJSON` to _not_ use Newtonsoft by default.

#### Added: OAssetInspector
- Replaces the default inspector used for all `Ore.Asset` derivatives.
- Offers various enhancements for your editing pleasure.
- Removed: fields from the `Asset` base class are now handled by the new inspector:
  - m_IsRequiredOnLaunch
  - m_HideFlags

#### Added: struct CodeJudge
- Allows you to easily measure blocks of code speed, and associate + aggregate the results by a given identifier string.
- Easiest method of reading results:

#### the rest
- Added: new struct StringStream - Serves as a proxy interface joining the StringBuilder and TextWriter APIs.
- Added: new APIs in `HashMap` support ref-returns:
  - method FindRef(key, out bool found) -> ref V
  - property in HashMap.Enumerator: CurrentValueRef -> ref V
- Added: event hooks in `ActiveScene`:
  - OnActiveSceneChanged
  - OnFixedUpdate
  - OnUpdate
  - OnLateUpdate
- Added: static API in `Filesystem`: TryDelete(FileInfo)
- Added: static APIs in `RecycledStringBuilder`: Borrow(), Return(builder)
- Added: static APIs in `Strings`:
  - AreEqualIgnoreWhitespace(a, b)
  - CompareIgnoreWhitespace(a, b)
  - Compare(a, b, ignoreChars[])
- Added: `TimeInterval`.ToString(units[, numberFormat, formatProvider]) - converts the interval to a string with the given unit conversion and format options.

- Changed: TimeInterval.ToString() now detects units and returns a string in those units.
  - Note: this string could be parsed back into a TimeInterval using the static helper TimeInterval.SmolParse(string).

- Fixed: TimeInterval property setters now work properly when m_AsFrames=true
- Fixed: text-reading APIs in `Filesystem` (TryReadText, TryReadLines) now properly detect and discard byte order marks (BOMs).

- Improved: cleaned up `DeviceDecider`


## [v6.1.2](../../tags/v6.1.2) - 2023-04-11

#### Hotfix
- Fixed: compilation errors in Unity 2019 (SerializedProperties.cs).


## [v6.1.1](../../tags/v6.1.1) - 2023-04-11

#### Compilation Hotfix
- Fixed: #47 - build compilation errrors due to lazy lack of `#if UNITY_INCLUDE_TESTS` in RecycledStringsBuilder.cs.
- Fixed: #46 - compilation errors in Unity 2019 (WebRequests.cs).

#### other stuff
- Added: new extensions to `SerializedProperty`:
  - IsNonReorderable()
  - IsReadOnly()
  - TryGetFieldInfo(out FieldInfo)
  - (these become useful in v6.2)


## [v6.1.0](../../tags/v6.1.0) - 2023-04-06

#### JsonAuthority updates
- Changed: Moved everything Newtonsoft.Json-related from JsonAuthority to a separate, conditionally-compiled class: `NewtonsoftAuthority`.
  - Note: The old method public interface remains honored, although the following signatures have been marked as Obsolete:
    - GenericParse(...) -> IDictionary
    - Genericize(...) -> IDictionary
    - Genericize(...) -> IList
    - The new class NewtonsoftAuthority now holds the proper implementation for the above.
  - Note: The old _property_ public interface, however, could not continue to be honored. should only affect Irontown though, so it's fine :wink:
- Added: a modified version of **MiniJson**! (#43)
- Added: The ability to choose which JSON library you want JsonAuthority to utilize:
  - enum `JsonProvider`
  - JsonAuthority.TrySetProvider(JsonProvider)
  - using class `JsonAuthority.ProviderScope`
- Added: Unit tests in `JsonAuthorityInEditor`

#### the rest
- Added: IDisposable struct `RecycledStringBuilder` - use it to safely reuse System.Text.StringBuilders and be more allocation-friendly.
  - See smol working example in Tests/Editor/MiscInEditor.cs
- Added: `ICoroutineRunner.IsRunning(key) -> bool`
  - Also: Removed: ICoroutineRunner.HaltAll() - never was useful in the public interface, and posed some signficant dangers.
- Added: `EditorBridge.Ping(object)` - useful for debugging, and you can call it anywhere without worrying about `#if UNITY_EDITOR` blocks.
- Changed: Moved the serialized property "Is Required On Launch" from the `OAssetSingleton` class to the parent Asset class, thus allowing even non-singleton assets to easily mark themselves for preload.
- Fixed: Edge case runtime NRE related to Preloaded Assets, and OAssetSingletons not being properly registered therewith due to someone forgetting to commit ProjectSettings.asset.
  - See also: upm/phoenix-unity#52



## [v6.0.0](../../tags/v6.0.0) - 2023-04-03
- Note: Gonna try to do alphabetical order (by full path) here; there's a lot.

<details>
  <summary>
  Improved: The following classes gained some XML documentation love: <b>(click expand)</b>
  </summary>
  _(and [Tooltip] for some, too!)_
  <ul>
    <!-- git diff --name-only unstable..stable -- '*.cs' | while read f; do echo "<li>[$f]($f) ($(git diff unstable..stable -- "$f" | grep -E '///' | wc -l) lines)</li>" ; done | grep -v '(0 lines)' -->
    <li>[Abstract/OAsset.cs](Abstract/OAsset.cs) (71 lines)</li>
    <li>[Abstract/OAssetSingleton.cs](Abstract/OAssetSingleton.cs) (74 lines)</li>
    <li>[Abstract/OComponent.cs](Abstract/OComponent.cs) (61 lines)</li>
    <li>[Abstract/OSingleton.cs](Abstract/OSingleton.cs) (105 lines)</li>
    <li>[Attributes/AutoCreateAssetAttribute.cs](Attributes/AutoCreateAssetAttribute.cs) (17 lines)</li>
    <li>[Runtime/HashMap+Collections.cs](Runtime/HashMap+Collections.cs) (5 lines)</li>
    <li>[Runtime/HashMap.cs](Runtime/HashMap.cs) (13 lines)</li>
    <li>[Runtime/Promise.cs](Runtime/Promise.cs) (5 lines)</li>
    <li>[Runtime/TimeInterval.cs](Runtime/TimeInterval.cs) (20 lines)</li>
    <li>[Runtime/VoidEvent.cs](Runtime/VoidEvent.cs) (14 lines)</li>
    <li>[Static/DeviceSpy.cs](Static/DeviceSpy.cs) (82 lines)</li>
    <li>[Static/OInvoke.cs](Static/OInvoke.cs) (63 lines)</li>
    <li>[Static/WebRequests.cs](Static/WebRequests.cs) (32 lines)</li>
  </ul>
</details>

- In `Asset<T>`:
  - Added: methods `DestroySelf()`, `DestroyOther(Object)` (for UnityEvent sugar).
  - Improved: `TryCreate<T>` validates that the type(s) you give it are properly assignable _before_ a `System.InvalidCast` or other exception can get thrown.

- In `OAssetSingleton<T>`:
  - Added: Instance properties: `IsRequiredOnLaunch{get;}`, `IsReplaceable{get;set;}`
  - Added: Yet another alias for Instance: `Agent` :dizzy_face:
  - Changed: Renamed serialized field `m_OnAfterInitialized` -> `m_OnFirstInitialized`, for clarity and parity with OSingleton.
  - Changed: The final reason why `TryInitialize()` might return false is if both are true: (1) "On First Initialized" event has been enabled, and (2) the event was NOT able to be queued for invokation. Thus, failure here most likely indicates a configuration error.
  - Fixed: Edge case when TryGuarantee would create a new instance, and try to initialize it twice.

- In `IComparator<T>`, `UnitySavvyComparator`:
  - Added: New interface `IUseComparator<T>` - primarily used to compare sets of HashMap\<K,V\> keys with each other, even if they come from HashMaps of different generic V types.
  - Changed: `UnitySavvyComparator` is now generic `UnitySavvyComparator<T>`, and implements IComparator\<T\> (_in addition to_ Comparator\<object\>).
    - Also: Added: `UnitySavvyComparator` (not generic), inherits from and de-genericizes `UnitySavvyComparator<Object>`. As before, this is likely the one you want.
  - (detail) Added: `bool IComparator<T>.Equals(object otherComparator)`, which doesn't require explicit implementation in inheritors since the signature matches object.Equals(object).

- In `OComponent`:
  - Changed: `Destroy{Self,GameObject}()` no longer takes the optional parameter "inSeconds". OInvoke.AfterDelay(...) might help you instead.
  - Added: `DestroyOther(Object)`

- In `OSingleton`:
  - Removed: Static properties `IsDontDestroyOnLoad{get;}`, `IsReplaceable{get;}`, `IsValidWhileDisabled{get;}`
  - Added: Instance properties `IsDontDestroyOnLoad{get;}`, `IsReplaceable{get;set;}`, `IsValidWhileDisabled{get;set;}`
  - Added: `OnValidate()` - toggling "Dont Destroy On Load" in play mode properly toggles which scene owns the singleton's GameObject.
  - Fixed: The "On First Initialized" event now disables itself after its first successful invoke, to properly enforce "first"ness.

- In `AutoCreateAssetAttribute`:
  - Added: Constructor taking bool 'doIt', with default value = true.
  - Note: This allows one to actually DISABLE the default auto asset creation associated with OAssetSingleton\<T\>s, specifically.

- In `HashMap<K,V>`:
  - Changed: Renamed nested type `KeyCollection` -> `KeySet`, which now predominantly implements ISet\<K\>.
    - Added: ^ which means a bunch of handy new ISet methods are now available.
  - Changed: Exposed the nested types KeySet and ValueCollection as the default (public) return types for HashMap.Keys and HashMap.Values.
  - Added: HashMap.Keys.MakeWriteProxy() _(advanced usage)_ - allows you to treat a HashMap's set of keys as a write-enabled proxy for the HashMap itself.
    - E.G. removing elements from the proxied KeySet also removes elements from its parent HashMap.
    - If you forget to call MakeWriteProxy() before you modify the KeySet, expect to see NotSupportedException("KeySet is read only.") thrown.
    - Also: Changed: KeySet ICollection methods which previously threw NotSupportedExceptions now have working implementations, iff the KeySet is a "write proxy". They still throw them if MakeWriteProxy() was never called.
  - Added: Generic method `bool Find<VCasted>(key, out casted) where VCasted : V` - allows for much nicer handling when `V` is, say, type `object`, and you want to pull a bunch of strings, ints, bools, whatever out of the HashMap.
  - Removed: HashMap.WithValueComparator(cmp) - nobody I know uses it, and you're better off using plain-ol' object initializer syntax ("new HashMap { ValueComparator = cmp }").

- In `Orator`:
  - Fixed: **Compilation error** when Newtonsoft.Json is unavailable.
  - Fixed: NullReferenceException when feeding null messages to Orator.Log(...) with a non-empty OratorFilter.MessageRegex active.
  - Changed: Renamed field "m_Filters" to "m_IgnoreFilters" for clarity.
  - Improved: The way OratorFilters are checked is now much more efficient at runtime.
  - Improved: Addresseed all compiler and ReSharper warnings. (Also: did the same for Static/OAssert.cs)

- In `VoidEvent`:
  - Added: Explicit constructors for specifying default enable state and/or an initial runtime listener (UnityAction).
  - Added: Operators + and -, with right-hand operands of type UnityAction. (included for `+=` and `-=` syntax sugar.)
  - Invoke() / TryInvoke() now catch exceptions and send them to Orator.NFE(...).
  - Also: TryInvoke() will return false in the event of an exception. For now, this behaviour is unique to VoidEvent.

- In `DeviceSpy`:
  - Removed: DeviceSpy.PromiseCountryFromIP(...)
  - Added: nested static class `GeoIP` (see next section).

- In `DeviceSpy.GeoIP` (new):
  - `Fetch([timeout])` - essentially just PromiseCountryFromIP(...), renamed. Still returns a Promise\<string\>.
    - Also: Added: Fetched value gets cached, persists across sessions, and will become the new default value for DeviceSpy.CountryISOString in subsequent sessions.
    - Also: Improved: Has better global state handling, safety, and accessibility when utilized by disjoint dependents.
  - `FetchIfStale(staleAfter[, timeout])` - **most callers should prefer this variant of Fetch().** Won't actually spawn a new web request if we already have a cached geoip value that hasn't gone stale yet.
  - `Reset()` - mostly a debug helper, resets the runtime and serialized state of GeoIP back to their defaults.
  - Properties: `CachedValue`, `LastFetchedAt`, `TimeSinceLastFetched`.

- In `WebRequests`:
  - Changed: Renamed `Promise(...)` -> `PromiseText(...)` for specificity of expected usage (to download some text).

- Changed: Renamed static class `SceneObjects` -> `UnityObjects`.
  - Also: Added: Wrappers for Object.{Destroy,DestroyImmediate}() that remove the hassle of knowing which one to use.


## [v5.3.0](../../tags/v5.3.0) - 2023-03-17
- In `JsonAuthority`:
  - Added: GenericParse(rawJson)
  - Changed: return type and optional parameters for Genericize(...) now specify IDictionary instead of HashMap.
    - Note: Thanks to contravariance in C#, this works as-is with the more qualified 'maker' delegates.
  - Improved: Exceptions are used for truly exceptional circumstances. (removes a "ping Levi?" Orator log)

- In `Promise<T>`:
  - Added: event OnCompleted - parameterless callback action that is called after the state changes from "pending" to something else, ignoring the success vs failure distinction.
  - Added: FailWith(T value) overload - sets a "flotsam" value to the promise before marking it as a failure.
  - Added: AwaitBlocking(), AwaitCoroutine([out key]) - handy methods for awaiting a promise. Often called after callbacks are all set.
  - Added: ToString() override (safely prints Value).
  - Changed: The following void methods now return the promise itself (`return this`):
    - Maybe(value)
    - Complete()
    - CompleteWith(value)
    - Forget()
    - Fail()
    - FailWith(...)
  - Changed: Forget() can now be called from any state.
  - Changed: It is now only an error to call Fail() / FailWith(...) if the promise has already _succeeded_; specifically, it is no longer an error to call these methods if the promise has been forgotten.
  - Changed: CompleteWith(value) can now be called even if the promise previously succeeded, allowing you to update the value of the promise.
    - Note: Doing so from within an OnSucceeded delegate will NOT propagate the updated value to the parameter passed to further delegates in the same invocation.
  - Changed: (impl. detail) Dispose() now calls MoveNext() once before disposing, in an attempt to ensure relevant events have been invoked.
  - Improved: Added inline XML doc comments.

- Added: in `DeviceSpy`: PromiseCountryFromIP() - uses an internal KA service to try and guess the user's country from their IP address.
  - Note: This is Ore's first usage of UnityWebRequest.
  - Note: If Ore ever goes public, this code will need to be scrubbed out.

- Added: static utility `WebRequests` (for UnityWebRequests):
  - extension request.Succeeded() - necessary to use the right APIs for your Unity version
  - extension request.GetErrorInfo()
  - extension request.Promise([errorSubstring]) - also nicely handles disposing

- Added: Test in `MiscInEditor`: ActionNullability - proves how delegate nullness works


## [v5.2.2](../../tags/v5.2.2) - 2023-03-09
- In `DeviceSpy`:
  - Fixed: Runtime Java error "no non-static method with name='resolveActivity'..." (in CalcAndroidBrowser()) (see: #37).
  - _fun fact: the JNI bridge **does** care if you explicitly cast `int` <-> `long` or not..._

- In `Promise<T>`:
  - Added: Now inherits from IDisposable. This doesn't really do anything meaningful unless `T` (the promised value's type) also implements `IDisposable`.
  - Added: (smol thing) You can now squelch the default OnFailure callback without having to reconstruct the Promise.


## [v5.2.1](../../tags/v5.2.1) - 2023-03-08 (later)
- In `Orator`:
  - Added: `OratorFilter`s may now be supplied to the Orator instance (Resources/Orator.asset), allowing you to filter logs by type or message regex.
  - Changed: Destroying the Orator instance will now hush all Orator logs, unless you define `AGGRESSIVE_ORATOR`.

- Fixed: Test assembly not compiling in Unity 2019 ([PrimesCorrectness](Tests/Runtime/PrimesCorrectness.cs)).


## [v5.2.0](../../tags/v5.2.0) - 2023-03-08
- In `HashMap`:
  - Added: overload `Map(key, value, overwrite)` - allows you to pass a parameter to specify overwriting policy. (works nicer with algorithms.)
  - Changed: `UnmapNulls()` -> Now uses the map's KeyComparator (or the default Comparator for \<V\>) `IsNone(v)` definition to determine which entries to unmap.

- In `Orator`:
  - Fixed: Squelched warning CS0414 for deprecated field.


## [v5.1.2](../../tags/v5.1.2) - 2023-03-04 (later)
- Fixed: null implication of generic parameter V in HashMap\<K,V\>.
  - Note: the API in question (HashMap.UnmapNulls()) will probably need to be renamed in the future, due to this change in internal logic.

- sorry Henru


## [v5.1.1](../../tags/v5.1.1) - 2023-03-04
- In `Orator`:
  - Added: stubs for a Orator log filtering system (will have to make it into next release).
  - Changed: Setting the Orator.asset variable "Log Once Memory Size" to 0 now properly disables the fancy "log once" behavior. You may opt to do so to save on performance, although most of the savings will be in editor rather than runtime.
  - Changed: `NFE(ex, ...)` - Exception parameter 'ex' is now null-safe.
  - Removed: variable "Force Assertions In Release" - it has merely been hidden (for now?), because it has actually never been fully implemented. It might make a come-back if there ever is a need/desire for the functionality.

- In `Promise<T>`:
  - Added: new method `Forget()` - marking neither success nor failure, the Promise is nevertheless treated as "Completed"—no value to show for it, nor any Exception or mark of erroneous behaviour. The promise was simply... _forgotten_.
  - Changed: IEnumerator.Reset() has been made a public function, because why shouldn't you be able to reset a promise? :)
  - Removed: The InvalidAsynchronousStateExceptions that were being thrown in `Fail()`/`FailWith(ex)` if called after the Promise was already completed (either successfully _or_ by forgetting it).

- Fixed: `TimeInterval.ThisSession` was using Time.realtimeSinceStartupAsDouble – a property which only entered the API since Unity 2020, so now if in Unity < 2020, Time.realtimeSinceStartup is used instead.
- Fixed: `DeviceSpy.CalcBrowser()` _actually_ should've been branching based on device API lvl, not target API lvl.


## [v5.1.0](../../tags/v5.1.0) - 2023-03-03
- Added: new "fakesync" class: `Promise<T>` - works similarly to System.Threading.Tasks but more optimal for Unity (and coroutines).
  - Also: Usage example(s) in [`AsyncTests`](Tests/Runtime/AsyncTests.cs).
- Added: new exception class: `MultiException` - use MultiException.Create(...) to glob multiple exceptions into one.
  - Note: Currently just creates a linked list with the built-in "InnerException" interface. I acknowledge this implementation has room for improvement.
- Added: new exception class: `FauxException` - use FauxException.Silence(Exception) (or new extension method Exception.Silenced()) to cause a deferred exception to be ignored.
  - Note: Currently will only be ignored by `Orator.NFE(exception)`. Future patches may implement something with `System.AppDomain.CurrentDomain.UnhandledException` or `UnityEngine.Application.logMessageReceived` to squelch further.
- Added: in `Orator`: static overloads that take a System.Type (or generic \<T\>) as a context.
  - Also: Changed: lower-case instance methods are now private, since you can call static methods from UnityEvents these days.
- Added: in `Filesystem`: GetTempPath([forFilepath])
  - Also: Added new optional parameters to TryGetLastException(...) + LogLastException(...). 

- Changed: Moved around some menu items under <kbd>Bore</kbd>.
  - Note: Some were removed and reimplemented as unit tests.
- Changed: `ActiveScene`'s default execution order lowered, from -500 -> -1337 (same bucket as Orator).

- Improved: Cleaned up `OAssert` (-200 lines), yeeted semi-circular logic tied to Orator.

- Fixed: (#35) DeviceSpy.Browser throwing AndroidJavaException on Android API < 33.
- Fixed: (#36) Unity 2019 incompatibilities which were introduced in release [v4.0.0][] (DeviceFactor.cs).
  - Also: Removed static nested functions from JsonAuthority.cs (_also_ incompatible with Unity 2019).
- Fixed: in `DeviceSpy`: CalcAndroidIDFA now actually works `~.~`


## [v5.0.0](../../tags/v5.0.0) - 2023-02-28
- Added: New attributes to replace (some of) Odin Inspector's:
  - [Required], [RequiredIn]
  - [DisableIf], [EnableIf]
  - [HideIf], [ShowIf]
  - [OnValueChanged]
  - [FoldoutGroup]
  - [ToggleGroup]
  - **Note:** These are currently just placeholders. They do not (currently) do anything special. The implementation for them is destined for a separate package: [ore-inspector][].
  - Also: Added enum `PrefabKind` (to match Odin's [RequiredIn] public interface).
- Added: `JsonAuthority.Serialize(obj, ...)`
  - allows you to be a lot more GC-friendly, reusing memory.
- Added: New unit test source file: [MiscInEditor.cs](Tests/Editor/MiscInEditor.cs)

- Changed: **Breaking!** Renamed class attribute `[AssetPath]` -> `[AutoCreateAsset]`.
  - it's both more descriptive of its function, _and_ collides less with attributes from other packages.
- Changed: Now keeping all non-nested enums in the `Static/` subdirectory.

- Removed: `Editor/ReadOnlyDrawer.cs` (has been moved to [ore-inspector][]).
  - Yes, this means that without this other package, the old [ReadOnly] attribute will become a no-op. Given how many packages add their own [ReadOnly] attribute, this was an intentional decision.


## [v4.1.0][] - 2023-02-27
- Changed: in `DeviceSpy`:
  - Browser - on WebGL builds, now returns the browser name _and_ its version number. Space-separated, parsed well by SerialVersion.
  - Also: Added: LittleBirdie.OnCheepCheep - callback event triggered whenever a LittleBirdie override is made.

- Improved: in `ActiveScene`:
  - Deprecation messages now point to the correct API to use (ActiveScene.Coroutines). No more wild goose chase!
  - Also: Fixed edge case where CoroutineRunnerBuffers were only checking key equality by reference, whereas they should've been calling object.Equals(a,b).
  - Related: `CoroutineRunner`.AdoptAndRun() now defends against the case where it's disabled in the hierarchy. (This is mainly important because it's technically in the public API.)

- Fixed: in `SerialVersion`:
  - ToString(true) - was including the tag hash in the reconstructed version.
  - ExtractOSVersion(ver) - now works regardless of scripting defines, looking for patterns in the parameter to determine what platform it's for.
  - Also: ExtractOSVersion now returns more standardized representations, which contain maximal extra info (such as platform prefixes).

- Fixed: in `HashMap`:
  - Union(other) was entirely broken (by not incrementing the internal entry count).
  - Union(other, overwrite:true) wasn't respecting a ValueComparator (if one was set).
  - Also: Added UnmapNulls() - QoL method


## [v4.0.1][] - 2023-02-24
- Added: New in `Strings`:
  - Constants: LOWERCASE, UPPERCASE, DIGITS, ALPHA, ALPHANUM, HEXADECIMAL
  - Utility method: ContainsOnly(str, char[])
  - Also: Trimmed the existing WHITESPACES array, and made sure all arrays are pre-sorted (ordinal order).
- Changed: DeviceSpy.Brand + DeviceSpy.Model now return an empty string when the info is unavailable.
  - Formerly returned "n/a".
- Improved: SerialVersion.ExtractOSVersion() internal logic.
  - Note: should also now keep more of the original string in the underlying SerialVersion returned, if it can be preserved without breaking the existing deserialization code.


## [v4.0.0][] - 2023-02-23
- Removed: Hard dependency on "com.unity.nuget.newtonsoft-json" v3.0.2 (package.json).
  - However, without it in the project, several APIs become unavailable or nonfunctional.
  - If you have a different Newtonsoft Json.NET provider in your project, you may try telling Ore to utilize it by adding `NEWTONSOFT_JSON` to your script compilation symbols (in <kbd>Project Settings</kbd> -> Player).
  - Please inform @levi.perez or [create an issue][] if you have any trouble with this.

- Added: Absorbed the [Decisions](https://leviperez.dev/upm/decisions) package (AKA "LAUD", now archived) into the Ore namespace, most notably adding the `DeviceDecider` data structure.
  - Also: Removed the original package's pointless interfaces (IEvaluator, IDecider), renamed \*Evaluator to \*Factor
  - Also: DeviceDeciders can now accept multiple pipe-separated (`|`) keys per serialized row, for instance to give a list of device models the same discrete value.
  - Also: Updated the custom editor drawers from the old package to contain more useful displays (curves).
- Added: New in `DeviceDimension`:
  - Enum values: AspectRatio, DisplayHz, IsBlueStacks, ThresholdRAM
  - Also: Fixed: DeviceDimension.ReportedGeo now queries its (still makeshift) runtime value from DeviceSpy.
- Added: New in `DeviceSpy`:
  - Properties: `UDID` -> may have a different value from IDFV, and is more safe from SystemInfo.deviceUniqueIdentifier returning "n/a" e.g. on WebGL.
  - Advanced API: nested class `LittleBirdie` -> allows you to modify the DeviceSpy's perception of the current device.
- Added: New in `TimeInterval`:
  - Constants: Minute, Hour, Day, Week
  - Method: Yield()
- Added: New in `DateTimes`:
  - Properties: Today, Yesterday, Tomorrow
  - Note: Unlike any System.DateTime equivalents, these implementations return UTC time instead of local time.
- Added: New in `HashMap`:
  - Copy constructor
  - Methods: MapAll(), Remap()\*, Union(), Intersect(), Except(), SymmetricExcept()
  - Also: Changed: \*The following old methods have been renamed for clarity of function:
    - Remap(K,V) -> OverMap(K,V)
    - TryMap(..., out V) -> Map(..., out V)
  - New method in `HashMap.Enumerator`: RemapCurrent(V) - allows inserting new values at existing keys while manually enumerating over a HashMap.
    - Also: Fixed: Enumerator now enforces that only one instance can modify the same HashMap at a time.
  - Also: Fixed: HashMap.KeyComparator throws an exception if it is changed in a non-empty HashMap.
  - Also: Improved: Consolidated & simplified old constructors.
  - Also: Improved: `Bucket` struct now uses aggressive inlining.
- Added: New in `Strings`: extension methods Coerce(), NullCoerce()
- Added: New in `Filesystem`: utility method GetFiles(path)
- Added: New in `JsonAuthority`: utility methods FixupNestedContainers(), Genericize()
  - Also: Fixed: JsonAuthority was initializing too late (or not at all in editor).
- Added: New in `Paths`: utility methods ExtractExtension(), DetectAssetPathAssumptions()
- Added: Unit tests:
  - `DeviceSpyInEditor`
  - `FilesystemInEditor`
- Added: Some new inline XML documentation for:
  - Runtime/HashMap.cs   (complete)
  - Static/Filesystem.cs (incomplete)
  - Static/Invoke.cs   (incomplete)
  - Static/Strings.cs  (incomplete)

- Changed: `SceneLord` API names are shorter without sacrificing descriptiveness.
  - Also: Added: SceneLord.AddActiveScene(buildIndex)
- Changed: `DelayedEvent` finally utilizes TimeIntervals and DelayedRoutines.
  - Also: now guards against additional invokes if the first invoke is still counting down its delay.
  - Also: Added: TryInvokeOnGlobalContext(), TryCancelInvoke()
  - Also: Fixed: DelayedEvent invocation payload is now much more exception-safe, exiting gracefully.
- Changed: Renamed static utility `Invoke` -> `OInvoke`.
  - (so you don't have to call like `Ore.Invoke.*` anymore)
- Changed: Renamed enum `ABIArch` -> `ABI`.
- Changed: Renamed editor test `FilesystemCorrectness` -> `FilesystemInEditor`.
  - Also: Fixed: Filesystem editor tests now works outside of KooBox. (was using a specific PNG under Assets/ before~)
- Changed: `OAsset.TryCreate(..., path)` now warns and returns false if there was a problem loading an existing asset at the given path.
  - Also: now detects assumptions about the given path if in editor, such as prepending "Assets/" or appending ".asset".
- Changed: `Filesystem.TryReadJson()` now takes a generic T out parameter.
  - Note: if you supply an IList or IDictionary out type, the returned structure will be deeply genericized. Related: JsonAuthority.FixupNestedContainers

- Fixed: (bandaid) Ore's `[ReadOnly]` attribute is a no-op if `ODIN_INSPECTOR` is defined.
  - Levi: _Odin..._
- Fixed: Build error: `Orator.cs line 190`
  - Also: Fixed: Orator uses an experimental PrefabStage API - now uses the proper API in Unity 2021+.
- Fixed: Some code quality schmutz (thank you @Irontown!)


## [v3.5.0][] - 2023-02-09
- Added: `OnEnableRunner` component - allows you to set up events on a specific GameObject's enable/disable, with optional delays.
- Added: HashMap.TrimExcess() - functionality already existed, but now the API matches System collections one more step.
- Added: (EditorBridge) New global menu item "Ore/Browse to Persistent Data Path".
- Changed: Integers.IsIndexOf() - null is now allowed in argument


## [v3.4.0][] - 2023-01-27
- Note: This CHANGELOG got some love. Markdown is better and now links to tags in KooLab!
- Added: Simple test code to calculate the runtime costs of various Unity Objects ([UnityTheseDays.cs](Tests/Runtime/UnityTheseDays.cs)).
- Added: Paths.AreEquivalent(p1,p2) - for when you don't know backslash vs forward slash, relative vs absolute, etc.
- Added: TimeInterval operators on System.DateTimes.
- Added: TimeInterval explicit cast to System.DateTime.
  - Note: TI representations of DateTimes always convert to/from UTC time.
- Exposed: TimeInterval.TicksAreFrames
- Fixed: TimeIntervalDrawer now works with frame count representations.
- Changed: TimeInterval implicit cast to TimeSpan is now an **explicit** cast.
- Changed: DateTimes.ToUnix*() can now return negative values (for timepoints before the epoch).
- Changed: DeviceSpy.ABIArch enum is no longer nested in the class (and has its own file).
- Changed: OAsset.TryCreate() can now return true in editor if asset at given path already exists.


## [v3.3.1][] - 2023-01-25 (API hotfix)
- Changed: `Filesystem.Try*Json()` - instead of a JsonSerializerSettings object, caller should pass in a custom JsonSerializer to override the defaults set in `JsonAuthority`.
  - Reasoning: It is better if caller could decide if they want to reuse a customized serializer, and potentially keep it cached, rather than reconstruct one every time the Filesystem utility is invoked.


## [v3.3.0][] - 2023-01-24
- Added: `JsonAuthority.SerializerSettings` - default settings automatically used globally.
  - Can be modified in-place to be automatically applied in subsequent Json.NET reads/writes.
- Added: `Filesystem.Try*Json()` now takes an optional `JsonSerializerSettings` parameter to override the JsonAuthority defaults.
  - You can provide custom `JsonConverter` objects by inserting them in the settings object's `Converters` property. This resolves issue #25.
- Added: [CopyableField] attribute for serialized fields - adds a "Copy" button next to most basic inspector value types.
- Added: Format string consts for roundtrip (accurate) stringification of floats / doubles (Floats.cs).
- Added: Aggressive inlining + doc comments for `Bitwise`: `LSB(...)`, `LSBye(...)`, `CTZ(...)`.
- Added: `SerialVersion.ToSemverString(stripExtras)` - much like the ToString override, except guarantees the resulting string adheres to the Semantic Versioning standard.
- Changed: `SerialVersion.None` now represents a non-empty value of "0", as this makes sense to interpret as a "non-version version".
- Fixed: `JsonAuthority.MakeReader()` / `MakeWriter()` - now constructed each as if a global JsonSerializer might *not* be the consumer of the reader/writer. (Still works the same either way.)
- Removed: Hidden compiler flag `BITWISE_CTZ_NOJUMP` and accompanying implementation. It was useful to nobody, and just made for messier-looking code.


## [v3.2.2][] - 2023-01-23
- Fixed: Filesystem.DefaultEncoding is no longer null by default. (How embarrassing...)
- Fixed: Filesystem records LastModifiedPath correctly in all modification cases.
- Fixed: Filesystem anticipates `System.Security.SecurityException`, which maps to `IOResult.NotPermitted`.
- Added: Filesystem.TryGetLastModified(out file).
- Added: Filesystem.LastReadPath 


## [v3.2.0][] - 2023-01-22
- Added: `DateTimes` static utility, for when using Ore.TimeInterval proxies just don't make sense.
  - Includes standardized method for (de)serializing DateTimes to/from PlayerPrefs.
- Added: `JsonAuthority` static utility, for standardizing how we serialize JSON objects (currently, thru Newtonsoft.Json).
- Added: Methods in `Parsing` to parse hexadecimal more specifically.
- Changed: Split `Invoke` APIs into overloads, mostly so that "ifAlive" objects are more clearly distinguished.
- Fixed: The failed assertion (Editor-only) when changing the path in [AssetPath("path")] attributes for an OAssetSingleton after an instance has already been created.
  - Note: You will still need to handle any file renaming adjustments yourself, if that is your intent.


## [v3.1.2][] - 2023-01-20
- Fixed: DeviceSpy IDFA/IDFV/IsTrackingLimited should work more completely for Android + iOS.
- Changed: DeviceSpy.RegionISOString was deceptive, as it returned an ISO 3166-2 **country** code, not region code. Renamed to DeviceSpy.CountryISOString.


## [v3.1.1][] - 2023-01-19
- Fixed: `Strings` / `Filesystem` text writers were using Unicode (UTF16-LE) by default--it *was* tweakable by package users, but here at Ore we believe in The Best Defaults. Therefore, we've made the new default encoding **UTF8** (with BOM).


## [v3.1.0][] - 2023-01-18
- Fixed: VoidEvent.Invoke() no longer triggers a failed assertion when it is disabled.
- Added: Official dependency on Newtonsoft.Json (com.unity.nuget.newtonsoft-json 3.0.2).
- Added: More TimeInterval APIs for working with system time structs and UNIX time (the 1970 epoch).


## [v3.0.0][] - 2023-01-13
- Note: I acknowledge that the major version should have increased much earlier, due to non-backwards-compatible API changes. From here on out, this will be done better; whether by fewer API changes or by incrementing the major version more frequently, we shall see.
- Fixed: `EditorBridge.IS_EDITOR` now has proper value.
- Changed: `IEvent.TryInvoke()` implementors should now return false if IsEnabled is false.
- Added: Static extension class `SceneObjects` - for extension methods to GameObject and Component instances. Currently defines `IsInPrefabAsset()` extensions.
- Changed: HashMap API nullability contracts (method parameters).
- Changed: More aggressive inlining for: Strings, TimeInterval
- Changed: TimeInterval's internal implementation for representing "system ticks" vs "frame ticks".
- Fixed: Exceptions thrown in DelayedRoutine's payload causing the routine to go rogue.
- Added: new public utilities in `Integers` & `Floats`.
- Removed: Stub scripts ReferencePaletteWindow, OConfig
- Removed: `Coalescer` data structure, for being no more useful (nor performant) than simple LINQ equivalent.
- Added: `OSingleton.IsValidWhileDisabled` (for edge case scenario found by Ayrton).


## [v2.12.0][] - 2023-01-05
- Changed: renamed VersionID -> SerialVersion; also simplified the implementation and added ctor from System.Version.
- Changed: The following classes now participate in a trialing of C#'s [MethodImpl(MethodImplOptions.AggressiveInlining)]:
  - Bitwise
  - DeviceSpy
  - Floats
  - Hashing
  - Integers
  - Lists
  - Parsing
  - Strings
- Changed: DeviceSpy timezone information API exposes underling TimeSpan offset.
- Changed: DeviceSpy RAM reporting is in MB by default (was MiB).
- Added: DeviceSpy API to report device region (ISO 3166-2).
- Added: DeviceSpy.ToJSON() - returns a JSON object containing all the public properties in DeviceSpy. Useful for debugging.
- Added: Primitive utility classes have received an API revamp + some optimizations and small additions. (Floats, Integers, Bitwise)
- Fixed: The 64-bit integer overloads of Primes.IsPrime() and Primes.Next() now work correctly (and pass tests).


## [v2.11.0][] - 2022-12-19
- Added: `DeviceSpy` now reports system language, browser, network carrier, and RAM usage.
- Added: `AndroidBridge` static API
- Fixed: `Heap.Push()` and `Heap.Pop()` now have proper overloads to handle arrays.
- Fixed: `TimeIntervals` not working correctly with DelayedRoutines when representing quantities of frames.
- Tests: `DelayedRoutine` correctness tests


## [v2.10.2][] - 2022-12-15
- Changed: package.json now properly accepts Unity 2019.4.


## [v2.10.1][] - 2022-12-12
- Fixed: C# syntaxes being used that were incompatible with Unity 2019.x.


## [v2.10.0][] - 2022-12-07
- Added: struct `DelayedRoutine` optimizes trivial coroutine use cases.
  - Note: DelayedRoutine's speed vs conventional `yield return` equivalents still needs to be measured.
- Added: instance types (ICoroutineRunner, CoroutineRunner, CoroutineRunnerBuffer) to make ActiveScene pretty again.
- Added: `Invoke` static API - namely good for `Invoke.NextFrame(*)` and `Invoke.AfterDelay(*)`.
  - Note: Invoke.AfterDelay probably has undefined behaviour if called before any scenes are loaded. Will investigate later.
- Improved: Utility APIs and operators in `TimeInterval`.
  - Also: Now using TimeInterval in more pre-existing places.
- Deprecated: The functionality of `ActiveScene.EnqueueCoroutine` and related APIs are now implemented through the static `ActiveScene.Coroutines.*` interface.
  - These APIs will not be fully removed until Ore v3.
- Deprecated: Coroutine-related helpers: `OComponent.{InvokeNextFrame,InvokeNextFrameIf,DelayInvoke}`. Using the new DeferringRoutine or static Invoke APIs is now the way.
  - Also will not be fully removed until at least Ore v3.


## [v2.9.4][] - 2022-12-05
- Added: `HashMap.UnmapAllKeys(where)` + `HashMap.UnmapAllValues(where)`
  - Bonus: HashMap.Enumerator now allows for deletion while iterating.
  - Added: new HashMap unit tests
- Added: `Transforms` static class, with space manipulation extensions.
- Added: `Filesystem.TryTouch(filepath)` - works like Unix `touch`.
- Fixed: Missing constructors for SerialSet subclasses.


## [v2.9.1][] - 2022-11-18
- Added: VoidEvent (like a DelayedEvent, but no delay).
- Added: Flesh to the TimeInterval API, makes it easier to use.


## [v2.8.0][] - 2022-11-09
- Added: TimeInterval struct + a custom drawer for it.


## [v2.7.0][] - 2022-10-31
- Added: (Editor) New GUI drawing helpers in `OGUI` / new class `OGUI.Draw`.
- Added: (Editor) Visual Testing Window (A.K.A. "VTW")! Menu bar -> Bore -> Tools -> Ore Visual Testing.
  - VTW Mode: "Raster Line" - visualizes the line drawing algorithm(s) from the `Raster` class.
  - VTW Mode: "Raster Circle" - same but for the circle drawer(s).
  - VTW Mode: "Color Analysis" - easy testing suite for various color APIs, primarily those in Ore's own `Colors` class.
  - VTW Mode: "Hash Maps" - datastructure visualizer for Ore's `HashMap<K,V>` class. Could also be useful for testing hash algorithm distribution in the future.
- Added: More public control over the internal load of HashMaps. (`ResetCapacity`, `Rehash`, also `HashMapParams.WithGrowthCurve`,`.WithGrowFactor`)
- Changed: Various internals about HashMap logic to garner a notable performance increase. (never enough though~)
- Changed: Improved speed tests in `HashMapSpeed`. Also lowered the test parameters so they no longer take forever to complete overall.
- Changed: `Primes` API rearranged to be more specific about what the returned primes should be used for, and replaced internal implementations with their test-determined faster counterparts.
- Changed: `IComparator` - all parameters are passed with the `in` ref keyword. Caller does not need to acquiesce.
- Fixed: `Raster.CircleDrawer` now makes a more correct circle by default (thanks to the VTW!).
- Added: Many new `Colors` utility APIs, such as `Colors.Random` (with `Dark` and `Light` variants), `Inverted`, `Grayscale`, (...)


## [v2.6.0][] - 2022-10-27
- Added: Previously-unimplemented HashMap members (KeyCollection, ValueCollection) are now implemented.
- Added: Better inline documentation for HashMap's specification + comparison to System alternatives.
- Added: More QoL overloads for Raster shapes + more information properties.
- Fixed: ActiveScene coroutines improperly cleaning up after they finish.
- Changed: ActiveScene Coroutine API. In general backwards compatible, though the API has been revamped.


## [v2.5.6][] - 2022-10-17
- Added: PrefValue\<T\> + PrefColor - helper classes for dealing with EditorPrefs.
- Merged: HashMap/Primes optimizations


## [v2.4.0][] - 2022-10-14
- Adopted: DeviceDimensions.cs (from Decisions package).


## [v2.3.3][] - 2022-10-10
- HashMaps: Fixed: Rare linear probing issue causing large hashmaps (N>10,000) to map collisions incorrectly.
- HashMaps: Optimized - tests are now within 1% of the speed of Dictionary/Hashtable


## [v2.3.2][] - 2022-10-06
- Removed stubs for APIs that aren't functional from the release track (issue #1).


## [v2.3.1][] - 2022-10-05
- Added: HashMap data structure !!!! (This is a _somewhat_ major feature addition)
- Added: Unit tests for Filesystem + HashMap + competing System implementations.


## [v2.2.1][] - 2022-09-12
- Fixed: SceneLord.LoadSceneAsync now has safety guards against spamming.
- Changed: SceneLord is no longer a required OAssetSingleton and can be created via the Asset menu.
- Changed: VersionID internals are now smarter.
- Added: (Editor-only) Property drawer for VersionIDs, does some validation, allows editing the underlying string.
- Fixed: More annoying OAssetSingleton warnings / failed asserts.
- Fixed: My head.


## [v2.2.0][] - 2022-08-25
- Removed: Automatic OAssetSingleton<> instantiations by default. You can still flag an OAssetSingleton for auto-creation at a specific path by using the [AssetPath(string)] type attribute.
- Added: Public API for asset creation, work either at runtime or edit time: OAsset.Create(...)
- Added: OAssetSingleton.TryGuarantee(out TSelf) - used to absolutely guarantee that you'll get an instance, even if one must be created. The only case where this fails is when the system is out of memory (in theory).
- Added: SceneLord OAssetSingleton - it's a helper for calling SceneManager functions from with serialized Unity Events (etc).


## [v2.1.4][] - 2022-08-24
- Fixed: Orator now only uses rich text in Editor, not in builds (i.e. Android Logcat).


## [v2.1.3][] - 2022-08-20
- Fixed: DelayedEvent fields on ScriptableObjects work (more) properly.


## [v2.1.2][] - 2022-08-19
- Fixed: DelayedEvent now uses ActiveScene properly to enqueue coroutines before scene load.
- Added: new Orator.LogOnce (etc) API prevents annoying spam in your logs.
- Fixed: Dependency loop issue after fresh pull.
- Removed: IImmortalSingleton. It was folly.
- Added: Attribute [AssetPath] for specifying custom paths for automatic OAssetSingleton creation.
- Added: A more complete Coroutine API for ActiveScene.
- Added: GreyLists, SerialSets
- Fixed: DelayedEvent in ScriptableObjects


## [v2.0.2][] - 2022-08-15
- Fixed: Defunct behaviour on Orator assets (and all other OAssetSingletons)
- Added: Certain attributes disable automatic OAssetSingleton creation: [CreateAssetMenu], [Obsolete], [OptionalAsset].
- Changed: All OAssetSingletons get auto-created in the root-level Resources folder.


## [v2.0.1][] - 2022-08-12
- Fixed: OAssert preventing builds
- Added: OAssert.Exists(), etc
- Fixed: EditorBridge warning, something something about namespaces 


## [v2.0.0][] - 2022-07-25
- BREAKING: Moved: ALL code from namespace `Bore` and to namespaces `Ore` and `Ore.Editor`.
- Moved: `Editor/Drawers.cs` -> `Editor/OGUI.cs` (associated functions are now also nested as appropriate).
- Added: `Ore.Editor.FoldoutHeader` + tested it on `DelayedEvent`'s custom property drawer.
- Added: `Ore.Editor.Styles` <- tried to not go too deep on this API / default usage.
- Added: the `Orator` & `OAssert` APIs, should be ready for most use cases now!
- Improved: `DelayedEvents`, `Orator`, pre-existing editor utilities.
- Fixed: all editor warnings.


## [v1.1.0][] - 2022-06-23
- Added: Editor helpers simplified and migrated from Levi's PyroDK.
- Refactor: Package structure (public C# interface is (mostly) unaffected).
- Added: Simplified versions of PyroDK static utilities (Bitwise.cs, Integers.cs, Floats.cs, Hashing.cs, etc)
- Added: Safe and defensive API for file IO (Static/Filesystem.cs)
- Added: DeviceSpy (e.g. used by LAUD deciders)
- Added: ActiveScene (Scene singleton) (useful for starting Coroutines from anywhere)


## [v1.0.0][] - 2022-05-17
- Moved all the boilerplate code in the now-deprecated "Bore" package into this "Ore" package.


<!-- Hyperlink Refs -->

[ore-inspector]: https://gitlab.com/UnityOre/ore-inspector
[create an issue]: ../../issues/new

<!-- - auto-generate with `git tag | awk -- '{print "["$1"]: ../../tags/"$1}' | sort -rV` -->

[v4.1.0]: ../../tags/v4.1.0
[v4.0.1]: ../../tags/v4.0.1
[v4.0.0]: ../../tags/v4.0.0
[v3.5.0]: ../../tags/v3.5.0
[v3.4.0]: ../../tags/v3.4.0
[v3.3.1]: ../../tags/v3.3.1
[v3.3.0]: ../../tags/v3.3.0
[v3.2.2]: ../../tags/v3.2.2
[v3.2.1]: ../../tags/v3.2.1
[v3.2.0]: ../../tags/v3.2.0
[v3.1.2]: ../../tags/v3.1.2
[v3.1.1]: ../../tags/v3.1.1
[v3.1.0]: ../../tags/v3.1.0
[v3.0.2]: ../../tags/v3.0.2
[v3.0.1]: ../../tags/v3.0.1
[v3.0.0]: ../../tags/v3.0.0
[v2.12.0]: ../../tags/v2.12.0
[v2.11.0]: ../../tags/v2.11.0
[v2.10.2]: ../../tags/v2.10.2
[v2.10.1]: ../../tags/v2.10.1
[v2.10.0]: ../../tags/v2.10.0
[v2.9.4]: ../../tags/v2.9.4
[v2.9.3]: ../../tags/v2.9.3
[v2.9.2]: ../../tags/v2.9.2
[v2.9.1]: ../../tags/v2.9.1
[v2.9.0]: ../../tags/v2.9.0
[v2.8.0]: ../../tags/v2.8.0
[v2.7.0]: ../../tags/v2.7.0
[v2.6.0]: ../../tags/v2.6.0
[v2.5.6]: ../../tags/v2.5.6
[v2.5.5]: ../../tags/v2.5.5
[v2.5.4]: ../../tags/v2.5.4
[v2.5.3]: ../../tags/v2.5.3
[v2.5.2]: ../../tags/v2.5.2
[v2.5.1]: ../../tags/v2.5.1
[v2.5.0]: ../../tags/v2.5.0
[v2.4.0]: ../../tags/v2.4.0
[v2.3.3]: ../../tags/v2.3.3
[v2.3.2]: ../../tags/v2.3.2
[v2.3.1]: ../../tags/v2.3.1
[v2.2.1]: ../../tags/v2.2.1
[v2.2.0]: ../../tags/v2.2.0
[v2.1.4]: ../../tags/v2.1.4
[v2.1.3]: ../../tags/v2.1.3
[v2.1.2]: ../../tags/v2.1.2
[v2.1.0]: ../../tags/v2.1.0
[v2.0.2]: ../../tags/v2.0.2
[v2.0.1]: ../../tags/v2.0.1
[v2.0.0]: ../../tags/v2.0.0
[v1.1.0]: ../../tags/v1.1.0
[v1.0.0]: https://leviperez.dev/bukowski
