/*! @file       Static/DurationPolicy.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2023-04-19
**/

namespace Ore
{
  public enum DurationPolicy
  {
    /// <summary>
    ///   Do not count the duration while a user is multitasked-out towards
    ///   timer results.
    /// </summary>
    IgnoreMultitask = 0,

    /// <summary>
    ///   Intended for games with older metrics definitions, which may be
    ///   immutable. Not preferred.
    /// </summary>
    IncludeMultitask = 1,

    /// <inheritdoc cref="IgnoreMultitask"/>
    Default = IgnoreMultitask
  }
}