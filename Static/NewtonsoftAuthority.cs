/*! @file       Static/JsonAuthority+Newtonsoft.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2023-04-03
**/

#if ( ORE_PREFER_NEWTONSOFT || PREFER_NEWTONSOFT ) && !ORE_NEWTONSOFT
#warning ORE_PREFER_NEWTONSOFT was provided, but com.unity.nuget.newtonsoft-json is absent from the project.
#endif

#if ORE_NEWTONSOFT
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;
#endif

using JetBrains.Annotations;

using System.Collections.Generic;
using System.Linq;

using System.IO;
using System.Text;

using StringComparison = System.StringComparison;
using CultureInfo      = System.Globalization.CultureInfo;
using Exception        = System.Exception;
using Type             = System.Type;


namespace Ore
{
  [PublicAPI]
  public static class NewtonsoftAuthority
  {
  #if ORE_NEWTONSOFT

    public static readonly JsonSerializerSettings SerializerSettings = new JsonSerializerSettings
    {
      // if in a pickle, klients may feel free to alter at runtime
      // (especially SerializerSettings.Converters!)

      #if DEBUG
      ReferenceLoopHandling = ReferenceLoopHandling.Error,
      Formatting            = Formatting.Indented,
      #else
      ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
      Formatting            = Formatting.None,
      #endif

      Culture = CultureInfo.InvariantCulture,

      DateParseHandling    = DateParseHandling.DateTime,
      DateFormatHandling   = DateFormatHandling.IsoDateFormat,
      DateTimeZoneHandling = DateTimeZoneHandling.Utc,

      ConstructorHandling    = ConstructorHandling.AllowNonPublicDefaultConstructor,
      MissingMemberHandling  = MissingMemberHandling.Ignore,
      ObjectCreationHandling = ObjectCreationHandling.Auto,
      NullValueHandling      = NullValueHandling.Ignore,
      DefaultValueHandling   = DefaultValueHandling.IgnoreAndPopulate,
        // -> see: https://www.newtonsoft.com/json/help/html/T_Newtonsoft_Json_DefaultValueHandling.htm

      MetadataPropertyHandling       = MetadataPropertyHandling.Ignore,
      PreserveReferencesHandling     = PreserveReferencesHandling.None,
      TypeNameHandling               = TypeNameHandling.None,
      TypeNameAssemblyFormatHandling = TypeNameAssemblyFormatHandling.Simple,
    };

    public static readonly JsonLoadSettings LoadStrict = new JsonLoadSettings
    {
      // if in a pickle, klients may feel free to alter at runtime

      #if DEBUG
      CommentHandling               = CommentHandling.Ignore,
      DuplicatePropertyNameHandling = DuplicatePropertyNameHandling.Error,
      LineInfoHandling              = LineInfoHandling.Load,
      #else
      CommentHandling               = CommentHandling.Ignore,
      DuplicatePropertyNameHandling = DuplicatePropertyNameHandling.Replace,
      LineInfoHandling              = LineInfoHandling.Ignore,
      #endif
    };

    public static readonly JsonMergeSettings MergeReplace = new JsonMergeSettings
    {
      PropertyNameComparison = StringComparison.InvariantCultureIgnoreCase,
      MergeArrayHandling     = MergeArrayHandling.Replace,
      MergeNullValueHandling = MergeNullValueHandling.Ignore
    };

    public static readonly JsonMergeSettings MergeConcat = new JsonMergeSettings
    {
      PropertyNameComparison = StringComparison.InvariantCultureIgnoreCase,
      MergeArrayHandling     = MergeArrayHandling.Concat,
      MergeNullValueHandling = MergeNullValueHandling.Ignore
    };


    [NotNull]
    public static JsonTextWriter MakeWriter([NotNull] TextWriter writer, [NotNull] out JsonConverter[] converters,
                                            JsonSerializerSettings overrides = null)
    {
      converters = SerializerSettings.Converters.ToArray();

      if (overrides is null)
      {
        overrides = SerializerSettings;
      }
      else if (!overrides.Converters.IsEmpty())
      {
        converters = overrides.Converters.Concat(converters).ToArray();
      }

      var jwriter = new JsonTextWriter(writer)
      {
        Culture              = overrides.Culture,
        Formatting           = overrides.Formatting,
        DateFormatHandling   = overrides.DateFormatHandling,
        DateTimeZoneHandling = overrides.DateTimeZoneHandling,
        DateFormatString     = overrides.DateFormatString,
        FloatFormatHandling  = overrides.FloatFormatHandling,
        StringEscapeHandling = overrides.StringEscapeHandling,
      };

      return jwriter;
    }

    [NotNull]
    public static JsonTextReader MakeReader([NotNull] TextReader reader, [NotNull] out JsonConverter[] converters,
                                            JsonSerializerSettings overrides = null)
    {
      converters = SerializerSettings.Converters.ToArray();

      if (overrides is null)
      {
        overrides = SerializerSettings;
      }
      else if (!overrides.Converters.IsEmpty())
      {
        converters = overrides.Converters.Concat(converters).ToArray();
      }

      var jreader = new JsonTextReader(reader)
      {
        Culture              = overrides.Culture,
        MaxDepth             = overrides.MaxDepth,
        DateParseHandling    = overrides.DateParseHandling,
        DateTimeZoneHandling = overrides.DateTimeZoneHandling,
        DateFormatString     = overrides.DateFormatString,
        FloatParseHandling   = overrides.FloatParseHandling,
      };

      return jreader;
    }


    [NotNull]
    public static JsonSerializerSettings EnableMetadata([CanBeNull] this JsonSerializerSettings settings,
                                                        bool enable = true)
    {
      if (settings is null)
      {
        settings = new JsonSerializerSettings();
      }

      settings.MetadataPropertyHandling = enable ? MetadataPropertyHandling.ReadAhead : MetadataPropertyHandling.Ignore;
        // --> have to use ReadAhead since server uses JavaScript to make JSON,
        // which can give unordered results.
        // I know... The perf!

      return settings;
    }

    [NotNull]
    public static JsonSerializerSettings EnableReferences([CanBeNull] this JsonSerializerSettings settings,
                                                          bool enable = true)
    {
      if (settings is null)
      {
        settings = new JsonSerializerSettings();
      }

      settings.PreserveReferencesHandling = enable ? PreserveReferencesHandling.All : PreserveReferencesHandling.None;

      return settings;
    }

    [NotNull]
    public static JsonSerializerSettings EnableExplicitTypes([CanBeNull] this JsonSerializerSettings settings,
                                                             bool enable = true)
    {
      if (settings is null)
      {
        settings = new JsonSerializerSettings();
      }

      settings.TypeNameHandling = enable ? TypeNameHandling.Objects : TypeNameHandling.None;

      return settings;
    }


    [NotNull]
    public static string Serialize(object obj, JsonSerializer serializer = null, StringBuilder cachedBuilder = null)
    {
      const int FALLBACK_BUILDER_SIZE = 256;

      if (cachedBuilder is null)
        cachedBuilder = new StringBuilder(FALLBACK_BUILDER_SIZE);

      var stream = new StringWriter(cachedBuilder, SerializerSettings.Culture);

      SerializeTo(stream, obj, serializer);

      return stream.ToString();
    }

    public static void SerializeTo([NotNull] TextWriter stream, object data, JsonSerializer serializer = null)
    {
      if (serializer is null)
      {
        serializer = JsonSerializer.CreateDefault(SerializerSettings);
      }

      using (var writer = new JsonTextWriter(stream))
      {
        serializer.Serialize(writer, data);
      }
    }


    public static object DeserializeStream([NotNull] TextReader stream, Type type, JsonSerializer serializer = null)
    {
      if (serializer is null)
      {
        serializer = JsonSerializer.CreateDefault(SerializerSettings);
      }

      using (var reader = new JsonTextReader(stream))
      {
        // may throw JsonException
        var maybeNull = serializer.Deserialize(reader, type);

        switch (maybeNull)
        {
          case JObject jobj:
            maybeNull = Genericize(jobj, map: null);
            break;

          case JArray jarr:
            maybeNull = Genericize(jarr, list: null);
            break;

          case IList<object> list:
            FixupNestedContainers(list);
            break;

          case HashMap<string,object> map:
            FixupNestedContainers(map);
            break;

          case Dictionary<string,object> dict:
            FixupNestedContainers(dict);
            break;
        }

        return maybeNull;
      }
    }

    public static bool TryDeserializeStream<T>([NotNull] TextReader stream, out T obj, JsonSerializer serializer = null)
    {
      object boxed;
      try
      {
        boxed = DeserializeStream(stream, typeof(T), serializer);
      }
      catch (Exception ex)
      {
        // TODO?
        #if DEBUG
        Orator.NFE(ex);
        #else
        if (ex is JsonException)
          Orator.NFE(ex);
        #endif

        obj = default;
        return false;
      }

      if (boxed is T casted)
      {
        obj = casted;
        return true;
      }

      obj = default;
      return boxed is null && !typeof(T).IsValueType;
    }


    public static bool TryDeserializeObject(string rawJson, Type type, out object obj)
    {
      if (rawJson.IsEmpty())
      {
        obj = null;
        return false;
      }

      try
      {
        obj = DeserializeStream(new StringReader(rawJson), type);
      }
      catch (Exception ex)
      {
        // TODO?
        #if DEBUG
        Orator.NFE(ex);
        #else
        if (ex is JsonException)
          Orator.NFE(ex);
        #endif

        obj = default;
        return false;
      }

      return obj != null;
    }

    public static bool TryDeserializeObject<T>(string rawJson, out T obj)
    {
      if (TryDeserializeObject(rawJson, typeof(T), out object boxed))
      {
        if (boxed is T casted)
        {
          obj = casted;
          return true;
        }

        if (boxed is null && !typeof(T).IsValueType)
        {
          obj = default;
          return true;
        }
      }

      obj = default;
      return false;
    }

    public static bool TryDeserializeOverwrite<T>(string rawJson, [NotNull] ref T obj, JsonSerializer serializer = null)
    {
      if (serializer is null)
      {
        serializer = JsonSerializer.CreateDefault(SerializerSettings);
      }

      try
      {
        var parsed = serializer.Deserialize(new StringReader(rawJson), typeof(JObject)) as JObject;
        if (parsed is null)
          return false;

        foreach (var field in typeof(T).GetFields(TypeMembers.INSTANCE))
        {
          if (field.IsDefined<System.NonSerializedAttribute>())
            continue;

          if (parsed.TryGetValue(field.Name, out JToken tok))
          {
            if (tok.Type == JTokenType.Null)
            {
              field.SetValue(obj, null);
              continue;
            }

            var value = tok.ToObject(field.FieldType, serializer);
            if (value != null)
              field.SetValue(obj, value);
          }
        }

        return true;
      }
      catch (Exception ex)
      {
        Orator.NFE(ex);
        return false;
      }
    }


    public static bool TryMakeObject<T>(IDictionary<string,object> data, out T obj)
    {
      try
      {
        var middleman = JObject.FromObject(data);
        obj = middleman.ToObject<T>();
        return true;
      }
      catch (Exception e)
      {
        Orator.NFE(e.Silenced());
        obj = default;
        return false;
      }
    }


    [CanBeNull]
    public static object GenericParse(string rawJson)
    {
      var parsed = JsonConvert.DeserializeObject(rawJson);

      if (parsed is null)
        return null;

      if (parsed is JValue jval)
      {
        return jval.Value;
      }
      if (parsed is JObject jobj)
      {
        return Genericize(jobj, map: null);
      }
      if (parsed is JArray jarr)
      {
        return Genericize(jarr, list: null);
      }

      return parsed;
    }

    [NotNull]
    public static IDictionary<string,object> GenericParseObject(string rawJson)
    {
      var jobj = JsonConvert.DeserializeObject<JObject>(rawJson, SerializerSettings);

      if (jobj is null)
      {
        return JsonAuthority.JsonObjMaker(1);
      }

      return Genericize(jobj, null);
    }

    [NotNull]
    public static IList<object> GenericParseArray(string rawJson)
    {
      var jarr = JsonConvert.DeserializeObject<JArray>(rawJson, SerializerSettings);

      if (jarr is null)
      {
        return JsonAuthority.JsonArrMaker(1);
      }

      return Genericize(jarr, list: null);
    }


    [NotNull]
    public static IList<object> FixupNestedContainers([NotNull] IList<object> list, int maxRecursionDepth = DEFAULT_MAX_RECURSE)
    {
      int ilen = list.Count;

      if (maxRecursionDepth < 0)
      {
        Orator.Error($"{nameof(JsonAuthority)}.{nameof(FixupNestedContainers)}: Max recursion depth reached.");
        return list;
      }

      for (int i = 0; i < ilen; ++i)
      {
        switch (list[i])
        {
          case JValue jval:
            list[i] = jval.Value;
            break;

          case JObject jobj:
            var newMap = JsonAuthority.JsonObjMaker(jobj.Count);

            foreach (var property in jobj)
            {
              newMap.Add(property.Key, property.Value);
            }

            list[i] = FixupNestedContainers(newMap, maxRecursionDepth - 1); // fake recursion
            break;

          case JArray jarr:
            var newArr = new object[jarr.Count];

            for (int n = 0; n < newArr.Length; ++n)
            {
              newArr[n] = jarr[n];
            }

            list[i] = FixupNestedContainers(newArr, maxRecursionDepth - 1); // regrettable recursion
            break;

          case JToken jtok:
            Orator.Reached($"ping Levi? type={jtok.Type}");
            list[i] = jtok.ToString(Formatting.None);
            break;
        }
      }

      return list;
    }

    [NotNull]
    public static IDictionary<string,object> FixupNestedContainers([NotNull] IDictionary<string,object> dict, int maxRecursionDepth = DEFAULT_MAX_RECURSE)
    {
      if (dict is HashMap<string,object> hashMap)
      {
        return FixupNestedContainers(hashMap, maxRecursionDepth);
      }

      if (maxRecursionDepth < 0)
      {
        Orator.Error($"{nameof(JsonAuthority)}.{nameof(FixupNestedContainers)}: Max recursion depth reached.");
        return dict;
      }

      var sneakySwap = new Dictionary<string,object>(dict.Count);

      foreach (var pair in dict)
      {
        switch (pair.Value)
        {
          case JValue jval:
            sneakySwap.Add(pair.Key, jval.Value);
            break;

          case JObject jobj:
            var newMap = JsonAuthority.JsonObjMaker(jobj.Count);

            foreach (var property in jobj)
            {
              newMap.Add(property.Key, property.Value);
            }

            sneakySwap.Add(pair.Key, FixupNestedContainers(newMap, maxRecursionDepth - 1));
            break;

          case JArray jarr:
            var newArr = JsonAuthority.JsonArrMaker(jarr.Count);

            for (int i = 0; i < newArr.Count; ++i)
            {
              newArr[i] = jarr[i];
            }

            sneakySwap.Add(pair.Key, FixupNestedContainers(newArr, maxRecursionDepth - 1));
            break;

          case JToken jtok:
            throw new UnanticipatedException("type=" + jtok.Type);

          default:
            sneakySwap.Add(pair.Key, pair.Value);
            break;
        }
      }

      return sneakySwap;
    }

    [NotNull]
    public static HashMap<string,object> FixupNestedContainers([NotNull] HashMap<string,object> map, int maxRecursionDepth = DEFAULT_MAX_RECURSE)
    {
      if (maxRecursionDepth < 0)
      {
        Orator.Error($"{nameof(JsonAuthority)}.{nameof(FixupNestedContainers)}: Max recursion depth reached.");
        return map;
      }

      // HashMap algo optimization <3

      using (var it = map.GetEnumerator())
      {
        while (it.MoveNext())
        {
          switch (it.CurrentValue)
          {
            case JValue jval:
              it.RemapCurrent(jval.Value);
              break;

            case JObject jobj:
              var newMap = JsonAuthority.JsonObjMaker(jobj.Count);

              foreach (var property in jobj)
              {
                newMap.Add(property.Key, property.Value);
              }

              it.RemapCurrent(FixupNestedContainers(newMap, maxRecursionDepth - 1)); // regrettable recursion
              break;

            case JArray jarr:
              var newArr = JsonAuthority.JsonArrMaker(jarr.Count);

              for (int j = 0; j < newArr.Count; ++j)
              {
                newArr[j] = jarr[j];
              }

              it.RemapCurrent(FixupNestedContainers(newArr, maxRecursionDepth - 1)); // fake recursion
              break;

            case JToken jtok:
              Orator.Reached($"ping Levi? type={jtok.Type}");
              it.RemapCurrent(jtok.ToString(Formatting.None));
              break;
          }
        }
      }

      return map;
    }


    [NotNull]
    public static IDictionary<string,object> Genericize([NotNull] JObject jObject, [CanBeNull] IDictionary<string,object> map)
    {
      if (map is null)
      {
        map = JsonAuthority.JsonObjMaker(jObject.Count);
      }
      else
      {
        map.Clear();

        if (map is HashMap<string,object> hashMap)
        {
          hashMap.EnsureCapacity(jObject.Count);
        }
      }

      foreach (var property in jObject)
      {
        switch (property.Value)
        {
          case null:
            continue;

          case JValue jval:
            map[property.Key] = jval.Value;
            break;

          case JObject jobj:
            map[property.Key] = Genericize(jobj, map: null);
            break;

          case JArray jarr:
            map[property.Key] = Genericize(jarr, list: null);
            break;

          default:
            throw new UnanticipatedException($"Json.NET JToken type={property.Value.Type}");
        }
      }

      return map;
    }

    public static IList<object> Genericize([NotNull] JArray jArray, [CanBeNull] IList<object> list)
    {
      if (list is null)
      {
        list = JsonAuthority.JsonArrMaker(jArray.Count);
      }
      else
      {
        list.Clear();
      }

      foreach (var token in jArray)
      {
        switch (token)
        {
          case JValue jval:
            list.Add(jval.Value);
            break;

          case JObject jobj:
            list.Add(Genericize(jobj, map: null));
            break;

          case JArray jarr:
            list.Add(Genericize(jarr, list: null));
            break;

          case null:
            throw new UnanticipatedException("Json.NET JToken type=<null>");

          default:
            throw new UnanticipatedException($"Json.NET JToken type={token.Type}");
        }
      }

      return list;
    }


    const int DEFAULT_MAX_RECURSE = 4;


    internal static void SetPrettyPrint(bool pretty)
    {
      SerializerSettings.Formatting = pretty ? Formatting.Indented : Formatting.None;
    }


    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterAssembliesLoaded)]
    #if UNITY_EDITOR
    [UnityEditor.InitializeOnLoadMethod]
    #endif
    static void OverrideDefaultSettings()
    {
      JsonConvert.DefaultSettings = () => SerializerSettings;
        // makes the settings defined here in JsonAuthority apply to any default
        // Json.NET serializers created from now on
    }


  #else // !ORE_NEWTONSOFT

    // nodo

  #endif // !ORE_NEWTONSOFT

  } // end static class NewtonsoftAuthority

}

