/*! @file       Static/ProfilerStat.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2023-04-19
**/

using JetBrains.Annotations;


namespace Ore
{
  /// <summary>
  ///   Not exhaustive.
  /// </summary>
  /// <remarks>
  ///   Primarily useful in Unity 2020+ (Unity.Profiling.ProfilerRecorder).
  /// </remarks>
  [PublicAPI]
  public static class ProfilerStat
  {

    #region Valid in release
    // GC
    public const string GCCollect           = "GC.Collect";
    // Internal
    public const string MainThread          = "Main Thread";
    // Memory
    public const string AudioReservedMemory = "Audio Reserved Memory";
    public const string AudioUsedMemory     = "Audio Used Memory";
    public const string GCReservedMemory    = "GC Reserved Memory";
    public const string GCUsedMemory        = "GC Used Memory";
    public const string SystemUsedMemory    = "System Used Memory"; // <--
    public const string TotalReservedMemory = "Total Reserved Memory";
    public const string TotalUsedMemory     = "Total Used Memory";
    //
    #endregion Valid in release

    #region DEBUG ONLY
    // Memory
    public const string AssetCount          = "Asset Count";
    public const string AudioClipCount      = "AudioClip Count";
    public const string AudioClipMemory     = "AudioClip Memory";
    public const string GameObjectCount     = "Game Object Count";
    public const string GCAllocated         = "GC Allocated In Frame";
    public const string GCAllocations       = "GC Allocation In Frame Count";
    public const string GCAllocTime         = "GC.Alloc";
    public const string SceneObjectCount    = "Scene Object Count";
    public const string TextureCount        = "Texture Count";
    public const string TextureMemory       = "Texture Memory";
    // Other
    public const string CoroutineCallTime   = "CoroutinesDelayedCalls";
    public const string Destroy             = "Destroy";
    public const string Instantiate         = "Instantiate";
    public const string TransformChanged    = "TransformChangedDispatch";
    // Particles
    public const string ParticleUpdate      = "ParticleSystem.Update";
    public const string ParticleUpdate2     = "ParticleSystem.Update2"; // lol @unity?
    public const string ParticleSimulate    = "ParticleSystem.Simulate";
    public const string ParticleSync        = "ParticleSystem.Sync";
    // Physics
    public const string Physics2DRaycast    = "Physics2D.Raycast";
    public const string Physics2DRaycastAll = "Physics2D.RaycastAll"; // ?
    public const string Physics2DSimulate   = "Physics2D.Simulate";
    public const string Physics2DStep       = "Physics2D.Step";
    public const string Physics2DSyncTrans  = "Physics2D.SyncTransformChanges";
    public const string Physics2DUpdate     = "Physics2D.UpdateTransforms";
    // PlayerLoop
    public const string ScriptsUpdate       = "BehaviourUpdate";
    public const string ScriptsFixedUpdate  = "FixedBehaviourUpdate";
    //
    #endregion DEBUG ONLY

  } // end static class ProfilerStat
}