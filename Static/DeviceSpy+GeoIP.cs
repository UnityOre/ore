/*! @file       Static/DeviceSpy+GeoIP.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2023-08-31
**/

using JetBrains.Annotations;

using UnityEngine;
using UnityEngine.Networking;

using DateTime = System.DateTime;
using Encoding = System.Text.Encoding;


namespace Ore
{
  public static partial class DeviceSpy
  {

    /// <summary>
    ///   Nested API to gift you some control over if/how/when the DeviceSpy
    ///   should utilize a proprietary geo-ip service, which takes an
    ///   internet-connected user's public IP address and gets an estimate as to
    ///   their device's current geo location.
    /// </summary>
    /// <remarks>
    ///   Current granularity: ISP's country only (2-char ISO 3166-a2 code).
    /// </remarks>
    [PublicAPI]
    public static class GeoIP
    {
      // TODO yeet PlayerPrefs

      /// <summary>
      ///   Gets the last cached value acquired by this system, or null if we
      ///   don't have one.
      /// </summary>
      /// <remarks>
      ///   Not guaranteed to be equivalent to the current value reported by
      ///   <see cref="DeviceSpy.CountryISOString"/>!
      /// </remarks>
      [CanBeNull]
      public static string CachedValue => PlayerPrefs.GetString(CACHED, null);

      /// <summary>
      ///   Get the timepoint (in UTC) when we last cached a geolocation value
      ///   received from our remote service.
      /// </summary>
      /// <returns>
      ///   <c>default(DateTime)</c> if we've never successfully cached a
      ///   geolocation provided by the remote service.
      /// </returns>
      /// <seealso cref="CachedValue"/>
      public static DateTime LastFetchedAt
      {
        get
        {
          if (s_LastFetchedAt is null)
          {
            s_LastFetchedAt = DateTimes.GetPlayerPref(FETCHED_AT);
          }

          return (DateTime)s_LastFetchedAt;
        }
      }

      /// <summary>
      ///   Get the <see cref="TimeInterval"/> since we last cached a
      ///   geolocation value received from our remote service, <b>rounded to
      ///   the nearest whole second.</b>
      /// </summary>
      /// <returns>
      ///   The equivalent of <see cref="DateTime.UtcNow"/> as a TimeInterval
      ///   if we've never successfully cached a geolocation.
      /// </returns>
      public static TimeInterval TimeSinceLastFetched => new TimeInterval(DateTime.UtcNow - LastFetchedAt)
                                                            .RoundToInterval(TimeInterval.Second);

      /// <summary>
      ///   Has a UnityWebRequest been made, and is it currently awaiting a
      ///   result?
      /// </summary>
      public static bool IsFetching => s_WWW != null && !s_Promise.IsCompleted;

      /// <summary>
      ///   Have we ever tried to fetch this user's geo IP? (that we know of)
      /// </summary>
      public static bool HasFetched => PlayerPrefs.HasKey(FETCHED_AT);

      /// <summary>
      ///   Is the current value reported by <see cref="DeviceSpy.CountryISOString"/>
      ///   the same as the last known GeoIP value?
      /// </summary>
      public static bool IsUsed => !CountryISOString.IsEmpty() &&
                                   string.Equals(CountryISOString, CachedValue, System.StringComparison.Ordinal);


      /// <summary>
      ///   <b>You should probably prefer this over <see cref="Fetch"/>.</b> <br/>
      ///   Attempts to asynchronously ping our remote geoip service.
      /// </summary>
      /// <param name="staleAfter">
      ///   The amount of time that must have passed since the last successful
      ///   <see cref="Fetch"/> before this call considers our cached
      ///   geolocation to be "stale" and attempt to refresh it with a new Fetch.
      /// </param>
      /// <inheritdoc cref="Fetch"/>
      [NotNull]
      // ReSharper disable once InvalidXmlDocComment
      public static Promise<string> FetchIfStale(TimeInterval staleAfter, int timeout = DEFAULT_TIMEOUT)
      {
        if (IsFetching)
        {
          return s_Promise;
        }

        if (TimeSinceLastFetched < staleAfter && PlayerPrefs.HasKey(CACHED))
        {
          string geo = CachedValue;
          LittleBirdie.CountryISOString = geo;
          s_Promise.Reset();
          return s_Promise.CompleteWith(geo);
        }

        return Fetch(timeout);
      }

      /// <summary>
      ///   <b>You should probably prefer <see cref="FetchIfStale"/> over this
      ///   method</b>, unless of course you know the difference between the two
      ///   and have judged accordingly.
      /// </summary>
      /// <param name="timeout">
      ///   The timeout interval (in whole seconds) allotted to the request. If
      ///   it is exceeded, the request will halt and catch fire.
      /// </param>
      /// <returns>
      ///   A static <see cref="Promise{T}"/> (of T string) ready for you to
      ///   await and/or subscribe delegates to. <br/>
      ///   If Fetch has already been called and is still awaiting a server
      ///   response, the same promise object that was originally returned will
      ///   be returned again.
      /// </returns>
      [NotNull]
      public static Promise<string> Fetch(int timeout = DEFAULT_TIMEOUT)
      {
        // getCountryWithIP does not care about inputs, they're just used for hashing
        // TODO Should change this up in the future for 3rd party - Darren
        // TODO and use git-filter-repo to make sure they are purged from the git history too - Levi
        const string PARAMS    = "appName=&ipAddress=&timestamp=&hash=74be16979710d4c4e7c6647856088456"; // TODO encrypt (& scrub?)
        const string API       = "https://api.boreservers.com/borePlatform2/getCountryWithIP.php"; // TODO encrypt (& scrub?)
        const string MIME      = "application/x-www-form-urlencoded";
        const string ERRORMARK = "\"error\"";

        if (IsFetching)
        {
          return s_Promise;
        }

        var req = new UnityWebRequest(API)
        {
          method          = UnityWebRequest.kHttpVerbPOST,
          downloadHandler = new DownloadHandlerBuffer(),
          uploadHandler   = new UploadHandlerRaw(PARAMS.ToBytes(Encoding.UTF8))
        };

        req.SetRequestHeader("Content-Type", MIME);

        req.timeout = timeout;

        s_Promise.Reset();

        var promise = req.PromiseText(ERRORMARK, s_Promise);
                      // Note: extension calls req.Dispose() for us

        if (promise.IsCompleted && !promise.IsSucceeded)
        {
          #if DEBUG
          Orator.Log(typeof(GeoIP), "Fetch failed rather quickly - you sure about your internet connection?");
          #endif

          if (s_WWW != null)
          {
            s_WWW.Dispose();
            s_WWW = null;
          }

          return promise;
        }

        s_WWW = req;

        promise.Completed += () =>
        {
          s_WWW = null;
        };

        promise.Succeeded += response =>
        {
          var parsed = JsonAuthority.ReadDictionary(response);

          if (!parsed.Find("result", out parsed) ||
              !parsed.Find("countryCode", out string geoCode) ||
              geoCode.IsEmpty())
          {
            promise.Forget()
                   .FailWith(new UnanticipatedException("Either server returned funky json... or we forgot how to parse it."));
            return;
          }

          if (!geoCode.Length.IsBetween(2,6))
          {
            promise.Forget()
                   .FailWith(new UnanticipatedException($"{nameof(GeoIP)}: funky geo returned? \"{geoCode}\""));
            return;
          }

          LittleBirdie.CountryISOString = geoCode;
          // (LittleBirdie is used to propogate changes to listeners)

          PlayerPrefs.SetString(CACHED, geoCode);

          var now = DateTime.UtcNow;
          now.SetPlayerPref(FETCHED_AT);
          s_LastFetchedAt = now;

          // meh... I *GUESS* since this is such rare code, triggering a save wouldn't kill us...
          PlayerPrefs.Save();
          // (but TODO just in case)
        };

        return promise;
      }

      /// <summary>
      ///   If a fetch is currently ongoing, this function aborts it & prevents
      ///   it from completing.
      /// </summary>
      public static void AbortFetch()
      {
        if (s_WWW is null)
          return;

        s_WWW.Abort();
        s_WWW.Dispose();
        s_WWW = null;

        if (!s_Promise.IsCompleted)
        {
          #if DEBUG
          Orator.Warn(typeof(GeoIP), "Called GeoIP.Reset() while a request is still pending; " +
                                      "undefined behaviour may result.");
          #endif

          s_Promise.Forget();
        }

        // just to double check that listeners aren't getting shafted:
        s_Promise.AwaitBlocking();

        s_Promise.Reset();
      }

      /// <summary>
      ///   Resets everything cached and handled by <see cref="GeoIP"/>, making
      ///   its state as if the current user just had their first launch again. <br/>
      ///   In the off-chance that there is an ongoing <see cref="Fetch"/>
      ///   request still running, it will be aborted, the associated promise
      ///   will be expedited, and in DEBUG, a warning will get logged to let
      ///   you know that this unicorn edge case has occurred.
      /// </summary>
      /// <remarks>
      ///   Intended for debugging.
      /// </remarks>
      // ReSharper disable once MemberHidesStaticFromOuterClass
      public static void Reset()
      {
        AbortFetch();

        s_CountryISO3166a2 = null;
        s_LastFetchedAt    = default(DateTime);

        PlayerPrefs.DeleteKey(CACHED);
        PlayerPrefs.DeleteKey(FETCHED_AT);
        PlayerPrefs.Save();
      }


      // -- private section --

      static          UnityWebRequest s_WWW;
      static readonly Promise<string> s_Promise = new Promise<string>();
      static          DateTime?       s_LastFetchedAt;

      const int    DEFAULT_TIMEOUT = 30;
      const string FETCHED_AT      = "DeviceSpy.GeoIP.FetchedAt";
      const string CACHED          = "DeviceSpy.GeoIP.Cached";

    } // end nested static class GeoIP

  } // end class DeviceSpy
}