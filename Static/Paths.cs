/*! @file       Static/Paths.cs
 *  @author     levianperez\@gmail.com
 *  @author     levi\@leviperez.dev
 *  @date       2022-06-03
**/

using JetBrains.Annotations;

using Path = System.IO.Path;
using StringComparer = System.StringComparer;


namespace Ore
{
  /// <summary>
  /// Utilities for handling filesystem path strings.
  /// </summary>
  [PublicAPI]
  public static class Paths // TODO rename to PathStrings in major release 9
  {
    public const int MaxLength = 260;

    public static readonly char[] DirectorySeparators =
    {
      DirectorySeparator,
      LameDirectorySeparator
    };

    public const char DirectorySeparator = '/';  // Path.AltDirectorySeparatorChar
    public const char LameDirectorySeparator = '\\'; // Path.DirectorySeparatorChar

    public static readonly char[] InvalidFileNameChars = Path.GetInvalidFileNameChars();
    public static readonly char[] InvalidPathChars = Path.GetInvalidPathChars();

    public static readonly StringComparer Comparer = StringComparer.InvariantCulture;


    public static bool IsValidPath([CanBeNull] string path)
    {
      return !path.IsEmpty() && path.IndexOfAny(InvalidPathChars) < 0;
    }

    public static bool IsValidFileName([CanBeNull] string filename)
    {
      return ExtractBasePath(filename, out string name) &&
             name.IndexOfAny(InvalidFileNameChars) < 0;
    }


    public static bool AreEquivalent([NotNull] string path1, [NotNull] string path2)
    {
      if (path1 == path2)
        return true;

      return Path.GetFullPath(path1) == Path.GetFullPath(path2);
    }


    public static string DetectAssetPathAssumptions([CanBeNull] string path)
    {
      if (path.IsEmpty())
      {
        return path;
      }

      if (!ExtractExtension(path, out _ ))
      {
        path += ".asset";
      }

      // ReSharper disable once PossibleNullReferenceException
      if (path.StartsWith("Assets/") || path.StartsWith("Packages/"))
      {
        return path;
      }

      return "Assets/" + path;
    }


    public static bool ExtractExtension([CanBeNull] string filepath, [NotNull] out string extension, bool includeDot = true)
    {
      extension = string.Empty;

      if (filepath.IsEmpty())
        return false;

      int slash = -1, dot = -1;
      int i = filepath.Length;

      while (i --> 0)
      {
        char c = filepath[i];

        if (dot < 0 && c == '.')
        {
          dot = i;
        }
        else if (c == DirectorySeparator || c == LameDirectorySeparator)
        {
          slash = i;
          break;
        }
      }

      if (dot < 0 || dot < slash || dot == filepath.Length - 1)
        return false;

      if (!includeDot)
        ++ dot;

      extension = filepath.Substring(dot);

      return true;
    }


    public static bool ExtractBasePath([CanBeNull] string filepath, out string basepath)
    {
      basepath = filepath;
      if (filepath.IsEmpty())
        return false;

      int slash = -1, trailSlash = filepath.Length;
      int i = trailSlash;
      while (i --> 0)
      {
        char c = filepath[i];

        if (c == DirectorySeparator || c == LameDirectorySeparator)
        {
          if (i + 1 == trailSlash)
          {
            trailSlash = i;
          }
          else
          {
            slash = i;
            break;
          }
        }
      }

      basepath = filepath.Substring(slash + 1, trailSlash);

      return basepath.Length > 0;
    }

    public static bool ExtractDirectoryPath([CanBeNull] string filepath, out string dirpath, bool trailing_slash = false)
    {
      dirpath = filepath;
      if (filepath.IsEmpty())
        return false;

      dirpath = filepath.TrimEnd(DirectorySeparators);

      int slash = dirpath.LastIndexOfAny(DirectorySeparators);
      if (slash > 0)
        dirpath = dirpath.Remove(slash);
      if (trailing_slash)
        dirpath += '/';

      return dirpath.Length > 0;
    }

    public static bool Decompose([CanBeNull] string filepath, out string dirpath, out string basepath, bool trailing_slash = true)
    {
      dirpath = basepath = filepath;
      if (filepath.IsEmpty())
        return false;

      dirpath = filepath.TrimEnd(DirectorySeparators);

      int slash = dirpath.LastIndexOfAny(DirectorySeparators);
      if (slash < 0)
        dirpath = ".";
      else
      {
        dirpath = dirpath.Remove(slash);
        basepath = dirpath.Substring(slash + 1);
      }

      if (trailing_slash)
        dirpath += '/';

      return basepath.Length > 0 && dirpath.Length > 0;
    }


    public static bool ContainsComponent([CanBeNull] string path, [CanBeNull] string name)
    {
      if (path is null || path.Length == 0 || name is null)
        return false;

      name = name.Trim(DirectorySeparators);

      if (name.Length == 0)
        return false;

      int pos = path.IndexOf(name, System.StringComparison.OrdinalIgnoreCase);
        // always ignore case because well Microsoft DOS.

               /* first check lhs for either no char or a dir separator ... */
      return ( pos == 0 || ( pos > 0 && ( path[pos - 1] == DirectorySeparator ||
                                          path[pos - 1] == LameDirectorySeparator ) ) ) &&
             ( ( pos += name.Length ) == path.Length || path[pos] == DirectorySeparator ||
               /* ... then check rhs -> */              path[pos] == LameDirectorySeparator );
               /* (comments as per Ayrton request) */
    }


    // TODO publish (after checking potential issues in builds)
    internal static string MakeRelativeToProject([NotNull] string path)
    {
      path = path.Replace(LameDirectorySeparator, DirectorySeparator);

      if (path.StartsWith(ProjectRoot))
      {
        return path.Substring(ProjectRoot.Length + 1);
      }

      return path;
    }


    internal static string ProjectRoot
    {
      get
      {
        if (s_ProjectRoot is null)
        {
          s_ProjectRoot = System.Environment.CurrentDirectory.Replace(LameDirectorySeparator, DirectorySeparator);
        }

        return s_ProjectRoot;
      }
    }

    static string s_ProjectRoot;

  } // end static class Paths

}
