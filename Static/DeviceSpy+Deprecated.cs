/*! @file       Static/DeviceSpy+Deprecated.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2023-09-05
**/

using ObsoleteAttribute = System.ObsoleteAttribute;


namespace Ore
{
  public static partial class DeviceSpy
  {

    /// <summary>
    ///   in megabytes.
    /// </summary>
    [Obsolete("if you can, use IsRAMLow for your purpose instead.")]
    public static float LowRAMThreshold => (float)(s_LowRAMThreshMega ?? (s_LowRAMThreshMega = CalcLowRAMThresholdMega()));


    [Obsolete("see DeviceSpy.CurrentRAM")]
    public static int CalcRAMUsageMB()
    {
      return (int)(CalcRAMUsageBytes() / Consts.Mega);
    }

    [Obsolete("see DeviceSpy.CurrentRAM")]
    public static int CalcRAMUsageMiB()
    {
      // it's important to know there's a difference between MB and MiB
      return (int)(CalcRAMUsageBytes() / Consts.Mebi);
    }

    [Obsolete("see DeviceSpy.CurrentRAM")]
    public static float CalcRAMUsagePercent()
    {
      return (CalcRAMUsageMiB() / (float)TotalRAM).Clamp01();
    }

  } // end class DeviceSpy
}