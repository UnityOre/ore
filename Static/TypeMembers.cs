/*! @file       Static/TypeMembers.cs
 *  @author     levianperez\@gmail.com
 *  @author     levi\@leviperez.dev
 *  @date       2022-06-20
**/

// ReSharper disable MemberCanBePrivate.Global

using JetBrains.Annotations;

using UnityEngine;

using System.Reflection;
using System.Collections;
using System.Collections.Generic;

using Type = System.Type;
using StringBuilder = System.Text.StringBuilder;


namespace Ore
{
  /// <summary>
  /// Reflection utilities for type (class/struct) members ported from PyroDK.
  /// </summary>
  public static class TypeMembers
  {

    public const BindingFlags        ALL =  BindingFlags.Instance |
                                            BindingFlags.Static   |
                                            BindingFlags.Public   |
                                            BindingFlags.NonPublic;

    public const BindingFlags   ALL_DECL =  ALL                   |
                                            BindingFlags.DeclaredOnly;

    public const BindingFlags   INSTANCE =  BindingFlags.Instance |
                                            BindingFlags.Public   |
                                            BindingFlags.NonPublic;

    public const BindingFlags     STATIC =  BindingFlags.Static   |
                                            BindingFlags.Public   |
                                            BindingFlags.NonPublic;

    public const BindingFlags     PUBLIC =  BindingFlags.Public   |
                                            BindingFlags.Instance |
                                            BindingFlags.Static;

    public const BindingFlags  NONPUBLIC =  BindingFlags.Instance |
                                            BindingFlags.Static   |
                                            BindingFlags.NonPublic;

    public const BindingFlags      ENUMS =  BindingFlags.Static   |
                                            BindingFlags.Public;


    public static readonly object[] NoParameters = System.Array.Empty<object>();


    public static bool IsDefined<T>([NotNull] this MemberInfo member, bool inherit = true)
      where T : System.Attribute
    {
      return member.IsDefined(typeof(T), inherit);
    }

    public static bool AreAnyDefined([NotNull] this MemberInfo member, [NotNull] IEnumerable<Type> attributes, bool inherit = true)
    {
      foreach (var attr in attributes)
      {
        if (member.IsDefined(attr, inherit))
          return true;
      }

      return false;
    }

    public static bool IsHidden([NotNull] this FieldInfo field)
    {
      return field.IsDefined<HideInInspector>() ||
             field.IsDefined<System.ObsoleteAttribute>();
    }

    public static bool IsArrayOrList([NotNull] this Type type)
    {
      // TODO migrate
      return type.IsArray || ( type.IsGenericType && type.GetGenericTypeDefinition() == typeof(List<>) );
    }

    public static Type GetListElementType([NotNull] this Type type)
    {
      // TODO migrate
      if (type.IsArray)
        return type.GetElementType();

      if (typeof(IList).IsAssignableFrom(type))
      {
        if (type.IsGenericType)
          return type.GenericTypeArguments[0];
        return typeof(object);
      }

      return null;
    }

    public static bool IsUnityStruct([NotNull] this Type type)
    {
      // TODO migrate
      while (IsArrayOrList(type))
      {
        type = type.IsArray ? type.GetElementType() : type.GetGenericArguments()[0];
        if (type is null)
          return false;
      }

      return type.IsValueType && ( type.Namespace?.StartsWith("Unity") ?? false );
    }

    public static bool IsDefaultConstructible([CanBeNull] this Type type, bool nonPublic = false)
    {
      // TODO migrate

      var flags = nonPublic ? INSTANCE : BindingFlags.Instance | BindingFlags.Public;

      return !ReferenceEquals(type, null) && !type.IsAbstract && !type.IsGenericTypeDefinition &&
             ( type.IsValueType || type.GetConstructor(flags, null, Type.EmptyTypes, null) != null );
    }


    public static object ConstructDefault([NotNull] this Type type, bool nonPublic = false)
    {
      // TODO migrate

      if (type == typeof(string))
        return string.Empty;

      if (typeof(Object).IsAssignableFrom(type))
        return null;

      if (IsDefaultConstructible(type, nonPublic))
        return System.Activator.CreateInstance(type, nonPublic);

      return null;
    }

    public static object ConstructWith([NotNull] this Type type, object arg)
    {
      // TODO migrate

      if (arg is null)
        return ConstructDefault(type);

      if (type == typeof(string))
        return arg.ToString();

      if (typeof(Object).IsAssignableFrom(type))
        return null;

      return System.Activator.CreateInstance(type, arg);
    }

    public static string NiceName([CanBeNull] this Type type)
    {
      using (new RecycledStringBuilder(out var bob))
      {
        AppendNiceName(bob, type);
        return bob.ToString();
      }
    }


    static readonly HashMap<Type,string> s_TypeNameCache = new HashMap<Type,string>
    {
      [typeof(object)]  = "object",
      [typeof(string)]  = "string",
      [typeof(bool)]    = "bool",
      [typeof(long)]    = "long",
      [typeof(int)]     = "int",
      [typeof(float)]   = "float",
      [typeof(double)]  = "double",
      [typeof(decimal)] = "decimal",
      [typeof(char)]    = "char",
      [typeof(short)]   = "short",
      [typeof(byte)]    = "byte",
      [typeof(ulong)]   = "ulong",
      [typeof(uint)]    = "uint",
      [typeof(ushort)]  = "ushort",
      [typeof(sbyte)]   = "sbyte",
      [typeof(void)]    = "void",
    };
    public static void AppendNiceName([NotNull] StringBuilder bob, [CanBeNull] Type type) // TODO migrate
    {
      if (type is null)
      {
        bob.Append("null");
        return;
      }

      if (s_TypeNameCache.Find(type, out string name))
      {
        bob.Append(name);
        return;
      }

      int start = bob.Length;

      if (type.IsArray)
      {
        AppendNiceName(bob, type.GetElementType()); // regrettable recursion.
        bob.Append("[]");
        goto CacheTypeName;
      }

      if (type.IsPointer)
      {
        AppendNiceName(bob, type.GetElementType()); // regrettable recursion.
        bob.Append("*");
        goto CacheTypeName;
      }

      if (type.DeclaringType != null && !type.IsGenericParameter)
      {
        AppendNiceName(bob, type.DeclaringType);    // regrettable recursion.
        bob.Append('.');
      }

      if (!type.IsGenericType)
      {
        bob.Append(type.Name);
        goto CacheTypeName;
      }

      name = type.Name;

      int tick = name.LastIndexOf('`');

      bob.Append(name, 0, tick)
         .Append('<');

      bool isDefinition = type.IsGenericTypeDefinition;
      var  typeArgs = isDefinition ? ((TypeInfo)type).GenericTypeParameters : type.GetGenericArguments();
      int  i = 0, ilen = typeArgs.Length;
      while (i < ilen)
      {
        if (isDefinition)
          bob.Append(typeArgs[i].Name);
        else
          AppendNiceName(bob, typeArgs[i]);         // regrettable recursion.

        if (++i < ilen)
          bob.Append(',');
      }

      bob.Append('>');

      CacheTypeName:
      s_TypeNameCache[type] = bob.ToString(start, bob.Length - start);
    }


    public static bool TryGetField([CanBeNull] this Type type, [NotNull] string name, out FieldInfo field, BindingFlags blags = INSTANCE)
    {
      field = type?.GetField(name, blags);
      return field != null;
    }

    public static bool TryGetSerializableField([CanBeNull] this Type type, [NotNull] string name, out FieldInfo field)
    {
      if (type is null)
      {
        field = null;
        return false;
      }

      do
      {
        field = type.GetField(name, INSTANCE);
        type  = type.BaseType;
      }
      while (field is null && type != null);

      return field != null && ( field.IsPublic || field.IsDefined<SerializeField>() );
    }

    public static bool TryGetInternalField([CanBeNull] this Type type, [NotNull] string name, out FieldInfo field)
    {
      return TryGetField(type, name, out field, NONPUBLIC);
    }


    public static bool TryGetStaticValue<T>([CanBeNull] this Type type, [NotNull] string name, out T value)
    {
      if (TryGetField(type, name, out var field, STATIC) &&
          typeof(T).IsAssignableFrom(field.FieldType))
      {
        value = (T)field.GetValue(null);
        return true;
      }

      value = default;
      return false;
    }

  } // end static class TypeMembers

}
