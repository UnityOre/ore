/*! @file       Static/DeviceSpy+Android.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2023-08-31
**/

#if UNITY_ANDROID

using UnityEngine;

namespace Ore
{
  public static partial class DeviceSpy
  {
    // TODO: CalcIsChromeOS() - https://docs.unity3d.com/ScriptReference/Android.AndroidDevice-hardwareType.html

    static bool CalcAndroidIsAndroidGO()
    {
      #if UNITY_EDITOR
        if (Application.isEditor) return false;
      #endif

      const string ACTIVITY_SERVICE = "activity"; // https://developer.android.com/reference/android/content/Context#ACTIVITY_SERVICE

      AndroidJavaObject actMan = null;
      try
      {
        actMan = AndroidBridge.Activity.Call<AndroidJavaObject>("getSystemService", ACTIVITY_SERVICE);
        if (actMan != null)
        {
          return actMan.Call<bool>("isLowRamDevice");
        }
      }
      catch (System.Exception ex)
      {
        Orator.NFE(ex);
      }
      finally
      {
        actMan?.Dispose();
      }

      return false;
    }

    static string CalcAndroidBrowser()
    {
      #if UNITY_EDITOR
        if (Application.isEditor) return string.Empty;
      #endif

      const string DUMMY_URL          = "https://example.com";
      const long   MATCH_DEFAULT_ONLY = 0x00010000; // https://developer.android.com/reference/android/content/pm/PackageManager#MATCH_DEFAULT_ONLY

      bool api33 = OSVersion.Major >= 33;

      AndroidJavaObject uri    = null,
                        intent = null,
                        flags  = null,
                        resolv = null;

      string name = string.Empty;

      try
      {
        uri    = AndroidBridge.MakeUri(DUMMY_URL);
        intent = AndroidBridge.MakeIntent("android.intent.action.VIEW", uri);

        // https://developer.android.com/reference/android/content/pm/PackageManager#resolveActivity(android.content.Intent,%20int)
        if (api33)
        {
          flags  = AndroidBridge.Classes.ResolveInfoFlags.CallStatic<AndroidJavaObject>("of", MATCH_DEFAULT_ONLY);
          resolv = AndroidBridge.PackageManager.Call<AndroidJavaObject>("resolveActivity", intent, flags);
        }
        else
        {
          resolv = AndroidBridge.PackageManager.Call<AndroidJavaObject>("resolveActivity", intent, (int)MATCH_DEFAULT_ONLY);
        }

        name = resolv?.Call<AndroidJavaObject>("loadLabel", AndroidBridge.PackageManager)
                     ?.Call<string>("toString")
                     ?? string.Empty;

        return name;
      }
      catch (System.Exception ex)
      {
        // elevated AndroidJavaException -> Exception
        // (since both would be dealt with equally, and we want neither to cause U.B.)
        Orator.NFE(ex);
      }
      finally
      {
        uri?.Dispose();
        intent?.Dispose();
        flags?.Dispose();
        resolv?.Dispose();
      }

      return name;
    }

    static string CalcAndroidCarrier()
    {
      #if UNITY_EDITOR
        if (Application.isEditor) return string.Empty;
      #endif

      const string TELEPHONY_SERVICE = "phone"; // https://developer.android.com/reference/android/content/Context#TELEPHONY_SERVICE

      AndroidJavaObject teleMan = null;
      try
      {
        teleMan = AndroidBridge.Activity.Call<AndroidJavaObject>("getSystemService", TELEPHONY_SERVICE);
        if (teleMan != null)
        {
          return teleMan.Call<string>("getNetworkOperatorName") ?? string.Empty;
        }
      }
      catch (AndroidJavaException aje)
      {
        Orator.NFE(aje);
      }
      finally
      {
        teleMan?.Dispose();
      }

      return string.Empty;
    }

    static string CalcAndroidIDFV()
    {
      #if UNITY_EDITOR
        if (Application.isEditor) return UDID;
      #endif

      AndroidJavaObject resolv = null;
      AndroidJavaClass  secure = null;

      try
      {
        resolv = AndroidBridge.Activity.Call<AndroidJavaObject>("getContentResolver");
        secure = new AndroidJavaClass("android.provider.Settings$Secure");

        string id = secure.CallStatic<string>("getString", resolv, "android_id");
        return id ?? string.Empty;
      }
      catch (AndroidJavaException aje)
      {
        Orator.NFE(aje);
      }
      finally
      {
        resolv?.Dispose();
        secure?.Dispose();
      }

      return string.Empty;
    }

    static string CalcAndroidIDFA()
    {
      #if UNITY_EDITOR
        if (Application.isEditor) return string.Empty;
      #endif

      AndroidJavaClass adidClient = null;
      AndroidJavaObject adidInfo = null;
      try
      {
        adidClient = new AndroidJavaClass("com.google.android.gms.ads.identifier.AdvertisingIdClient");
        adidInfo = adidClient.CallStatic<AndroidJavaObject>("getAdvertisingIdInfo", AndroidBridge.Activity);

        s_IsAdTrackingLimited = adidInfo.Call<bool>("isLimitAdTrackingEnabled");
        if (s_IsAdTrackingLimited)
          return string.Empty;

        string id = adidInfo.Call<string>("getId");

        return id ?? string.Empty;
      }
      catch (AndroidJavaException aje)
      {
        Orator.NFE(aje);
      }
      finally
      {
        adidClient?.Dispose();
        adidInfo?.Dispose();
      }

      return string.Empty;
    }

    static string CalcAndroidISO6391() // 2-letter retval
    {
      #if UNITY_EDITOR
        if (Application.isEditor) return string.Empty;
      #endif

      try
      {
        return AndroidBridge.SystemLocale.Call<string>("getLanguage")?.ToUpperInvariant() ?? string.Empty;
      }
      catch (AndroidJavaException aje)
      {
        Orator.NFE(aje);
      }

      return string.Empty;
    }

    static string CalcAndroidISO6392() // 3-letter retval
    {
      #if UNITY_EDITOR
        if (Application.isEditor) return string.Empty;
      #endif

      try
      {
        return AndroidBridge.SystemLocale.Call<string>("getISO3Language")?.ToUpperInvariant() ?? string.Empty;
      }
      catch (AndroidJavaException aje)
      {
        Orator.NFE(aje);
      }

      return string.Empty;
    }

    static bool s_AndroidMemInfoBadBit;
    static bool CalcAndroidMemoryInfo(out long totalMem, out long availMem, out long threshold, out bool lowMemory)
    {
      // https://developer.android.com/reference/android/app/ActivityManager.MemoryInfo#threshold

      const string ACTIVITY_SERVICE = "activity"; // https://developer.android.com/reference/android/content/Context#ACTIVITY_SERVICE

      totalMem  = SystemInfo.systemMemorySize * Consts.Mega;
      availMem  = 400 * Consts.Mega;
      threshold = 200 * Consts.Mega;
      lowMemory = false;

      #if UNITY_EDITOR
        if (Application.isEditor) return false;
      #endif

      if (s_AndroidMemInfoBadBit)
        return false;

      AndroidJavaObject actiMan = null,
                        memInfo = null;
      try
      {
        memInfo = new AndroidJavaObject("android.app.ActivityManager$MemoryInfo");
        actiMan = AndroidBridge.Activity.Call<AndroidJavaObject>("getSystemService", ACTIVITY_SERVICE);
        actiMan.Call("getMemoryInfo", memInfo);

        totalMem  = memInfo.Get<long>("totalMem");
        availMem  = memInfo.Get<long>("availMem");
        threshold = memInfo.Get<long>("threshold");
        lowMemory = memInfo.Get<bool>("lowMemory");

        return true;
      }
      catch (System.Exception ex)
      {
        s_AndroidMemInfoBadBit = true;
        Orator.NFE(ex);
      }
      finally
      {
        actiMan?.Dispose();
        memInfo?.Dispose();
      }

      return false;
    }

    static int  s_AndroidCacheMemInfoFrame = -1;
    static long s_AndroidCacheTotalMem, s_AndroidCacheAvailMem, s_AndroidCacheThreshold;
    static bool s_AndroidCacheLowMemory;
    static bool CalcAndroidMemoryInfoCached(out long totalMem, out long availMem, out long threshold, out bool lowMemory)
    {
      const int GRACE_FRAMES = 37;

      if (s_AndroidCacheMemInfoFrame > 0 && Time.frameCount - s_AndroidCacheMemInfoFrame <= GRACE_FRAMES)
      {
        totalMem  = s_AndroidCacheTotalMem;
        availMem  = s_AndroidCacheAvailMem;
        threshold = s_AndroidCacheThreshold;
        lowMemory = s_AndroidCacheLowMemory;
        return true;
      }

      if (!CalcAndroidMemoryInfo(out totalMem,
                                 out availMem,
                                 out threshold,
                                 out lowMemory))
      {
        return false;
      }

      s_TotalRAMMega     = (float)((double)totalMem  / Consts.Mega);
      s_LowRAMThreshMega = (float)((double)threshold / Consts.Mega);

      s_AndroidCacheMemInfoFrame = Time.frameCount;
      s_AndroidCacheTotalMem     = totalMem;
      s_AndroidCacheAvailMem     = availMem;
      s_AndroidCacheThreshold    = threshold;
      s_AndroidCacheLowMemory    = lowMemory;

      return true;
    }

  } // end class DeviceSpy
}

#endif // UNITY_ANDROID
