/*! @file       Static/EditorBridge.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2022-02-28
**/

//#undef UNITY_EDITOR // periodically test me by flipping with "//"

// ReSharper disable MemberCanBePrivate.Global

using JetBrains.Annotations;

using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

using System.Collections.Generic;
using System.Linq;

using ConditionalAttribute = System.Diagnostics.ConditionalAttribute;


namespace Ore
{

#if UNITY_EDITOR

  [PublicAPI]
  public static class EditorBridge
  {

    // constant value like these are REQUIRED in certain contexts,
    // e.g. default parameter values
    public const bool IS_EDITOR = true;
    #if DEBUG
    public const bool IS_DEBUG = true;
    #else
    public const bool IS_DEBUG = false;
    #endif


    [Conditional("UNITY_EDITOR")]
    public static void Ping(Object obj)
    {
      if (obj)
        EditorGUIUtility.PingObject(obj);
    }

    [Conditional("UNITY_EDITOR")]
    public static void Ping(int id)
    {
      if (id != 0)
        EditorGUIUtility.PingObject(id);
    }


    public static Object[] GetSubAssets([NotNull] Object asset)
    {
      if (!AssetDatabase.IsMainAsset(asset))
        return System.Array.Empty<Object>();

      return AssetDatabase.LoadAllAssetRepresentationsAtPath(AssetDatabase.GetAssetPath(asset));
    }

    public static bool HasSubAssets([NotNull] Object asset)
    {
      return GetSubAssets(asset).Length > 0;
    }


    public static bool DeleteSubAsset([NotNull] Object subAsset, bool commit)
    {
      if (!AssetDatabase.IsSubAsset(subAsset))
        return false;

      string path = AssetDatabase.GetAssetPath(subAsset);
      if (path.IsEmpty())
        return false;

      AssetDatabase.RemoveObjectFromAsset(subAsset);
      Object.DestroyImmediate(subAsset, allowDestroyingAssets: true);

      return !subAsset && (!commit || CommitChangedPath(path));
    }

    public static int DeleteAllSubAssets([NotNull] Object mainAsset, bool commit)
    {
      if (!IsMainAsset(mainAsset))
        return -1;

      int removed = 0;

      var subAssets = GetSubAssets(mainAsset);

      int i = subAssets.Length;
      while (i --> 0)
      {
        if (DeleteSubAsset(subAssets[i], commit: false))
          ++ removed;
      }

      if (commit && removed > 0 && !CommitChangedAsset(mainAsset))
        return -removed;

      return removed;
    }


    public static bool CommitChangedPath(string path)
    {
      if (path.IsEmpty())
        return false;

      AssetDatabase.SaveAssets();
      AssetDatabase.ImportAsset(path, ImportAssetOptions.ForceUpdate);

      return true;
    }

    public static bool CommitChangedAsset([NotNull] Object asset)
    {
      return CommitChangedPath(AssetDatabase.GetAssetPath(asset));
    }

    [Conditional("UNITY_EDITOR")]
    public static void CommitAllChanges(bool reimport = true)
    {
      AssetDatabase.SaveAssets();

      if (reimport)
        AssetDatabase.Refresh(ImportAssetOptions.Default);
    }


    public static bool IsAsset(Object obj)
    {
      return obj && AssetDatabase.Contains(obj);
    }

    public static bool IsMainAsset(Object asset)
    {
      return asset && AssetDatabase.IsMainAsset(asset.GetInstanceID());
    }

    public static bool IsPreloadedAsset(Object asset)
    {
      if (!IsAsset(asset))
        return false;

      var preloaded = PlayerSettings.GetPreloadedAssets();
      int i = preloaded.Length;
      while (i --> 0)
      {
        if (preloaded[i] == asset)
          return true;
      }

      return false;
    }

    public static bool TrySetPreloadedAsset(Object asset, bool set)
    {
      if (!IsMainAsset(asset))
        return false;

      var buffer = new List<Object>(PlayerSettings.GetPreloadedAssets());

      int changed = buffer.RemoveAll(obj => !obj || (!set && obj == asset));

      if (set && !buffer.Contains(asset))
      {
        buffer.Add(asset);
        ++ changed;
      }

      if (changed > 0)
      {
        PlayerSettings.SetPreloadedAssets(buffer.ToArray());
      }

      return true;
    }


    [Conditional("UNITY_EDITOR")]
    public static void BrowseTo(string path)
    {
      EditorUtility.RevealInFinder(path);
    }


    const string MENU_PREFIX = Consts.MenuItemPrefixOre + nameof(EditorBridge) + "/";
    const int    MENU_PRIO   = Consts.MenuPriorityOre;


    [MenuItem(MENU_PREFIX + "Browse to Persistent Data Path", priority = MENU_PRIO)]
    [Conditional("UNITY_EDITOR")]
    public static void BrowseToPersistentDataPath()
    {
      EditorUtility.RevealInFinder(Application.persistentDataPath);
    }

    [MenuItem(MENU_PREFIX + "Browse to Temp Path", priority = MENU_PRIO)]
    [Conditional("UNITY_EDITOR")]
    public static void BrowseToTempPath()
    {
      EditorUtility.RevealInFinder(Application.temporaryCachePath);
    }


    // Assets/ menu items = slightly different:

    [MenuItem("Assets/Ore/Delete Sub Asset(s)", priority = MENU_PRIO)]
    [Conditional("UNITY_EDITOR")]
    static void Menu_DeleteSubAssets()
    {
      int removed = 0;
      var filtered = Selection.GetFiltered<Object>(SelectionMode.Assets | SelectionMode.Editable);

      foreach (var asset in filtered)
      {
        if (IsMainAsset(asset))
          removed += DeleteAllSubAssets(asset, commit: false);
        else if (DeleteSubAsset(asset, commit: false))
          ++ removed;
      }

      if (removed <= 0)
      {
        Orator.Warn(typeof(EditorBridge), "Could not delete any subassets from the selection. Bug?");
        return;
      }

      CommitAllChanges(reimport: true);

      Orator.Log($"Deleted <color=#FF0000>{removed}</color> subAssets from the selection!");
    }

    [MenuItem("Assets/Ore/Delete Sub Asset(s)", isValidateFunction: true)]
    static bool Menu_CanDeleteSubAssets()
    {
      var filtered = Selection.GetFiltered<Object>(SelectionMode.Assets | SelectionMode.Editable);
      return filtered.Length > 0 && filtered.Any(ass => AssetDatabase.IsSubAsset(ass) || HasSubAssets(ass));
    }


    [MenuItem("Assets/Ore/Delete Missing Scripts", priority = MENU_PRIO)]
    static void Menu_DeleteMissingScripts()
    {
      int removed = 0;

      foreach (var prefab in Selection.GetFiltered<GameObject>(SelectionMode.Assets | SelectionMode.Editable))
      {
        int subRemoved = GameObjectUtility.RemoveMonoBehavioursWithMissingScript(prefab);

        if (subRemoved == 0)
          continue;

        removed += subRemoved;

        Orator.Log($"Removed {subRemoved} missing scripts from \"{prefab.name}\".");
      }

      Orator.Log($"Removed {removed} total missing script components.");
    }

    [MenuItem("Assets/Ore/Delete Missing Scripts", isValidateFunction: true)]
    static bool Menu_CanDeleteMissingScripts()
    {
      var filtered = Selection.GetFiltered<GameObject>(SelectionMode.Assets | SelectionMode.Editable);
      return filtered.Length > 0 && filtered.Any(ass => ass.GetComponentsInChildren<MonoBehaviour>().Any(c => !c));
    }

  } // end static class EditorBridge

#else // if !UNITY_EDITOR

  [PublicAPI]
  public static class EditorBridge
  {

    // constant value like these are REQUIRED in certain contexts,
    // e.g. default parameter values
    public const bool IS_EDITOR = false;
    #if DEBUG
    public const bool IS_DEBUG = true;
    #else
    public const bool IS_DEBUG = false;
    #endif


    [Conditional("UNITY_EDITOR")]
    public static void Ping(Object obj)
    {
    }

    [Conditional("UNITY_EDITOR")]
    public static void Ping(int id)
    {
    }

    public static Object[] GetSubAssets([NotNull] Object asset)
    {
      return System.Array.Empty<Object>();
    }

    public static bool HasSubAssets([NotNull] Object asset)
    {
      return false;
    }


    public static bool DeleteSubAsset([NotNull] Object subAsset, bool commit)
    {
      return false;
    }

    public static int DeleteAllSubAssets([NotNull] Object mainAsset, bool commit)
    {
      return 0;
    }


    public static bool CommitChangedPath(string path)
    {
      return true;
    }

    public static bool CommitChangedAsset([NotNull] Object asset)
    {
      return true;
    }

    [Conditional("UNITY_EDITOR")]
    public static void CommitAllChanges(bool reimport = true)
    {
    }

    public static bool IsAsset(Object obj)
    {
      return false;
    }

    public static bool IsMainAsset(Object asset)
    {
      return asset;
    }

    public static bool IsPreloadedAsset(Object asset)
    {
      return false;
    }

    public static bool TrySetPreloadedAsset(Object asset, bool set)
    {
      return true;
    }

    [Conditional("UNITY_EDITOR")]
    public static void BrowseTo(string path)
    {
    }

    [Conditional("UNITY_EDITOR")]
    public static void BrowseToPersistentDataPath()
    {
    }

    [Conditional("UNITY_EDITOR")]
    public static void BrowseToTempPath()
    {
    }

  } // end static class EditorBridge

#endif // UNITY_EDITOR

}
