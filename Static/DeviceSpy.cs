/*! @file       Static/DeviceSpy.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2022-01-20
**/

#if KOOBOX_EXPLORE // deprecated symbol
#warning KOOBOX_EXPLORE is deprecated. Define ORE_DEBUG instead.
#define ORE_DEBUG
#endif

using System.Collections.Generic;

using JetBrains.Annotations;

using UnityEngine;
using UnityEngine.Rendering;

#if UNITY_EDITOR
using MenuItem = UnityEditor.MenuItem;
#endif

using TimeSpan   = System.TimeSpan;
using RegionInfo = System.Globalization.RegionInfo;


namespace Ore
{
  [PublicAPI]
  public static partial class DeviceSpy
  {

  #region     Public section

    // ReSharper disable ConvertToNullCoalescingCompoundAssignment
    // ReSharper disable ConvertIfStatementToNullCoalescingAssignment

    public static ABI ABI => (ABI)(s_ABI ?? (s_ABI = CalcABIArch()));

    public static float AspectRatio => (float)(s_AspectRatio ?? (s_AspectRatio = CalcAspectRatio()));

    public static float AvailableRAM => CalcAvailRAMMega();

    public static string Brand
    {
      get
      {
        if (s_Brand is null)
          (s_Brand, s_Model) = CalcMakeModel();
        return s_Brand;
      }
    }

    public static string Browser => s_Browser ?? (s_Browser = CalcBrowserName());

    public static string Carrier => s_Carrier ?? (s_Carrier = CalcCarrier());

    public static string CountryISOString => s_CountryISO3166a2 ?? (s_CountryISO3166a2 = CalcISO3166a2());

    /// <summary>
    ///   in megabytes.
    /// </summary>
    public static float CurrentRAM => (float)((double)CalcRAMUsageBytes() / Consts.Mega);

    /// <summary>
    ///   poor-man's instantaneous FPS.
    /// </summary>
    public static float CurrentFPS => 1f / Time.unscaledDeltaTime;

    public static float DiagonalInches => (float)(s_DiagonalInches ?? (s_DiagonalInches = CalcScreenDiagonalInches()));

    public static float DPI => (float)(s_DPI ?? (s_DPI = Screen.dpi));

    public static string FallbackID => s_FallbackID ?? (s_FallbackID = CalcFallbackID());

    public static SerialVersion GLVersion => s_GLVersion ?? (s_GLVersion = CalcGraphicsVersion());

    public static string IDFA => s_IDFA ?? (s_IDFA = CalcIDFA());

    public static string IDFV => s_IDFV ?? (s_IDFV = CalcIDFV());

    public static bool Is64Bit => ABI == ABI.ARM64 || s_ABI == ABI.x86_64;

    public static bool IsAndroidGO => (bool)(s_IsAndroidGO ?? (s_IsAndroidGO = CalcIsAndroidGO()));

    public static bool IsBlueStacks => (bool)(s_IsBlueStacks ?? (s_IsBlueStacks = CalcIsBlueStacks()));

    public static bool IsRAMLow => CalcIsRAMLow();

    public static bool IsT1Graphics => (bool)(s_IsT1Graphics ?? (s_IsT1Graphics = CalcIsT1Graphics()));

    public static bool IsTablet => (bool)(s_IsTablet ?? (s_IsTablet = CalcIsTablet()));

    public static bool IsTrackingLimited
    {
      get
      {
        #if UNITY_EDITOR
          return false;
        #elif UNITY_ANDROID
          if (s_IDFA is null)
            s_IDFA = CalcAndroidIDFA(); // TODO somehow reset cached value so it can update?
          return s_IsAdTrackingLimited;
        #elif UNITY_IOS
          return s_IsAdTrackingLimited = UnityEngine.iOS.Device.advertisingTrackingEnabled;
        #else
          return s_IsAdTrackingLimited = false;
        #endif
      }
    }

    public static string LanguageISOString => s_LangISO6391 ?? (s_LangISO6391 = CalcISO6391());

    public static string Model
    {
      get
      {
        if (s_Model is null)
          (s_Brand, s_Model) = CalcMakeModel();
        return s_Model;
      }
    }

    [NotNull]
    public static SerialVersion OSVersion => s_OSVersion ?? (s_OSVersion = CalcOSVersion());

    public static int ScreenRefreshHz => s_ScreenRefreshHz ?? CalcRefreshHz();

    public static string TimezoneISOString => Strings.MakeISOTimezone(TimezoneOffset);

    public static TimeSpan TimezoneOffset => (TimeSpan)(s_TimezoneOffset ?? (s_TimezoneOffset = CalcTimezoneOffset()));

    /// <summary>
    ///   in megabytes.
    /// </summary>
    public static float TotalRAM => (float)(s_TotalRAMMega ?? (s_TotalRAMMega = CalcTotalRAMMega()));

    public static string UDID => s_UDID ?? (s_UDID = CalcVendorUDID());


    public static bool IsSlowFrame()
    {
      return IsSlowFrame(Time.unscaledDeltaTime);
    }

    public static bool IsSlowFrame(float deltaTime)
    {
      return deltaTime > 1.5f / ScreenRefreshHz && ( Application.targetFrameRate < 1 ||
                                                     deltaTime > 1.5f / Application.targetFrameRate );
    }

    /// <returns>
    ///   The given fps normalized to this device's maximum possible FPS,
    ///   clamped between [0,1].
    /// </returns>
    public static float NormalizeFPS(float fps)
    {
      int   target = Application.targetFrameRate;
      float max    = ScreenRefreshHz;

      if (target > 0 && target < max)
        max = target;
      else if (max < 1f)
        max = 1f;

      return (fps / max).Clamp01();
    }

    /// <returns>
    ///   The given RAM measurement (in megabytes) normalized to this device's
    ///   TOTAL USED RAM (including RAM used by the system and other apps).
    ///   The result is clamped [0,1].
    /// </returns>
    /*public*/ static float NormalizeRAM(float megabytes)
    {
      return (megabytes / (TotalRAM - AvailableRAM)).Clamp01();
    }


    public static string ToJson()
    {
      var json = new Dictionary<string,object>();

      foreach (var property in typeof(DeviceSpy).GetProperties(TypeMembers.STATIC))
      {
        try
        {
          var value = property.GetValue(null);
          if (property.PropertyType.IsPrimitive)
          {
            json[property.Name] = value;
          }
          else if (value is TimeSpan span)
          {
            json[property.Name] = new TimeInterval(span).ToString();
          }
          else
          {
            json[property.Name] = value?.ToString();
          }
        }
        catch (System.Exception e)
        {
          Orator.NFE(e);
          json[property.Name] = null;
        }
      }

      // Doing conditional compilation for the extras to prevent potential app size increase
      // (resulting from less engine code getting stripped)

      #if ORE_DEBUG

      json["OSVersionStripped"] = OSVersion.ToString(stripExtras: true);

      #if !UNITY_EDITOR && UNITY_ANDROID
      json["TargetAPI"]       = AndroidBridge.TargetAPI;
      #endif

      #if UNITY_ANDROID
      if (CalcAndroidMemoryInfoCached(out long total, out long avail, out long thresh, out bool isLow))
      {
        json["AndroidMemoryInfo"] = new Dictionary<string,object>
        {
          ["totalMem"]  = Strings.FormatBytesKibi(total),
          ["availMem"]  = Strings.FormatBytesKibi(avail),
          ["threshold"] = Strings.FormatBytesKibi(thresh),
          ["lowMemory"] = isLow
        };
      }
      #endif

      json["NormalizedFPS"] = NormalizeFPS(CurrentFPS);
      json["NormalizedRAM"] = NormalizeRAM(CurrentRAM);

      json["Screen"] = new Dictionary<string,object>
      {
        ["width"]       = Screen.width,
        ["height"]      = Screen.height,
        ["brightness"]  = Screen.brightness,
        ["resolutions"] = Screen.resolutions,
      };

      json["Application"] = new Dictionary<string,object>
      {
        ["absoluteURL"]               = Application.absoluteURL,
        ["unityVersion"]              = Application.unityVersion,
        ["version"]                   = Application.version,
        #if UNITY_ANDROID
        ["VersionCode"]               = AndroidBridge.VersionCode,
        #endif
        ["backgroundLoadingPriority"] = Application.backgroundLoadingPriority,
        ["targetFrameRate"]           = Application.targetFrameRate,
        ["dataPath"]                  = Application.dataPath,
        ["persistentDataPath"]        = Application.persistentDataPath,
        ["streamingAssetsPath"]       = Application.streamingAssetsPath,
        ["temporaryCachePath"]        = Application.temporaryCachePath,
        ["consoleLogPath"]            = Application.consoleLogPath,
        ["ProjectRoot"]               = Paths.ProjectRoot, // just curious
      };

      json["SystemInfo"] = new Dictionary<string,object>
      {
        ["deviceName"]                   = SystemInfo.deviceName,
        ["batteryStatus"]                = SystemInfo.batteryStatus,
        ["batteryLevel"]                 = SystemInfo.batteryLevel,
        ["npotSupport"]                  = SystemInfo.npotSupport,
        ["supportsInstancing"]           = SystemInfo.supportsInstancing,
        ["supportsShadows"]              = SystemInfo.supportsShadows,
        ["supportsVibration"]            = SystemInfo.supportsVibration,
        ["graphicsDevice"]               = new Dictionary<string,object>
        {
          ["Name"]     = SystemInfo.graphicsDeviceName,
          ["Type"]     = SystemInfo.graphicsDeviceType,
          ["Vendor"]   = SystemInfo.graphicsDeviceVendor,
          ["Version"]  = SystemInfo.graphicsDeviceVersion,
          ["ID"]       = SystemInfo.graphicsDeviceID,
          ["VendorID"] = SystemInfo.graphicsDeviceVendorID,
        },
        ["graphicsMemorySize"]           = SystemInfo.graphicsMemorySize,
        ["graphicsMultiThreaded"]        = SystemInfo.graphicsMultiThreaded,
        ["graphicsShaderLevel"]          = SystemInfo.graphicsShaderLevel,
        ["maxTextureSize"]               = SystemInfo.maxTextureSize,
        ["renderingThreadingMode"]       = SystemInfo.renderingThreadingMode,
        ["supportsComputeShaders"]       = SystemInfo.supportsComputeShaders,
        ["supportsLocationService"]      = SystemInfo.supportsLocationService,
        ["supportsMipStreaming"]         = SystemInfo.supportsMipStreaming,
        ["supportsHardwareQuadTopology"] = SystemInfo.supportsHardwareQuadTopology,
        ["Supports(TextureFormat.ASTC_10x10)"]         = SystemInfo.SupportsTextureFormat(TextureFormat.ASTC_10x10),
        ["Supports(TextureFormat.ETC2_RGBA8Crunched)"] = SystemInfo.SupportsTextureFormat(TextureFormat.ETC2_RGBA8Crunched),
        ["Supports(TextureFormat.PVRTC_RGBA4)"]        = SystemInfo.SupportsTextureFormat(TextureFormat.PVRTC_RGBA4),
      };

      #endif // ORE_DEBUG

      return JsonAuthority.Write(json);
    }

    public static void Reset()
    {
      PlayerPrefs.DeleteKey(PREFKEY_UDID);
      PlayerPrefs.DeleteKey(PREFKEY_IDFV);
      PlayerPrefs.DeleteKey(PREFKEY_FBID);

      GeoIP.Reset();
      //    ^ calls PlayerPrefs.Save()

      s_ABI                 = default;
      s_AspectRatio         = default;
      s_Brand               = default;
      s_Browser             = default;
      s_Carrier             = default;
      s_CountryISO3166a2    = default;
      s_DiagonalInches      = default;
      s_DPI                 = default;
      s_FallbackID          = default;
      s_IDFA                = default;
      s_IDFV                = default;
      s_IsAdTrackingLimited = default;
      s_IsAndroidGO         = default;
      s_IsBlueStacks        = default;
      s_IsT1Graphics        = default;
      s_IsTablet            = default;
      s_LangISO6391         = default;
      s_LowRAMThreshMega    = default;
      s_Model               = default;
      s_OSVersion           = default;
      s_ScreenRefreshHz     = default;
      s_TimezoneOffset      = default;
      s_TotalRAMMega        = default;
      s_UDID                = default;

      LittleBirdie.Explode();
    }


    internal static long CalcRAMUsageBytes()
    {
      #if UNITY_2020_1_OR_NEWER
      {
        if (RuntimeSamplers.SampleSystemRAM(out long bytes))
          return bytes;

        return UnityEngine.Profiling.Profiler.GetTotalReservedMemoryLong();
      }
      #else
      {
        return System.GC.GetTotalMemory(forceFullCollection: false);
      }
      #endif
    }


    #if UNITY_EDITOR

    const string MENU_PREFIX = Consts.MenuItemPrefixOre + nameof(DeviceSpy) + "/";
    const int    MENU_PRIO   = Consts.MenuPriorityOre;

    [MenuItem(MENU_PREFIX + "Log + Write JSON", priority = MENU_PRIO)]
    static void Menu_MakeJson()
    {
      string path = Filesystem.GetTempPath($"{nameof(DeviceSpy)}.json");
      string json = ToJson();

      if (Filesystem.TryWriteText(path, json))
      {
        Orator.Log(typeof(DeviceSpy), $"Wrote json to \"{path}\": {json}");
        UnityEditor.EditorUtility.RevealInFinder(path);
      }
      else
      {
        Filesystem.LogLastException();
        Orator.Warn(typeof(DeviceSpy), $"Couldn't write to \"{path}\", but here's the json anyway: {json}");
      }
    }

    #endif // UNITY_EDITOR


    // Extended public section: DeviceSpy+GeoIP.cs, DeviceSpy+LB.cs

  #endregion  Public section



  #region     Private section

    static ABI?          s_ABI;
    static float?        s_AspectRatio;
    static string        s_Brand;
    static string        s_Browser;
    static string        s_Carrier;
    static string        s_CountryISO3166a2;
    static float?        s_DiagonalInches;
    static float?        s_DPI;
    static string        s_FallbackID;
    static SerialVersion s_GLVersion;
    static string        s_IDFA;
    static string        s_IDFV;
    static bool          s_IsAdTrackingLimited;
    static bool?         s_IsAndroidGO;
    static bool?         s_IsBlueStacks;
    static bool?         s_IsT1Graphics;
    static bool?         s_IsTablet;
    static string        s_LangISO6391;
    static float?        s_LowRAMThreshMega;
    static string        s_Model;
    static SerialVersion s_OSVersion;
    static int?          s_ScreenRefreshHz;
    static TimeSpan?     s_TimezoneOffset;
    static float?        s_TotalRAMMega;
    static string        s_UDID;

    const int MIN_UUID_BYTES = 8; // 16 is ideal though.

    const string PREFKEY_UDID = "VENDOR_UDID";
    const string PREFKEY_FBID = "VENDOR_FBID"; // FB as in fallback
    const string PREFKEY_IDFV = "VENDOR_IDFV";


    static bool CalcIsAndroidGO()
    {
      #if UNITY_ANDROID
      return CalcAndroidIsAndroidGO();
      #else
      return false;
      #endif
    }

    static float CalcAvailRAMMega()
    {
      #if UNITY_ANDROID
      if (CalcAndroidMemoryInfoCached(out _ , out long avail, out _ , out _ ))
      {
        return (float)((double)avail / Consts.Mega);
      }
      #elif UNITY_IOS
        // TODO
      #elif UNITY_WEBGL
        // TODO
      #endif

      return TotalRAM - CurrentRAM;
    }

    static bool CalcIsRAMLow()
    {
      #if UNITY_ANDROID
      if (CalcAndroidMemoryInfoCached(out _ , out _ , out _ , out bool isLow))
      {
        return isLow;
      }
      #elif UNITY_IOS
        // TODO
      #elif UNITY_WEBGL
        // TODO
      #endif

      // fallback
      return AvailableRAM < (float)(s_LowRAMThreshMega ?? (s_LowRAMThreshMega = CalcLowRAMThresholdMega()));
    }

    static float CalcTotalRAMMega()
    {
      double lowestEstimate = SystemInfo.systemMemorySize * Consts.Mega;

      #if UNITY_ANDROID
      if (CalcAndroidMemoryInfoCached(out long total, out _ , out _ , out _ ))
      {
        lowestEstimate = total;
      }
      #elif UNITY_IOS
        // TODO
      #elif UNITY_WEBGL
        // TODO
      #endif

      return (float)(lowestEstimate / Consts.Mega).AtLeast(1);
    }

    static float CalcLowRAMThresholdMega()
    {
      #if UNITY_ANDROID
      if (CalcAndroidMemoryInfoCached(out _ , out _ , out long thresh, out _ ))
      {
        return (float)((double)thresh / Consts.Mega);
      }
      #elif UNITY_IOS
        // TODO
      #elif UNITY_WEBGL
        // TODO
      #endif

      return (TotalRAM * 0.5f).AtLeast(250);
    }

    static int CalcRefreshHz()
    {
      #if UNITY_2022_1_OR_NEWER
      return (int)Screen.currentResolution.refreshRateRatio.value.Rounded().AtLeast(30);
      #else
      return Screen.currentResolution.refreshRate.AtLeast(30);
      #endif
    }

    static (string make, string model) CalcMakeModel()
    {
      #if UNITY_IOS
        return ("Apple", SystemInfo.deviceModel);
      #else
        string makemodel = SystemInfo.deviceModel;

        // a la: https://docs.unity3d.com/ScriptReference/SystemInfo-deviceModel.html
        if (makemodel == SystemInfo.unsupportedIdentifier)
          return (string.Empty, string.Empty);

        int split = makemodel.IndexOfAny(new []{ ' ', '-' });
        if (split < 0)
          return (makemodel, makemodel);

        return (makemodel.Remove(split), makemodel.Substring(split + 1));
      #endif
    }

    [NotNull]
    static SerialVersion CalcOSVersion()
    {
      switch (SystemInfo.operatingSystemFamily)
      {
        case OperatingSystemFamily.MacOSX:
          return new SerialVersion(SystemInfo.operatingSystem);

        default:
          return SerialVersion.ExtractOSVersion(SystemInfo.operatingSystem);
      }
    }

    static string CalcFallbackID()
    {
      string guid = PlayerPrefs.GetString(PREFKEY_FBID);
      if (guid != null && guid.Length >= 2 * MIN_UUID_BYTES)
        return guid;

      guid = Strings.MakeGUID();

      PlayerPrefs.SetString(PREFKEY_FBID, guid);

      return guid;
    }

    static string CalcVendorUDID()
    {
      string udid = PlayerPrefs.GetString(PREFKEY_UDID); // don't tell Irontown
      if (udid != null && udid.Length >= 2 * MIN_UUID_BYTES)
        return udid;

      udid = SystemInfo.deviceUniqueIdentifier;

      if (udid.Equals(SystemInfo.unsupportedIdentifier) || udid.Length < 2 * MIN_UUID_BYTES)
      {
        udid = FallbackID;
      }

      PlayerPrefs.SetString(PREFKEY_UDID, udid);
      PlayerPrefs.Save(); // meh, should let someone else save?

      return udid;
    }

    static string CalcBrowserName()
    {
      #if UNITY_ANDROID
        return CalcAndroidBrowser().Trim();
      #elif UNITY_IOS
        return string.Empty; // TODO
      #elif UNITY_WEBGL
        return SystemInfo.deviceModel;
      #else
        return string.Empty;
      #endif
    }

    static string CalcCarrier()
    {
      #if UNITY_ANDROID
        return CalcAndroidCarrier().Trim();
      #elif UNITY_IOS
        return string.Empty; // TODO
      #elif UNITY_WEBGL
        return string.Empty;
      #else
        return string.Empty;
      #endif
    }

    static string CalcISO6391()
    {
      string iso6391 = Strings.MakeISO6391(Application.systemLanguage);

      if (iso6391.IsEmpty())
      {
        #if UNITY_ANDROID
          iso6391 = CalcAndroidISO6391();
        #else // TODO other platforms
          iso6391 = string.Empty;
        #endif
      }

      // handle Tagalog ("TL") collison with 2-letter ISO code for Finnish
      if (iso6391 == "FI")
      {
        #if UNITY_ANDROID
          if (CalcAndroidISO6392() == "FIL") // FIL for Filipino, sometimes aggregated as Tagalog
            return "TL";
        #else // TODO other platforms
          if (CountryISOString == "PH") // reasonable assumption
            return "TL";
        #endif
      }

      return iso6391;
    }

    static string CalcISO3166a2() // 2-letter region code
    {
      string geo = GeoIP.CachedValue;
      return geo.IsEmpty() ? RegionInfo.CurrentRegion.TwoLetterISORegionName : geo;
        // RegionInfo.CurrentRegion is a temporary fallback,
        // and does not give accurate geo on most devices.
        // ADDENDUM: On Android, RegionInfo.CurrentRegion seems to depend on the
        //           device's system langauge to guess region designation.
    }

    static TimeSpan CalcTimezoneOffset()
    {
      // TODO there might be a better (100x faster) Java API to call for Android ~
      return System.TimeZoneInfo.Local.GetUtcOffset(DateTimes.Today);
    }

    static string CalcIDFA()
    {
      #if UNITY_ANDROID
        return CalcAndroidIDFA();
      #elif UNITY_IOS
        return UnityEngine.iOS.Device.advertisingIdentifier;
      #else
        return string.Empty; // TODO (?)
      #endif
    }

    static string CalcIDFV()
    {
      #if UNITY_ANDROID
        return CalcAndroidIDFV();
      #elif UNITY_IOS
        return CalcAppleIDFV();
      #else
        return UDID;
      #endif
    }

    static float CalcScreenDiagonalInches()
    {
      float w = Screen.width  / Screen.dpi;
      float h = Screen.height / Screen.dpi;
      return Mathf.Sqrt(w * w + h * h);
    }

    static float CalcAspectRatio()
    {
      if (Screen.height < Screen.width)
        return (float)Screen.width / Screen.height;
      else
        return (float)Screen.height / Screen.width;
    }

    static bool CalcIsT1Graphics()
    {
      // Note: this is only a binary split for Mobile + Web
      // https://docs.unity3d.com/Manual/graphics-tiers.html
      return Graphics.activeTier == GraphicsTier.Tier1;
    }

    static string CalcGraphicsVersion()
    {
      const int kKeepParts = 2;
      const int kMaxLength = 16;

      using (new RecycledStringBuilder(out var bob))
      {
        string[] splits = SystemInfo.graphicsDeviceVersion.Split(' ');

        for (int i = 0; i < splits.Length; ++i)
        {
          if (i >= kKeepParts && bob.Length + splits[i].Length + 1 > kMaxLength)
            break;

          if (i > 0)
            bob.Append(' ');

          bob.Append(splits[i]);
        }

        return bob.ToString();
      }
    }

    static bool CalcIsTablet()
    {
      #if AD_MEDIATION_MAX
        return MaxSdkUtils.IsTablet();
      #else
        return Model.Contains("iPad") || CalcIsTabletByScreenSize();
      #endif
    }

    static bool CalcIsTabletByScreenSize()
    {
      const float MIN_DIAGONAL_INCHES = 6.5f;
      const float MAX_ASPECT_RATIO = 2.0f;
      return DiagonalInches > MIN_DIAGONAL_INCHES && AspectRatio < MAX_ASPECT_RATIO;
    }

    static bool CalcIsBlueStacks()
    {
      #if !UNITY_EDITOR && UNITY_ANDROID
        foreach (string dir in new string[]{  "/sdcard/windows/BstSharedFolder",
                                              "/mnt/windows/BstSharedFolder" })
        {
          if (System.IO.Directory.Exists(dir))
            return true;
        }
      #endif

      return false;
    }

    static ABI CalcABIArch()
    {
      #if !UNITY_EDITOR && UNITY_IOS // TODO iOS needs to be tested
        if (System.Environment.Is64BitOperatingSystem)
          return ABI.ARM64;
        else
          return ABI.ARM;

      #else

        string type = SystemInfo.processorType;

        // Android and Android-like devices are pretty standard here
        if (type.StartsWith("ARM64"))
          return ABI.ARM64;
        else if (type.StartsWith("ARMv7"))
          return ABI.ARM32;

        // Chrome OS (should be a rare case)
        if (System.Environment.Is64BitOperatingSystem)
          return ABI.x86_64;
        else
          return ABI.x86;

      #endif // !UNITY_IOS
    }

  #endregion  Private section

  } // end class DeviceSpy

}
