/*! @file       Static/Primes.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2022-08-18
**/

using JetBrains.Annotations;

using System.Collections.Generic;

using Math = System.Math;

using Random = UnityEngine.Random;


namespace Ore
{
  public static class Primes
  {
    // TODO make me an OAssetSingleton? so we can use serialization to choose the best prime pool per project!

    [NotNull]
    public static IReadOnlyList<int> HashableSizes => s_115xHash193Sizes;


    public const int MinValue     = 3;          // I know, technically 2 is prime... BUT NOT IN MY HAUS
    public const int MaxValue     = 2146435069; // largest prime that can also be an array size
    public const int MaxSizePrime = 12389291;   // last value of the precomputed HashtableSizes list

    public const long  MaxLongValue         = (1L  << 62) - 57; // https://primes.utm.edu/lists/2small/0bit.html
    public const ulong MaxUnsignedLongValue = (1UL << 63) - 25;


    [PublicAPI]
    public static bool IsPrime(int value)
    {
      return IsPrimeNoLookup(value);
    }

    [PublicAPI]
    public static bool IsPrime(long value)
    {
      return IsLongPrimeNoLookup(value);
    }


    [PublicAPI]
    public static int GetRandom(int min = MinValue, int max = MaxValue)
    {
      return NearestTo(Random.Range(min, max));
    }


    [PublicAPI]
    // ReSharper disable once CognitiveComplexity
    public static int NearestTo(int value)
    {
      if (value < 3)
        return 2;

      value |= 1;

      int d    = 2;
      int sqrt = (int)Math.Sqrt(value);
      while (!IsPrimeNoLookup(value, sqrt))
      {
        value += d;

        if (value >= MaxValue)
          return MaxValue;

        if (d > 0)
        {
          d = -1 * (d + 2);

          while (value < sqrt * sqrt)
          {
            // uncommon case that this scope is entered, or even has to loop
            ++ sqrt;
          }
        }
        else
        {
          d = -1 * (d - 2);
        }

        while (value < sqrt * sqrt)
        {
          -- sqrt;
        }

        // Funky fallthrough scopes, I know, but it's fast and works so bite me
      }

      return value;
    }


    [PublicAPI]
    public static int NextHashableSize(int current, int hashprime = int.MaxValue, int incr = 1)
    {
      if (current < MinValue)
        return MinValue;
      if (current >= MaxValue)
        return MaxValue;

      int lhs = 0;
      int rhs = s_115xHash193Sizes.Length - 1;

      while (lhs <= rhs)
      {
        int idx = (lhs + rhs) >> 1;

        if (s_115xHash193Sizes[idx] < current)
        {
          lhs = idx + 1;
        }
        else if (current < s_115xHash193Sizes[idx])
        {
          rhs = idx - 1;
        }
        else
        {
          lhs = idx;
          break;
        }
      }

      lhs += incr;

      if (lhs < s_115xHash193Sizes.Length)
      {
        current = s_115xHash193Sizes[lhs];

        if ((current - 1) % hashprime != 0)
          return current;

        incr = 0; // avoid double incrementing
      }

      // should be rare case (will happen for massive data structures, or bad hashprimes)
      return NextNoLookup(current + incr, hashprime);
    }

    [PublicAPI]
    public static int Next(int current, int hashprime = int.MaxValue)
    {
      return NextNoLookup(current, hashprime);
    }


    /// <remarks>
    ///   WARNING: Large primes are notoriously slowwww.
    ///   Ideally, avoid this overload during runtime.
    /// </remarks>
    [PublicAPI]
    public static long Next(long current)
    {
      return NextLongNoLookup(current);
    }


  #region Internal section

    static readonly int[] s_115xHash193Sizes = /* same lookup works for hashprimes 97 + 101 */
    {
      5,11,17,23,29,37,47,59,71,89,
      107,127,149,173,211,251,293,347,401,463,
      541,631,733,853,983,1151,1327,1531,1777,2053,
      2371,2729,3163,3643,4201,4861,5623,6469,7451,8573,
      9871,11369,13093,15061,17327,19937,22937,26387,30347,34913,
      40153,46181,53113,61091,70271,80819,92951,106903,122953,141403,
      162623,187027,215087,247363,284477,327163,376241,432713,497633,572281,
      658127,756853,870391,1000969,1151141,1323851,1522447,1750871,2013511,2315567,
      2662921,3062377,3521747,4050019,4657537,5356187,6159617,7083581,8146133,9368059,
      10773289,12389291,
    };

    internal static bool IsPrimeLookup(int value)
    {
      if (value < 2)
        return false;

      if ((value & 1) == 0)
        return value == 2;

      // Note: in practical contexts, BinarySearch here measures much slower than the NoLookup variant

      int lhs = 0;
      int rhs = s_115xHash193Sizes.Length - 1;

      while (lhs <= rhs)
      {
        int idx = (lhs + rhs) >> 1;

        if (s_115xHash193Sizes[idx] < value)
        {
          lhs = idx + 1;
        }
        else if (value < s_115xHash193Sizes[idx])
        {
          rhs = idx - 1;
        }
        else
        {
          return true;
        }
      }

      return IsPrimeNoLookup(value);
    }

    internal static bool IsPrimeNoLookup(int value)
    {
      if (value < 2)
        return false;

      if ((value & 1) == 0)
        return value == 2;

      return IsPrimeNoLookup(value, (int)Math.Sqrt(value));
    }

    internal static bool IsLongPrimeNoLookup(long value)
    {
      if (value < 2L)
        return false;

      if ((value & 1) == 0)
        return value == 2L;

      if (value < MaxValue)
        return IsPrimeNoLookup((int)value, (int)Math.Sqrt(value));

      return value == MaxValue || IsLongPrimeNoLookup(value, (long)Math.Sqrt(value));
    }


    static bool IsPrimeNoLookup(int value, int sqrt)
    {
      for (int i = 3; i <= sqrt; i += 2)
      {
        if (value % i == 0)
          return false;
      }

      return true;
    }

    static bool IsLongPrimeNoLookup(long value, long sqrt)
    {
      // minor TODO: this is a pretty long walk... is there nothing better?

      for (long i = 3; i <= sqrt; i += 2L)
      {
        if (value % i == 0)
          return false;
      }

      return true;
    }

    internal static int NextNoLookup(int current, int hashprime = int.MaxValue)
    {
      if (current < MinValue)
        return MinValue;
      if (current >= MaxValue)
        return MaxValue;

      current = (current | 1) + 2;

      int sqrt = (int)Math.Sqrt(current);

      while (current < MaxValue)
      {
        if ((current - 1) % hashprime != 0 && IsPrimeNoLookup(current, sqrt))
          return current;

        current += 2;

        ++ sqrt;

        if (current < sqrt * sqrt)
        {
          -- sqrt;
        }
      }

      return MaxValue;
    }

    static long NextLongNoLookup(long current)
    {
      if (current < MinValue)
        return MinValue;
      if (current > MaxLongValue)
        return MaxLongValue;

      current = (current | 1) + 2;

      long sqrt = (long)Math.Sqrt(current);

      while (current < long.MaxValue - 1)
      {
        if (IsLongPrimeNoLookup(current, sqrt))
          return current;

        current += 2;

        ++ sqrt;

        if (current < sqrt * sqrt)
        {
          -- sqrt;
        }
      }

      return MaxLongValue;
    }


    #if UNITY_INCLUDE_TESTS

    internal static int NextHashableSize_Orig(int current, int hashprime = int.MaxValue, int incr = 1)
    {
      if (current < MinValue)
        return MinValue;
      if (current >= MaxValue)
        return MaxValue;

      // ( squelch warning about using obsolete method list.BinarySearch(val) )
      #pragma warning disable CS0618
      int idx = s_115xHash193Sizes.BinarySearch(current);
      #pragma warning restore CS0618

      if (idx < 0)
      {
        idx = ~idx;
      }

      idx += incr;

      if (idx < s_115xHash193Sizes.Length)
      {
        current = s_115xHash193Sizes[idx];

        if ((current - 1) % hashprime != 0)
          return current;

        incr = 0;
      }

      return NextNoLookup(current + incr, hashprime);
    }

    internal static int NextHashableSize_NoBS(int current, int hashprime = int.MaxValue, int incr = 1)
    {
      if (current < MinValue)
        return MinValue;
      if (current >= MaxValue)
        return MaxValue;

      current += incr;

      for (int i = 0; i < s_115xHash193Sizes.Length; ++i)
      {
        if (s_115xHash193Sizes[i] <= current)
          continue;

        current = s_115xHash193Sizes[i];

        if ((current - 1) % hashprime != 0)
          return current;

        break;
      }

      return NextNoLookup(current, hashprime);
    }

    #endif // UNITY_INCLUDE_TESTS

  #endregion Internal section

  } // end class Primes
}
