/*! @file       Static/DeviceSpy+iOS.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2023-08-31
**/

#if UNITY_IOS

using UnityEngine;
using UnityEngine.iOS;

namespace Ore
{
  public static partial class DeviceSpy
  {

    static string CalcAppleIDFV()
    {
      // TODO scrap PlayerPrefs
      // I know on iOS native we use the user's keychain to persist IDFV across
      // fresh installs... I don't think PlayerPrefs is as robust.

      #if UNITY_EDITOR
        if (Application.isEditor) return SystemInfo.deviceUniqueIdentifier;
      #endif

      string idfv = PlayerPrefs.GetString(PREFKEY_IDFV);

      if (idfv.IsEmpty() || idfv == SystemInfo.unsupportedIdentifier)
      {
        idfv = Device.vendorIdentifier;

        if (idfv.IsEmpty())
          return UDID;

        PlayerPrefs.SetString(PREFKEY_IDFV, idfv);
      }

      return idfv;
    }

    // TODO the rest

  } // end class DeviceSpy
}

#endif // UNITY_IOS