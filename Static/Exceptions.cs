/*! @file       Runtime/Exceptions.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2023-03-02
**/

#if KOOBOX // deprecated symbol
#warning KOOBOX is deprecated. Define ORE_DEBUG instead.
#define ORE_DEBUG
#endif

using System.Diagnostics;
using JetBrains.Annotations;

using Exception     = System.Exception;
using Type          = System.Type;
using StringBuilder = System.Text.StringBuilder;
using StackTrace    = System.Diagnostics.StackTrace;


namespace Ore
{
  public static class Exceptions
  {
    [PublicAPI]
    public static Exception Silenced([CanBeNull] this Exception self)
    {
      return FauxException.Silence(self);
    }


    #if ORE_DEBUG

    static readonly GreyList<Type> s_TraceBlacklist = new GreyList<Type>(GreyListType.Blacklist)
    {
      null,
      typeof(Orator),
      typeof(OAssert),
      typeof(Exceptions),
    };


    internal static void DisableTraceBlacklist() => s_TraceBlacklist.Type = GreyListType.NoFilter;
    internal static void EnableTraceBlacklist()  => s_TraceBlacklist.Type = GreyListType.Blacklist;


    internal static string FormatCurrentStackTrace(int nFrames)
    {
      using (new RecycledStringBuilder(out var bob))
        return FormatCurrentStackTrace(nFrames, bob);
    }

    static string FormatCurrentStackTrace(int nFrames, StringBuilder bob)
    {
      var stacktrace = new StackTrace(skipFrames: 2, fNeedFileInfo: true);

      int i = 0, ilen = stacktrace.FrameCount;

      if (nFrames < ilen)
        ilen = nFrames;

      string logIndexFmt = Integers.MakeIndexPreformattedString(ilen) + ": ";

      var topType = default(Type);

      for (; i < ilen; ++i)
      {
        var frame  = stacktrace.GetFrame(i);
        var method = frame.GetMethod();
        var type   = method?.DeclaringType;

        if (s_TraceBlacklist.Rejects(type))
          continue;

        Debug.Assert(type != null, "type != null");

        if (topType is null)
          topType = type;

        // TODO differentiate between user and non-user assemblies (like PyroDK)

        // begin printing frames

        bob.AppendFormat(logIndexFmt, i);

        if (!type.Namespace.IsEmpty() && type.Namespace != topType.Namespace)
        {
          bob.Append(type.Namespace).Append('.');
        }

        bob.AppendSignature(method);

        // create file and lineno link: (this format is quite specific in Unity~)
        string file = frame.GetFileName();
        if (!file.IsEmpty())
        {
          bob.Append(" (at ")
              // ReSharper disable once AssignNullToNotNullAttribute
             .Append(Paths.MakeRelativeToProject(file))
             .Append(':')
             .Append(frame.GetFileLineNumber())
             .Append(')');
        }

        bob.Append('\n');
      }

      return bob.ToString();
    }

    #endif // ORE_DEBUG

  } // end static class Exceptions
}