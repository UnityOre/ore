/*! @file       Static/GitStamp.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2023-09-28
**/

using JetBrains.Annotations;

using UnityEngine;

using System.Collections.Generic;


namespace Ore
{
  public static class GitStamp
  {
    [PublicAPI]
    public struct RepoState
    {
      public string Path;
      public string URL;
      public string Hash;
      public string RefName;
      public string Version;

      public static readonly RepoState None = new RepoState
      {
        Path    = string.Empty,
        URL     = string.Empty,
        Hash    = string.Empty,
        RefName = string.Empty,
        Version = string.Empty
      };
    }


    [PublicAPI]
    public static bool IsGitControlledProject => !ProjectState.Hash.IsEmpty();
    [PublicAPI]
    public static RepoState ProjectState => MaybeGetState(Application.identifier);
    [PublicAPI]
    public static RepoState OreState => MaybeGetState("dev.leviperez.ore", "dev.leviperez.ore-arcade");
    [PublicAPI]
    public static RepoState FNXState => MaybeGetState("com.bore.phoenix-unity");
    [PublicAPI, NotNull]
    public static string BuildMasterName => MaybeGetState("whoami").Path;
    [PublicAPI, NotNull]
    public static string BuildMasterEmail => MaybeGetState("whoami").URL;


    public static RepoState MaybeGetState(string id, params string[] altIds)
    {
      if (s_Data.Count == 0)
        ReadData();

      if (s_Data.TryGetValue(id, out var state))
        return state;

      if (altIds is null || altIds.Length == 0)
        return RepoState.None;

      foreach (string altId in altIds)
      {
        if (s_Data.TryGetValue(altId, out state))
          return state;
      }

      return RepoState.None;
    }


    //
    // privates

    static IDictionary<string,RepoState> s_Data = new Dictionary<string,RepoState>();
      // using Dictionary to preserve internal order

    static string s_BuildMasterName;

    internal const string JsonResource  = "Generated/" + nameof(GitStamp);

    internal static bool DataEqual(IDictionary<string,RepoState> data)
    {
      if (s_Data.Count != data.Count)
        return false;

      foreach (var kvp in s_Data)
      {
        if (!data.TryGetValue(kvp.Key, out var val) ||
            !kvp.Value.Equals(val))
        {
          return false;
        }
      }

      return true;
    }

    internal static void SetData(IDictionary<string,RepoState> data)
    {
      s_Data = data;
    }

    static void ReadData()
    {
      s_Data.Clear();

      var asset = Resources.Load<TextAsset>(JsonResource);

      if (!asset || asset.text.IsEmpty())
        return;

      if (!JsonAuthority.TryRead(asset.text, out Dictionary<string,RepoState> data))
      {
        Orator.Warn(typeof(GitStamp), "wasn't able to read git data file from Resources/Generated");
      }

      s_Data = data;

      Resources.UnloadAsset(asset);
    }

  } // end class GitGuy
}