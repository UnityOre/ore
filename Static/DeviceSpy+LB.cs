/*! @file       Static/DeviceSpy+LB.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2023-08-31
**/

using JetBrains.Annotations;

using UnityEngine;

using TimeSpan = System.TimeSpan;


namespace Ore
{
  public static partial class DeviceSpy
  {
    /// <summary>
    ///   Special API which allows you to globally override DeviceSpy's perception
    ///   of the current device.
    /// </summary>
    /// <remarks>
    ///   By design, nothing within this class is defensively validated (aside from data type, of course). <br/>
    ///   <i>Little birdies can lie.</i> <br/>
    ///   <b>USE WITH CAUTION!</b>
    /// </remarks>
    [PublicAPI]
    public static class LittleBirdie
    {
      public delegate void PropertyAction([NotNull] string propertyName, [CanBeNull] object value);
      public static event PropertyAction OnCheepCheep;

      // ReSharper disable MemberHidesStaticFromOuterClass

      public static ABI ABI
      {
        get => DeviceSpy.ABI;
        set
        {
          if (s_ABI.Equals(value))
            return;

          s_ABI = value;
          OnCheepCheep?.Invoke(nameof(ABI), value);
        }
      }

      public static float AspectRatio
      {
        get => DeviceSpy.AspectRatio;
        set
        {
          if (s_AspectRatio.Equals(value))
            return;

          s_AspectRatio = value;
          OnCheepCheep?.Invoke(nameof(AspectRatio), value);
        }
      }

      public static string Brand
      {
        get => DeviceSpy.Brand;
        set
        {
          if (s_Brand == value)
            return;

          s_Brand = value;
          OnCheepCheep?.Invoke(nameof(Brand), value);
        }
      }

      public static string Browser
      {
        get => DeviceSpy.Browser;
        set
        {
          if (s_Browser == value)
            return;

          s_Browser = value;
          OnCheepCheep?.Invoke(nameof(Browser), value);
        }
      }

      public static string Carrier
      {
        get => DeviceSpy.Carrier;
        set
        {
          if (s_Carrier == value)
            return;

          s_Carrier = value;
          OnCheepCheep?.Invoke(nameof(Carrier), value);
        }
      }

      public static string CountryISOString
      {
        get => DeviceSpy.CountryISOString;
        set
        {
          if (s_CountryISO3166a2 == value)
            return;

          s_CountryISO3166a2 = value;
          OnCheepCheep?.Invoke(nameof(CountryISOString), value);
        }
      }

      public static float DPI
      {
        get => DeviceSpy.DPI;
        set
        {
          if (s_DPI.Equals(value))
            return;

          s_DPI = value;
          OnCheepCheep?.Invoke(nameof(DPI), value);
        }
      }

      public static string FallbackID
      {
        get => DeviceSpy.FallbackID;
        set
        {
          if (s_FallbackID == value)
            return;

          if (value.IsEmpty())
            PlayerPrefs.DeleteKey(PREFKEY_FBID);

          s_FallbackID = value;
          OnCheepCheep?.Invoke(nameof(FallbackID), value);
        }
      }

      public static SerialVersion GLVersion
      {
        get => DeviceSpy.GLVersion;
        set
        {
          if (s_GLVersion == value)
            return;

          s_GLVersion = value;
          OnCheepCheep?.Invoke(nameof(GLVersion), value);
        }
      }

      public static string IDFA
      {
        get => DeviceSpy.IDFA;
        set
        {
          if (s_IDFA == value)
            return;

          s_IDFA = value;
          OnCheepCheep?.Invoke(nameof(IDFA), value);
        }
      }

      public static string IDFV
      {
        get => DeviceSpy.IDFV;
        set
        {
          if (s_IDFV == value)
            return;

          if (value.IsEmpty())
            PlayerPrefs.DeleteKey(PREFKEY_IDFV);

          s_IDFV = value;
          OnCheepCheep?.Invoke(nameof(IDFV), value);
        }
      }

      public static bool IsAndroidGO
      {
        get => DeviceSpy.IsAndroidGO;
        set
        {
          if (s_IsAndroidGO.Equals(value))
            return;

          s_IsAndroidGO = value;
          OnCheepCheep?.Invoke(nameof(IsAndroidGO), value);
        }
      }

      public static bool IsBlueStacks
      {
        get => DeviceSpy.IsBlueStacks;
        set
        {
          if (s_IsBlueStacks.Equals(value))
            return;

          s_IsBlueStacks = value;
          OnCheepCheep?.Invoke(nameof(IsBlueStacks), value);
        }
      }

      public static bool IsT1Graphics
      {
        get => DeviceSpy.IsT1Graphics;
        set
        {
          if (s_IsT1Graphics.Equals(value))
            return;

          s_IsT1Graphics = value;
          OnCheepCheep?.Invoke(nameof(IsT1Graphics), value);
        }
      }

      public static bool IsTablet
      {
        get => DeviceSpy.IsTablet;
        set
        {
          if (s_IsTablet.Equals(value))
            return;

          s_IsTablet = value;
          OnCheepCheep?.Invoke(nameof(IsTablet), value);
        }
      }

      public static bool IsTrackingLimited
      {
        get => DeviceSpy.IsTrackingLimited;
        set
        {
          if (s_IsAdTrackingLimited.Equals(value))
            return;

          s_IsAdTrackingLimited = value;
          OnCheepCheep?.Invoke(nameof(IsTrackingLimited), value);
        }
      }

      public static string LanguageISOString
      {
        get => DeviceSpy.LanguageISOString;
        set
        {
          if (s_LangISO6391 == value)
            return;

          s_LangISO6391 = value;
          OnCheepCheep?.Invoke(nameof(LanguageISOString), value);
        }
      }

      [System.Obsolete]
      public static float LowRAMThreshold
      {
        get => DeviceSpy.LowRAMThreshold;
        set
        {
          if (s_LowRAMThreshMega.Equals(value))
            return;

          s_LowRAMThreshMega = value;
          OnCheepCheep?.Invoke(nameof(LowRAMThreshold), value);
        }
      }

      public static string Model
      {
        get => DeviceSpy.Model;
        set
        {
          if (s_Model == value)
            return;

          s_Model = value;
          OnCheepCheep?.Invoke(nameof(Model), value);
        }
      }

      public static SerialVersion OSVersion
      {
        get => DeviceSpy.OSVersion;
        set
        {
          if (s_OSVersion == value)
            return;

          s_OSVersion = value;
          OnCheepCheep?.Invoke(nameof(OSVersion), value);
        }
      }

      public static int ScreenRefreshHz
      {
        get => DeviceSpy.ScreenRefreshHz;
        set
        {
          if (s_ScreenRefreshHz.Equals(value))
            return;

          s_ScreenRefreshHz = value;
          OnCheepCheep?.Invoke(nameof(ScreenRefreshHz), value);
        }
      }

      public static TimeSpan TimezoneOffset
      {
        get => DeviceSpy.TimezoneOffset;
        set
        {
          if (s_TimezoneOffset.Equals(value))
            return;

          s_TimezoneOffset = value;
          OnCheepCheep?.Invoke(nameof(TimezoneOffset), value);
        }
      }

      public static float TotalRAM
      {
        get => DeviceSpy.TotalRAM;
        set
        {
          if (s_TotalRAMMega.Equals(value))
            return;

          s_TotalRAMMega = value;
          OnCheepCheep?.Invoke(nameof(TotalRAM), value);
        }
      }

      public static string UDID
      {
        get => DeviceSpy.UDID;
        set
        {
          if (s_UDID == value)
            return;

          if (value.IsEmpty())
            PlayerPrefs.DeleteKey(PREFKEY_UDID);

          s_UDID = value;
          OnCheepCheep?.Invoke(nameof(UDID), value);
        }
      }

      // ReSharper restore MemberHidesStaticFromOuterClass


      internal static void Explode()
      {
        if (OnCheepCheep is null)
          return;

        // null signals to listeners that perhaps they should call DeviceSpy.X again~
        OnCheepCheep(nameof(ABI),               null);
        OnCheepCheep(nameof(AspectRatio),       null);
        OnCheepCheep(nameof(Brand),             null);
        OnCheepCheep(nameof(Browser),           null);
        OnCheepCheep(nameof(Carrier),           null);
        OnCheepCheep(nameof(CountryISOString),  null);
        OnCheepCheep(nameof(DPI),               null);
        OnCheepCheep(nameof(FallbackID),        null);
        OnCheepCheep(nameof(GLVersion),         null);
        OnCheepCheep(nameof(IDFA),              null);
        OnCheepCheep(nameof(IDFV),              null);
        OnCheepCheep(nameof(IsAndroidGO),       null);
        OnCheepCheep(nameof(IsBlueStacks),      null);
        OnCheepCheep(nameof(IsT1Graphics),      null);
        OnCheepCheep(nameof(IsTablet),          null);
        OnCheepCheep(nameof(IsTrackingLimited), null);
        OnCheepCheep(nameof(LanguageISOString), null);
        OnCheepCheep(nameof(Model),             null);
        OnCheepCheep(nameof(OSVersion),         null);
        OnCheepCheep(nameof(ScreenRefreshHz),   null);
        OnCheepCheep(nameof(TimezoneOffset),    null);
        OnCheepCheep(nameof(TotalRAM),          null);
        OnCheepCheep(nameof(UDID),              null);
      }

    } // end nested class LittleBirdie

  } // end class DeviceSpy
}