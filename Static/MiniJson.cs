/*! @file       Runtime/MiniJson.cs
 *  @author     Calvin Rien (https://gist.github.com/darktable/1411710)
 *              (license info at end of file)
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2023-04-01
**/

using JetBrains.Annotations;

using System.Collections;
using System.Collections.Generic;
using System.Linq;

using System.IO;

using Type     = System.Type;
using TypeCode = System.TypeCode;

using SerializeField = UnityEngine.SerializeField;
using NonSerialized  = System.NonSerializedAttribute;


namespace Ore
{
  using JsonObj = IDictionary<string,object>;
  using JsonArr = IList<object>;


  /// <summary>
  ///   This class encodes and decodes JSON strings. <br/> <br/>
  ///
  ///   JSON uses Arrays and Objects. These correspond here to the datatypes IList and IDictionary.
  ///   All numbers are parsed to doubles. <br/> <br/>
  ///
  ///   Spec. details, see https://www.json.org/
  /// </summary>
  public static class MiniJson
  {

    /// <inheritdoc cref="Deserialize(string,System.Type)"/>
    [CanBeNull]
    public static object Deserialize([CanBeNull] string json)
    {
      // ReSharper disable once AssignNullToNotNullAttribute
      return json.IsEmpty() ? null : RecursiveParser.Parse(new StringReader(json));
    }

    /// <param name="json">
    ///   The JSON string to parse.
    /// </param>
    /// <param name="type">
    ///   Optionally provide a runtime type to try and construct & deserialize the
    ///   json onto. WARNING: supplying this parameter means you agree to the
    ///   reflection overhead. Other deserializers <i>also</i> use reflection;
    ///   I'm just giving you fair warning. <br/> <br/>
    ///   Oh, and the type must be default constructible.
    /// </param>
    /// <returns>
    ///   If a type was provided, returns an object of that type, the type itself
    ///   (if it represents a static class), or null. <br/> <br/>
    ///   Otherwise, any of the standard JSON types. That is, a <see cref="JsonArr"/>,
    ///   a <see cref="JsonObj"/>, a double, a long, a bool, a string, or null.
    /// </returns>
    [CanBeNull]
    public static object Deserialize([CanBeNull] string json, Type type)
    {
      // ReSharper disable once AssignNullToNotNullAttribute
      return json.IsEmpty() ? null : DeserializeStream(new StringReader(json), type);
    }

    public static bool TryDeserialize<T>([CanBeNull] string json, out T obj)
      where T : new()
    {
      if (json.IsEmpty())
      {
        obj = default;
        return false;
      }

      // ReSharper disable once AssignNullToNotNullAttribute
      return TryDeserializeStream(new StringReader(json), out obj);
    }

    public static bool TryDeserializeOverwrite<T>([CanBeNull] string json, [NotNull] ref T obj)
    {
      // ReSharper disable once AssignNullToNotNullAttribute
      return !json.IsEmpty() && TryDeserializeStreamOverwrite(new StringReader(json), ref obj);
    }


    [CanBeNull]
    public static object DeserializeStream([NotNull] TextReader stream, Type type = null)
    {
      var parsed = RecursiveParser.Parse(stream);
      if (type is null || parsed is null || type.IsInstanceOfType(parsed))
        return parsed;

      var obj = type.ConstructDefault(nonPublic: true);
      if (obj is null)
      {
        Orator.Warn(type, "Given type is not default constructible!");
        return parsed;
      }

      if (obj is IReadJson custom)
      {
        custom.ReadJson(parsed);
      }
      else if (parsed is JsonObj jobj)
      {
        // reflection warning!
        _ = ReflectFields(type, jobj, obj);
      }

      return obj;
    }

    public static bool TryDeserializeStream<T>([NotNull] TextReader stream, out T obj)
      where T : new()
    {
      var parsed = RecursiveParser.Parse(stream);

      if (parsed is T casted)
      {
        obj = casted;
        return true;
      }

      obj = new T();

      if (obj is IReadJson custom)
      {
        return custom.ReadJson(parsed);
      }

      if (parsed is JsonObj jobj)
      {

        if (obj is IDictionary<string,object> dict)
        {
          foreach (var kvp in jobj)
          {
            dict.Add(kvp);
          }
          return true;
        }

        // reflection warning!
        return ReflectFields(typeof(T), jobj, obj);
      }

      if (parsed is JsonArr jarr && typeof(IList).IsAssignableFrom(typeof(T)))
      {
        if (!(obj is IList list))
          return false;

        foreach (var item in jarr)
        {
          list.Add(item);
        }

        return true;
      }

      obj = default;
      return typeof(T) == typeof(object);
    }

    public static bool TryDeserializeStreamOverwrite<T>([NotNull] TextReader stream, [NotNull] ref T obj)
    {
      var parsed = RecursiveParser.Parse(stream) as JsonObj;
      if (parsed is null)
        return false;

      if (obj is IReadJson custom)
      {
        return custom.ReadJson(parsed);
      }

      if (typeof(T) != typeof(object) && parsed is T already)
      {
        obj = already;
        return true;
      }

      // reflection warning!
      return ReflectFields(obj.GetType(), parsed, obj);
    }


    /// <summary>
    ///   Converts an IDictionary / IList object or a simple type (string, int,
    ///   etc.) into a JSON string
    /// </summary>
    /// <param name="obj">
    ///   A Dictionary&lt;string, object&gt; / List&lt;object&gt;
    /// </param>
    /// <param name="pretty">
    ///   Pretty print the JSON.
    /// </param>
    /// <returns>
    ///   A JSON encoded string, or null if object 'json' is not serializable
    /// </returns>
    [CanBeNull]
    public static string Serialize([CanBeNull] object obj, bool pretty = EditorBridge.IS_DEBUG)
    {
      return RecursiveSerializer.MakeString(obj, pretty);
    }

    public static void SerializeTo([NotNull] TextWriter stream, [CanBeNull] object obj, bool pretty = EditorBridge.IS_DEBUG)
    {
      RecursiveSerializer.WriteTo(stream, obj, pretty);
    }


    internal static bool ReflectFields(Type type, JsonObj data, object target)
    {
      if (target is null || data is null)
        return false;

      int count = 0;

      try
      {
        // primarily entered in the object <- object recursive case

        if (target is IReadJson custom)
        {
          return custom.ReadJson(data);
        }

        if (target is IDictionary dict)
        {
          var gparms = type.GetGenericArguments();

          var vtype = gparms.Length == 2 ? gparms[1] : typeof(object);
          if (vtype == typeof(object))
          {
            foreach (var kvp in data)
              dict.Add(kvp.Key, kvp.Value);
          }
          else
          {
            foreach (var kvp in data)
              dict.Add(kvp.Key, ConvertValue(vtype, kvp.Value));
          }

          return true;
        }
      }
      catch (System.Exception ex)
      {
        #if DEBUG
        Orator.NFE(ex);
        #else
        _ = ex;
        #endif

        return false;
      }

      foreach (var field in type.GetFields(TypeMembers.INSTANCE))
      {
        if (field.IsDefined<NonSerialized>() || !data.TryGetValue(field.Name, out object value))
          continue;

        try
        {
          value = ConvertValue(field.FieldType, value);
          field.SetValue(target, value);
        }
        catch (System.Exception ex)
        {
          // TODO move, change, or rewrite this catch in response to destructive tests
          #if DEBUG
          Orator.NFE(ex);
          #else
          _ = ex;
          #endif
        }

        if (++count >= data.Count)
          break;
      }

      return count > 0;
    }


    //
    // beyond = impl:
    //


    internal static object ConvertValue(Type lhsType, object value)
    {
      if (value is null)
        return lhsType.ConstructDefault();

      int typeMatr = (int)Type.GetTypeCode(lhsType) << 8 | (int)Type.GetTypeCode(value.GetType());

      string str = value as string;

      switch (typeMatr)
      {
        case 0x00_00: // Empty    <- null
        case 0x00_01: // Empty    <- object
        case 0x00_03: // Empty    <- bool
        case 0x00_0B: // Empty    <- long
        case 0x00_0E: // Empty    <- double
        case 0x00_12: // Empty    <- string
          Orator.Error(typeof(MiniJson), $"how the heck is this field of empty type?! {typeMatr:X2}");
          return null;

        case 0x03_01: // Boolean  <- object
        case 0x04_01: // Char     <- object
        case 0x05_01: // SByte    <- object
        case 0x06_01: // Byte     <- object
        case 0x07_01: // Int16    <- object
        case 0x08_01: // UInt16   <- object
        case 0x09_01: // Int32    <- object
        case 0x0A_01: // UInt32   <- object
        case 0x0B_01: // Int64    <- object
        case 0x0C_01: // UInt64   <- object
        case 0x0D_01: // Single   <- object
        case 0x0E_01: // Double   <- object
        case 0x0F_01: // Decimal  <- object
        case 0x10_01: // DateTime <- object
        case 0x10_03: // DateTime <- bool
          // ???
          #if DEBUG
          Orator.Warn(typeof(MiniJson), $"Not sure how to convert this case: {typeMatr:X2}");
          #endif
          goto default;

        case 0x01_00: // Object   <- null
        case 0x03_03: // Boolean  <- bool
        case 0x0B_0B: // Int64    <- long
        case 0x0E_0E: // Double   <- double
        case 0x12_12: // String   <- string
          // avg case. no need to convert!
          return value;

        case 0x03_00: // Boolean  <- null
        case 0x04_00: // Char     <- null
        case 0x05_00: // SByte    <- null
        case 0x06_00: // Byte     <- null
        case 0x07_00: // Int16    <- null
        case 0x08_00: // UInt16   <- null
        case 0x09_00: // Int32    <- null
        case 0x0A_00: // UInt32   <- null
        case 0x0B_00: // Int64    <- null
        case 0x0C_00: // UInt64   <- null
        case 0x0D_00: // Single   <- null
        case 0x0E_00: // Double   <- null
        case 0x0F_00: // Decimal  <- null
        case 0x10_00: // DateTime <- null
        case 0x12_00: // String   <- null
          return lhsType.ConstructDefault();

        case 0x02_00: // DBNull   <- null
        case 0x02_01: // DBNull   <- object
        case 0x02_03: // DBNull   <- bool
        case 0x02_0B: // DBNull   <- long
        case 0x02_0E: // DBNull   <- double
        case 0x02_12: // DBNull   <- string
          return System.DBNull.Value;

        case 0x12_01: // String   <- object
          return value.ToString();

        case 0x01_01: // Object   <- object
          if (typeof(IReadJson).IsAssignableFrom(lhsType))
          {
            var blank = lhsType.ConstructDefault(nonPublic: true);

            if (((IReadJson)blank).ReadJson(value))
            {
              return blank;
            }

            #if DEBUG
            Orator.Warn(typeof(MiniJson), $"Could not read \"{str}\" into custom IReadJson ({lhsType.NiceName()})!");
            #endif
          }

          var obj = value as JsonObj;
          var arr = value as JsonArr;

          if ( lhsType.IsInstanceOfType(value) ||
             ( obj != null && lhsType.IsAssignableFrom(typeof(JsonObj)) ) ||
             ( arr != null && lhsType.IsAssignableFrom(typeof(JsonArr)) ) )
          {
            return value;
          }

          if (obj != null)
          {
            value = lhsType.ConstructDefault(nonPublic: true);
            ReflectFields(lhsType, obj, value); // regrettable recursion.
            return value;
          }

          var eleType = lhsType.GetListElementType();
          if (eleType != null)
          {
            int ilen = arr?.Count ?? 0;
            var list = System.Array.CreateInstance(eleType, ilen);

            for (int i = 0; i < ilen; ++i)
            {
              // regrettable recursion:
              list.SetValue(ConvertValue(eleType, arr[i]), i);
            }

            return lhsType.IsArray ? list : lhsType.ConstructWith(list);
          }

          goto case 0x01_03;

        case 0x01_03: // Object   <- bool
        case 0x01_0B: // Object   <- long
        case 0x01_0E: // Object   <- double
        case 0x01_12: // Object   <- string
          if (typeof(IReadJson).IsAssignableFrom(lhsType))
          {
            var blank = lhsType.ConstructDefault(nonPublic: true);

            if (((IReadJson)blank).ReadJson(value))
            {
              return blank;
            }

            #if DEBUG
            Orator.Warn(typeof(MiniJson), $"Could not read \"{str}\" into custom IReadJson ({lhsType.NiceName()})!");
            #endif
          }

          if (lhsType == typeof(UnityEngine.LayerMask))
          {
            return new UnityEngine.LayerMask
            {
              // regrettable recursion:
              value = (int)ConvertValue(typeof(int), value)
            };
          }

          return lhsType.ConstructWith(value);

        case 0x04_03: // Char     <- bool
        case 0x05_03: // SByte    <- bool
        case 0x06_03: // Byte     <- bool
        case 0x07_03: // Int16    <- bool
        case 0x08_03: // UInt16   <- bool
        case 0x09_03: // Int32    <- bool
        case 0x0A_03: // UInt32   <- bool
        case 0x0B_03: // Int64    <- bool
        case 0x0C_03: // UInt64   <- bool
        case 0x0D_03: // Single   <- bool
        case 0x0E_03: // Double   <- bool
        case 0x0F_03: // Decimal  <- bool
          value = (bool)value ? 1 : 0;
          goto default;

        case 0x12_03: // String   <- bool
          return (bool)value ? "true" : "false";

        case 0x03_0B: // Boolean  <- long
          return (long)value != 0;

        case 0x10_0B: // DateTime <- long
          return System.DateTime.FromBinary((long)value);

        case 0x12_0B: // String   <- long
          return ((long)value).ToInvariant();

        case 0x03_0E: // Boolean  <- double
          return (double)value != 0.0;


        case 0x10_0E: // DateTime <- double
          // interpret as unix epoch time in seconds
          return DateTimes.FromUnixSeconds((double)value);

        case 0x12_0E: // String   <- double
          return ((double)value).ToInvariant();

        case 0x03_12: // Boolean  <- string
          return !str.IsEmpty() && str[0] != '0' && str[0] != 'f' && str[0] != 'F';

        case 0x04_12: // Char     <- string
          return str.Length == 0 ? default(char) : str[0];

        case 0x10_12: // DateTime <- string
          return DateTimes.FromISO8601(str);

        case 0x05_12: // SByte    <- string
          return str.Length == 0 ? default(sbyte) : sbyte.Parse(str, Strings.InvariantFormatter);
        case 0x06_12: // Byte     <- string
          return str.Length == 0 ? default(byte) : byte.Parse(str, Strings.InvariantFormatter);
        case 0x07_12: // Int16    <- string
          return str.Length == 0 ? default(short) : short.Parse(str, Strings.InvariantFormatter);
        case 0x08_12: // UInt16   <- string
          return str.Length == 0 ? default(ushort) : ushort.Parse(str, Strings.InvariantFormatter);
        case 0x09_12: // Int32    <- string
          return str.Length == 0 ? default(int) : int.Parse(str, Strings.InvariantFormatter);
        case 0x0A_12: // UInt32   <- string
          return str.Length == 0 ? default(uint) : uint.Parse(str, Strings.InvariantFormatter);
        case 0x0B_12: // Int64    <- string
          return str.Length == 0 ? default(long) : long.Parse(str, Strings.InvariantFormatter);
        case 0x0C_12: // UInt64   <- string
          return str.Length == 0 ? default(ulong) : ulong.Parse(str, Strings.InvariantFormatter);
        case 0x0D_12: // Single   <- string
          return str.Length == 0 ? default(float) : float.Parse(str, Strings.InvariantFormatter);
        case 0x0E_12: // Double   <- string
          return str.Length == 0 ? default(double) : double.Parse(str, Strings.InvariantFormatter);
        case 0x0F_12: // Decimal  <- string
          return str.Length == 0 ? default(decimal) : decimal.Parse(str, Strings.InvariantFormatter);

     // case 0x04_0B: // Char     <- long
     // case 0x05_0B: // SByte    <- long
     // case 0x06_0B: // Byte     <- long
     // case 0x07_0B: // Int16    <- long
     // case 0x08_0B: // UInt16   <- long
     // case 0x09_0B: // Int32    <- long
     // case 0x0A_0B: // UInt32   <- long
     // case 0x0C_0B: // UInt64   <- long
     // case 0x0D_0B: // Single   <- long
     // case 0x0E_0B: // Double   <- long
     // case 0x0F_0B: // Decimal  <- long
     // case 0x04_0E: // Char     <- double
     // case 0x05_0E: // SByte    <- double
     // case 0x06_0E: // Byte     <- double
     // case 0x07_0E: // Int16    <- double
     // case 0x08_0E: // UInt16   <- double
     // case 0x09_0E: // Int32    <- double
     // case 0x0A_0E: // UInt32   <- double
     // case 0x0B_0E: // Int64    <- double
     // case 0x0C_0E: // UInt64   <- double
     // case 0x0D_0E: // Single   <- double
     // case 0x0F_0E: // Decimal  <- double
        default:
          return System.Convert.ChangeType(value, lhsType);
      }
    }


    struct RecursiveParser : System.IDisposable
    {
      public static object Parse([NotNull] TextReader stream)
      {
        if (stream.Peek() == -1)
        {
          stream.Close();
          return null;
        }

        var instance = new RecursiveParser(stream);

        try
        {
          return instance.ParseByToken(instance.NextToken);
        }
        catch (System.OverflowException ofe)
        {
          const string MSG = "Hey you, here's the most likely case: Somebody provided a string that is not valid JSON.";

          System.Exception ex = new InvalidDataException(MSG, ofe);

          #if !DEBUG
          ex = ex.Silenced();
          #endif

          Orator.NFE(ex);
        }
        finally
        {
          ((System.IDisposable)instance).Dispose();
        }

        return null;
      }

      static bool IsWordBreak(char c)
      {
        const string WORD_BREAK = "{}[],:\"";
        return char.IsWhiteSpace(c) || WORD_BREAK.IndexOf(c) != -1;
      }

      enum TOKEN
      {
        NONE,
        CURLY_OPEN,
        CURLY_CLOSE,
        SQUARED_OPEN,
        SQUARED_CLOSE,
        COLON,
        COMMA,
        STRING,
        NUMBER,
        TRUE,
        FALSE,
        NULL
      };


      TextReader m_Stream;


      RecursiveParser(TextReader stream)
      {
        m_Stream = stream;
      }

      void System.IDisposable.Dispose()
      {
        m_Stream.Dispose();
        m_Stream = null;
      }

      JsonObj ParseObject()
      {
        var jobj = JsonAuthority.JsonObjMaker(16);

        // ditch opening brace
        m_Stream.Read();

        // {
        while (true)
        {
          switch (NextToken)
          {
            case TOKEN.NONE:
              return null;
            case TOKEN.COMMA:
              continue;
            case TOKEN.CURLY_CLOSE:
              return jobj;
            default:
              // name
              string name = ParseString();
              if (name == null)
                return null;

              // :
              if (NextToken != TOKEN.COLON)
                return null;

              // ditch the colon
              m_Stream.Read();

              // value
              jobj[name] = ParseByToken(NextToken);
              continue;
          }
        }
      }

      JsonArr ParseArray()
      {
        var array = JsonAuthority.JsonArrMaker(4);

        // ditch opening bracket
        m_Stream.Read();

        // [
        while (true)
        {
          var nextToken = NextToken;

          switch (nextToken) 
          {
            case TOKEN.NONE:
              return null;
            case TOKEN.COMMA:
              continue;
            case TOKEN.SQUARED_CLOSE:
              return array;
            default:
              array.Add(ParseByToken(nextToken));
              break;
          }
        }
      }

      object ParseByToken(TOKEN token)
      {
        switch (token)
        {
          case TOKEN.STRING:
            return ParseString();
          case TOKEN.NUMBER:
            return ParseNumber();
          case TOKEN.CURLY_OPEN:
            return ParseObject();
          case TOKEN.SQUARED_OPEN:
            return ParseArray();
          case TOKEN.TRUE:
            return true;
          case TOKEN.FALSE:
            return false;
          default:
          case TOKEN.NULL:
            return null;
        }
      }

      string ParseString()
      {
        // ditch opening quote
        m_Stream.Read();

        using (new RecycledStringBuilder(out var builder))
        {
          LoopTop:

          if (m_Stream.Peek() == -1)
            return builder.ToString();

          char c = NextChar;

          switch (c)
          {
            case '"':
              return builder.ToString();

            case '\\':
              if (m_Stream.Peek() == -1)
                return builder.ToString();

              c = NextChar;

              switch (c)
              {
                case '"':
                case '\\':
                case '/':
                  builder.Append(c);
                  break;
                case 'b':
                  builder.Append('\b');
                  break;
                case 'f':
                  builder.Append('\f');
                  break;
                case 'n':
                  builder.Append('\n');
                  break;
                case 'r':
                  builder.Append('\r');
                  break;
                case 't':
                  builder.Append('\t');
                  break;
                case 'u':
                  var hex = new char[4];

                  for (int i = 0; i < 4; ++i)
                  {
                    hex[i] = NextChar;
                  }

                  builder.Append((char)System.Convert.ToInt32(new string(hex), 16));
                  break;
              } // end inner switch
              break;

            default:
              builder.Append(c);
              break;
          } // end outer switch

          goto LoopTop;
        }
      }

      object ParseNumber()
      {
        string number = NextWord;

        if (number.IndexOf('.') == -1)
        {
          _ = long.TryParse(number, out long parsedInt);
          return parsedInt;
        }

        _ = double.TryParse(number, out double parsedDouble);
        return parsedDouble;
      }

      void EatWhitespace()
      {
        while (char.IsWhiteSpace(PeekChar))
        {
          m_Stream.Read();

          if (m_Stream.Peek() == -1)
            break;
        }
      }

      char PeekChar => System.Convert.ToChar(m_Stream.Peek()); // ToChar can throw OverflowException

      char NextChar => System.Convert.ToChar(m_Stream.Read()); // ToChar can throw OverflowException

      string NextWord
      {
        get
        {
          using (new RecycledStringBuilder(out var word))
          {
            while (!IsWordBreak(PeekChar))
            {
              word.Append(NextChar);

              if (m_Stream.Peek() == -1)
                break;
            }

            return word.ToString();
          }
        }
      }

      TOKEN NextToken
      {
        get
        {
          EatWhitespace();

          if (m_Stream.Peek() == -1)
            return TOKEN.NONE;

          switch (PeekChar)
          {
            case '{':
              return TOKEN.CURLY_OPEN;
            case '}':
              m_Stream.Read();
              return TOKEN.CURLY_CLOSE;
            case '[':
              return TOKEN.SQUARED_OPEN;
            case ']':
              m_Stream.Read();
              return TOKEN.SQUARED_CLOSE;
            case ',':
              m_Stream.Read();
              return TOKEN.COMMA;
            case '"':
              return TOKEN.STRING;
            case ':':
              return TOKEN.COLON;
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
            case '-':
              return TOKEN.NUMBER;
          }

          switch (NextWord)
          {
            case "false":
              return TOKEN.FALSE;
            case "true":
              return TOKEN.TRUE;
            case "null":
              return TOKEN.NULL;
          }

          return TOKEN.NONE;
        }
      }

    } // end struct class RecursiveParser


    struct RecursiveSerializer : System.IDisposable
    {
      public static string MakeString(object obj, bool pretty, int indentIncr = 2)
      {
        using (var stringer = new RecursiveSerializer(pretty, indentIncr))
        {
          stringer.WriteValue(obj);
          return stringer.m_Stream.ToString();
        }
      }

      public static void WriteTo(TextWriter stream, object obj, bool pretty, int indentIncr = 2)
      {
        using (var writer = new RecursiveSerializer(stream, pretty, indentIncr))
        {
          writer.WriteValue(obj);
        }
      }


      RecursiveSerializer(bool pretty, int indentIncr)
      {
        m_Stream     = StringStream.ForBuilder();
        m_Indent     = 0;
        m_Pretty     = pretty;
        m_IndentIncr = indentIncr;

        if (pretty)
        {
          m_OpenObj = "{\n";
          m_OpenArr = "[\n";
          m_Comma   = ",\n";
          m_Colon   = ": ";
        }
        else
        {
          m_OpenObj = "{";
          m_OpenArr = "[";
          m_Comma   = ",";
          m_Colon   = ":";
        }
      }

      RecursiveSerializer(TextWriter stream, bool pretty, int indentIncr)
      {
        m_Stream     = StringStream.ForWriter(stream);
        m_Indent     = 0;
        m_Pretty     = pretty;
        m_IndentIncr = indentIncr;

        if (pretty)
        {
          m_OpenObj = "{\n";
          m_OpenArr = "[\n";
          m_Comma   = ",\n";
          m_Colon   = ": ";
        }
        else
        {
          m_OpenObj = "{";
          m_OpenArr = "[";
          m_Comma   = ",";
          m_Colon   = ":";
        }
      }

      void System.IDisposable.Dispose()
      {
        m_Stream.Dispose();
      }


      StringStream m_Stream;
      int          m_Indent;

      readonly bool   m_Pretty;
      readonly int    m_IndentIncr;
      readonly string m_OpenObj;
      readonly string m_OpenArr;
      readonly string m_Comma;
      readonly string m_Colon;


      void WriteValue(object value)
      {
        if (value == null)
        {
          m_Stream.Put("null");
        }
        else if (value is string str)
        {
          WriteString(str);
        }
        else if (value is bool b)
        {
          m_Stream.Put(b ? "true" : "false");
        }
        else if (value is IWriteJson custom)
        {
          m_Stream.Put(custom.WriteJson(m_Pretty));
        }
        else if (value is IDictionary dict)
        {
          WriteObject(dict);
        }
        else if (value is IEnumerable list)
        {
          WriteArray(list);
        }
        else if (value is char c)
        {
          m_Stream.Put('\"');
          m_Stream.Put(c);
          m_Stream.Put('\"');
        }
        else
        {
          WriteOther(value);
        }
      }

      void WriteObject(IDictionary dict)
      {
        m_Indent += m_IndentIncr;

        int indent = m_Pretty ? m_Indent : 0;

        m_Stream.Put(m_OpenObj);

        bool first = true;

        var iter = dict.GetEnumerator();
        while (iter.MoveNext())
        {
          if (iter.Key is null)
            continue;

          if (!first)
            m_Stream.Put(m_Comma);

          first = false;

          m_Stream.Put(' ', indent);

          WriteString(iter.Key.ToString());

          m_Stream.Put(m_Colon);

          WriteValue(iter.Value);
        }

        m_Indent -= m_IndentIncr;

        if (m_Pretty)
        {
          m_Stream.Put('\n');
          m_Stream.Put(' ', m_Indent);
        }

        m_Stream.Put('}');
      }

      void WriteReflectedFields(object obj, Type type, bool nonPublic)
      {
        // reflection warning!

        m_Indent += m_IndentIncr;

        int indent = m_Pretty ? m_Indent : 0;

        m_Stream.Put(m_OpenObj);

        bool first = true;

        var flags = obj is null ? TypeMembers.STATIC : TypeMembers.INSTANCE;

        foreach (var field in type.GetFields(flags))
        {
          if (field.IsNotSerialized || ( !nonPublic && !field.IsPublic && !field.IsDefined<SerializeField>() ))
            continue;

          if (!first)
            m_Stream.Put(m_Comma);

          first = false;

          m_Stream.Put(' ', indent);

          WriteString(field.Name);

          m_Stream.Put(m_Colon);

          WriteValue(field.GetValue(obj));
        }

        if (obj is null)
        {
          foreach (var prop in type.GetProperties(flags))
          {
            if (prop.IsDefined<NonSerialized>())
              continue;

            var getter = prop.GetGetMethod(nonPublic);
            if (getter is null || getter.IsDefined<NonSerialized>())
              continue;

            if (!first)
              m_Stream.Put(m_Comma);

            first = false;

            m_Stream.Put(' ', indent);

            WriteString(prop.Name);

            m_Stream.Put(m_Colon);

            WriteValue(getter.Invoke(null, TypeMembers.NoParameters));
          }
        }

        m_Indent -= m_IndentIncr;

        if (m_Pretty)
        {
          m_Stream.Put('\n');
          m_Stream.Put(' ', m_Indent);
        }

        m_Stream.Put('}');
      }

      void WriteArray(IEnumerable array)
      {
        m_Indent += m_IndentIncr;

        int indent = m_Pretty ? m_Indent : 0;

        m_Stream.Put(m_OpenArr);

        bool first = true;

        foreach (object obj in array)
        {
          if (!first)
            m_Stream.Put(m_Comma);

          first = false;

          m_Stream.Put(' ', indent);

          WriteValue(obj);
        }

        m_Indent -= m_IndentIncr;

        if (m_Pretty)
        {
          m_Stream.Put('\n');
          m_Stream.Put(' ', m_Indent);
        }

        m_Stream.Put(']');
      }

      void WriteString(string str)
      {
        m_Stream.Put('\"');

        foreach (var c in str)
        {
          switch (c)
          {
            case '"':
              m_Stream.Put("\\\"");
              break;
            case '\\':
              m_Stream.Put("\\\\");
              break;
            case '\b':
              m_Stream.Put("\\b");
              break;
            case '\f':
              m_Stream.Put("\\f");
              break;
            case '\n':
              m_Stream.Put("\\n");
              break;
            case '\r':
              m_Stream.Put("\\r");
              break;
            case '\t':
              m_Stream.Put("\\t");
              break;
            default:
              int codepoint = System.Convert.ToInt32(c);
              if ((codepoint >= 32) && (codepoint <= 126))
              {
                m_Stream.Put(c);
              }
              else
              {
                m_Stream.Put("\\u");
                m_Stream.Put(codepoint.ToString("x4"));
              }
              break;
          }
        }

        m_Stream.Put('\"');
      }

      void WriteOther(object value)
      {
        // NOTE: decimals lose precision during serialization.
        // They always have, I'm just letting you know.
        // Previously floats and doubles lost precision too.

        var type = value.GetType();
        var code = Type.GetTypeCode(type);

        switch (code)
        {
          default:
          case TypeCode.String:
            WriteString(value.ToString());
            break;

          case TypeCode.Single:
          case TypeCode.Double:
          case TypeCode.Decimal:
            m_Stream.Put(System.Convert.ToDouble(value).ToString("R", Strings.InvariantFormatter));
            break;

          case TypeCode.Int32:
          case TypeCode.Int64:
          case TypeCode.UInt32:
          case TypeCode.UInt64:
          case TypeCode.Int16:
          case TypeCode.UInt16:
          case TypeCode.Byte:
          case TypeCode.SByte:
            string str = ((System.IConvertible)value).ToInvariant();
            if (type.IsEnum)
              WriteString(str);
            else
              m_Stream.Put(str);
            break;

          case TypeCode.DateTime:
            m_Stream.Put(((System.DateTime)value).ToUniversalTime().ToISO8601());
            break;

          case TypeCode.Empty:
          case TypeCode.DBNull:
            m_Stream.Put("null");
            break;

          case TypeCode.Object:
            if (value is System.TimeSpan span)
            {
              m_Stream.Put(span.Ticks.ToInvariant());
            }
            else if (value is UnityEngine.LayerMask mask)
            {
              m_Stream.Put(mask.value.ToInvariant());
            }
            else if (value is Type ztatic)
            {
              WriteReflectedFields(null, ztatic, nonPublic: false);
            }
            else if (type.IsSerializable)
            {
              WriteReflectedFields(value, type, nonPublic: false);
            }
            else if (type.IsValueType)
            {
              WriteReflectedFields(value, type, nonPublic: true);
            }
            else if (typeof(UnityEngine.Object).IsAssignableFrom(type))
            {
              if (JsonAuthority.s_WhiteObjectTypes.Any(t => t.IsAssignableFrom(type)))
                WriteReflectedFields(value, type, nonPublic: false);
              else
                m_Stream.Put("{}");
            }
            else
            {
              WriteString(value.ToString());
            }
            break;
        }
      }

    } // end nested struct RecursiveSerializer

  } // end static class MiniJson

}


// Original COPYRIGHT:

/*
 * Copyright (c) 2013 Calvin Rien
 *
 * Based on the JSON parser by Patrick van Bergen
 * http://techblog.procurios.nl/k/618/news/view/14605/14863/How-do-I-write-my-own-parser-for-JSON.html
 *
 * Simplified it so that it doesn't throw exceptions
 * and can be used in Unity iPhone with maximum code stripping.
 *
 * Modified for Ore by Levi Perez (2023).
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
