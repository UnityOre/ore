/*! @file       Static/Vectors.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2023-04-18
**/

using JetBrains.Annotations;

using UnityEngine;


namespace Ore
{
  [PublicAPI]
  public static class Vectors
  {

  #region Vector4s & Quaternions

    public static Vector4 ToVec4(in this Quaternion q)
    {
      return new Vector4(q.x, q.y, q.z, q.w);
    }

    public static Quaternion Quat2D(float angleDeg)
    {
      angleDeg = angleDeg / 2f * Mathf.Deg2Rad;
      return new Quaternion(x: 0f,
                            y: 0f,
                            z: Mathf.Sin(angleDeg).Squeezed(),
                            w: Mathf.Cos(angleDeg).Squeezed());
    }

  #endregion Vector4s & Quaternions


  #region Vector2s

    public static bool Approximately(this Vector2 self, Vector2 other)
    {
      return (self.x -= other.x) * self.x < Floats.Epsilon2
          && (self.y -= other.y) * self.y < Floats.Epsilon2;
    }

    public static bool Approximately(this Vector2 self, Vector2 other, float epsilon)
    {
      epsilon *= epsilon;
      return (self.x -= other.x) * self.x < epsilon
          && (self.y -= other.y) * self.y < epsilon;
    }

    public static bool ApproximatelyZero(this Vector2 self)
    {
      return self.x * self.x < Floats.Epsilon2
          && self.y * self.y < Floats.Epsilon2;
    }

    public static float Determinant(Vector2 lhs, Vector2 rhs)
    {
      return lhs.x * rhs.y - lhs.y * rhs.x;
    }

    public static bool IsClockwiseOf(this Vector2 self, Vector2 other)
    {
      return Determinant(other, self) < 0f;
    }

    /// <summary>
    ///   Signed angle! Assumes <paramref name="self"/> is already normal.
    /// </summary>
    public static float NormalAngleRadians(this Vector2 self)
    {
      return (float)(self.y < 0f ? -1 * System.Math.Acos(self.x)
                                 :      System.Math.Acos(self.x));
    }

    /// <inheritdoc cref="NormalAngleRadians"/>
    public static float NormalAngle(this Vector2 self)
    {
      return (float)(self.y < 0f ? -Mathf.Rad2Deg * System.Math.Acos(self.x)
                                 :  Mathf.Rad2Deg * System.Math.Acos(self.x));
    }

    /// <remarks>
    ///   `UnityEngine.Random.insideUnitCircle` is NOT the same thing as this.
    /// </remarks>>
    public static Vector2 RandomOnUnitCircle()
    {
      float rads = Random.value * 2f * Mathf.PI;
      return new Vector2(Mathf.Cos(rads), Mathf.Sin(rads));
    }

    public static void RotateRadians(ref this Vector2 self, float radians)
    {
      double sin = -radians; // negate direction so rotation is more user-intuitive

      double cos = System.Math.Cos(sin);
      sin = System.Math.Sin(sin);

      (self.x, self.y) = ((float)(self.x *  cos + self.y * sin),
                          (float)(self.x * -sin + self.y * cos));
    }

    public static void Rotate(ref this Vector2 self, float degrees)
    {
      RotateRadians(ref self, degrees * Mathf.Deg2Rad);
    }

    public static Vector2 RotatedRadians(this Vector2 self, float radians)
    {
      RotateRadians(ref self, radians);
      return self;
    }

    public static Vector2 Rotated(this Vector2 self, float degrees)
    {
      RotateRadians(ref self, degrees * Mathf.Deg2Rad);
      return self;
    }

    public static Vector2 SinusoidalLoop(Vector2 a, Vector2 b, float t)
    {
      return Vector2.LerpUnclamped(a, b, Floats.SinusoidalParameter(t));
    }

    public static Vector2 SmoothStepLoop(Vector2 a, Vector2 b, float t)
    {
      return Vector2.LerpUnclamped(a, b, Floats.SmoothStepParameter(t));
    }

  #endregion Vector2s

  } // end static class Vectors
}