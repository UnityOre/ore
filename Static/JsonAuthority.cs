/*! @file       Static/JsonAuthority.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2023-01-22
 *
 *  TODO XML documentation
**/

#if PREFER_NEWTONSOFT // deprecated symbol
#warning PREFER_NEWTONSOFT is deprecated and may become unsupported in the next major version.
#define ORE_PREFER_NEWTONSOFT
#endif

// skip doing the same for ORE_NEWTONSOFT as it's not user-provided.

using JetBrains.Annotations;

using UnityEngine;

#if UNITY_EDITOR
using MenuItem = UnityEditor.MenuItem;
#endif

using System.Collections.Generic;

using Type = System.Type;
using TextWriter = System.IO.TextWriter;
using TextReader = System.IO.TextReader;


namespace Ore
{
  using JsonObj = IDictionary<string,object>;
  using JsonArr = IList<object>;

  using ObjMaker = System.Func<int, IDictionary<string,object>>;
  using ArrMaker = System.Func<int, IList<object>>;
    // using these statements because fully-qualified delegates can't participate in C# contravariance


  [PublicAPI]
  public static class JsonAuthority
  {

    //
    #region Primary API
    //

    [NotNull]
    public static string Write([CanBeNull] object data,
                               object serializer = null)
    {
      string json;

      switch (Provider)
      {
        case JsonProvider.None:
          json = data?.ToString();
          break;

        default:
        case JsonProvider.MiniJson:
          json = MiniJson.Serialize(data, PrettyPrint);
          break;

        case JsonProvider.NewtonsoftJson:
          #if ORE_NEWTONSOFT
          using (new RecycledStringBuilder(out var strBuilder))
            json = NewtonsoftAuthority.Serialize(data, serializer as Newtonsoft.Json.JsonSerializer, strBuilder);
          break;
          #else
          if (!TrySetProvider(DefaultProvider))
            Provider = JsonProvider.None;
          throw new UnanticipatedException("Provider should have never been set to NewtonsoftJson if it isn't available.");
          #endif

        case JsonProvider.JsonUtility:
          json = JsonUtility.ToJson(data, PrettyPrint);
          break;
      }

      return json ?? "null";
    }

    [NotNull]
    public static TextWriter Write([CanBeNull] object data, [NotNull] TextWriter toStream,
                                   object serializer = null)
    {
      switch (Provider)
      {
        case JsonProvider.None:
          if (data != null)
            toStream.Write(data);
          break;

        default:
        case JsonProvider.MiniJson:
          MiniJson.SerializeTo(toStream, data, PrettyPrint); // TODO only MiniJson closes the stream... !
          break;

        case JsonProvider.NewtonsoftJson:
          #if ORE_NEWTONSOFT
          NewtonsoftAuthority.SerializeTo(toStream, data, serializer as Newtonsoft.Json.JsonSerializer);
          break;
          #else
          if (!TrySetProvider(DefaultProvider))
            Provider = JsonProvider.None;
          throw new UnanticipatedException("Provider should have never been set to NewtonsoftJson if it isn't available.");
          #endif

        case JsonProvider.JsonUtility:
          toStream.Write(JsonUtility.ToJson(data, PrettyPrint));
          break;
      }

      return toStream;
    }


    [CanBeNull]
    public static JsonObj ReadDictionary([CanBeNull] string json)
    {
      switch (Provider)
      {
        case JsonProvider.None:
          var dummyMap = JsonObjMaker(1);
          dummyMap[nameof(json)] = json;
          return dummyMap;

        default:
        case JsonProvider.MiniJson:
          return MiniJson.Deserialize(json) as JsonObj;

        case JsonProvider.NewtonsoftJson:
          #if ORE_NEWTONSOFT
          return NewtonsoftAuthority.GenericParseObject(json);
          #else
          if (!TrySetProvider(DefaultProvider))
            Provider = JsonProvider.None;
          throw new UnanticipatedException("Provider should have never been set to NewtonsoftJson if it isn't available.");
          #endif

        case JsonProvider.JsonUtility:
          Orator.Warn<JsonProvider>($"JsonUtility cannot support {nameof(ReadDictionary)} -> IDictionary; falling back to Provider {DefaultProvider}.");
          goto case DefaultProvider;
      }
    }

    [CanBeNull]
    public static JsonArr ReadArray([CanBeNull] string json)
    {
      switch (Provider)
      {
        case JsonProvider.None:
          var dummyList = JsonArrMaker(1);
          dummyList.Add(json);
          return dummyList;

        default:
        case JsonProvider.MiniJson:
          return MiniJson.Deserialize(json) as JsonArr;

        case JsonProvider.NewtonsoftJson:
          #if ORE_NEWTONSOFT
          return NewtonsoftAuthority.GenericParseArray(json);
          #else
          if (!TrySetProvider(DefaultProvider))
            Provider = JsonProvider.None;
          throw new UnanticipatedException("Provider should have never been set to NewtonsoftJson if it isn't available.");
          #endif

        case JsonProvider.JsonUtility:
          Orator.Warn<JsonProvider>($"JsonUtility cannot support {nameof(ReadArray)} -> IList; falling back to Provider {DefaultProvider}.");
          goto case DefaultProvider;
      }
    }


    [CanBeNull]
    public static object Read([NotNull] string json, Type type = null)
    {
      if (!ReferenceEquals(type, null) && type.IsSubclassOf(typeof(Object)))
      {
        Orator.Error(type, "UnityEngine.Object types cannot be passed to JsonAuthority.Read(*). Use TryPopulate(*) instead.");
        return null;
      }

      switch (Provider)
      {
        case JsonProvider.None:
          return (!type?.IsAssignableFrom(typeof(string)) ?? false) ? null : json;

        default:
        case JsonProvider.MiniJson:
          return MiniJson.Deserialize(json, type);

        case JsonProvider.NewtonsoftJson:
          #if ORE_NEWTONSOFT
          _ = NewtonsoftAuthority.TryDeserializeObject(json, type, out object parsed);
          return parsed;
          #else
          if (!TrySetProvider(DefaultProvider))
            Provider = JsonProvider.None;
          throw new UnanticipatedException("Provider should have never been set to NewtonsoftJson if it isn't available.");
          #endif

        case JsonProvider.JsonUtility:
          if (type is null || type.IsGenericType)
          {
            Orator.Warn<JsonProvider>($"JsonUtility cannot support {nameof(Read)} -> (anonymous/generic type); falling back to Provider {DefaultProvider}.");
            goto case DefaultProvider;
          }

          return JsonUtility.FromJson(json, type);
      }
    }

    [CanBeNull]
    public static object Read([NotNull] TextReader stream, Type type = null)
    {
      if (!ReferenceEquals(type, null) && type.IsSubclassOf(typeof(Object)))
      {
        Orator.Error(type, "UnityEngine.Object types cannot be passed to JsonAuthority.Read(*). Use TryPopulate(*) instead.");
        return null;
      }

      switch (Provider)
      {
        case JsonProvider.None:
          if (!type?.IsAssignableFrom(typeof(string)) ?? false)
            Orator.Warn(type, "JsonProvider is set to None");
          return stream.ReadToEnd();

        default:
        case JsonProvider.MiniJson:
          return MiniJson.DeserializeStream(stream, type);

        case JsonProvider.NewtonsoftJson:
          #if ORE_NEWTONSOFT
          return NewtonsoftAuthority.DeserializeStream(stream, type);
          #else
          if (!TrySetProvider(DefaultProvider))
            Provider = JsonProvider.None;
          throw new UnanticipatedException("Provider should have never been set to NewtonsoftJson if it isn't available.");
          #endif

        case JsonProvider.JsonUtility:
          if (type is null || type.IsGenericType)
          {
            Orator.Warn<JsonProvider>($"JsonUtility cannot support {nameof(Read)} -> (anonymous/generic type); falling back to Provider {DefaultProvider}.");
            goto case DefaultProvider;
          }

          return JsonUtility.FromJson(stream.ReadToEnd(), type);
      }
    }


    public static bool TryRead<T>([CanBeNull] string json, out T obj)
      where T : new()
    {
      if (typeof(T).IsSubclassOf(typeof(Object)))
      {
        Orator.Error<T>("UnityEngine.Object types cannot be passed to JsonAuthority.TryRead(*). Use TryPopulate(*) instead.");
        obj = default;
        return false;
      }

      switch (Provider)
      {
        case JsonProvider.None:
          if (typeof(T).IsAssignableFrom(typeof(string)))
          {
            obj = (T)(object)json;
            return true;
          }
          obj = default;
          return false;

        default:
        case JsonProvider.MiniJson:
          return MiniJson.TryDeserialize(json, out obj);

        case JsonProvider.NewtonsoftJson:
          #if ORE_NEWTONSOFT
          return NewtonsoftAuthority.TryDeserializeObject(json, out obj);
          #else
          if (!TrySetProvider(DefaultProvider))
            Provider = JsonProvider.None;
          throw new UnanticipatedException("Provider should have never been set to NewtonsoftJson if it isn't available.");
          #endif

        case JsonProvider.JsonUtility:
          try
          {
            obj = JsonUtility.FromJson<T>(json);
            return true;
          }
          catch (System.Exception ex) // TODO which exception(s) shall we expect?
          {
            Orator.NFE(ex);
            obj = default;
            return false;
          }
      }
    }

    public static bool TryRead<T>([NotNull] TextReader stream, out T obj)
      where T : new()
    {
      if (typeof(T).IsSubclassOf(typeof(Object)))
      {
        Orator.Error<T>("UnityEngine.Object types cannot be passed to JsonAuthority.TryRead(*). Use TryPopulate(*) instead.");
        obj = default;
        return false;
      }

      switch (Provider)
      {
        case JsonProvider.None:
          if (typeof(T) == typeof(string))
          {
            try
            {
              obj = (T)(object)stream.ReadToEnd();
              return true;
            }
            finally
            {
              stream.Close();
            }
          }
          break;

        default:
        case JsonProvider.MiniJson:
          return MiniJson.TryDeserializeStream(stream, out obj);

        case JsonProvider.NewtonsoftJson:
          #if ORE_NEWTONSOFT
          return NewtonsoftAuthority.TryDeserializeStream(stream, out obj);
          #else
          if (!TrySetProvider(DefaultProvider))
            Provider = JsonProvider.None;
          throw new UnanticipatedException("Provider should have never been set to NewtonsoftJson if it isn't available.");
          #endif

        case JsonProvider.JsonUtility:
          try
          {
            obj = JsonUtility.FromJson<T>(stream.ReadToEnd());
            return true;
          }
          catch (System.Exception ex) // TODO which exception(s) shall we expect?
          {
            Orator.NFE(ex);
            break;
          }
      }

      obj = default;
      return false;
    }


    public static bool TryPopulate<T>([NotNull] TextReader stream, ref T obj)
    {
      switch (Provider)
      {
        case JsonProvider.None:
          if (typeof(T) == typeof(string))
          {
            try
            {
              obj = (T)(object)stream.ReadToEnd();
              return true;
            }
            finally
            {
              stream.Close();
            }
          }
          return false;

        default:
        case JsonProvider.MiniJson:
          return MiniJson.TryDeserializeStreamOverwrite(stream, ref obj);

        case JsonProvider.NewtonsoftJson:
          #if ORE_NEWTONSOFT
          return NewtonsoftAuthority.TryDeserializeOverwrite(stream.ReadToEnd(), ref obj);
          #else
          if (!TrySetProvider(DefaultProvider))
            Provider = JsonProvider.None;
          throw new UnanticipatedException("Provider should have never been set to NewtonsoftJson if it isn't available.");
          #endif

        case JsonProvider.JsonUtility:
          try
          {
            JsonUtility.FromJsonOverwrite(stream.ReadToEnd(), obj);
            return true;
          }
          catch (System.Exception ex) // TODO
          {
            Orator.NFE(ex);
            return false;
          }
      }
    }

    public static bool TryPopulate<T>([CanBeNull] string json, [NotNull] ref T obj)
    {
      if (json.IsEmpty())
        return false;

      switch (Provider)
      {
        case JsonProvider.None:
          if (typeof(T).IsAssignableFrom(typeof(string)))
          {
            // ReSharper disable once AssignNullToNotNullAttribute
            obj = (T)(object)json;
            return true;
          }
          return false;

        default:
        case JsonProvider.MiniJson:
          return MiniJson.TryDeserializeOverwrite(json, ref obj);

        case JsonProvider.NewtonsoftJson:
          #if ORE_NEWTONSOFT
          return NewtonsoftAuthority.TryDeserializeOverwrite(json, ref obj);
          #else
          if (!TrySetProvider(DefaultProvider))
            Provider = JsonProvider.None;
          throw new UnanticipatedException("Provider should have never been set to NewtonsoftJson if it isn't available.");
          #endif

        case JsonProvider.JsonUtility:
          try
          {
            JsonUtility.FromJsonOverwrite(json, obj);
            return true;
          }
          catch (System.Exception ex) // TODO
          {
            Orator.NFE(ex);
            return false;
          }
      }
    }

    //
    #endregion Primary API
    //

    //
    #region Additional Public API
    //

    public struct Scope : System.IDisposable
    {
      public Scope(JsonProvider provider, bool prettyPrint, ObjMaker objMaker = null, ArrMaker arrMaker = null)
      {
        m_Disposable = true;
        m_Provider   = Provider;
        m_WasPretty  = PrettyPrint;
        PrettyPrint  = prettyPrint;

        if (!TrySetProvider(provider))
        {
          Orator.Error<Scope>($"Failed to set Provider to {provider}; {Provider} will remain as the active Provider.");
        }

        m_ObjMaker = JsonObjMaker;
        if (objMaker != null)
          JsonObjMaker = objMaker;

        m_ArrMaker = JsonArrMaker;
        if (arrMaker != null)
          JsonArrMaker = arrMaker;

        m_NumWhiteTypes = 0;
      }

      public Scope(params Type[] allowUnityTypes)
        : this(Provider, PrettyPrint)
      {
        if (!allowUnityTypes.IsEmpty())
        {
          s_WhiteObjectTypes.AddRange(allowUnityTypes);
          m_NumWhiteTypes = allowUnityTypes.Length;
        }
      }

      public Scope(JsonProvider provider)
        : this(provider, PrettyPrint)
      {
      }

      public Scope(bool prettyPrint)
        : this(Provider, prettyPrint)
      {
      }

      public Scope([CanBeNull] ObjMaker objMaker, ArrMaker arrMaker = null)
        : this(Provider, PrettyPrint, objMaker, arrMaker)
      {
      }


      void System.IDisposable.Dispose()
      {
        if (!m_Disposable)
          return;

        m_Disposable = false;
        Provider     = m_Provider;
        PrettyPrint  = m_WasPretty;
        JsonObjMaker = m_ObjMaker;
        JsonArrMaker = m_ArrMaker;

        if (m_NumWhiteTypes == 0)
          return;

        s_WhiteObjectTypes.RemoveRange(s_WhiteObjectTypes.Count - m_NumWhiteTypes, m_NumWhiteTypes);
      }


      bool m_Disposable;

      readonly JsonProvider m_Provider;
      readonly bool         m_WasPretty;
      readonly ObjMaker     m_ObjMaker;
      readonly ArrMaker     m_ArrMaker;

      readonly int m_NumWhiteTypes;

    } // end nested class Scope


    public static bool PrettyPrint
    {
      get => s_PrettyPrint;
      set
      {
        s_PrettyPrint = value;
        #if ORE_NEWTONSOFT
        NewtonsoftAuthority.SetPrettyPrint(value);
        #endif
      }
    }

    #if ORE_NEWTONSOFT && ORE_PREFER_NEWTONSOFT
    public const JsonProvider DefaultProvider = JsonProvider.NewtonsoftJson;
    #else
    public const JsonProvider DefaultProvider = JsonProvider.MiniJson;
    #endif

    public static JsonProvider Provider { get; private set; } = DefaultProvider;

    public static bool TrySetProvider(JsonProvider provider)
    {
      if (Provider == provider)
        return true;

      if (provider == JsonProvider.NewtonsoftJson)
      {
        #if ORE_NEWTONSOFT
        #else
        return false;
        #endif
      }

      Provider = provider;
      return true;
    }


    /// <summary>
    ///   Use this to validate JSON strings, primarily at DEBUG (or else edit)
    ///   time.
    /// </summary>
    public static bool IsValidJson(string json)
    {
      var throwaway = MiniJson.Deserialize(json);
      return throwaway != null || json == "null";
    }


    public static bool ReflectFields<T>([NotNull] JsonObj data, out T obj)
    {
      var type = typeof(T);
      if (!type.IsDefaultConstructible() || ( !type.IsSerializable && !type.IsValueType ))
      {
        obj = default;
        return false;
      }

      switch (Provider)
      {
        default:
        case JsonProvider.MiniJson:
          try
          {
            obj = (T)type.ConstructDefault();
            return MiniJson.ReflectFields(type, data, obj);
          }
          catch (System.Exception ex)
          {
            Orator.NFE(ex);
            obj = default;
            return false;
          }

        case JsonProvider.NewtonsoftJson:
          #if ORE_NEWTONSOFT
          return NewtonsoftAuthority.TryMakeObject(data, out obj);
          #else
          if (!TrySetProvider(DefaultProvider))
            Provider = JsonProvider.None;
          throw new UnanticipatedException("Provider should have never been set to NewtonsoftJson if it isn't available.");
          #endif
      }
    }


    public static bool ResolvePath<T>([CanBeNull] object objectOrArray, [NotNull] string path, out T value)
    {
      // TODO - might remove this API

      value = default;

      var currObj = objectOrArray as JsonObj;
      var currArr = objectOrArray as JsonArr;
      if (currObj is null && currArr is null)
        return false;

      var splits = path.Split('.');

      for (int i = 0; i < splits.Length; ++i)
      {
        string slug = splits[i];
        if (slug.IsEmpty())
        {
          objectOrArray = currObj as object ?? currArr;
          continue;
        }

        if (Parsing.TryParseLastIndex(slug, out int idx, out int end))
        {
          if (currObj != null)
            currArr = currObj[slug.Remove(end)] as JsonArr;

          if (currArr is null || idx < 0 || idx >= currArr.Count)
            break;

          objectOrArray = currArr[idx];
        }
        else if (currObj is null)
        {
          break;
        }
        else
        {
          objectOrArray = currObj[slug];
        }

        currObj = objectOrArray as JsonObj;
        currArr = objectOrArray as JsonArr;

        if (currObj is null && currArr is null)
          break;
      }

      if (objectOrArray is T casted)
      {
        value = casted;
        return true;
      }

      if (currObj != null)
      {
        return ReflectFields(currObj, out value);
      }

      if (typeof(T).IsSubclassOf(typeof(System.IConvertible)) &&
          objectOrArray is System.IConvertible prim)
      {
        value = (T)prim.ToType(typeof(T), Strings.InvariantFormatter);
        return true;
      }

      return false;
    }


    public static void WriteCSV(StringStream toStream, [NotNull] ICollection<string> colNames, [NotNull] IList<JsonObj> rows)
    {
      if (colNames.IsEmpty())
        return;

      int i = colNames.Count - 1;
      foreach (string colName in colNames)
      {
        toStream.Put('\"');
        toStream.Put(colName);
        toStream.Put((i --> 0) ? "\"," : "\"\n");
      }

      foreach (var row in rows)
      {
        i = colNames.Count - 1;
        foreach (string colName in colNames)
        {
          toStream.Put('\"');
          toStream.Put((string)MiniJson.ConvertValue(typeof(string), row[colName]));
          toStream.Put((i --> 0) ? "\"," : "\"\n");
        }
      }
    }

    public static string WriteCSV([NotNull] ICollection<string> colNames, [NotNull] IList<JsonObj> rows)
    {
      using (var stream = StringStream.ForBuilder())
      {
        WriteCSV(stream, colNames, rows);
        return stream.ToString();
      }
    }

    public static string WriteCSV([NotNull] IList<JsonObj> rows, params string[] colNames)
    {

      using (var stream = StringStream.ForBuilder())
      {
        if (colNames is null)
        {
          if (!rows.IsEmpty())
            WriteCSV(stream, rows[0].Keys, rows);
        }
        else
        {
          WriteCSV(stream, colNames, rows);
        }

        return stream.ToString();
      }
    }

    // TODO Tabulate(IList<JsonObj> rows) -> JsonObj

    //
    #endregion Additional Public API
    //


    ////////////////////////////////////////////////////////////////////////////

    //
    #region internals / privates
    //

    static bool s_PrettyPrint = EditorBridge.IS_DEBUG;

    internal static readonly List<Type> s_WhiteObjectTypes = new List<Type>();

    internal static ObjMaker JsonObjMaker = DefaultObjMaker;
    internal static ArrMaker JsonArrMaker = DefaultArrMaker;


    internal static JsonObj DefaultObjMaker(int capacity)
    {
      return new HashMap<string,object>(capacity);
    }
    internal static JsonArr DefaultArrMaker(int capacity)
    {
      return new List<object>(capacity);
    }


    #if UNITY_EDITOR

    const string MENU_PREFIX = Consts.MenuItemPrefixOre + nameof(JsonAuthority) + "/";
    const int    MENU_PRIO   = Consts.MenuPriorityOre;

    [MenuItem(MENU_PREFIX + "Set Provider = Default", priority = MENU_PRIO)]
    static void Menu_SetProviderDefault()
    {
      Provider = DefaultProvider;
      Debug.Log($"Set global JsonProvider to default: {DefaultProvider}");
    }


    [MenuItem(MENU_PREFIX + "Set Provider = None", priority = MENU_PRIO)]
    static void Menu_SetProviderNone()
    {
      Provider = JsonProvider.None;
      Debug.Log("Set global JsonProvider: None");
    }

    [MenuItem(MENU_PREFIX + "Set Provider = None", isValidateFunction: true)]
    static bool Validate_CanProvideNone()
    {
      return Provider != JsonProvider.None;
    }


    [MenuItem(MENU_PREFIX + "Set Provider = MiniJson", priority = MENU_PRIO)]
    static void Menu_SetProviderMiniJson()
    {
      Provider = JsonProvider.MiniJson;
      Debug.Log("Set global JsonProvider: MiniJson");
    }

    [MenuItem(MENU_PREFIX + "Set Provider = MiniJson", isValidateFunction: true)]
    static bool Validate_CanProvideMiniJson()
    {
      return Provider != JsonProvider.MiniJson;
    }


    [MenuItem(MENU_PREFIX + "Set Provider = NewtonsoftJson", priority = MENU_PRIO)]
    static void Menu_SetProviderNewtonsoft()
    {
      Provider = JsonProvider.NewtonsoftJson;
      Debug.Log("Set global JsonProvider: NewtonsoftJson");
    }

    [MenuItem(MENU_PREFIX + "Set Provider = NewtonsoftJson", isValidateFunction: true)]
    static bool Validate_CanProvideNewtonsoft()
    {
      #if ORE_NEWTONSOFT
      return Provider != JsonProvider.NewtonsoftJson;
      #else
      return false;
      #endif
    }


    [MenuItem(MENU_PREFIX + "Set Pretty Print = ON", priority = MENU_PRIO)]
    static void Menu_EnablePrettyPrint()
    {
      PrettyPrint = true;
    }

    [MenuItem(MENU_PREFIX + "Set Pretty Print = ON", isValidateFunction: true)]
    static bool Validate_CanEnablePrettyPrint()
    {
      return !PrettyPrint;
    }


    [MenuItem(MENU_PREFIX + "Set Pretty Print = OFF", priority = MENU_PRIO)]
    static void Menu_DisablePrettyPrint()
    {
      PrettyPrint = false;
    }

    [MenuItem(MENU_PREFIX + "Set Pretty Print = OFF", isValidateFunction: true)]
    static bool Validate_CanDisablePrettyPrint()
    {
      return PrettyPrint;
    }

    #endif // UNITY_EDITOR

    //
    #endregion internals / privates
    //

    //
    #region deprecations
    //

    const bool HARD_BAN_DEPRECATIONS = false;


    [System.Obsolete("Serialize is deprecated. Use Write(*) instead.", HARD_BAN_DEPRECATIONS)]
    public static string Serialize(object data, object serializer = null)
    {
      return Write(data, serializer);
    }

    [System.Obsolete("SerializeTo is deprecated. Use Write(*) instead.", HARD_BAN_DEPRECATIONS)]
    public static void SerializeTo(TextWriter stream, object data,
                                   object serializer = null)
    {
      Write(data, stream, serializer);
    }

    [System.Obsolete("Woops! Looks like your arguments to Write(*) are flipped.", error: false)]
    public static TextWriter Write(TextWriter stream, object data,
                                   object serializer = null)
    {
      return Write(data, stream, serializer);
    }

    [System.Obsolete("Deserialize (type=null) is deprecated. Use Read(*) instead.", HARD_BAN_DEPRECATIONS)]
    public static object Deserialize(string json,
                                     ObjMaker  mapMaker = null,
                                     ArrMaker listMaker = null)
    {
      using (new Scope(mapMaker, listMaker))
        return Read(json, type: null);
    }

    [System.Obsolete("DeserializeObject is deprecated. Use Read(*) instead.", HARD_BAN_DEPRECATIONS)]
    public static object DeserializeObject(string json, Type type,
                                           ObjMaker  mapMaker = null,
                                           ArrMaker listMaker = null)
    {
      using (new Scope(mapMaker, listMaker))
        return Read(json, type);
    }

    [System.Obsolete("DeserializeObject is deprecated. Use ReadDictionary(*) instead.", HARD_BAN_DEPRECATIONS)]
    public static JsonObj DeserializeObject(string json,
                                            ObjMaker  mapMaker = null,
                                            ArrMaker listMaker = null)
    {
      using (new Scope(mapMaker, listMaker))
        return ReadDictionary(json);
    }

    [System.Obsolete("DeserializeArray is deprecated. Use ReadArray(*) instead.", HARD_BAN_DEPRECATIONS)]
    public static JsonArr DeserializeArray(string json,
                                           ObjMaker  mapMaker = null,
                                           ArrMaker listMaker = null)
    {
      using (new Scope(mapMaker, listMaker))
        return ReadArray(json);
    }

    [System.Obsolete("DeserializeStream is deprecated. Use Read(*) instead.", HARD_BAN_DEPRECATIONS)]
    public static object DeserializeStream(TextReader stream, Type type = null,
                                           ObjMaker  mapMaker = null,
                                           ArrMaker listMaker = null)
    {
      using (new Scope(mapMaker, listMaker))
        return Read(stream, type);
    }

    [System.Obsolete("TryDeserialize is deprecated. Use TryRead(*) instead.", HARD_BAN_DEPRECATIONS)]
    public static bool TryDeserialize<T>(string json, out T obj,
                                         ObjMaker  mapMaker = null,
                                         ArrMaker listMaker = null)
      where T : new()
    {
      using (new Scope(mapMaker, listMaker))
        return TryRead(json, out obj);
    }

    [System.Obsolete("TryDeserializeStream is deprecated. Use TryRead(*) instead.", HARD_BAN_DEPRECATIONS)]
    public static bool TryDeserializeStream<T>(TextReader stream, out T obj,
                                               ObjMaker  mapMaker = null,
                                               ArrMaker listMaker = null)
      where T : new()
    {
      using (new Scope(mapMaker, listMaker))
        return TryRead(stream, out obj);
    }

    [System.Obsolete("TryDeserializeOverwrite is deprecated. Use TryPopulate(*) instead.", HARD_BAN_DEPRECATIONS)]
    public static bool TryDeserializeOverwrite<T>(string json, ref T obj,
                                                  ObjMaker  mapMaker = null,
                                                  ArrMaker listMaker = null)
      where T : new()
    {
      using (new Scope(mapMaker, listMaker))
        return TryPopulate(json, ref obj);
    }

    [System.Obsolete("TryDeserializeStreamOverwrite is deprecated. Use TryPopulate(*) instead.", HARD_BAN_DEPRECATIONS)]
    public static bool TryDeserializeStreamOverwrite<T>(TextReader stream, ref T obj,
                                                        ObjMaker  mapMaker = null,
                                                        ArrMaker listMaker = null)
      where T : new()
    {
      using (new Scope(mapMaker, listMaker))
        return TryPopulate(stream, ref obj);
    }

    [System.Obsolete("GenericParse is deprecated. Use ReadDictionary(*) instead.", HARD_BAN_DEPRECATIONS)]
    public static JsonObj GenericParse(string    rawJson,
                                       ObjMaker  mapMaker = null,
                                       ArrMaker listMaker = null)
    {
      using (new Scope(mapMaker, listMaker))
        return ReadDictionary(rawJson);
    }

    [System.Obsolete("Genericize is deprecated. Use NewtonsoftAuthority.Genericize(*) instead.", HARD_BAN_DEPRECATIONS)]
    public static JsonObj Genericize(object jObject, JsonObj map,
                                     ObjMaker  mapMaker = null,
                                     ArrMaker listMaker = null)
    {
      #if ORE_NEWTONSOFT
      if (jObject is Newtonsoft.Json.Linq.JObject jobj)
      {
        using (new Scope(mapMaker, listMaker))
          return NewtonsoftAuthority.Genericize(jobj, map);
      }
      #endif

      return map;
    }

    [System.Obsolete("Genericize is deprecated. Use NewtonsoftAuthority.Genericize(*) instead.", HARD_BAN_DEPRECATIONS)]
    public static JsonArr Genericize(object jArray, JsonArr list,
                                     ArrMaker listMaker = null,
                                     ObjMaker  mapMaker = null)
    {
      #if ORE_NEWTONSOFT
      if (jArray is Newtonsoft.Json.Linq.JArray jarr)
      {
        using (new Scope(mapMaker, listMaker))
          return NewtonsoftAuthority.Genericize(jarr, list);
      }
      #endif

      return list;
    }

    //
    #endregion deprecations
    //

  } // end class JsonAuthority
}