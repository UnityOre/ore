/*! @file       Static/MeGUI.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2023-05-04
 *
 *  TODO consider conditional compilation with DEBUG symbol
**/

using JetBrains.Annotations;

using UnityEngine;


namespace Ore
{

  /// <summary>
  ///   Static helpers for Unity's IMGUI (im_Me_diate mode GUI) API.
  ///   Contrasts with <see cref="YouGUI"/>.
  /// </summary>
  public static class MeGUI
  {

    public static readonly GUIContent Scratch = new GUIContent("fef");

    [NotNull]
    public static GUIStyle TextStyle
    {
      get
      {
        // unfortunately, deferred init is required for GUIStyles :unamused:

        if (s_TextStyle is null)
        {
          s_TextStyle = new GUIStyle
          {
            normal   = new GUIStyleState { textColor = Colors.White },
            clipping = TextClipping.Overflow,
            richText = true,
          };
        }

        return s_TextStyle;
      }
    }

    [NotNull]
    public static GUIStyle ButtonStyle
    {
      get
      {
        if (s_ButtonStyle is null)
        {
          s_ButtonStyle = new GUIStyle(GUI.skin.button)
          {
            clipping = TextClipping.Overflow,
            richText = true,
          };
        }

        return s_ButtonStyle;
      }
    }

    [NotNull]
    public static GUIStyle BoxStyle
    {
      get
      {
        if (s_BoxStyle is null)
        {
          s_BoxStyle = new GUIStyle(GUI.skin.box)
          {
            alignment = TextAnchor.UpperLeft,
            richText  = true,
          };
        }

        return s_BoxStyle;
      }
    }


    public static Rect SafeArea => Screen.safeArea.FlipY(Screen.height / 2f);

    [NotNull]
    public static Rect[] Cutouts
    {
      get
      {
        float about = Screen.height / 2f;
        var cutouts = Screen.cutouts;
        int i = cutouts.Length;
        while (i --> 0)
        {
          cutouts[i] = cutouts[i].FlipY(about);
        }
        return cutouts;
      }
    }


    public static Vector2 CalcContentScale(GUIContent content, GUIStyle style = null, float widthFill = 0.85f)
    {
      float scale = Screen.width * widthFill;

      scale /= (style ?? TextStyle).CalcSize(content).x;

      return new Vector2(scale, scale);
    }

    public static void NormalizeGUIScale(Vector2 referenceSize, out Rect screen)
    {
      screen = new Rect(0f, 0f, Screen.width, Screen.height);

      float scale;

      if (screen.width < screen.height == referenceSize.x < referenceSize.y)
      {
        // prefer reference width
        scale = screen.width / referenceSize.x;
        screen.height = referenceSize.x * screen.height / screen.width;
        screen.width  = referenceSize.x;
      }
      else
      {
        // prefer reference height
        scale = screen.height / referenceSize.y;
        screen.width  = referenceSize.y * screen.width / screen.height;
        screen.height = referenceSize.y;
      }

      GUI.matrix = new Matrix4x4
      {
        m00 = scale,
        m11 = scale,
        m22 = 1f,
        m33 = 1f,
      };   
    }


    public static bool IsTapped(Rect pos)
    {
      var evt = Event.current;

      // VERIFIED: EventType.MouseUp is trigged for touch taps too
      // (at least tested in Unity 2019, may also be because Input.simulateMouseWithTouches == true)

      #if UNITY_2020_1_OR_NEWER
      if (evt.type == EventType.MouseDown || evt.type == EventType.TouchDown)
      #else
      if (evt.type == EventType.MouseDown)
      #endif
      {
        s_LastTapStart = evt.mousePosition;
        return false;
      }

      #if UNITY_2020_1_OR_NEWER
      if (evt.type == EventType.MouseUp || evt.type == EventType.TouchUp)
      #else
      if (evt.type == EventType.MouseUp)
      #endif
      {
        if (!pos.Contains(s_LastTapStart) || !pos.Contains(evt.mousePosition))
          return false;

        s_LastTapStart = default;
        evt.Use();
        return true;
      }

      return false;
    }


    public static void BeginGL(Color32 color, int mode)
    {
      if (!s_OverlayMat)
      {
        // NOTICE: If this is the only thing that relies on this shader existing,
        //         you MUST add this shader to the "Always Included Shaders" list
        //         in Project Settings -> Graphics.
        // Note: I may relieve you all from this burden later, but it might
        //       include adding this shader to a Resources folder...
        s_OverlayMat = new Material(Shader.Find("Dirt Cheap/Particles (Alpha Blended)"))
        {
          renderQueue = 4001
        };
      }

      s_OverlayMat.SetPass(0);

      GL.PushMatrix();
      GL.LoadIdentity();
      GL.Begin(mode);
      GL.Color(color);
    }

    public static void BeginLineGL(Color32 color)
    {
      BeginGL(color, GL.LINE_STRIP);
    }

    public static void EndGL()
    {
      GL.End();
      GL.PopMatrix();
    }


    public static void DrawLine(Vector2 from, Vector2 to, Color32 color)
    {
      BeginLineGL(color);
      GL.Vertex3(from.x, from.y, 0f);
      GL.Vertex3(to.x,   to.y,   0f);
      EndGL();
    }

    public static void DrawOutline(Rect box, Color32 color, float z = 0f)
    {
      if (box.width.ApproximatelyZero() && box.height.ApproximatelyZero())
        return;

      BeginLineGL(color);

      float x = box.x;
      float y = box.y;

      GL.Vertex3(x, y, z);
      y += box.height;
      GL.Vertex3(x, y, z);
      x += box.width;
      GL.Vertex3(x, y, z);
      y = box.y;
      GL.Vertex3(x, y, z);
      x = box.x;
      GL.Vertex3(x, y, z);

      EndGL();
    }

    public static void DrawBox(Rect box, Color32 fill, Color32 outline, float z = 0f)
    {
      if (box.width.ApproximatelyZero() && box.height.ApproximatelyZero())
        return;

      if (!fill.IsClear())
      {
        BeginGL(fill, GL.TRIANGLES);

        float x = box.x;
        float y = box.y;

        GL.Vertex3(x, y, z);
        x += box.width;
        GL.Vertex3(x, y, z);
        y += box.height;
        GL.Vertex3(x, y, z);
        GL.Vertex3(x, y, z);
        x -= box.width;
        GL.Vertex3(x, y, z);
        y -= box.height;
        GL.Vertex3(x, y, z);

        EndGL();
      }

      if (!outline.IsClear())
      {
        DrawOutline(box, outline);
      }
    }


    static Vector2 s_LastTapStart;

    static GUIStyle s_TextStyle, s_ButtonStyle, s_BoxStyle;

    static Material s_OverlayMat;

  } // end static class MeGUI


  #if UNITY_EDITOR

  // TODO?
  internal static class YouGUI { /* helpers for Unity's UGUI (?) */ }

  #endif

}
