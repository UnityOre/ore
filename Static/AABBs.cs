/*! @file       Static/AABBs.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2023-05-24
 *
 *  TODO test compiling without this file, if code size changes
 *  TODO test perf difference after IL2CPP using `in` and `ref` keywords
**/

using JetBrains.Annotations;

using UnityEngine;


namespace Ore
{
  [PublicAPI]
  public static class AABBs
  {
    [System.Flags]
    public enum Edge
    {
      None  = 0b0000,
      All   = 0b1111,

      /// <summary> normal: [0,+1] </summary>
      Up    = 0b0001,
      /// <summary> normal: [+1,0] </summary>
      Right = 0b0010,
      /// <summary> normal: [0,-1] </summary>
      Down  = 0b0100,
      /// <summary> normal: [-1,0] </summary>
      Left  = 0b1000,

      GUIUp = Down,
      GUIDown = Up,
    }

    [System.Flags]
    public enum Face
    {
      None  = 0b000000,
      All   = 0b111111,

      /// <summary> normal: [0,+1,0] </summary>
      Up    = 0b000001,
      /// <summary> normal: [+1,0,0] </summary>
      Right = 0b000010,
      /// <summary> normal: [0,-1,0] </summary>
      Down  = 0b000100,
      /// <summary> normal: [-1,0,0] </summary>
      Left  = 0b001000,
      /// <summary> normal: [0,0,+1] </summary>
      Front = 0b010000,
      /// <summary> normal: [0,0,-1] </summary>
      Back  = 0b100000,
    }


    [Pure]
    public static Bounds To3D(in this Rect rect, float thickness = 1f, float z = 0f)
    {
      var center = (Vector3)rect.center;
      center.z = z;

      var size = (Vector3)rect.size;
      size.z = thickness;

      return new Bounds(center, size);
    }

    [Pure]
    public static Rect To2D(in this Bounds bounds)
    {
      return new Rect(bounds.min, bounds.size);
    }


    [Pure]
    public static Rect FromMinMax(Vector2 min, Vector2 max)
    {
      return new Rect(min, max - min);
    }

    [Pure]
    public static Bounds FromMinMax(Vector3 min, Vector3 max)
    {
      return new Bounds((min + max) / 2f, max - min);
    }


    public static void Expand(ref this Rect rect, float up, float right, float down, float left)
    {
      rect.Set
      (
        x:      rect.x - left,
        y:      rect.y - down,
        width:  rect.width  + left + right,
        height: rect.height + up   + down
      );
    }

    public static void Expand(ref this Rect rect, float dx, float dy)
    {
      dx /= 2f;
      dy /= 2f;
      Expand(ref rect, up: dy, right: dx, down: dy, left: dx);
    }

    public static void Expand(ref this Rect rect, float amount)
    {
      amount /= 2f;
      Expand(ref rect, amount, amount, amount, amount);
    }

    public static void Expand(ref this Bounds bounds, float up, float right, float down, float left, float front, float back)
    {
      var min = bounds.min;
      var max = bounds.max;

      min.x -= left;
      min.y -= down;
      min.z -= back;
      max.x += right;
      max.y += up;
      max.z += front;

      bounds.SetMinMax(min, max);
    }

    public static void Expand(ref this Bounds bounds, float dx, float dy, float dz)
    {
      bounds.Expand(new Vector3(dx, dy, dz));
    }


    [Pure]
    public static Rect Expanded(in this Rect rect, float up, float right, float down, float left)
    {
      return new Rect
      (
        x:      rect.x - left,
        y:      rect.y - down,
        width:  rect.width  + left + right,
        height: rect.height + up   + down
      );
    }

    [Pure]
    public static Rect Expanded(in this Rect rect, float dx, float dy)
    {
      dx /= 2f;
      dy /= 2f;
      return Expanded(in rect, up: dy, right: dx, down: dy, left: dx);
    }

    [Pure]
    public static Rect Expanded(in this Rect rect, float amount)
    {
      amount /= 2f;
      return Expanded(in rect, amount, amount, amount, amount);
    }

    [Pure]
    public static Bounds Expanded(in this Bounds bounds, float up, float right, float down, float left, float front, float back)
    {
      var min = bounds.min;
      var max = bounds.max;

      min.x -= left;
      min.y -= down;
      min.z -= back;
      max.x += right;
      max.y += up;
      max.z += front;

      return FromMinMax(min, max);
    }

    [Pure]
    public static Bounds Expanded(in this Bounds bounds, float dx, float dy, float dz)
    {
      dx /= 2f;
      dy /= 2f;
      dz /= 2f;
      return Expanded(in bounds, up: dy, right: dx, down: dy, left: dx, front: dz, back: dz);
    }

    [Pure]
    public static Bounds Expanded(in this Bounds bounds, float amount)
    {
      amount /= 2f;
      return Expanded(in bounds, amount, amount, amount, amount, amount, amount);
    }


    [Pure]
    public static bool ApproximatelyZero(in this Rect rect)
    {
      return rect.width.ApproximatelyZero() &&
            rect.height.ApproximatelyZero();
    }

    [Pure]
    public static bool ApproximatelyZero(in this Bounds bounds)
    {
      return bounds.extents.sqrMagnitude.ApproximatelyZero(Floats.Epsilon2);
    }


    [Pure]
    public static float Area(in this Rect rect)
    {
      return (rect.width * rect.height).Abs();
    }

    [Pure]
    public static float Volume(in this Bounds bounds)
    {
      var size = bounds.size;
      return (size.x * size.y * size.z).Abs();
    }


    [Pure]
    public static Rect WithAbsArea(this Rect rect)
    {
      if (rect.width < 0f)
      {
        rect.x += rect.width;
        rect.width *= -1;
      }

      if (rect.height < 0f)
      {
        rect.y += rect.height;
        rect.height *= -1;
      }

      return rect;
    }

    [Pure]
    public static Bounds WithAbsVolume(this Bounds bounds)
    {
      var ext = bounds.extents;
      return new Bounds(bounds.center, new Vector3(ext.x.Abs(), ext.y.Abs(), ext.z.Abs()));
    }


    [Pure]
    public static Rect FlipX(this Rect rect, float about)
    {
      rect.x = 2 * about - (rect.x + rect.width);
      return rect;
    }

    [Pure]
    public static Rect FlipY(this Rect rect, float about)
    {
      rect.y = 2 * about - (rect.y + rect.height);
      return rect;
    }


    [Pure]
    public static Vector2 RandomPoint(in this Rect rect)
    {
      return new Vector2
      (
        x: Random.value * rect.width  + rect.xMin,
        y: Random.value * rect.height + rect.yMin
      );
    }

    [Pure]
    public static Vector3 RandomPoint(in this Bounds bounds)
    {
      var min = bounds.min;
      var size = bounds.size;

      return new Vector3
      (
        x: Random.value * size.x + min.x,
        y: Random.value * size.y + min.y,
        z: Random.value * size.z + min.z
      );
    }


    [Pure]
    public static Vector2 RandomPointOnEdge(in this Rect rect, Edge edges = Edge.All)
    {
      // int n = GetEdges(rect, s_EdgeScratch, edges);
      //
      // if (n == 0)
      // {
      //   return rect.center; // hrm.. potentially unsavory default?
      // }
      //
      // var (a,b) = s_EdgeScratch[Integers.RandomIndex(n)];
      //
      // return Vector2.LerpUnclamped(a, b, Random.value);

      var (a,b) = RandomEdge(rect, edges);
      return Vector3.LerpUnclamped(a, b, Random.value);
    }

    [Pure]
    public static Vector3 RandomPointOnFace(in this Bounds bounds, Face faces = Face.All)
    {
      // int n = GetFaceDiagonals(bounds, s_FaceDiagScratch, faces);
      //
      // if (n == 0)
      // {
      //   return bounds.center;
      // }
      //
      // var (a,c) = s_FaceDiagScratch[Integers.RandomIndex(n)];

      var (a,c) = RandomFaceDiagonal(bounds, faces);

      // While the diagonal (a,c) fully describes the face, it does not
      //   explicitly span every point.
      // Thus, need to lerp on each axis separately in order to span the whole face:
      return new Vector3
      (
        x: Mathf.LerpUnclamped(a.x, c.x, Random.value),
        y: Mathf.LerpUnclamped(a.y, c.y, Random.value),
        z: Mathf.LerpUnclamped(a.z, c.z, Random.value)
      );
    }


    [Pure]
    public static (Vector2 a, Vector2 b) RandomEdge(in Rect rect, Edge edges = Edge.All)
    {
      edges &= Edge.All;

      if (edges == Edge.None)
        return (rect.center,rect.center);

      // turns out this is more complicated of a problem than I thought /.^

      int bits = (int)edges;

      int lsb = Bitwise.LSB(bits);

      if ((bits & ~lsb) == 0)
      {
        // only 1 edge specified
        return GetEdge(rect, edges);
      }

      int tz = Random.Range(0, 4);

      lsb = 1 << tz;

      if ((bits & lsb) == 0)
      {
        lsb = ~lsb;
      }

      return GetEdge(rect, (Edge)lsb);
    }

    [Pure]
    public static (Vector3 a, Vector3 c) RandomFaceDiagonal(in Bounds bounds, Face faces = Face.All)
    {
      faces &= Face.All;

      if (faces == Face.None)
        return (bounds.center,bounds.center);

      int bits = (int)faces;

      int lsb = Bitwise.LSB(bits);

      if ((bits & ~lsb) == 0)
      {
        // only 1 edge specified
        return GetFaceDiagonal(bounds, faces);
      }

      int tz = Random.Range(0, 6);

      lsb = 1 << tz;

      if ((bits & lsb) == 0)
      {
        lsb = ~lsb;
      }

      return GetFaceDiagonal(bounds, (Face)lsb);
    }


    [Pure]
    public static (Vector2 a, Vector2 b) GetEdge(in Rect rect, Edge edge)
    {
      edge = (Edge)(Bitwise.LSB((int)edge) & 0b1111);

      if (edge == Edge.None)
        return (rect.center,rect.center);

      var min = rect.min;
      var max = rect.max;

      Vector2 a, b;
      switch (edge)
      {
        default:
        case Edge.Up:
          a.x = min.x;
          a.y = max.y;
          b   = max;
          break;
        case Edge.Right:
          a.x = max.x;
          a.y = min.y;
          b   = max;
          break;
        case Edge.Down:
          a   = min;
          b.x = max.x;
          b.y = min.y;
          break;
        case Edge.Left:
          a   = min;
          b.x = min.x;
          b.y = max.y;
          break;
      }

      return (a,b);
    }

    [Pure]
    public static (Vector3 a, Vector3 c) GetFaceDiagonal(in Bounds bounds, Face face)
    {
      face = (Face)(Bitwise.LSB((int)face) & 0b111111);

      if (face == Face.None)
        return (bounds.center,bounds.center);

      var min = bounds.min;
      var max = bounds.max;

      Vector3 a, c;
      switch (face)
      {
        default:
        case Face.Up:
          a.x = min.x;
          a.y = max.y;
          a.z = min.z;
          c   = max;
          break;
        case Face.Right:
          a.x = max.x;
          a.y = min.y;
          a.z = min.z;
          c   = max;
          break;
        case Face.Down:
          a   = min;
          c.x = max.x;
          c.y = min.y;
          c.z = max.z;
          break;
        case Face.Left:
          a   = min;
          c.x = min.x;
          c.y = max.y;
          c.z = max.z;
          break;
        case Face.Front:
          a.x = min.x;
          a.y = min.y;
          a.z = max.z;
          c   = max;
          break;
        case Face.Back:
          a   = min;
          c.x = max.x;
          c.y = max.y;
          c.z = min.z;
          break;
      }

      return (a,c);
    }


    public static int GetEdges(in Rect rect, [NotNull] (Vector2 a, Vector2 b)[] outList, Edge edges = Edge.All)
    {
      Vector2 a = default, b = default;

      var min = rect.min;
      var max = rect.max;

      int i = 0;
      int bits = (int)edges & 0b1111;

      while (bits != 0)
      {
        if (i >= outList.Length)
        {
          throw new System.IndexOutOfRangeException("outList array must be pre-sized. Use size=4 to be safe.");
        }

        int lsb = Bitwise.LSB(bits);

        switch ((Edge)lsb)
        {
          case Edge.Up:
            a.x = min.x;
            a.y = max.y;
            b   = max;
            break;
          case Edge.Right:
            a.x = max.x;
            a.y = min.y;
            b   = max;
            break;
          case Edge.Down:
            a   = min;
            b.x = max.x;
            b.y = min.y;
            break;
          case Edge.Left:
            a   = min;
            b.x = min.x;
            b.y = max.y;
            break;
        }

        outList[i++] = (a,b);

        bits &= ~lsb;
      }

      return i;
    }

    public static int GetFaceDiagonals(in Bounds bounds, [NotNull] (Vector3 a, Vector3 c)[] outList, Face faces = Face.All)
    {
      // a and c describe the diagonal line through the face
      Vector3 a = default, c = default;

      var min = bounds.min;
      var max = bounds.max;

      int i = 0;
      int bits = (int)faces & 0b111111;

      while (bits != 0)
      {
        if (i >= outList.Length)
        {
          throw new System.IndexOutOfRangeException("outList array must be pre-sized. Use size=6 to be safe.");
        }

        int lsb = Bitwise.LSB(bits);

        switch ((Face)lsb)
        {
          case Face.Up:
            a.x = min.x;
            a.y = max.y;
            a.z = min.z;
            c   = max;
            break;
          case Face.Right:
            a.x = max.x;
            a.y = min.y;
            a.z = min.z;
            c   = max;
            break;
          case Face.Down:
            a   = min;
            c.x = max.x;
            c.y = min.y;
            c.z = max.z;
            break;
          case Face.Left:
            a   = min;
            c.x = min.x;
            c.y = max.y;
            c.z = max.z;
            break;
          case Face.Front:
            a.x = min.x;
            a.y = min.y;
            a.z = max.z;
            c   = max;
            break;
          case Face.Back:
            a   = min;
            c.x = max.x;
            c.y = max.y;
            c.z = min.z;
            break;
        }

        outList[i++] = (a,c);

        bits &= ~lsb;
      }

      return i;
    }


    //
    // private section
    //

    static readonly (Vector2 a, Vector2 b)[] s_EdgeScratch     = new (Vector2 a, Vector2 b)[4];
    static readonly (Vector3 a, Vector3 c)[] s_FaceDiagScratch = new (Vector3 a, Vector3 c)[6];

  } // end static class AABBs
}