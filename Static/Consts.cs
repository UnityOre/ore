/*! @file       Static/Consts.cs
 *  @author     Ayrton Muniz (ayrton.muniz\@bore.com)
 *  @date       2023-06-14
**/

using JetBrains.Annotations;


namespace Ore
{
  #if UNITY_EDITOR // #if to hopefully avoid hurting aggressive stripping policies
  [PublicAPI]
  #endif
  public static class Consts
  {

  #region     Basics

    public const bool IsDebug  = EditorBridge.IS_DEBUG;
    public const bool IsEditor = EditorBridge.IS_EDITOR;
    #if ORE_DEBUG
    public const bool IsOreDebug = true;
    #else
    public const bool IsOreDebug = false;
    #endif

  #endregion  Basics


  #region     IEC & SI Units

    public const long Kebi = 1024;
    public const long Mebi = Kebi*Kebi;
    public const long Gebi = Kebi*Kebi*Kebi;
    public const long Tebi = Kebi*Kebi*Kebi*Kebi;

    public const long Kilo = 1000;
    public const long Mega = Kilo*Kilo;
    public const long Giga = Kilo*Kilo*Kilo;
    public const long Tera = Kilo*Kilo*Kilo*Kilo;

  #endregion  IEC & SI Units


  #region     Internal Standards

    public const string MenuItemPrefix          = "Ore/";
    public const string MenuItemPrefixOre       = "Ore/";
    public const int    MenuPriorityStandard    = -10;
    public const int    MenuPrioritySpecial     = -100;
    public const int    MenuPriorityUnspecified =  0;
    public const int    MenuPriorityOre         = -50;

  #endregion  Internal Standards

  }
}
