/*! @file       Tests/Editor/MiscInEditor.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2023-02-28
**/

using Ore;

using NUnit.Framework;

using UnityEngine;
using UnityEngine.TestTools;

using System.Text;
using System.Text.RegularExpressions;
using System.Security.Cryptography;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;

using AssException = UnityEngine.Assertions.AssertionException;
using Type = System.Type;


// ReSharper disable once CheckNamespace
internal static class MiscInEditor
{

  [Test]
  [System.Obsolete("MultiException is obsolete as of v8.7.0.")]
  public static void MultiException()
  {
    var nse = new System.NotSupportedException("(top)");
    var ioe = new System.InvalidOperationException("(middle)");
    var nie = new System.NotImplementedException("(bottom)");

    Assert.Throws<MultiException>(() =>
    {
      var mex = Ore.MultiException.Create(nse, ioe, nie);
      LogAssert.Expect(LogType.Exception, new Regex(@"NotSupportedException: \(top\)"));
      Orator.NFE(mex);
      throw mex;
    });
  }

  [Test]
  public static void AssException()
  {
    Assert.Throws<AssException>(() => throw new AssException("message", "userMessage"));
  }

  [Test]
  public static void OAssertExceptionContract()
  {
    bool expectException = Orator.RaiseExceptions;

    if (expectException)
    {
      Assert.Throws<AssException>(() => OAssert.True(false));
    }
    else
    {
      Assert.DoesNotThrow(() => OAssert.True(false));
    }

    bool ok = false;
    Assert.DoesNotThrow(() =>
    {
      bool pop = LogAssert.ignoreFailingMessages;
      LogAssert.ignoreFailingMessages = true;

      ok = OAssert.Fails(false) &&
          !OAssert.Fails(true) &&
           OAssert.FailsNullCheck(null) &&
           OAssert.FailsNullChecks(new object(), null, new object());

      LogAssert.ignoreFailingMessages = pop;
    });
    Assert.True(ok);
  }


  [Test]
  public static void ActionNullability()
  {
    System.Action action = null;

    Assert.DoesNotThrow(() =>
                        {
                          action += Orator.Reached;
                        });

    Assert.NotNull(action);

    Assert.DoesNotThrow(() =>
                        {
                          action -= Orator.Reached;
                        });

    Assert.Null(action);

    Assert.DoesNotThrow(() =>
                        {
                          action -= Orator.Reached;
                        });

    Assert.Null(action);
  }

  [Test]
  public static void DelegateCovariance()
  {
    object objMaker() => new object();
    int    intMaker() => 123;
    int[]  arrMaker() => new int[1];

    void delegateConsumer(System.Func<object> functor)
    {
      Assert.DoesNotThrow(() =>
                          {
                            _ = functor();
                          });
    }

    delegateConsumer(objMaker);

    object boxedObjMaker = (System.Func<object>) objMaker;
    object boxedIntMaker = (System.Func<int>)    intMaker;
    object boxedArrMaker = (System.Func<int[]>)  arrMaker;

    Assert.IsAssignableFrom<System.Func<object>>(boxedObjMaker);
    Assert.IsNotAssignableFrom<System.Func<object>>(boxedIntMaker);
    Assert.IsNotAssignableFrom<System.Func<object>>(boxedArrMaker);

    Debug.Log("Well this sucks. Covariance—more like NOvariance. Move on I guess.");

    Assert.True(boxedObjMaker is System.Delegate);

    var dog = boxedIntMaker as System.Delegate;
    Assert.NotNull(dog);
    Assert.AreEqual(123, dog.DynamicInvoke());
    // well, at least that's something.
  }

  [MethodImpl(MethodImplOptions.AggressiveInlining)]
  static bool Approximately(float a, float b)
  {
    return (a -= b) * a < 1e-12f;
  }

  [Test]
  public static void ManagedAggressiveInlining()
  {
    float a = 1f;
    float b = 1f;

    Assert.True(Approximately(a, b));
    Assert.AreEqual(1f, a, "a == 1f");
    Assert.AreEqual(1f, b, "b == 1f");
  }


  // [Test]
  // public static void ReflectSystem()
  // {
  //   var type = Type.GetType("System.Collections.HashHelpers");
  //
  //   Assert.NotNull(type, "typeof(System.Collections.HashHelpers) != null");
  //
  //   foreach (var member in type.GetMembers(TypeMembers.STATIC))
  //   {
  //     Debug.Log(member);
  //   }
  //
  //   var method = typeof(string).GetMethod("UseRandomizedHashing", TypeMembers.STATIC);
  //
  //   Assert.NotNull(method, "string.UseRandomizedHashing()");
  //
  //   Debug.Log("string.UseRandomizedHashing() => " + method.Invoke(null, System.Array.Empty<object>()));
  // }


  [Test]
  public static void RecycledStringBob()
  {
    int precount = RecycledStringBuilder.AliveCount;

    using (new RecycledStringBuilder(out var bob))
    {
      Assert.NotNull(bob);
      Assert.AreEqual(precount + 1, RecycledStringBuilder.AliveCount, "in scope AliveCount");

      bob.Append("fef");

      Assert.AreEqual(3, bob.Length, "bob.Length");

      Assert.AreEqual("fef", bob.ToString(), "bob.ToString()");
    }

    Assert.AreEqual(precount, RecycledStringBuilder.AliveCount, "post scope AliveCount");
  }

  [Test]
  public static void MappedStructIsImmutable()
  {
    // (a proof)

    var map = new HashMap<string,Vector3>
    {
      ["fef"] = new Vector3(1, 2, 3)
    };

    bool sanity = map.Find("fef", out var strct);
    Assert.True(sanity, "map[\"fef\"]");

    Assert.AreEqual(1f, strct.x, "strct.x");

    strct.x = 3.14f;

    Debug.Log(strct);

    sanity = map.Find("fef", out var again);
    Assert.True(sanity, "map[\"fef\"] (again)");

    Debug.Log(again);

    Assert.AreNotEqual(3.14f, again.x, "again.x");

    // new FindRef method should alleviate this:

    ref var vecref = ref map.FindRef("fef", out sanity);
    Assert.True(sanity, "map.FindRef(\"fef\")");

    vecref.x = 1337f;

    sanity = map.Find("fef", out again);
    Assert.True(sanity, "map[\"fef\"] (again again)");

    Debug.Log(again);

    Assert.AreEqual(1337f, again.x, "again.x");
  }


  [Test]
  public static void StringEqualityIgnoreWhitespace()
  {
    string fef = "fef";
    string[] tests =
    {
      " fef\n\r",
      "\tf e f ",
      " fe  \n f ",
      "fef",
    };

    foreach (var test in tests)
    {
      Assert.True(Strings.AreEqualIgnoreWhitespace(fef, test));
    }

    foreach (var test in tests)
    {
      Assert.True(Strings.AreEqualIgnoreWhitespace(test, fef));
    }

    Assert.False(Strings.AreEqualIgnoreWhitespace(fef, "bub"));
    Assert.False(Strings.AreEqualIgnoreWhitespace(fef, "fef1"));
    Assert.False(Strings.AreEqualIgnoreWhitespace(fef, "fe"));
  }


  [Test]
  public static void NiceTypeNames()
  {
    (Type test, string expected)[] tests =
    {
      (typeof(string),                 "string"),
      (typeof(long[]),                 "long[]"),
      (typeof(List<object>),           "List<object>"),
      (typeof(JsonAuthority),          "JsonAuthority"),
      (typeof(JsonAuthority.Scope),    "JsonAuthority.Scope"),
      (typeof(Promise<IList<Object>>), "Promise<IList<Object>>"),
      (typeof(Promise<>),              "Promise<T>"),
      (typeof(HashMap<,>),             "HashMap<K,V>"),
      (typeof(HashMap<string,bool>),   "HashMap<string,bool>"),
      (typeof(void),                   "void"),
      (typeof(void*),                  "void*"),
      (typeof(int*),                   "int*"),
    };

    foreach (var (test,expected) in tests)
    {
      LogAssert.Expect(LogType.Log, expected);

      Debug.Log(test.NiceName());
    }

    Debug.Log(typeof((string s,int i)).NiceName());
  }

  [Test]
  public static void IECByteStrings()
  {
    (long bytes, string expectedKibi, string expectedSI)[] tests =
    {
      (0L,              "0 B",       "0 B"),
      (1337L,           "1337 B",    "1337 B"),
      (1_000_000L,      "976.6 KiB", "1000 KB"),
      (10_000_000L,     "9.5 MiB",   "10 MB"),
      (1_000_000_000L,  "953.7 MiB", "1000 MB"),
      (10_000_000_000L, "9.3 GiB",   "10 GB"),
    };

    foreach (var (test, expectedKibi, expectedSI) in tests)
    {
      string actual = Strings.FormatBytesKibi(test);

      LogAssert.Expect(LogType.Log, expectedKibi);

      Debug.Log(actual);

      actual = Strings.FormatBytesSI(test);

      LogAssert.Expect(LogType.Log, expectedSI);

      Debug.Log(actual);
    }
  }


  [Test]
  public static void MD5Correctness()
  {
    const string INPUT = "Never gonna give you up"  +
                         "Never gonna let you down" +
                         "Never gonna run around"   +
                         "and desert you."          +
                         "Never gonna make you cry" +
                         "Never gonna say goodbye"  +
                         "Never gonna tell a lie"   +
                         "and hurt you.";

    string expected = KAFMD5(INPUT);
    string actual   = Hashing.MD5(INPUT);

    Assert.AreEqual(expected, actual);

    actual = Hashing_MD5_Old(INPUT);

    Assert.AreEqual(expected, actual);
  }

  [Test]
  public static void MD5Speed([Values(100)] int n)
  {
    const string INPUT = "Never gonna give you up"  +
                         "Never gonna let you down" +
                         "Never gonna run around"   +
                         "and desert you."          +
                         "Never gonna make you cry" +
                         "Never gonna say goodbye"  +
                         "Never gonna tell a lie"   +
                         "and hurt you.";

    string expected = KAFMD5(INPUT);

    (string name, System.Func<string,string> test)[] TESTS =
    {                                 // (using n=100)
      ("A (KAF)",  KAFMD5),           //  KAF: 7502µs
      ("B (Ore)", Hashing.MD5),      // Ore: 5372µs -> ~39.7% faster
      ("C (Ore)", Hashing_MD5_Old),  // Alt1: 3297µs -> ~127.5% faster
    };

    var idxs = new List<int>();

    int i = TESTS.Length;
    while (i --> 0)
    {
      idxs.Add(i);
    }

    while (n --> 0)
    {
      idxs.Shuffle(); // mixup order to eliminate predictive execution bias

      foreach (int idx in idxs)
      {
        var (name,test) = TESTS[idx];

        string actual;
        using (new CodeJudge(name))
        {
          actual = test(INPUT);
        }

        Assert.AreEqual(expected, actual);
      }
    } // end outer while

    foreach (var (name,_) in TESTS)
    {
      CodeJudge.ReportJson(name, out string json);
      Debug.Log(json);
      CodeJudge.Remove(name);
    }
  }


  static string KAFMD5(string input)
  {
    var hash = new StringBuilder();

    var md5provider = new MD5CryptoServiceProvider();
    byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(input));

    for (int i = 0; i < bytes.Length; i++)
    {
      hash.Append(bytes[i].ToString("x2"));
    }

    return hash.ToString();
  }

  static string Hashing_MD5_Old(string input)
  {
    using (new RecycledStringBuilder(out var hash))
    {
      byte[] bytes = input.ToBytes(UTF8NoBOM);

      bytes = MD5er.ComputeHash(bytes);

      for (int i = 0; i < bytes.Length; i++)
      {
        hash.Append(bytes[i].ToString("x2"));
      }

      return hash.ToString();
    }
  }

  static readonly Encoding UTF8NoBOM = new UTF8Encoding(false);

  static readonly MD5 MD5er = MD5.Create();

} // end class MiscInEditor
