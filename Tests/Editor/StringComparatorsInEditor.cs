/*! @file       Tests/Editor/StringComparatorsInEditor.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2023-05-08
**/

using Ore;

using NUnit.Framework;

using UnityEngine;

using System.Collections.Generic;


// ReSharper disable once CheckNamespace
internal static class StringComparatorsInEditor
{

  static readonly string[] s_TestStrings =
  {
    "",
    "fef",
    "Levi Perez",
    "♠♣♦♥",
    Strings.MakeGUID(),
    Strings.MakeGUID().Remove(23),
    Strings.MakeGUID().Remove(17),
    Strings.MakeGUID().Remove( 9),
  };


  [Test]
  public static void HashCorrectness()
  {
    var cmp = new StringComparator();

    foreach (string test in s_TestStrings)
    {
      int h0 = cmp.GetHashCode(test);

      Debug.Log($"{cmp}.GetHashCode(\"{test}\")[0] -> {h0}");

      int h1 = cmp.GetHashCode(test);

      Debug.Log($"{cmp}.GetHashCode(\"{test}\")[1] -> {h1}");

      Assert.AreEqual(h0, h1, "h1 != h0");
    }
  }

  [Test]
  public static void HashSpeed([Values(1000, 10000)] int n)
  {
    (string test, IEqualityComparer<string> cmp)[] cmps =
    {
      ("0| EntropicHasher",        new EntropicHasher(Random.state)),
      ("0| StringComparator",      new StringComparator()),
      ("1| Comparator<string>",    new Comparator<string>()),
      ("2| System.StringComparer", System.StringComparer.Ordinal),
    };

    while (n --> 0)
    {
      cmps.Shuffle();

      foreach (var (test, cmp) in cmps)
      {
        using (new CodeJudge(test))
        {
          foreach (string str in s_TestStrings)
          {
            int h0 = cmp.GetHashCode(str);
            int h1 = cmp.GetHashCode(str);
            Assert.AreEqual(h0, h1);
          }

          int nn = 50;
          while (nn --> 0)
          {
            _ = cmp.GetHashCode(Strings.MakeGUID());
          }
        }
      }
    }

    var reports = new List<IDictionary<string,object>>();

    System.Array.Sort(cmps, (lhs,rhs) => string.CompareOrdinal(lhs.test, rhs.test));

    foreach (var (test, _) in cmps)
    {
      reports.Add(CodeJudge.Report(test));

      CodeJudge.Remove(test);
    }

    string csv = JsonAuthority.WriteCSV(reports, "identifier", "callCount", "totalTime", "avgTime", "stdev");

    Debug.Log(csv);

    string file = Filesystem.GetTempPath("StringHashSpeed.csv");

    if (Filesystem.TryWriteText(file, csv))
    {
      EditorBridge.BrowseTo(file);
    }
  }

  [Test]
  public static void HashCollision([Values(100, 1000)] int tableSize)
  {
    // TODO
  }

} // end class StringComparatorsInEditor
