/*! @file       Tests/Editor/PrimesInEditor.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2023-04-27
**/

using Ore;

using NUnit.Framework;

using UnityEngine;

using System.Collections.Generic;
using System.Linq;

using Math = System.Math;

// generated tests with:
/*
git grep -hEA1 --break '\[Test\]' -- 'Tests/Runtime/Primes*.cs' | \
sed -E -e 's|^[ -]{2}||' \
-e 's| (\w+)(\(.*\))[^)]*$| \1\2\n{\n  PrimesCorrectness.\1\2;\n}\n|' | \
sed -E -e '/^  / s|\[.+\] [^ ]+ ||' | tsd klip
*/
// (fixing up as necessary.)

// ReSharper disable once CheckNamespace
internal static class PrimesInEditor
{

  [System.Obsolete]
  [Test]
  public static void BinarySearchCorrectness([Values(500)] int n)
  {
    PrimesCorrectness.BinarySearchCorrectness(n);
  }


  [Test]
  public static void IsPrimeNoLookup([Values(200)] int n)
  {
    PrimesCorrectness.IsPrimeNoLookup(n);
  }


  [Test]
  public static void IsPrimeLookup([Values(200)] int n)
  {
    PrimesCorrectness.IsPrimeLookup(n);
  }


  [Test]
  public static void IsLongPrimeNoLookup([Values(100)] int n)
  {
    PrimesCorrectness.IsLongPrimeNoLookup(n);
  }


  [Test]
  public static void GetRandom([Values(200)] int n)
  {
    PrimesCorrectness.GetRandom(n);
  }


  [Test]
  public static void Next()
  {
    PrimesCorrectness.Next();
  }


  [Test]
  public static void NextNoLookup()
  {
    PrimesCorrectness.NextNoLookup();
  }


  [Test]
  public static void NextHashableSize()
  {
    PrimesCorrectness.NextHashableSize();
  }

  [Test]
  public static void NextHashableSizeSpeed([Values(600)] int iterations,
                                           [Values(200)] int subIterations)
  {
    (string id, System.Func<int,int,int,int> func)[] AB =
    {
      ("NxtHshSz..InlineBS", Primes.NextHashableSize),
      ("NxtHshSz..OrigBS",   Primes.NextHashableSize_Orig),
      ("NxtHshSz..NextNL",   (p, hp, x) => Primes.NextNoLookup(p + x, hp)),
      ("NxtHshSz..NoBS",     Primes.NextHashableSize_NoBS),
    };

    var primes = Primes10K.GetTestValues(nPrime: iterations >> 1,
                                              nNonPrime: iterations >> 1,
                                  includeConstantValues: false);

    var hashprimes = new int[5];
    int i = hashprimes.Length;
    while (i --> 0)
    {
      hashprimes[i] = Hashing.HashPrimes[i];
    }

    i = iterations;
    while (i --> 0)
    {
      int p   = primes[i];
      int hp  = hashprimes.RandomItem();
      int idx = Integers.RandomIndex(AB.Length);

      using (new CodeJudge(AB[idx].id))
      {
        int ii = subIterations;
        while (ii --> 0)
        {
          p = AB[idx].func(p, hp, idx % 2);

          Assert.GreaterOrEqual(p, Primes.MinValue, "yikes smol");
          Assert.LessOrEqual   (p, Primes.MaxValue, "yikes bigg");

          if (p == Primes.MaxValue)
          {
            Debug.Log("hit ceiling~");
            p = ii;
          }
        }
      }
    }

    foreach (var (id, _ ) in AB)
    {
      CodeJudge.ReportJson(id, out string json);

      Assert.IsNotEmpty(json, "no report string?");

      Debug.Log(json);

      CodeJudge.Remove(id);
    }
  }


  [Test]
  public static void NearestTo()
  {
    PrimesCorrectness.NearestTo();
  }


  [Test]
  public static void FindGoodHashPrimes()
  {
    string text;
    using (new RecycledStringBuilder("static readonly int[] HashPrimes = {\n", out var bob))
    {
      int perline = 10;

      foreach (int hashprime in Hashing.HashPrimes)
      {
        bool good = true;

        foreach (int prime in Primes.HashableSizes)
        {
          if ((prime - 1) % hashprime == 0)
          {
            good = false;
            break;
          }
        }

        if (good)
        {
          bob.Append(hashprime).Append(',');
          if (--perline == 0)
          {
            bob.AppendLine();
            perline = 10;
          }
        }
      }

      bob.Append("\n};");

      text = bob.ToString();
    }

    GUIUtility.systemCopyBuffer = text;

    Debug.Log("Copied C# to clipboard:\n" + text);
  }

  [Test, Timeout(10000)]
  public static void FindGoodSizePrimes(
    [Values(97, 101, 193)] int hashprime,
    [Values(1.125f, 1.15f, 1.2f, 1.25f)] float growFactor)
  {
    string text;
    using (new RecycledStringBuilder("static readonly int[] s_", out var bob))
    {
      bob.Append((int)(growFactor * 100 + 0.5f)).Append('x');
      bob.Append("Hash").Append(hashprime).Append("Sizes");
      bob.Append(" =\n{\n  ");

      const int perline = 10;

      int count = 0;

      int p = 5;
      while (p <= 12633961)
      {
        if ((p - 1) % hashprime != 0)
        {
          bob.Append(p).Append(',');

          if (++count % perline == 0)
          {
            bob.Append("\n  ");
          }
        }

        p = (int)(p * growFactor + 0.5f);
        p = Primes.NextNoLookup(p);
      }

      bob.Append("\n};\n");

      bob.Insert(0, $"const int N_PRIMESIZES = {count};\n\n");

      bob.Insert(0, $"const int HASHPRIME = {hashprime};\n\n");

      bob.Insert(0, $"const float GROWFACTOR = {growFactor:F2}f;\n\n");

      text = bob.ToString();
    }

    string file = Filesystem.GetTempPath($"PrimeSizes_{growFactor:0.##}f_{hashprime}.cs");

    if (Filesystem.TryWriteText(file, text))
    {
      EditorBridge.BrowseTo(file);
    }
    else
    {
      Filesystem.LogLastException();
      Debug.Log(text);
    }
  }


  [Test]
  public static void PrimeDeviationPerDigit()
  {
    var primedeltas = new List<int>(10000);
    
    double sum = 0, sumperdig = 0;

    for (int i = 1, ilen = Primes10K.InOrder.Length; i < ilen; ++i)
    {
      int d = Primes10K.InOrder[i] - Primes10K.InOrder[i -1];
      sum += d;
      primedeltas.Add(d);

      int digits = Integers.CalcDigits(Primes10K.InOrder[i]);
      sumperdig += (double)d / digits;
    }

    sum       /= primedeltas.Count;
    sumperdig /= primedeltas.Count;

    double stdev = primedeltas.Average(v => Math.Pow(v - sum, 2));
    stdev = Math.Sqrt(stdev);

    double stdevperdig = primedeltas.Average(v => Math.Pow((double)v / Integers.CalcDigits(v) - sumperdig, 2));
    stdevperdig = Math.Sqrt(stdevperdig);

    Debug.Log($"Avg + Stdev of first 10k primes: avg={sum:F1},stdev={stdev:F1}");
    Debug.Log($"Avg + Stdev PER DIGIT of primes: avg={sumperdig:F1},stdev={stdevperdig:F1}");

    Assert.Pass();
  }

} // end class PrimesInEditor
