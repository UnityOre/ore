/*! @file       Runtime/JsonAuthorityInEditor.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2022-08
**/

using Ore;
using Ore.Tests;

using NUnit.Framework;

using UnityEngine;
using UnityEngine.TestTools;

using System.Collections.Generic;
using System.Text.RegularExpressions;

#if ORE_NEWTONSOFT
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
#endif

using JsonObj = System.Collections.Generic.Dictionary<string,object>;
using JsonArr = System.Collections.Generic.List<object>;


// ReSharper disable once CheckNamespace
internal static class JsonAuthorityInEditor
{

  internal static readonly (string json, object expected)[] TestJsons =
  {
    ("null", null),
    ("true", true),
    ("123",  123),
    ("{\"fef\":123}", new JsonObj { ["fef"] = 123 }),
    ("{\n  \"m_Fef12\": \"feffity fef fef\\nfef.\"\n}", new JsonObj { ["m_Fef12"] = "feffity fef fef\nfef." }),
    ("[1,2,3]", new JsonArr { 1, 2, 3 }),
    ("{\"fefArray\": [\"fef1\", \"fef2\"], \"nothing\": true}", new JsonObj { ["fefArray"] = new JsonArr { "fef1", "fef2" }, 
                                                                              ["nothing"]  = true }),
  };


  [Test]
  public static void SerializeCurrentProvider([Values(true, false)] bool pretty)
  {
    using (new JsonAuthority.Scope(pretty))
    {
      Assert.AreEqual(JsonAuthority.PrettyPrint, pretty, "pretty");

      Debug.Log($"JsonAuthority.PrettyPrint: {pretty}");
      Debug.Log($"JsonAuthority.Provider: {JsonAuthority.Provider}");

      foreach (var (expected, test) in TestJsons)
      {
        string json = JsonAuthority.Write(test);

        MegaAssert.AreEquivalentJson(expected, json);
      }
    }
  }

  [Test]
  public static void DeserializeCurrentProvider()
  {
    Debug.Log($"JsonAuthority.Provider: {JsonAuthority.Provider}");

    foreach (var (test, expected) in TestJsons)
    {
      object parsed = JsonAuthority.Read(test);

      MegaAssert.AreEquivalentJson(expected, parsed, $"'{test}' (parsed type: {parsed?.GetType().NiceName() ?? "null"})");
    }
  }


  [Test]
  public static void Serialize([Values(true, false)] bool pretty,
                               [Values(JsonProvider.MiniJson, JsonProvider.NewtonsoftJson)] JsonProvider provider)
  {
    MegaAssert.CheckNewtonsoft(provider, willReflectFields: false);

    using (new JsonAuthority.Scope(provider, pretty))
    {
      Assert.AreEqual(provider, JsonAuthority.Provider);

      #if ORE_NEWTONSOFT
      if (provider == JsonProvider.NewtonsoftJson)
      {
        Assert.AreEqual(pretty ? Formatting.Indented : Formatting.None,
                        NewtonsoftAuthority.SerializerSettings.Formatting,
                        "SerializerSettings.Formatting");
      }
      #endif

      SerializeCurrentProvider(pretty);
    }
  }

  [Test]
  public static void Deserialize([Values(JsonProvider.MiniJson, JsonProvider.NewtonsoftJson)] JsonProvider provider)
  {
    MegaAssert.CheckNewtonsoft(provider, willReflectFields: false);

    using (new JsonAuthority.Scope(provider))
    {
      Assert.AreEqual(provider, JsonAuthority.Provider);

      DeserializeCurrentProvider();
    }
  }


  [Test]
  public static void ReflectFields([Values(true, false)] bool pretty,
                                   [Values(JsonProvider.MiniJson, JsonProvider.NewtonsoftJson)] JsonProvider provider)
  {
    MegaAssert.CheckNewtonsoft(provider, willReflectFields: true);

    using (new JsonAuthority.Scope(provider, pretty))
    {
      var ti = TimeInterval.OfSeconds(1);

      object expected = provider == JsonProvider.MiniJson ? // MiniJson respects IWriteJson
                        "10000000" :
                        "{\"Ticks\":10000000,\"m_AsFrames\":false}";
      object actual   = JsonAuthority.Write(ti);

      Debug.Log(actual);

      MegaAssert.AreEquivalentJson(expected, actual, "Serializing TimeInterval");

      expected = ti;
      actual   = JsonAuthority.Read((string)actual, typeof(TimeInterval));

      MegaAssert.AreEquivalentJson(expected, actual, "Deserializing TimeInterval");

      var ivec = new Vector2Int(3, 4);

      expected = $"{{\"m_X\":{ivec.x},\"m_Y\":{ivec.y}}}";
      actual   = JsonAuthority.Write(ivec);

      Debug.Log(actual);

      MegaAssert.AreEquivalentJson(expected, actual, "Serializing Vector2Int");

      expected = ivec;
      actual   = JsonAuthority.Read((string)actual, typeof(Vector2Int));

      MegaAssert.AreEquivalentJson(expected, actual, "Deserializing Vector2Int");
    }
  }

  [Test]
  public static void ReflectClass([Values(true, false)] bool pretty,
                                  [Values(JsonProvider.MiniJson, JsonProvider.NewtonsoftJson)] JsonProvider provider)
  {
    MegaAssert.CheckNewtonsoft(provider, willReflectFields: true);

    using (new JsonAuthority.Scope(provider, pretty))
    {
      Assert.AreEqual(provider, JsonAuthority.Provider);

      var lhs = ReflectMe.Make();

      string json = JsonAuthority.Write(lhs);

      Assert.IsNotEmpty(json);

      Debug.Log($"lhs:\n{json}");

      var fieldMap = JsonAuthority.ReadDictionary(json);

      Assert.NotNull(fieldMap);

      foreach (var field in ReflectMe.ExpectSerializedFields())
      {
        Assert.True(fieldMap.ContainsKey(field.Name), "fieldMap.ContainsKey(field.Name)");
      }

      foreach (var field in ReflectMe.ExpectNonSerializedFields())
      {
        Assert.False(fieldMap.ContainsKey(field.Name), "fieldMap.ContainsKey(field.Name)");
      }

      bool ok = JsonAuthority.TryRead(json, out ReflectMe rhs);
      Assert.True(ok, nameof(JsonAuthority.TryRead));

      Debug.Log($"rhs:\n{JsonAuthority.Write(rhs)}");

      Assert.False(ReferenceEquals(lhs, rhs), "ReferenceEquals(lhs, rhs)");
      Assert.True(ReflectMe.DeepEquals(lhs, rhs), "DeepEquals(lhs, rhs)");
      Assert.AreNotEqual(lhs.NonSerialized, rhs.NonSerialized, "lhs.NonSerialized != rhs.NonSerialized");

      var rhs2 = rhs;
      Assert.True(ReferenceEquals(rhs, rhs2), "ReferenceEquals(rhs, rhs2)");

      long nonSrl = rhs.NonSerialized;

      json = "{\"I32\":666,\"F32\":-1.414,\"Bool\":false,\"String\":null,\"StringArray\":[\"I\",\"THIRST\"],\"StringList\":[\"FOR\",\"BROTH\"],\"StringDict\":{},\"BoxedDict\":{\"fef\":\"always\"}}";

      ok = JsonAuthority.TryPopulate(json, ref rhs);
      Assert.True(ok, nameof(JsonAuthority.TryPopulate));

      Assert.True(ReferenceEquals(rhs, rhs2), "ReferenceEquals(rhs, rhs2) - post-overwrite");
      Assert.AreEqual(nonSrl, rhs.NonSerialized, "nonSrl == rhs.NonSerialized");
      Assert.False(ReflectMe.DeepEquals(lhs, rhs), "DeepEquals(lhs, rhs)");
    }
  }

  [Test]
  public static void ReflectStatic([Values(true, false)] bool pretty,
                                   [Values(JsonProvider.MiniJson, JsonProvider.NewtonsoftJson)] JsonProvider provider)
  {
    MegaAssert.CheckNewtonsoft(provider, willReflectFields: true);

    using (new JsonAuthority.Scope(provider, pretty))
    {
      var type = typeof(DeviceSpy);

      DoItAgain:

      string json = JsonAuthority.Write(type);

      Debug.Log(json);

      Assert.IsNotEmpty(json);
      Assert.Greater(json.Length, 2);

      var parsed = JsonAuthority.ReadDictionary(json);

      Assert.NotNull(parsed, "parsed");
      Assert.Positive(parsed.Count, "parsed.Count");

      foreach (var kvp in parsed)
      {
        var prop = type.GetProperty(kvp.Key, TypeMembers.STATIC);

        object expected;
        if (prop is null)
        {
          var field = type.GetField(kvp.Key, TypeMembers.STATIC);

          Assert.NotNull(field, $"{type.NiceName()}.{kvp.Key}");

          expected = field.GetValue(null);
        }
        else
        {
          expected = prop.GetMethod.Invoke(null, System.Array.Empty<object>());
        }

        var actual = kvp.Value;

        MegaAssert.AreEquivalentJson(expected, actual, $"{type.NiceName()}.{kvp.Key}");
      }

      // try a system class
      if (type == typeof(DeviceSpy))
      {
        type = typeof(System.Diagnostics.Stopwatch);
        goto DoItAgain;
      }
    }
  }


#if ORE_NEWTONSOFT

  [Test]
  public static void NsoftMiscJsonFromMap()
  {
    var map = new HashMap<object,object>
    {
      ["fef"]                     = DeviceDimension.DisplayHz,
      [DeviceDimension.OSVersion] = DeviceSpy.OSVersion,
      ["guid"]                    = System.Guid.NewGuid(),
      ["platform"]                = Application.platform,
      ["platformStr"]             = Application.platform.ToInvariant(),
    };

    var serializer = JsonSerializer.CreateDefault();

    Assert.DoesNotThrow(() =>
                        {
                          var jobj = JObject.FromObject(map, serializer);
                          Debug.Log(jobj.ToString(Formatting.Indented));
                        });

    Assert.DoesNotThrow(() =>
                        {
                          var jobj = JObject.FromObject(HashMapParams.Default, serializer);
                          Debug.Log(jobj.ToString(Formatting.Indented));
                        });

    Assert.DoesNotThrow(() =>
                        {
                          var jarr = new JArray(JObject.FromObject(map, serializer));
                          Debug.Log(jarr.ToString(Formatting.Indented));
                        });
  }

  [Test]
  public static void NsoftMiscJsonToObject()
  {
    string jObj = @"{
    ""fef"": ""fef!"",
    ""one"": 1
    }";

    var map = JsonConvert.DeserializeObject<HashMap<string,object>>(jObj);

    Assert.NotNull(map);
    Assert.Positive(map.Count);
    Assert.True(map.Find("fef", out var fef) && fef.ToString() == "fef!");
    Assert.True(map.Find("one", out var one) && one.GetHashCode() == 1);
  }

  [Test]
  public static void NsoftMiscJsonSerialize()
  {
    var serializer = JsonSerializer.CreateDefault();
    var sbob = new System.Text.StringBuilder(2048);

    var map = new HashMap<object,object>
    {
      ["fef"]                     = DeviceDimension.DisplayHz,
      [DeviceDimension.OSVersion] = DeviceSpy.OSVersion,
      ["guid"]                    = System.Guid.NewGuid(),
      ["platform"]                = Application.platform,
      ["platformStr"]             = Application.platform.ToInvariant(),
    };

    Assert.DoesNotThrow(() =>
                        {
                          string json = NewtonsoftAuthority.Serialize(map, serializer, sbob);
                          Debug.Log(json);
                        });

    var list = new List<IDictionary<object,object>>
    {
      map
    };

    Assert.DoesNotThrow(() =>
                        {
                          string json = NewtonsoftAuthority.Serialize(list, serializer, sbob);
                          Debug.Log(json);
                        });

    var map2 = new HashMap<string,object>
    {
      ["data"] = list
    };

    Assert.DoesNotThrow(() =>
                        {
                          string json = NewtonsoftAuthority.Serialize(map2, serializer, sbob);
                          Debug.Log(json);
                        });
  }

#endif // ORE_NEWTONSOFT

} // end class JsonAuthorityInEditor
