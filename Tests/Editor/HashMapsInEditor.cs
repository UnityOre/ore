/*! @file       Tests/Editor/HashMapsInEditor.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2023-04-27
**/

using NUnit.Framework;

// generated tests with:
/*
git grep -hEA1 --break '\[Test\]' -- 'Tests/Runtime/HashMap*.cs' | \
sed -E -e 's|^[ -]{2}||' -e 's| (\w+)(\([^)]*\)).*$| \1\2\n{\n  HashMapCorrectness.\1\2;\n}\n|' | tsd klip
*/
// (fixing up as necessary.)

// ReSharper disable once CheckNamespace
internal static class HashMapsInEditor
{

  [Test]
  public static void DefaultParameters()
  {
    HashMapCorrectness.DefaultParameters();
  }


  [Test]
  public static void ContainsKey()
  {
    HashMapCorrectness.ContainsKey();
  }


  [Test]
  public static void TryMap()
  {
    HashMapCorrectness.TryMap();
  }


  [Test]
  public static void Unmap()
  {
    HashMapCorrectness.Unmap();
  }


  [Test]
  public static void OverMap()
  {
    HashMapCorrectness.OverMap();
  }


  [Test]
  public static void Union()
  {
    HashMapCorrectness.Union();
  }


  [Test]
  public static void Intersect()
  {
    HashMapCorrectness.Intersect();
  }


  [Test]
  public static void Except()
  {
    HashMapCorrectness.Except();
  }


  [Test]
  public static void SymmetricExcept()
  {
    HashMapCorrectness.SymmetricExcept();
  }


  [Test]
  public static void IndexOperator()
  {
    HashMapCorrectness.IndexOperator();
  }


  [Test]
  public static void Clear()
  {
    HashMapCorrectness.Clear();
  }


  [Test]
  public static void ClearNoAlloc()
  {
    HashMapCorrectness.ClearNoAlloc();
  }


  [Test]
  public static void EnsureCapacity()
  {
    HashMapCorrectness.EnsureCapacity();
  }


  [Test]
  public static void Enumerator()
  {
    HashMapCorrectness.Enumerator();
  }


  [Test]
  public static void EnumeratorUnmapCurrent()
  {
    HashMapCorrectness.EnumeratorUnmapCurrent();
  }

} // end class HashMapsInEditor