/*! @file       Tests/Editor/ShellInEditor.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2023-09-29
**/

using NUnit.Framework;

using UnityEngine;

using Ore;
using Ore.Editor;


// ReSharper disable once CheckNamespace
internal static class ShellInEditor
{

  static readonly string[] k_BashOneshots =
  {
    "whoami",
    "which find",
    "echo feet | grep e",
    "git status --short --branch -- $@ && shift $# && git stash list",
  };

  static readonly string[] k_GitOneshots =
  {
    "config user.name",
    "rev-parse --show-toplevel",
    "name-rev --name-only HEAD",
    "log -1 --format=\"HEAD: %H\"",
  };


  [Test]
  public static void RunBashOneshot([ValueSource(nameof(k_BashOneshots))] string command)
  {
    if (!Shell.KnowsBash)
    {
      Assert.Inconclusive("local path to bash executable not known.");
    }

    var result = Shell.RunBashOneshot(command);
    Assert.False(result.PrintFailure(verbose: true), "result.PrintFailure()");
    Assert.False(result.IsEmpty, "result.IsEmpty");
  }

  [Test]
  public static void RunGitOneshot([ValueSource(nameof(k_GitOneshots))] string command)
  {
    if (!Shell.KnowsGit)
    {
      Assert.Inconclusive("local path to git executable not known.");
    }

    var result = Shell.RunGitOneshot(command);
    Assert.False(result.PrintFailure(verbose: true), "result.PrintFailure()");
    Assert.False(result.IsEmpty, "result.IsEmpty");
  }

}