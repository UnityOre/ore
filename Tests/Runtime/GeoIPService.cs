/*! @file       Tests/Runtime/GeoIPService.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2023-04-18
**/

using Ore;

using NUnit.Framework;

using UnityEngine;
using UnityEngine.TestTools;

using System.Collections;


// ReSharper disable once CheckNamespace
internal static class GeoIPService
{

  [OneTimeSetUp]
  public static void SetupForAll()
  {
    if (DeviceSpy.GeoIP.IsFetching)
    {
      DeviceSpy.GeoIP.Reset();
    }
  }


  [Test, Order(0)]
  public static void ResetCache()
  {
    Assert.DoesNotThrow(DeviceSpy.GeoIP.Reset, "DeviceSpy.GeoIP.Reset");

    Assert.IsEmpty(DeviceSpy.GeoIP.CachedValue, "DeviceSpy.GeoIP.CachedValue");
  }


  [UnityTest, Order(5)]
  public static IEnumerator ForceFetch()
  {
    var promise = DeviceSpy.GeoIP.Fetch(timeout: 15);

    promise.Succeeded += Debug.Log; // raw json response

    yield return promise;

    string geo = DeviceSpy.GeoIP.CachedValue;

    Debug.Log(geo);

    Assert.True(promise.IsCompleted, "promise.IsCompleted");
    Assert.True(promise.IsSucceeded, "promise.Succeeded");

    Assert.AreEqual(DeviceSpy.CountryISOString, geo);
  }

} // end static class GeoIPService
