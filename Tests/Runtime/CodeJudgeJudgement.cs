/*! @file       Tests/Runtime/CodeJudgeJudgement.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2023-04-21
**/

using Ore;

using JetBrains.Annotations;

using NUnit.Framework;

using UnityEngine;
using UnityEngine.TestTools;

using System.Collections;
using System.Collections.Generic;


// ReSharper disable once CheckNamespace
internal static class CodeJudgeJudgement
{
  class ParallelJudy : MonoBehaviour, IMonoBehaviourTest
  {
    public bool IsTestFinished { get; private set; }

    public int SampleCount { get; set; }
    public int LoopCount   { get; set; }


    readonly List<string>    m_TestsAlive  = new List<string>();
    readonly HashSet<string> m_ReportQueue = new HashSet<string>();


    IEnumerator Start()
    {
      int loops = LoopCount;
      StartCoroutine(JudgeWaitEnumerator(new WaitForFrames(loops)));
      StartCoroutine(JudgeWaitEnumerator(new WaitForFrames2(loops)));
      StartCoroutine(JudgeWaitEnumerator(new WaitForFrames(loops)));
      StartCoroutine(JudgeWaitEnumerator(new WaitForFrames2(loops)));

      while (m_TestsAlive.Count > 0)
        yield return null;

      IsTestFinished = true;
    }


    IEnumerator JudgeWaitEnumerator([NotNull] IEnumerator wait)
    {
      string id = $"{wait.GetType().NiceName()} {{{SampleCount},{LoopCount}}}";

      m_TestsAlive.Add(id);

      int s = SampleCount;
      while (s --> 0)
      {
        using (new CodeJudge(id))
        {
          while (wait.MoveNext())
          {
            _ = wait.Current;
          }

          wait.Reset();
        }

        yield return null;
      }

      m_ReportQueue.Add(id);
      m_TestsAlive.Remove(id);
    }

    void OnDisable()
    {
      foreach (string id in m_ReportQueue)
      {
        using (new JsonAuthority.Scope(prettyPrint: false))
        {
          CodeJudge.ReportJson(id, out string json);
          Debug.Log(json);
        }

        CodeJudge.Report(id, out TimeInterval time, out long count);

        Assert.Positive(time.Ticks, "totalTime");
        Assert.Positive(count, "callCount");

        CodeJudge.Remove(id);
      }
    }

    // TODO this renders garbage without a camera in the unit test scene
    // void OnGUI()
    // {
    //   GUI.color = Color.yellow;
    //
    //   foreach (string test in m_TestsAlive)
    //   {
    //     CodeJudge.ReportOnGUI(test);
    //   }
    // }

  } // end nested class ParallelJudy


  [UnityTest]
  public static IEnumerator RunJudy([Values(60)] int samples, [Values(500)] int loops)
  {
    return new MonoBehaviourTest<ParallelJudy>
    {
      component =
      {
        SampleCount = samples,
        LoopCount   = loops,
      }
    };
  }

} // end class CodeJudgeJudgement


sealed class WaitForFrames2 : CustomYieldInstruction, IEnumerator
  // apparently ~30% slower than v1 :O
{
  public WaitForFrames2(int nFrames)
  {
    m_Start     = nFrames;
    m_Remaining = nFrames;
  }

  public override bool keepWaiting
  {
    get
    {
      bool wait = m_Remaining --> 0;

      if (!wait)
        Reset();

      return wait;
    }
  }

#if UNITY_2020_1_OR_NEWER

  public override void Reset() => m_Remaining = m_Start;

#else

  // a real brutish way to replace `override`...
  public new void Reset() => m_Remaining = m_Start;
  void IEnumerator.Reset() => m_Remaining = m_Start;

#endif

  readonly int m_Start;
           int m_Remaining;
}