/*! @file       Tests/Runtime/JsonAuthorityCorrectness.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2023-11-02
**/

using Ore;

using NUnit.Framework;

using UnityEngine.TestTools;

using System.Collections;
using Ore.Tests;
using UnityEngine;


// ReSharper disable once CheckNamespace
public static class JsonAuthorityCorrectness
{

  [UnityTest]
  public static IEnumerator ReadWriteComponent()
  {
    yield return null;

    // write

    var comp = ActiveScene.Instance;

    string json = JsonAuthority.Write(comp);

    Debug.Log(json);

    MegaAssert.AreEquivalentJson("{}", json);

    int nWhitelist = JsonAuthority.s_WhiteObjectTypes.Count;

    using (new JsonAuthority.Scope(typeof(Component)))
    {
      json = JsonAuthority.Write(comp);

      Debug.Log(json);

      Assert.IsNotEmpty(json, "json");
      Assert.Greater(json.Length, 2, "json.Length > 2");
    }

    Assert.AreEqual(nWhitelist, JsonAuthority.s_WhiteObjectTypes.Count);

    // read

    Assert.True(JsonAuthority.TryPopulate(json, ref comp), "JsonAuthority.TryPopulate(json, ref comp)");

  }

} // end class JsonAuthorityCorrectness
