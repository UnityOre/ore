/*! @file       Tests/Runtime/AsyncTests.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2023-03-02
**/

using Ore;

using NUnit.Framework;

using UnityEngine;
using UnityEngine.TestTools;

using System.Collections;

using Regex = System.Text.RegularExpressions.Regex;


// ReSharper disable once CheckNamespace
internal static class AsyncTests
{

  [UnityTest]
  public static IEnumerator CoroutinePromises()
  {
    var test = new MonoBehaviourTest<PromiseCoroutineTester>();
    return test;
  }

  class PromiseCoroutineTester : MonoBehaviour, IMonoBehaviourTest
  {
    public bool IsTestFinished { get; private set; }

    void Start()
    {
      var promise = new Promise<string>();

      _ = StartCoroutine(Provide(promise));
      _ = StartCoroutine(Consume(promise));
    }

    IEnumerator Provide(Promise<string> outPromise)
    {
      /**/ Assert.False(outPromise.IsCompleted);

      yield return new WaitForFrames(Random.Range(1,60));

      /**/ Assert.False(outPromise.IsCompleted);

      outPromise.CompleteWith("as promised!");

      /**/ Assert.True(outPromise.IsSucceeded);
    }

    IEnumerator Consume(Promise<string> inPromise)
    {
      /**/ Assert.False(inPromise.IsCompleted);

      inPromise.Succeeded += Debug.Log;

      /**/ LogAssert.Expect(LogType.Log, "as promised!");

      yield return inPromise;

      /**/ Assert.True(inPromise.IsSucceeded);

      /**/ LogAssert.Expect(LogType.Log, "as promised!");
      inPromise.Succeeded += Debug.Log;

      IsTestFinished = true;
    }
  } // end nested class PromiseCoroutineTester


  [UnityTest]
  public static IEnumerator SharedWaitObjectConsumption([Values(1,3)] int i, [Values(1,3)] int n, [Values(false, true)] bool realtime)
  {
    const float seconds = 1f;

    var test = new MonoBehaviourTest<SharedWaitObjectTest>();

    if (realtime)
    {
      test.component.SetupRealtime(seconds, i, n);
    }
    else
    {
      test.component.Setup(seconds, i, n);
    }

    double start = Time.realtimeSinceStartup;

    yield return test;

    double end = Time.realtimeSinceStartup;

    void approximatelyEqual()
    {
      Assert.AreEqual(start + seconds * i, end, i / 60.0);
    }

    if (n == 1 || !realtime)
    {
      approximatelyEqual();
    }
    else
    {
      Assert.Throws<AssertionException>(approximatelyEqual);
      Debug.Log("Confirmed: Sharing wait objects results in unexpected behaviour.");
    }
  }

  class SharedWaitObjectTest : MonoBehaviour, IMonoBehaviourTest
  {
    public bool IsTestFinished { get; private set; }

    public void SetupRealtime(float seconds, int i, int n)
    {
      m_WaitTime    = seconds;
      m_WaitObject  = new WaitForSecondsRealtime(seconds);
      m_Iterations  = i;
      m_NumParallel = n;
    }

    public void Setup(float seconds, int i, int n)
    {
      m_WaitTime    = seconds;
      m_WaitObject  = new WaitForSeconds(seconds);
      m_Iterations  = i;
      m_NumParallel = n;
    }


    float  m_WaitTime;
    object m_WaitObject;
    int m_Iterations, m_NumParallel;


    IEnumerator Start()
    {
      while (m_WaitObject is null)
      {
        // wait for setup
        yield return null;
      }

      int n = m_NumParallel;
      while (n --> 0)
      {
        _ = StartCoroutine(Consume(m_Iterations));
      }
    }

    IEnumerator Consume(int i)
    {
      while (i --> 0)
      {
        double start = Time.realtimeSinceStartup;

        yield return m_WaitObject;

        double end   = Time.realtimeSinceStartup;

        try
        {
          Assert.AreEqual(start + m_WaitTime, 
                          end,
                          4 / 60.0);
        }
        catch (AssertionException aex)
        {
          LogAssert.Expect(LogType.Exception, new Regex("."));
          Debug.LogException(aex, this);
        }
      }

      if (--m_NumParallel == 0)
      {
        IsTestFinished = true;
      }
    }
  } // end nested class SharedWaitObjectTest

} // end class AsyncTests
