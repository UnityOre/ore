/*! @file       Tests/Runtime/HashMapSpeed.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2022-09-29
 *
 *  Speed Tests:
 *  [x] HashMap vs Dictionary
 *  [x] HashMap vs Hashtable
 *  [x] HashMap vs HashSet
 *  [ ] HashMap vs List (binary search)
 *  [ ] HashMap vs List (linear search)
 *  [ ] HashMap vs Array (binary search)
 *  [ ] HashMap vs Array (linear search)
 *  [x] Hashtable vs Dictionary
 *  [ ] HashMap.Clear vs HashMap.ClearNoAlloc
**/

#if KOOBOX_EXPLORE // deprecated symbol
#warning KOOBOX_EXPLORE is deprecated. Define ORE_DEBUG instead.
#define ORE_DEBUG
#endif

using Ore;

using NUnit.Framework;

using UnityEngine;
using UnityEngine.TestTools;

using System.Linq;
using System.Collections;
using System.Collections.Generic;

using Debug = UnityEngine.Debug;
using Stopwatch = System.Diagnostics.Stopwatch;
using StringBuilder = System.Text.StringBuilder;


// ReSharper disable once CheckNamespace
public static class HashMapSpeed
{

  const int   NFRAMES = 30;
  const int   N       = 5000;
  const float TOOSLOW = 5f * NFRAMES;


  internal static HashSet<string> GetTestSet(int n)
  {
    var set = new HashSet<string>();

    while (n --> 0)
    {
      string val = Strings.MakeGUID().Remove(Random.Range(1, 32));
      if (!set.Add(val))
      {
        ++ n;
      }
    }

    return set;
  }

  internal static List<string> GetTestList(int n)
  {
    return GetTestSet(n).ToList();
  }

  internal static string[] GetTestArray(int n)
  {
    return GetTestSet(n).ToArray();
  }

  internal static Dictionary<string,string> GetTestDict(int n)
  {
    var keys = GetTestSet(n);
    var dict = new Dictionary<string,string>(n);

    foreach (string key in keys)
    {
      dict[key] = key;
    }

    return dict;
  }

  internal static Hashtable GetTestTable(int n)
  {
    return new Hashtable(GetTestDict(n));
  }

  internal static HashMap<string,string> GetTestHashMap(HashMapParams parms)
  {
    var map = new HashMap<string,string>(parms);

    int n = map.Capacity;
    while (n --> 0)
    {
      string guid = Strings.MakeGUID().Remove(Random.Range(1, 32));
      if (!map.Map(guid, guid))
      {
        ++ n;
      }
    }

    return map;
  }

  internal static List<string> GetSomeKeysFor(IDictionary lookup, int nExist, int nFake)
  {
    var keys = new List<string>(lookup.Count);

    if (lookup.Count > 0)
    {
      var iter = lookup.GetEnumerator();

      while (nExist > 0)
      {
        while (iter.MoveNext())
        {
          keys.Add(iter.Key?.ToString() ?? "");
          if (--nExist == 0)
            break;
        }

        iter.Reset();
      }
    }

    while (nFake --> 0)
    {
      string guid = Strings.MakeGUID().Remove(Random.Range(1, 32));
      if (lookup.Contains(guid))
      {
        ++ nFake;
      }
      else
      {
        keys.Add(guid);
      }
    }

    return keys;
  }


  static IEnumerator DoLookupTest(IDictionary lookup, string name, float pExist)
  {
    var tests = GetSomeKeysFor(lookup, (int)(pExist * N + 0.5f), (int)((1f-pExist) * N + 0.5f));

    var stopwatch = new Stopwatch();

    yield return null;

    int i = NFRAMES;
    while (i --> 0)
    {
      int j = tests.Count;
      int nExist = 0;

      while (j --> 0)
      {
        stopwatch.Start();
        if (lookup.Contains(tests[j]))
        {
          stopwatch.Stop();
          Assert.AreEqual(tests[j], lookup[tests[j]], "map[key] == value");
          ++ nExist;
        }
        else
        {
          stopwatch.Stop();
        }
      }

      Assert.AreEqual(0f, (float)nExist/tests.Count - pExist, 0.03f);

      yield return null;
    }

    var time = new TimeInterval(stopwatch.ElapsedTicks);

    var map = lookup as HashMap<string,string>;

    // RFC 4180 CSV:
    s_CSVLine.AppendFormat("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7:0.#}\",\"{8}\"",
                           name,
                           lookup.Count,
                           map is null ? 101 : map.Parameters.HashPrime,
                           map is null ? (lookup is Hashtable ? 0.72f : 1f) : map.Parameters.LoadFactor,
                           lookup.Count,
                           N * (1f + pExist) * NFRAMES,
                           map is null ? "" : map.CollisionCover.ToInvariant(),
                           time.Micros,
                           map is null ? "" : map.LifetimeAllocs.ToInvariant()
                          );


    Assert.Less(time.Millis, TOOSLOW);
  }


  static readonly float[] PERCENTS = { 0.15f, 0.85f };
  static readonly int[]   SIZES    = { 20, 200, 2000 };


  [UnityTest]
  public static IEnumerator LookupHashMap(
    [ValueSource(nameof(PERCENTS))] float pExist,
    [ValueSource(nameof(SIZES))]      int size )
  {
    const string TESTNAME = nameof(LookupHashMap);

    var lookup = GetTestHashMap(size);

    System.GC.Collect();
    return DoLookupTest(lookup, TESTNAME, pExist);
  }

  [UnityTest]
  public static IEnumerator LookupDictionary(
    [ValueSource(nameof(PERCENTS))] float pExist,
    [ValueSource(nameof(SIZES))]      int size )
  {
    const string TESTNAME = nameof(LookupDictionary);

    var lookup = GetTestDict(size);

    System.GC.Collect();
    return DoLookupTest(lookup, TESTNAME, pExist);
  }

  [UnityTest]
  public static IEnumerator LookupHashtable(
    [ValueSource(nameof(PERCENTS))] float pExist,
    [ValueSource(nameof(SIZES))]      int size )
  {
    const string TESTNAME = nameof(LookupHashtable);

    var lookup = GetTestTable(size);

    System.GC.Collect();
    return DoLookupTest(lookup, TESTNAME, pExist);
  }


  [UnityTest]
  public static IEnumerator LookupBoxlessHashMap(
    [ValueSource(nameof(PERCENTS))] float pExist,
    [ValueSource(nameof(SIZES))]    int   size )
  {
    System.GC.Collect();

    var map = GetTestHashMap(size);

    var tests = GetSomeKeysFor(map, (int)(pExist * N + 0.5f), (int)((1f - pExist) * N + 0.5f));

    var stopwatch = new Stopwatch();

    yield return null;

    int i = NFRAMES;
    while (i --> 0)
    {
      int j      = tests.Count;
      int nExist = 0;

      while (j --> 0)
      {
        stopwatch.Start();
        if (map.Find(tests[j], out string result))
        {
          stopwatch.Stop();
          Assert.AreEqual(tests[j], result);
          ++ nExist;
        }
        else
        {
          stopwatch.Stop();
        }
      }

      Assert.AreEqual(0f, (float)nExist / tests.Count - pExist, 0.03f);

      yield return null;
    }

    var time = new TimeInterval(stopwatch.ElapsedTicks);

    // RFC 4180 CSV:
    s_CSVLine.AppendFormat("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7:0.#}\",\"{8}\"",
                           nameof(LookupBoxlessHashMap),
                           size,
                           map.Parameters.HashPrime,
                           map.Parameters.LoadFactor,
                           size,
                           N * NFRAMES,
                           map.CollisionCover,
                           time.Micros,
                           map.LifetimeAllocs
                          );

    Assert.Less(time.Millis, TOOSLOW);
  }

  [UnityTest]
  public static IEnumerator LookupBoxlessDictionary(
    [ValueSource(nameof(PERCENTS))] float pExist,
    [ValueSource(nameof(SIZES))]    int   size )
  {
    System.GC.Collect();

    var lookup = GetTestDict(size);

    var tests = GetSomeKeysFor(lookup, (int)(pExist * N + 0.5f), (int)((1f - pExist) * N + 0.5f));

    var stopwatch = new Stopwatch();

    yield return null;

    int i = NFRAMES;
    while (i --> 0)
    {
      int j      = tests.Count;
      int nExist = 0;


      while (j --> 0)
      {
        stopwatch.Start();
        if (lookup.TryGetValue(tests[j], out string result))
        {
          stopwatch.Stop();
          Assert.AreEqual(tests[j], result);
          ++ nExist;
        }
        else
        {
          stopwatch.Stop();
        }
      }

      Assert.AreEqual(0f, (float)nExist /tests.Count - pExist, 0.03f);

      yield return null;
    }

    var time = new TimeInterval(stopwatch.ElapsedTicks);

    // RFC 4180 CSV:
    s_CSVLine.AppendFormat("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7:0.#}\"",
                           nameof(LookupBoxlessDictionary),
                           3,
                           101,
                           1f,
                           size,
                           N * NFRAMES,
                           "",
                           time.Micros
                          );

    Assert.Less(time.Millis, TOOSLOW);
  }


  [UnityTest]
  public static IEnumerator InsertHashtable([ValueSource(nameof(SIZES))] int size)
  {
    System.GC.Collect();

    var stopwatch = new Stopwatch();

    yield return null;

    int i = NFRAMES;
    while (i --> 0)
    {
      var table = new Hashtable();
      var tests = GetTestList(size);

      int j = size;

      while (j --> 0)
      {
        string test = tests[j];
        int precount = table.Count;

        stopwatch.Start();
        table[test] = test;
        table[test] = test;
        stopwatch.Stop();

        Assert.AreEqual(precount + 1, table.Count);
        Assert.AreEqual(test,         table[test]);
      }

      yield return null;
    }

    var time = new TimeInterval(stopwatch.ElapsedTicks);

    // RFC 4180 CSV:
    s_CSVLine.AppendFormat("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7:0.#}\"",
                           nameof(InsertHashtable),
                           (int)(0.72f * 3),
                           101,
                           0.72f,
                           size,
                           2 * size * NFRAMES,
                           "",
                           time.Micros
                          );

    Assert.Less(time.Millis, TOOSLOW);
  }

  [UnityTest]
  public static IEnumerator InsertBoxlessHashMap([ValueSource(nameof(SIZES))] int size)
  {
    System.GC.Collect();

    var stopwatch = new Stopwatch();

    int sumColsns = 0;
    int sumAllocs = 0;

    yield return null;

    int i = NFRAMES;
    while (i --> 0)
    {
      var map = new HashMap<string,string>();
      var tests = GetTestList(size);

      int j = size;

      while (j --> 0)
      {
        string test = tests[j];
        int precount = map.Count;

        stopwatch.Start();
        map[test] = test;
        map[test] = test;
        stopwatch.Stop();

        Assert.AreEqual(precount + 1, map.Count);
        Assert.AreEqual(test,         map[test]);
      }

      // Debug.Log($"chain={map.LongestChain}, collisions={map.Collisions}, collisionCover={map.CollisionCover:F2}");

      sumColsns += map.Collisions;
      sumAllocs += map.LifetimeAllocs;

      yield return null;
    }

    var time = new TimeInterval(stopwatch.ElapsedTicks);

    // RFC 4180 CSV:
    s_CSVLine.AppendFormat("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7:0.#}\",\"{8}\"",
                           nameof(InsertBoxlessHashMap),
                           HashMapParams.Default.CalcLoadLimit(),
                           HashMapParams.Default.HashPrime,
                           HashMapParams.Default.LoadFactor,
                           size,
                           2 * size * NFRAMES,
                           sumColsns / (float)(size * NFRAMES),
                           time.Micros,
                           sumAllocs
                          );

    Assert.Less(time.Millis, TOOSLOW);
  }

  [UnityTest]
  public static IEnumerator InsertBoxlessDictionary([ValueSource(nameof(SIZES))] int size)
  {
    System.GC.Collect();

    var stopwatch = new Stopwatch();

    yield return null;

    int i = NFRAMES;
    while (i --> 0)
    {
      var dict = new Dictionary<string,string>();
      var tests = GetTestList(size);

      int j = size;

      while (j --> 0)
      {
        string test     = tests[j];
        int    precount = dict.Count;

        stopwatch.Start();
        dict[test] = test;
        dict[test] = test;
        stopwatch.Stop();

        Assert.AreEqual(precount + 1, dict.Count);
        Assert.AreEqual(test,         dict[test]);
      }

      yield return null;
    }

    var time = new TimeInterval(stopwatch.ElapsedTicks / NFRAMES);

    // RFC 4180 CSV:
    s_CSVLine.AppendFormat("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7:0.#}\"",
                           nameof(InsertBoxlessDictionary),
                           3,
                           101,
                           1f,
                           size,
                           2 * size * NFRAMES,
                           "",
                           time.Micros
                          );

    Assert.Less(time.Millis, TOOSLOW);
  }


  #if ORE_DEBUG

  static readonly int[]   ISIZES   = { 8, 9, 10 };
  static readonly int[]   HPRIMES  = { 97, 101, 193 };
  static readonly float[] LFACTORS = { 0.66f, 0.72f, 0.75f };

  [UnityTest]
  public static IEnumerator ExploreParamsInsert([ValueSource(nameof(SIZES))]    int   size,
                                                [ValueSource(nameof(ISIZES))]   int   initSize,
                                                [ValueSource(nameof(HPRIMES))]  int   hashPrime,
                                                [ValueSource(nameof(LFACTORS))] float loadFactor)
  {
    System.GC.Collect();

    var stopwatch = new Stopwatch();

    int sumColsns = 0;
    int sumAllocs = 0;

    var parms = new HashMapParams(initSize, loadFactor, isFixed: false, hashPrime);

    yield return null;

    int i = NFRAMES;
    while (i --> 0)
    {
      var map = new HashMap<string,string>(parms);
      var tests = GetTestList(size);

      int j = size;

      while (j --> 0)
      {
        string test = tests[j];
        int precount = map.Count;

        stopwatch.Start();
        map[test] = test;
        map[test] = test;
        stopwatch.Stop();

        Assert.AreEqual(precount + 1, map.Count);
        Assert.AreEqual(test,         map[test]);
      }

      // Debug.Log($"chain={map.LongestChain}, collisions={map.Collisions}, collisionCover={map.CollisionCover:F2}");

      sumColsns += map.Collisions;
      sumAllocs += map.LifetimeAllocs;

      yield return null;
    }

    var time = new TimeInterval(stopwatch.ElapsedTicks);

    // RFC 4180 CSV:
    s_CSVLine.AppendFormat("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7:0.#}\",\"{8}\"",
                           nameof(ExploreParamsInsert),
                           initSize,
                           hashPrime,
                           loadFactor,
                           size,
                           2 * size * NFRAMES,
                           sumColsns / (float)(size * NFRAMES),
                           time.Micros,
                           sumAllocs
                          );

    Assert.Less(time.Millis, TOOSLOW);
  }

  [UnityTest]
  public static IEnumerator ExploreParamsLookup([ValueSource(nameof(SIZES))]    int   size,
                                                [ValueSource(nameof(HPRIMES))]  int   hashPrime,
                                                [ValueSource(nameof(LFACTORS))] float loadFactor)
  {
    System.GC.Collect();

    var map = GetTestHashMap(new HashMapParams(size,
                                               loadFactor,
                                               isFixed: true,
                                               hashPrime));

    var tests = GetSomeKeysFor(map, N >> 1, N >> 1);

    var stopwatch = new Stopwatch();

    yield return null;

    int i = NFRAMES;
    while (i --> 0)
    {
      int j      = tests.Count;
      int nExist = 0;

      while (j --> 0)
      {
        stopwatch.Start();
        if (map.Find(tests[j], out string result))
        {
          stopwatch.Stop();
          Assert.AreEqual(tests[j], result);
          ++ nExist;
        }
        else
        {
          stopwatch.Stop();
        }
      }

      Assert.AreEqual(N >> 1, nExist);

      yield return null;
    }

    var time = new TimeInterval(stopwatch.ElapsedTicks);

    // RFC 4180 CSV:
    s_CSVLine.AppendFormat("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7:0.#}\",\"{8}\"",
                           nameof(ExploreParamsLookup),
                           size,
                           hashPrime,
                           loadFactor,
                           size,
                           N * NFRAMES,
                           map.CollisionCover,
                           time.Micros,
                           map.LifetimeAllocs
                          );

    Assert.Less(time.Millis, TOOSLOW);
  }

  #endif // ORE_DEBUG


  static StringBuilder s_CSVLine, s_CSVAll;

  [OneTimeSetUp]
  public static void SetupForAll()
  {
    s_CSVLine = RecycledStringBuilder.Borrow();
    s_CSVAll  = RecycledStringBuilder.Borrow();
    s_CSVAll.AppendLine("\"Test Name\",\"Init Size\",\"Hash Prime\",\"Load Factor\",\"Map Size\",\"# Calls\",\"% Collisions\",\"Total Time (µs)\",\"# Allocs\"");
  }

  [TearDown]
  public static void TeardownPrevious()
  {
    string line = s_CSVLine.ToString();

    s_CSVLine.Clear();

    Assert.IsNotEmpty(line);

    Debug.Log(line);

    s_CSVAll.AppendLine(line);
  }

  [OneTimeTearDown]
  public static void FinalTeardown()
  {
    string text = s_CSVAll.ToString();

    // Debug.Log(text);

    RecycledStringBuilder.Return(s_CSVLine);
    RecycledStringBuilder.Return(s_CSVAll);
    s_CSVLine = null;
    s_CSVAll  = null;

    string file = Filesystem.GetTempPath(nameof(HashMapSpeed) + ".csv");

    if (Filesystem.TryWriteText(file, text))
    {
      Debug.Log($"Wrote results as CSV to \"{file}\"! Attempting to open a file browser there...");
      EditorBridge.BrowseTo(file);
      // TODO only BrowseTo in specific cases (like if test was manually ran), or give some other toggle
    }
    else
    {
      Filesystem.LogLastException();
    }
  }

}
