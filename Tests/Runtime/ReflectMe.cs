/*! @file       Tests/Runtime/ReflectMe.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2023-04-26
**/

using Ore;

using UnityEngine;

using System.Collections;
using System.Collections.Generic;

using FieldInfo = System.Reflection.FieldInfo;


[System.Serializable]
public class ReflectMe
{
  public static ReflectMe Make()
  {
    return new ReflectMe
    {
      I32         = 12345,
      F32         = Floats.Pi,
      Bool        = true,
      String      = $"{nameof(ReflectMe)}.{nameof(Make)}",

      Interval = new TimeInterval(3.5),
      Vec2     = new Vector2(3f, 4f),

      StringArray = new [] { "1st", "2nd", "3rd" },
      StringList  = new List<string> { "", "fef" },
      StringDict  = new Dictionary<string,string> { ["key"] = "value" },
      BoxedDict   = new Dictionary<string,object> { ["key"] = "value", ["fef"] = true },

      DeepStructs = new [] { ReflectMeStruct.MakeRandom(), ReflectMeStruct.MakeRandom() },

      m_Private        = nameof(m_Private),
      m_SerializeField = nameof(m_SerializeField),
      m_NonSerialized  = Primes.GetRandom()
    };
  }

  public static bool DeepEquals(ReflectMe lhs, ReflectMe rhs)
  {
    if (ReferenceEquals(lhs, rhs))
      return true;

    if (lhs.Bool != rhs.Bool            ||
        lhs.I32  != rhs.I32             ||
        !lhs.F32.Approximately(rhs.F32) ||
        lhs.String != rhs.String        ||
        lhs.Interval != rhs.Interval    ||
        lhs.Vec2 != rhs.Vec2            ||
        lhs.m_SerializeField != rhs.m_SerializeField) // skip m_Private, m_NonSerialized
    {
      return false;
    }

    (ICollection lhs,ICollection rhs)[] collectionPairs =
    {
      (lhs.StringArray, rhs.StringArray),
      (lhs.StringList,  rhs.StringList),
      (lhs.StringDict,  rhs.StringDict),
      (lhs.BoxedDict,   rhs.BoxedDict),
      (lhs.DeepStructs, rhs.DeepStructs),
    };

    foreach (var (lhsc,rhsc) in collectionPairs)
    {
      if (lhsc is null)
      {
        if (rhsc is null)
          continue;

        return false;
      }

      if (rhsc is null)
        return false;

      foreach (var item in lhsc)
      {
        bool slowContains = false;

        foreach (var slow in rhsc) // bleh, I'm tired. let's hurry this up
        {
          if (Equals(item, slow))
          {
            slowContains = true;
            break;
          }
        }

        if (!slowContains)
          return false;
      }
    }

    return true;
  }

  public static IEnumerable<FieldInfo> ExpectSerializedFields()
  {
    var type = typeof(ReflectMe);
    var flags = TypeMembers.INSTANCE;

    yield return type.GetField(nameof(I32),              flags);
    yield return type.GetField(nameof(F32),              flags);
    yield return type.GetField(nameof(Bool),             flags);
    yield return type.GetField(nameof(String),           flags);
    yield return type.GetField(nameof(Vec2),             flags);
    yield return type.GetField(nameof(Interval),         flags);
    yield return type.GetField(nameof(StringArray),      flags);
    yield return type.GetField(nameof(StringList),       flags);
    yield return type.GetField(nameof(StringDict),       flags);
    yield return type.GetField(nameof(BoxedDict),        flags);
    yield return type.GetField(nameof(DeepStructs),      flags);
    yield return type.GetField(nameof(m_SerializeField), flags);
  }

  public static IEnumerable<FieldInfo> ExpectNonSerializedFields()
  {
    var type = typeof(ReflectMe);
    var flags = TypeMembers.INSTANCE;

    yield return type.GetField(nameof(m_Private), flags);
    yield return type.GetField(nameof(m_NonSerialized), flags);
  }


  public ReflectMe()
  {
    m_NonSerialized = TimeInterval.SinceMidnight(local: true);
  }

  public long NonSerialized => m_NonSerialized;

  public int    I32;
  public float  F32;
  public bool   Bool;
  public string String;

  public TimeInterval Interval;
  public Vector2      Vec2;

  public string[]                  StringArray;
  public List<string>              StringList;
  public Dictionary<string,string> StringDict;
  public Dictionary<string,object> BoxedDict;

  public ReflectMeStruct[] DeepStructs;

  [SerializeField]
  string m_SerializeField;

  string m_Private;

  [System.NonSerialized]
  long m_NonSerialized;

} // end class ReflectMe


[System.Serializable]
public struct ReflectMeStruct
{
  public static ReflectMeStruct MakeRandom()
  {
    return new ReflectMeStruct
    {
      Bool     = Random.value >= 0.5f,
      I32      = Primes.GetRandom(),
      F32      = (float)DateTimes.NowUnixSeconds(),
      String   = Strings.MakeGUID().Remove(16),
      Interval = TimeInterval.Day * Random.value,
      Vec2     = Random.insideUnitCircle
    };
  }


  public bool         Bool;
  public int          I32;
  public float        F32;
  public string       String;
  public TimeInterval Interval;
  public Vector2      Vec2;


  public override bool Equals(object other)
  {
    return other is ReflectMeStruct me     &&
           Bool == me.Bool                 &&
           I32 == me.I32                   &&
           F32.Approximately(me.F32)       &&
           String == me.String             &&
           Interval == me.Interval         &&
           Vec2.x.Approximately(me.Vec2.x) &&
           Vec2.y.Approximately(me.Vec2.y);
  }

  public override int GetHashCode()
  {
    int hash = Hashing.MakeHash(Bool, I32, F32, String);
    return Hashing.MakeHash(hash, Interval, Vec2);
  }

} // end struct ReflectMeStruct