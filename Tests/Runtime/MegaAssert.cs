/*! @file       Tests/Runtime/MegaAssert.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2023-05-24
**/

using NUnit.Framework;
using NUnit.Framework.Constraints;

using UnityEngine;
using UnityEngine.TestTools;

using System.Collections;
using System.Collections.Generic;

using JsonObj = System.Collections.Generic.Dictionary<string,object>;
using JsonArr = System.Collections.Generic.List<object>;

using Regex = System.Text.RegularExpressions.Regex;



// ReSharper disable once CheckNamespace
namespace Ore.Tests // one of the only files in this directory to use a namespace
{                            // (this is intentional)
  public static class MegaAssert
  {

    public static void ApproximatelyEqual(TimeInterval a, TimeInterval b, string message = null)
    {
      if (a.TicksAreFrames && b.TicksAreFrames)
      {
        Assert.AreEqual(a.Ticks, b.Ticks, message);
        return;
      }

      Assert.AreEqual(a.WithSystemTicks().Ticks,
                      b.WithSystemTicks().Ticks,
                      TimeInterval.TICKS_PER_FRAME_60FPS,
                      message);
    }


    public static void AreEquivalentJson(object expected, object actual, string message = null)
      {
        if (expected is null)
        {
          Assert.Null(actual, message);
        }
        else if (expected is IDictionary<string,object> jobj)
        {
          var dict = actual as IDictionary<string,object>;
    
          Assert.NotNull(dict, message);
    
          int countdown = jobj.Count;
    
          foreach (var kvp in dict)
          {
            Assert.True(jobj.TryGetValue(kvp.Key, out var exp), $"(did not contain kvp: {kvp})");
    
            // regrettable recursion
            AreEquivalentJson(exp, kvp.Value, message);
    
            -- countdown;
          }
    
          Assert.Zero(countdown, message);
        }
        else if (expected is ICollection jarr)
        {
          var list = actual as ICollection;
    
          Assert.NotNull(list, message);
    
          int countdown = jarr.Count;
    
          foreach (var item in list)
          {
            Assert.Contains(item, jarr, message);
            -- countdown;
          }
    
          Assert.Zero(countdown, message);
        }
        else if (expected is string)
        {
          var isExpected = new EqualConstraint(expected)
            .Using((System.Comparison<string>)Strings.CompareIgnoreWhitespace);
          Assert.That(actual, isExpected);
        }
        else if (expected is System.TimeSpan span)
        {
          expected = span.Ticks;
    
          Assert.AreEqual(expected, actual, message);
        }
        else
        {
          var enumType = expected.GetType();
          if (enumType.IsEnum)
          {
            if (actual is string enumStr)
            {
              actual = System.Enum.Parse(enumType, enumStr);
            }
            else if (actual is long enumVal)
            {
              actual = System.Enum.ToObject(enumType, enumVal);
            }
            else
            {
              Assert.Fail($"enum conversion: from \"{actual}\" to {enumType.NiceName()}.{expected}");
            }
    
            Assert.NotNull(actual, "actual");
          }
    
          Assert.AreEqual(expected, actual, message);
        }
      }


    public static void CheckNewtonsoft(JsonProvider provider, bool willReflectFields = false)
    {
      if (provider != JsonProvider.NewtonsoftJson)
        return;

      #if ORE_NEWTONSOFT

      if (willReflectFields)
      {
        Assert.Inconclusive("Newtonsoft is known to serialize too many reflected fields/properties; Reflection-based tests will always fail.");
      }
      else
      {
        Orator.WarnOnce("Newtonsoft is known to serialize too many reflected fields/properties...");
      }

      #else

      _ = willReflectFields;

      Debug.Log("Newtonsoft.Json is unavailable to Ore; forcing it as a provider should log a non-fatal error.");

      LogAssert.Expect(LogType.Error, new Regex("Failed to set Provider to "));

      using (new JsonAuthority.Scope(JsonAuthority.Provider))
      {
        _ = JsonAuthority.Write("fef");
        Assert.Inconclusive("Error logged as expected.");
      }

      #endif
    }

  } // end class MegaAssert
}

