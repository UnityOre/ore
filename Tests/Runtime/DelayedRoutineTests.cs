/*! @file       Tests/Runtime/DelayedRoutineTests.cs
 *  @author     Levi Perez (levi\@leviperez.dev)
 *  @date       2022-12-15
**/

using Ore;
using Ore.Tests;

using NUnit.Framework;

using UnityEngine;
using UnityEngine.TestTools;

using System.Collections;


// ReSharper disable once CheckNamespace
public static class DelayedRoutineTests
{

  [UnityTest]
  public static IEnumerator DelayByFrame([Values(0, 1, 10, 100, 333, 1234)] int frames)
  {
    var start = TimeInterval.NowFrames;

    start.Ticks += 1;

    void action()
    {
      var    expected = TimeInterval.OfFrames(frames);
      var    actual   = TimeInterval.NowFrames - start;
      string message  = $"delta={actual - expected}, lastFrame={TimeInterval.LastFrame}";

      Debug.Log(message);

      MegaAssert.ApproximatelyEqual(expected, actual, message);
    }

    var delay = TimeInterval.OfFrames(frames);

    return new DelayedRoutine(action, delay);
  }

  [UnityTest]
  public static IEnumerator DelayBySeconds([Values(0f, 1f, 3.14f, 5f)] float seconds)
  {
    var start = TimeInterval.ThisSession;

    Debug.Log($"start={start}");

    void action()
    {
      var expected = TimeInterval.OfSeconds(seconds);
      var actual   = TimeInterval.ThisSession - start;

      expected.Ticks += TimeInterval.SmoothTicksLastFrame.Rounded();

      string message = $"delta={actual - expected}, lastFrame={TimeInterval.LastFrame}";

      Debug.Log(message);

      MegaAssert.ApproximatelyEqual(expected, actual, message);
    }

    var delay = TimeInterval.OfSeconds(seconds);

    return new DelayedRoutine(action, delay);
  }

  [UnityTest]
  public static IEnumerator DelayOnActiveScene([Values("3.14s", "3 frames", "0.5sec", "1", "2 ticks", "1f")] string timeInterval)
  {
    var delay = TimeInterval.SmolParse(timeInterval);

    Debug.Log($"SmolParse(\"{timeInterval}\") -> {delay}");

    Assert.Positive(delay.Ticks);

    var start = delay.TicksAreFrames ? TimeInterval.NowFrames : TimeInterval.ThisSession;

    void action()
    {
      var    expected = delay;
      var    actual   = (delay.TicksAreFrames ? TimeInterval.NowFrames : TimeInterval.ThisSession) - start;
      string message  = $"delta={actual - expected}, lastFrame={TimeInterval.LastFrame}";

      Debug.Log(message);

      MegaAssert.ApproximatelyEqual(expected, actual, message);
    }

    ActiveScene.Coroutines.Run(new DelayedRoutine(action, delay), out string handle);

    do
    {
      yield return null;
    }
    while (ActiveScene.Coroutines.IsRunning(handle));
  }

} // end static class DelayedRoutineTests
